require "rails_helper"

RSpec.describe Cms::NewsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/news").to route_to("cms/news#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/news/new").to route_to("cms/news#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/news/1").to route_to("cms/news#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/news/1/edit").to route_to("cms/news#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/news").to route_to("cms/news#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/news/1").to route_to("cms/news#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/news/1").to route_to("cms/news#destroy", :id => "1")
    end

  end
end

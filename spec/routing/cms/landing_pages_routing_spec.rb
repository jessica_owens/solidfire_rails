require "rails_helper"

RSpec.describe Cms::LandingPagesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/landing_pages").to route_to("cms/landing_pages#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/landing_pages/new").to route_to("cms/landing_pages#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/landing_pages/1").to route_to("cms/landing_pages#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/landing_pages/1/edit").to route_to("cms/landing_pages#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/landing_pages").to route_to("cms/landing_pages#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/landing_pages/1").to route_to("cms/landing_pages#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/landing_pages/1").to route_to("cms/landing_pages#destroy", :id => "1")
    end

  end
end

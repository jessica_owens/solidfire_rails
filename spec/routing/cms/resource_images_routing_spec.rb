require "rails_helper"

RSpec.describe Cms::ResourceImagesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/resource_images").to route_to("cms/resource_images#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/resource_images/new").to route_to("cms/resource_images#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/resource_images/1").to route_to("cms/resource_images#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/resource_images/1/edit").to route_to("cms/resource_images#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/resource_images").to route_to("cms/resource_images#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/resource_images/1").to route_to("cms/resource_images#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/resource_images/1").to route_to("cms/resource_images#destroy", :id => "1")
    end

  end
end

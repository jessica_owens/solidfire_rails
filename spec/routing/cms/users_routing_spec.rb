require "rails_helper"

RSpec.describe Cms::UsersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/users").to route_to("cms/users#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/users/new").to route_to("cms/users#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/users/1").to route_to("cms/users#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/users/1/edit").to route_to("cms/users#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/users").to route_to("cms/users#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/users/1").to route_to("cms/users#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/users/1").to route_to("cms/users#destroy", :id => "1")
    end

  end
end

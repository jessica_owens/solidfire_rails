require "rails_helper"

RSpec.describe Cms::ProjectRequestTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/project_request_types").to route_to("cms/project_request_types#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/project_request_types/new").to route_to("cms/project_request_types#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/project_request_types/1").to route_to("cms/project_request_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/project_request_types/1/edit").to route_to("cms/project_request_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/project_request_types").to route_to("cms/project_request_types#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/project_request_types/1").to route_to("cms/project_request_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/project_request_types/1").to route_to("cms/project_request_types#destroy", :id => "1")
    end

  end
end

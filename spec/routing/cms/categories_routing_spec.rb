require "rails_helper"

RSpec.describe Cms::CategoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/categories").to route_to("cms/categories#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/categories/new").to route_to("cms/categories#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/categories/1").to route_to("cms/categories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/categories/1/edit").to route_to("cms/categories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/categories").to route_to("cms/categories#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/categories/1").to route_to("cms/categories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/categories/1").to route_to("cms/categories#destroy", :id => "1")
    end

  end
end

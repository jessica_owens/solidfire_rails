require "rails_helper"

RSpec.describe Cms::ProjectRequestsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/project_requests").to route_to("cms/project_requests#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/project_requests/new").to route_to("cms/project_requests#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/project_requests/1").to route_to("cms/project_requests#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/project_requests/1/edit").to route_to("cms/project_requests#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/project_requests").to route_to("cms/project_requests#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/project_requests/1").to route_to("cms/project_requests#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/project_requests/1").to route_to("cms/project_requests#destroy", :id => "1")
    end

  end
end

require "rails_helper"

RSpec.describe Cms::TagsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/tags").to route_to("cms/tags#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/tags/new").to route_to("cms/tags#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/tags/1").to route_to("cms/tags#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/tags/1/edit").to route_to("cms/tags#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/tags").to route_to("cms/tags#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/tags/1").to route_to("cms/tags#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/tags/1").to route_to("cms/tags#destroy", :id => "1")
    end

  end
end

require "rails_helper"

RSpec.describe Cms::OrganicLandingsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/organic_landings").to route_to("cms/organic_landings#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/organic_landings/new").to route_to("cms/organic_landings#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/organic_landings/1").to route_to("cms/organic_landings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/organic_landings/1/edit").to route_to("cms/organic_landings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/organic_landings").to route_to("cms/organic_landings#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/organic_landings/1").to route_to("cms/organic_landings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/organic_landings/1").to route_to("cms/organic_landings#destroy", :id => "1")
    end

  end
end

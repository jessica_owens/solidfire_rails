require "rails_helper"

RSpec.describe Cms::ProjectRequestStatusesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/project_request_statuses").to route_to("cms/project_request_statuses#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/project_request_statuses/new").to route_to("cms/project_request_statuses#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/project_request_statuses/1").to route_to("cms/project_request_statuses#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/project_request_statuses/1/edit").to route_to("cms/project_request_statuses#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/project_request_statuses").to route_to("cms/project_request_statuses#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/project_request_statuses/1").to route_to("cms/project_request_statuses#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/project_request_statuses/1").to route_to("cms/project_request_statuses#destroy", :id => "1")
    end

  end
end

require "rails_helper"

RSpec.describe Cms::ResourcesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/resources").to route_to("cms/resources#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/resources/new").to route_to("cms/resources#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/resources/1").to route_to("cms/resources#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/resources/1/edit").to route_to("cms/resources#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/resources").to route_to("cms/resources#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/resources/1").to route_to("cms/resources#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/resources/1").to route_to("cms/resources#destroy", :id => "1")
    end

  end
end

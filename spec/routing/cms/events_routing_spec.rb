require "rails_helper"

RSpec.describe Cms::EventsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/events").to route_to("cms/events#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/events/new").to route_to("cms/events#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/events/1").to route_to("cms/events#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/events/1/edit").to route_to("cms/events#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/events").to route_to("cms/events#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/events/1").to route_to("cms/events#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/events/1").to route_to("cms/events#destroy", :id => "1")
    end

  end
end

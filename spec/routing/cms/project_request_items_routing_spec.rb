require "rails_helper"

RSpec.describe Cms::ProjectRequestItemsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/project_request_items").to route_to("cms/project_request_items#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/project_request_items/new").to route_to("cms/project_request_items#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/project_request_items/1").to route_to("cms/project_request_items#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/project_request_items/1/edit").to route_to("cms/project_request_items#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/project_request_items").to route_to("cms/project_request_items#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cms/project_request_items/1").to route_to("cms/project_request_items#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cms/project_request_items/1").to route_to("cms/project_request_items#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/project_request_items/1").to route_to("cms/project_request_items#destroy", :id => "1")
    end

  end
end

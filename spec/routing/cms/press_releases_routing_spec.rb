require "rails_helper"

RSpec.describe Cms::PressReleasesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/press_releases").to route_to("cms/press_releases#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/press_releases/new").to route_to("cms/press_releases#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/press_releases/1").to route_to("cms/press_releases#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/press_releases/1/edit").to route_to("cms/press_releases#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/press_releases").to route_to("cms/press_releases#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/press_releases/1").to route_to("cms/press_releases#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/press_releases/1").to route_to("cms/press_releases#destroy", :id => "1")
    end

  end
end

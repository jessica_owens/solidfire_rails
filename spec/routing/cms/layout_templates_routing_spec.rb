require "rails_helper"

RSpec.describe Cms::LayoutTemplatesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/layout_templates").to route_to("cms/layout_templates#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/layout_templates/new").to route_to("cms/layout_templates#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/layout_templates/1").to route_to("cms/layout_templates#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/layout_templates/1/edit").to route_to("cms/layout_templates#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/layout_templates").to route_to("cms/layout_templates#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/layout_templates/1").to route_to("cms/layout_templates#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/layout_templates/1").to route_to("cms/layout_templates#destroy", :id => "1")
    end

  end
end

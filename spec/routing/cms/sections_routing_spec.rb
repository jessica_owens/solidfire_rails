require "rails_helper"

RSpec.describe Cms::SectionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cms/sections").to route_to("cms/sections#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/sections/new").to route_to("cms/sections#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/sections/1").to route_to("cms/sections#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/sections/1/edit").to route_to("cms/sections#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cms/sections").to route_to("cms/sections#create")
    end

    it "routes to #update" do
      expect(:put => "/cms/sections/1").to route_to("cms/sections#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/sections/1").to route_to("cms/sections#destroy", :id => "1")
    end

  end
end

require 'rails_helper'

RSpec.describe 'home page' do
  before(:each) do
    visit(why_solidfire_path)
  end

  it 'the expected path should equal the board path' do
    expect(current_path).to eq why_solidfire_path
  end

  it 'shows H1 content on page' do
    expect(page).to have_content('The definitive all-flash array for the next generation data center.')
  end

  it 'check learn more link is correct' do
    href = find('#learn-more-solutions')['href']
    expect(href).to have_content(solutions_overview_path)
  end

  it 'check resources header link is correct' do
    href = find('.resources-header')['href']
    expect(href).to have_content(resources_path)

    visit(resources_path)
    page.status_code.should == 200
  end

  it 'check blog header link is correct' do
    href = find('.blog-header')['href']
    expect(href).to have_content('http://www.solidfire.com/blog/')
  end

  it 'check support header link is correct' do
    href = find('.support-header')['href']
    expect(href).to have_content(support_path)

    visit(support_path)
    page.status_code.should == 200
  end

  it 'check partner portal header link is correct' do
    href = find('.partner-portal-header')['href']
    expect(href).to have_content('https://solidfirepartners.force.com/CloudBuilders/portallogin')
  end

  it 'check logo header link is correct' do
    href = find('#sf-logo-top-nav')['href']
    expect(href).to have_content(home_index_path)

    visit(home_index_path)
    page.status_code.should == 200
  end

  it 'check why sf header link is correct' do
    href = find('#why-sf-nav')['href']
    expect(href).to have_content(why_solidfire_path)

    visit(why_solidfire_path)
    page.status_code.should == 200
  end

  it 'check why sf mobile header link is correct' do
    href = find('#why-sf-nav-mobile')['href']
    expect(href).to have_content(why_solidfire_path)

    visit(why_solidfire_path)
    page.status_code.should == 200
  end

  it 'check platform header link is correct' do
    href = find('#platform')['href']
    expect(href).to have_content(platform_overview_path)

    visit(platform_overview_path)
    page.status_code.should == 200
  end

  it 'check element software header link is correct' do
    href = find('.element-software-header')['href']
    expect(href).to have_content(elements_os_path)

    visit(elements_os_path)
    page.status_code.should == 200
  end

  it 'check nodes header link is correct' do
    href = find('.nodes-header')['href']
    expect(href).to have_content(hardware_path)

    visit(hardware_path)
    page.status_code.should == 200
  end

  it 'check solutions header link is correct' do
    href = find('#solutions')['href']
    expect(href).to have_content(solutions_overview_path)

    visit(solutions_overview_path)
    page.status_code.should == 200
  end

  it 'check support item header link is correct' do
    href = find('.support-item-header')['href']
    expect(href).to have_content(support_path)

    visit(support_path)
    page.status_code.should == 200
  end

  it 'check openstack header link is correct' do
    href = find('.openstack-header')['href']
    expect(href).to have_content(openstack_path)

    visit(openstack_path)
    page.status_code.should == 200
  end

  it 'check cloudstack header link is correct' do
    href = find('.cloudstack-header')['href']
    expect(href).to have_content(cloudstack_path)

    visit(cloudstack_path)
    page.status_code.should == 200
  end

  it 'check vmware header link is correct' do
    href = find('.vmware-header')['href']
    expect(href).to have_content(vmware_solutions_path)

    visit(vmware_solutions_path)
    page.status_code.should == 200
  end

  it 'check cloud header link is correct' do
    href = find('.cloud-header')['href']
    expect(href).to have_content(cloud_orchestration_path)

    visit(cloud_orchestration_path)
    page.status_code.should == 200
  end

  it 'check vdi header link is correct' do
    href = find('.vdi-header')['href']
    expect(href).to have_content(vdi_path)

    visit(vdi_path)
    page.status_code.should == 200
  end

  it 'check database header link is correct' do
    href = find('.database-header')['href']
    expect(href).to have_content(database_path)

    visit(database_path)
    page.status_code.should == 200
  end

  it 'check agile infrastructure-header header link is correct' do
    href = find('.agile-infrastructure-header')['href']
    expect(href).to have_content(agile_infrastructure_path)

    visit(agile_infrastructure_path)
    page.status_code.should == 200
  end

  it 'check elementx header link is correct' do
    href = find('.elementx-header')['href']
    expect(href).to have_content(elementx_path)

    visit(elementx_path)
    page.status_code.should == 200
  end

  it 'check service providers header link is correct' do
    href = find('.service-providers-header')['href']
    expect(href).to have_content(service_providers_path)

    visit(service_providers_path)
    page.status_code.should == 200
  end

  it 'check how to buy header link is correct' do
    href = find('#how-to-buy')['href']
    expect(href).to have_content(how_to_buy_path)

    visit(how_to_buy_path)
    page.status_code.should == 200
  end

  it 'check careers header link is correct' do
    href = find('.careers-header')['href']
    expect(href).to have_content(careers_path)

    visit(careers_path)
    page.status_code.should == 200
  end

  it 'check events header link is correct' do
    href = find('.events-header')['href']
    expect(href).to have_content(events_path)

    visit(events_path)
    page.status_code.should == 200
  end

  it 'check resources rollover box header link is correct' do
    href = find('.resources-box-header')['href']
    expect(href).to have_content(resources_path)

    visit(resources_path)
    page.status_code.should == 200
  end

  it 'check webinars header link is correct' do
    href = find('.webinars-header')['href']
    expect(href).to have_content(type_result_path('webinars'))

    visit(type_result_path('webinars'))
    page.status_code.should == 200
  end

  it 'check news header link is correct' do
    href = find('.news-header')['href']
    expect(href).to have_content(news_path)

    visit(news_path)
    page.status_code.should == 200
  end

  it 'check pr header link is correct' do
    href = find('.pr-header')['href']
    expect(href).to have_content(pr_index_path)

    visit(pr_index_path)
    page.status_code.should == 200
  end

  it 'check customers header link is correct' do
    href = find('.customers-header')['href']
    expect(href).to have_content(customers_path)

    visit(customers_path)
    page.status_code.should == 200
  end

  it 'check company header link is correct' do
    href = find('.company-header')['href']
    expect(href).to have_content(overview_path)

    visit(overview_path)
    page.status_code.should == 200
  end

  it 'check channel partners header link is correct' do
    href = find('.channel-partners-header')['href']
    expect(href).to have_content(channel_partners_path)

    visit(channel_partners_path)
    page.status_code.should == 200
  end

  it 'check technology partners header link is correct' do
    href = find('.technology-partners-header')['href']
    expect(href).to have_content(technology_partners_path)

    visit(technology_partners_path)
    page.status_code.should == 200
  end

  it 'check analyst reviews header link is correct' do
    href = find('.analyst-reviews-header')['href']
    expect(href).to have_content(analyst_reviews_path)

    visit(analyst_reviews_path)
    page.status_code.should == 200
  end

  it 'check why solidfire mobile header link is correct' do
    href = find('.why-solidfire-mobile')['href']
    expect(href).to have_content(why_solidfire_path)

    visit(why_solidfire_path)
    page.status_code.should == 200
  end

  it 'check platform mobile header link is correct' do
    href = find('.platform-mobile-header')['href']
    expect(href).to have_content(platform_overview_path)

    visit(platform_overview_path)
    page.status_code.should == 200
  end

  it 'check element software mobile header link is correct' do
    href = find('.element-software-mobile-header')['href']
    expect(href).to have_content(elements_os_path)

    visit(elements_os_path)
    page.status_code.should == 200
  end

  it 'check hardware mobile header link is correct' do
    href = find('.nodes-mobile-header')['href']
    expect(href).to have_content(hardware_path)

    visit(hardware_path)
    page.status_code.should == 200
  end

  it 'check ai mobile header link is correct' do
    href = find('.agile-infrastructure-mobile-header')['href']
    expect(href).to have_content(agile_infrastructure_path)

    visit(agile_infrastructure_path)
    page.status_code.should == 200
  end

  it 'check solutions mobile header link is correct' do
    href = find('.solutions-mobile-header')['href']
    expect(href).to have_content(solutions_overview_path)

    visit(solutions_overview_path)
    page.status_code.should == 200
  end

  it 'check cloud mobile header link is correct' do
    href = find('.cloud-mobile-header')['href']
    expect(href).to have_content(cloud_orchestration_path)

    visit(cloud_orchestration_path)
    page.status_code.should == 200
  end

  it 'check openstack mobile header link is correct' do
    href = find('.openstack-mobile-header')['href']
    expect(href).to have_content(openstack_path)

    visit(openstack_path)
    page.status_code.should == 200
  end

  it 'check cloudstack mobile header link is correct' do
    href = find('.cloudstack-mobile-header')['href']
    expect(href).to have_content(cloudstack_path)

    visit(cloudstack_path)
    page.status_code.should == 200
  end

  it 'check virtualization mobile header link is correct' do
    href = find('.virtualization-mobile-header')['href']
    expect(href).to have_content(virtual_infrastructure_path)

    visit(virtual_infrastructure_path)
    page.status_code.should == 200
  end

  it 'check vdi mobile header link is correct' do
    href = find('.vdi-mobile-header')['href']
    expect(href).to have_content(vdi_path)

    visit(vdi_path)
    page.status_code.should == 200
  end

  it 'check database mobile header link is correct' do
    href = find('.database-mobile-header')['href']
    expect(href).to have_content(database_path)

    visit(database_path)
    page.status_code.should == 200
  end

  it 'check data protection mobile header link is correct' do
    href = find('.data-protection-mobile-header')['href']
    expect(href).to have_content(data_protection_path)

    visit(data_protection_path)
    page.status_code.should == 200
  end

  it 'check service providers mobile header link is correct' do
    href = find('.service-providers-mobile-header')['href']
    expect(href).to have_content(service_providers_path)

    visit(service_providers_path)
    page.status_code.should == 200
  end

  it 'check blog mobile header link is correct' do
    href = find('.blog-mobile-header')['href']
    expect(href).to have_content('http://www.solidfire.com/blog/')
  end

  it 'check support mobile header link is correct' do
    href = find('.support-mobile-header')['href']
    expect(href).to have_content(support_path)

    visit(support_path)
    page.status_code.should == 200
  end

  it 'check support mobile header link is correct' do
    href = find('.support-item-mobile-header')['href']
    expect(href).to have_content(support_path)

    visit(support_path)
    page.status_code.should == 200
  end

  it 'check partner portal mobile header link is correct' do
    href = find('.partner-portal-mobile-header')['href']
    expect(href).to have_content('https://solidfirepartners.force.com/CloudBuilders/portallogin')
  end

  it 'check careers mobile header link is correct' do
    href = find('.careers-mobile-header')['href']
    expect(href).to have_content(careers_path)

    visit(careers_path)
    page.status_code.should == 200
  end

  it 'check resources mobile header link is correct' do
    href = find('.resources-mobile-header')['href']
    expect(href).to have_content(resources_path)

    visit(resources_path)
    page.status_code.should == 200
  end

  it 'check contact mobile header link is correct' do
    href = find('.contact-mobile-header')['href']
    expect(href).to have_content(contact_path)

    visit(contact_path)
    page.status_code.should == 200
  end

  it 'check events mobile header link is correct' do
    href = find('.events-mobile-header')['href']
    expect(href).to have_content(events_path)

    visit(events_path)
    page.status_code.should == 200
  end

  it 'check webinars mobile header link is correct' do
    href = find('.webinars-mobile-header')['href']
    expect(href).to have_content(type_result_path('webinars'))

    visit(type_result_path('webinars'))
    page.status_code.should == 200
  end

  it 'check news mobile header link is correct' do
    href = find('.news-mobile-header')['href']
    expect(href).to have_content(news_path)

    visit(news_path)
    page.status_code.should == 200
  end

  it 'check pr mobile header link is correct' do
    href = find('.pr-mobile-header')['href']
    expect(href).to have_content(pr_index_path)

    visit(pr_index_path)
    page.status_code.should == 200
  end

  it 'check company mobile header link is correct' do
    href = find('.company-mobile-header')['href']
    expect(href).to have_content(overview_path)

    visit(overview_path)
    page.status_code.should == 200
  end

  it 'check channel partners mobile header link is correct' do
    href = find('.channel-partners-mobile-header')['href']
    expect(href).to have_content(channel_partners_path)

    visit(channel_partners_path)
    page.status_code.should == 200
  end

  it 'check technology partners mobile header link is correct' do
    href = find('.technology-partners-mobile-header')['href']
    expect(href).to have_content(technology_partners_path)

    visit(technology_partners_path)
    page.status_code.should == 200
  end

  it 'check customers mobile header link is correct' do
    href = find('.customers-mobile-header')['href']
    expect(href).to have_content(customers_path)

    visit(customers_path)
    page.status_code.should == 200
  end

  it 'check why sf footer link is correct' do
    href = find('.why-sf-footer')['href']
    expect(href).to have_content(why_solidfire_path)

    visit(why_solidfire_path)
    page.status_code.should == 200
  end

  it 'check platform footer link is correct' do
    href = find('.platform-footer')['href']
    expect(href).to have_content(platform_overview_path)

    visit(platform_overview_path)
    page.status_code.should == 200
  end

  it 'check element software footer link is correct' do
    href = find('.element-software-footer')['href']
    expect(href).to have_content(elements_os_path)

    visit(elements_os_path)
    page.status_code.should == 200
  end

  it 'check hardware footer link is correct' do
    href = find('.nodes-footer')['href']
    expect(href).to have_content(hardware_path)

    visit(hardware_path)
    page.status_code.should == 200
  end

  it 'check support footer link is correct' do
    href = find('.support-footer')['href']
    expect(href).to have_content(support_path)

    visit(support_path)
    page.status_code.should == 200
  end

  it 'check solutions footer link is correct' do
    href = find('.solutions-footer')['href']
    expect(href).to have_content(solutions_overview_path)

    visit(solutions_overview_path)
    page.status_code.should == 200
  end

  it 'check openstack footer link is correct' do
    href = find('.openstack-footer')['href']
    expect(href).to have_content(openstack_path)

    visit(openstack_path)
    page.status_code.should == 200
  end

  it 'check cloudstack footer link is correct' do
    href = find('.cloudstack-footer')['href']
    expect(href).to have_content(cloudstack_path)

    visit(cloudstack_path)
    page.status_code.should == 200
  end

  it 'check vmware footer link is correct' do
    href = find('.vmware-footer')['href']
    expect(href).to have_content(vmware_solutions_path)

    visit(vmware_solutions_path)
    page.status_code.should == 200
  end

  it 'check cloud footer link is correct' do
    href = find('.cloud-footer')['href']
    expect(href).to have_content(cloud_orchestration_path)

    visit(cloud_orchestration_path)
    page.status_code.should == 200
  end

  it 'check vdi footer link is correct' do
    href = find('.vdi-footer')['href']
    expect(href).to have_content(vdi_path)

    visit(vdi_path)
    page.status_code.should == 200
  end

  it 'check database footer link is correct' do
    href = find('.database-footer')['href']
    expect(href).to have_content(database_path)

    visit(database_path)
    page.status_code.should == 200
  end

  it 'check agile infrastructure_path footer link is correct' do
    href = find('.agile-infrastructure-footer')['href']
    expect(href).to have_content(agile_infrastructure_path)

    visit(agile_infrastructure_path)
    page.status_code.should == 200
  end

  it 'check element x footer link is correct' do
    href = find('.elementx-footer')['href']
    expect(href).to have_content(elementx_path)

    visit(elementx_path)
    page.status_code.should == 200
  end

  it 'check service providers_path footer link is correct' do
    href = find('.service-providers-footer')['href']
    expect(href).to have_content(service_providers_path)

    visit(service_providers_path)
    page.status_code.should == 200
  end

  it 'check how to buy footer link is correct' do
    href = find('.how-to-buy-footer')['href']
    expect(href).to have_content(how_to_buy_path)

    visit(how_to_buy_path)
    page.status_code.should == 200
  end

  it 'check resources footer link is correct' do
    href = find('.resources-footer')['href']
    expect(href).to have_content(resources_path)

    visit(resources_path)
    page.status_code.should == 200
  end

  it 'check blog footer link is correct' do
    href = find('.blog-footer')['href']
    expect(href).to have_content('http://www.solidfire.com/blog/')
  end

  it 'check partner portal footer link is correct' do
    href = find('.partner-portal-footer')['href']
    expect(href).to have_content('https://solidfirepartners.force.com/CloudBuilders/portallogin')
  end

  it 'check company footer link is correct' do
    href = find('.company-overview-footer')['href']
    expect(href).to have_content(overview_path)

    visit(overview_path)
    page.status_code.should == 200
  end

  it 'check customers footer link is correct' do
    href = find('.customers-footer')['href']
    expect(href).to have_content(customers_path)

    visit(customers_path)
    page.status_code.should == 200
  end

  it 'check channel partners footer link is correct' do
    href = find('.channel-partners-footer')['href']
    expect(href).to have_content(channel_partners_path)

    visit(channel_partners_path)
    page.status_code.should == 200
  end

  it 'check technology partners footer link is correct' do
    href = find('.technology-partners-footer')['href']
    expect(href).to have_content(technology_partners_path)

    visit(technology_partners_path)
    page.status_code.should == 200
  end

  it 'check news footer link is correct' do
    href = find('.news-footer')['href']
    expect(href).to have_content(news_path)

    visit(news_path)
    page.status_code.should == 200
  end

  it 'check pr footer link is correct' do
    href = find('.pr-footer')['href']
    expect(href).to have_content(pr_index_path)

    visit(pr_index_path)
    page.status_code.should == 200
  end

  it 'check events footer link is correct' do
    href = find('.events-footer')['href']
    expect(href).to have_content(events_path)

    visit(events_path)
    page.status_code.should == 200
  end

  it 'check webinars footer link is correct' do
    href = find('.webinars-footer')['href']
    expect(href).to have_content(type_result_path('webinars'))

    visit(type_result_path('webinars'))
    page.status_code.should == 200
  end

  it 'check contact footer link is correct' do
    href = find('.contact-footer')['href']
    expect(href).to have_content(contact_path)

    visit(contact_path)
    page.status_code.should == 200
  end

  it 'check terms footer link is correct' do
    href = find('.terms-footer')['href']
    expect(href).to have_content(legal_path)

    visit(legal_path)
    page.status_code.should == 200
  end

  it 'check privacy footer link is correct' do
    href = find('.privacy-footer')['href']
    expect(href).to have_content(privacy_path)

    visit(privacy_path)
    page.status_code.should == 200
  end

  it 'check cookies footer link is correct' do
    href = find('.cookies-footer')['href']
    expect(href).to have_content(cookies_path)

    visit(cookies_path)
    page.status_code.should == 200
  end

  it 'check contact bottom footer link is correct' do
    href = find('.contact-bottom-footer')['href']
    expect(href).to have_content(contact_path)

    visit(contact_path)
    page.status_code.should == 200
  end

  it 'check facebook footer link is correct' do
    href = find('.facebook-footer')['href']
    expect(href).to have_content('https://www.facebook.com/SolidFire')
  end

  it 'check twitter footer link is correct' do
    href = find('.twitter-footer')['href']
    expect(href).to have_content('https://twitter.com/solidfire')
  end

  it 'check instagram footer link is correct' do
    href = find('.instagram-footer')['href']
    expect(href).to have_content('https://instagram.com/fueledbysolidfire/')
  end

  it 'check gplus footer link is correct' do
    href = find('.gplus-footer')['href']
    expect(href).to have_content('https://plus.google.com/+Solidfire/posts')
  end

  it 'check youtube footer link is correct' do
    href = find('.youtube-footer')['href']
    expect(href).to have_content('https://www.youtube.com/channel/UC1Ovc5q08uE_trkdaeqwb_A')
  end

  it 'check slideshare footer link is correct' do
    href = find('.slideshare-footer')['href']
    expect(href).to have_content('http://www.slideshare.net/SolidFireInc/')
  end

end


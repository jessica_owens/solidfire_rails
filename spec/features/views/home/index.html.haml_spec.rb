require 'rails_helper'

RSpec.describe 'home page' do
  before(:each) do
    visit(root_path)
  end

  it 'the expected path should equal the board path' do
    expect(current_path).to eq root_path
  end

  it 'has EMC & Pure link' do
    href = find('#pr')['href']
    expect(href).to have_content('/lp/comparing-flash-storage-platforms')
  end

end


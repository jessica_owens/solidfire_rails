require 'rails_helper'

RSpec.describe 'about board page'  do
  before(:each) do
    visit overview_path
  end

  it 'the expected path should equal the about overview path' do
    expect(current_path).to eq overview_path
  end

  it 'shows H1 content on page' do
    expect(page).to have_content('SolidFire delivers an unstoppable force for data center transformation')
  end
end

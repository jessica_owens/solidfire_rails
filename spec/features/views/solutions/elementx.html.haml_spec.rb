require 'rails_helper'

RSpec.describe 'checking for elementX page' do
  before(:each) do
    visit elementx_path
  end

  it 'the expected path should equal the elementx path' do
    expect(current_path).to eq elementx_path
  end

  it 'shows H1 content on page' do
    expect(page).to have_content('SolidFire Element X')
  end

end

require 'rails_helper'

RSpec.describe 'home page' do
  before(:each) do
    visit(hardware_path)
  end

  it 'the expected path should equal the board path' do
    expect(current_path).to eq hardware_path
  end

  it 'shows H1 content on page' do
    expect(page).to have_content('SolidFire SF Series storage hardware')
  end

end


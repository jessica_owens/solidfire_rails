require 'rails_helper'

RSpec.describe 'home page' do
  before(:each) do
    visit(scale_out_path)
  end

  it 'the expected path should equal the board path' do
    expect(current_path).to eq scale_out_path
  end

  it 'shows H1 content on page' do
    expect(page).to have_content('Scale-out storage')
  end

end


require 'rails_helper'

RSpec.describe 'home page' do
  before(:each) do
    visit(platform_overview_path)
  end

  it 'the expected path should equal the board path' do
    expect(current_path).to eq platform_overview_path
  end

  it 'shows H1 content on page' do
    expect(page).to have_content('The world’s most agile data centers demand SolidFire’s all-flash storage platform')
  end

end


#!/bin/bash

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_categories --db solidfire_rails --out /workspace/mongo-files/script/cms_categories.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_events --db solidfire_rails --out /workspace/mongo-files/script/cms_events.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_landing_pages --db solidfire_rails --out /workspace/mongo-files/script/cms_landing_pages.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_layout_templates --db solidfire_rails --out /workspace/mongo-files/script/cms_layout_templates.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_news --db solidfire_rails --out /workspace/mongo-files/script/cms_news.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_organic_landings --db solidfire_rails --out /workspace/mongo-files/script/cms_organic_landings.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_press_releases --db solidfire_rails --out /workspace/mongo-files/script/cms_press_releases.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_project_request_statuses --db solidfire_rails --out /workspace/mongo-files/script/cms_project_request_statuses.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_project_request_types --db solidfire_rails --out /workspace/mongo-files/script/cms_project_request_types.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_project_requests --db solidfire_rails --out /workspace/mongo-files/script/cms_project_requests.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_resource_images --db solidfire_rails --out /workspace/mongo-files/script/cms_resource_images.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_resources --db solidfire_rails --out /workspace/mongo-files/script/cms_resources.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_sections --db solidfire_rails --out /workspace/mongo-files/script/cms_sections.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_tags --db solidfire_rails --out /workspace/mongo-files/script/cms_tags.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_users --db solidfire_rails --out /workspace/mongo-files/script/cms_users.json

echo 'production collections exported'

mongo <<EOF
use solidfire_rails_development
db.cms_resources.drop()
db.cms_press_releases.drop()
db.cms_landing_pages.drop()
db.cms_categories.drop()
db.cms_events.drop()
db.cms_layout_templates.drop()
db.cms_news.drop()
db.cms_organic_landings.drop()
db.cms_project_request_statuses.drop()
db.cms_project_request_types.drop()
db.cms_project_requests.drop()
db.cms_resource_images.drop()
db.cms_sections.drop()
db.cms_tags.drop()
db.cms_users.drop()
exit
EOF

echo 'collections dropped from local dev'

mongoimport --host localhost --port 27017 --collection cms_categories --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_categories.json

mongoimport --host localhost --port 27017 --collection cms_events --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_events.json

mongoimport --host localhost --port 27017 --collection cms_landing_pages --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_landing_pages.json

mongoimport --host localhost --port 27017 --collection cms_layout_templates --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_layout_templates.json

mongoimport --host localhost --port 27017 --collection cms_news --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_news.json

mongoimport --host localhost --port 27017 --collection cms_organic_landings --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_organic_landings.json

mongoimport --host localhost --port 27017 --collection cms_press_releases --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_press_releases.json

mongoimport --host localhost --port 27017 --collection cms_project_request_statuses --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_project_request_statuses.json

mongoimport --host localhost --port 27017 --collection cms_project_request_types --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_project_request_types.json

mongoimport --host localhost --port 27017 --collection cms_project_requests --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_project_requests.json

mongoimport --host localhost --port 27017 --collection cms_resource_images --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_resource_images.json

mongoimport --host localhost --port 27017 --collection cms_resources --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_resources.json

mongoimport --host localhost --port 27017 --collection cms_sections --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_sections.json

mongoimport --host localhost --port 27017 --collection cms_tags --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_tags.json

mongoimport --host localhost --port 27017 --collection cms_users --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_users.json

echo 'collections imported into local'


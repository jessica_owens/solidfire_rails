# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)

use Rack::ReverseProxy do
  reverse_proxy /^\/blog(.*)$/, 'http://www-solidfire-prod.ec2.barkleylabs.com/blog/$1', :timeout => 500, :preserve_host => true
  reverse_proxy /^\/solidsolutionsblog(.*)$/, 'http://www-solidfire-prod.ec2.barkleylabs.com/solidsolutionsblog/$1', :timeout => 500, :preserve_host => true
end

run Rails.application




#!/bin/bash

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_resources --db solidfire_rails --out /workspace/mongo-files/script/cms_resources.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_press_releases --db solidfire_rails --out /workspace/mongo-files/script/cms_press_releases.json

mongoexport --host c686.candidate.18.mongolayer.com --port 10686 --username sfmongo --password QLvf8uGXC3Z5G7wf --collection cms_landing_pages --db solidfire_rails --out /workspace/mongo-files/script/cms_landing_pages.json

echo 'resources and press release collections exported'

#mongo localhost:27017/solidfire_rails_development --eval "db.cms_resources.drop(); db.cms_press_releases.drop(); db.shutdownServer()"

mongo <<EOF
use solidfire_rails_development
db.cms_resources.drop()
db.cms_press_releases.drop()
db.cms_landing_pages.drop()
exit
EOF

echo 'resources and press release collections dropped'

mongoimport --host localhost --port 27017 --collection cms_resources --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_resources.json

mongoimport --host localhost --port 27017 --collection cms_press_releases --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_press_releases.json

mongoimport --host localhost --port 27017 --collection cms_landing_pages --db solidfire_rails_development --type json --file /workspace/mongo-files/script/cms_landing_pages.json

echo 'resources and press release collections imported'

rake sitemap:create

echo 'sitemap generated!'

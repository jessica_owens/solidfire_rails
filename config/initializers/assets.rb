# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
#Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

#%w( home, company ).each do |controller|
#  Rails.application.config.assets.precompile += ["#{controller}.js", "#{controller}.css"]
# end

Rails.application.config.assets.precompile += %w( company.js )
Rails.application.config.assets.precompile += %w( case_studies.js )
Rails.application.config.assets.precompile += %w( home.js )
Rails.application.config.assets.precompile += %w( solutions.js )
Rails.application.config.assets.precompile += %w( developers.js )
Rails.application.config.assets.precompile += %w( presses.js )
Rails.application.config.assets.precompile += %w( cms/home.js )
Rails.application.config.assets.precompile += %w( cms/resources.js )
Rails.application.config.assets.precompile += %w( cms/tags.js )
Rails.application.config.assets.precompile += %w( cms/categories.js )
Rails.application.config.assets.precompile += %w( cms/users.js )
Rails.application.config.assets.precompile += %w( cms/login.js )
Rails.application.config.assets.precompile += %w( cms/login.css )
Rails.application.config.assets.precompile += %w( resources.js )
Rails.application.config.assets.precompile += %w( landing_pages.js )
Rails.application.config.assets.precompile += %w( errors.js )
Rails.application.config.assets.precompile += %w( isotope.pkgd.min.js )
Rails.application.config.assets.precompile += %w( fit-columns.js )
Rails.application.config.assets.precompile += %w( flash_array_guides.js )
Rails.application.config.assets.precompile += %w( cms/resource_images.js )
Rails.application.config.assets.precompile += %w( jquery.validate.min.js )
Rails.application.config.assets.precompile += %w( cms/resource_order.js )
Rails.application.config.assets.precompile += %w( cms/organic_landings.js )
Rails.application.config.assets.precompile += %w( infrastructure_as_a_service.js )
Rails.application.config.assets.precompile += %w( elio_404.js )
Rails.application.config.assets.precompile += %w( errors.js )
Rails.application.config.assets.precompile += %w( cms/news.js )
Rails.application.config.assets.precompile += %w( news.js )
Rails.application.config.assets.precompile += %w( cms/press_releases.js )
Rails.application.config.assets.precompile += %w( press_release.js )
Rails.application.config.assets.precompile += %w( great_feats.js )
Rails.application.config.assets.precompile += %w( cms/landing_pages.js )
Rails.application.config.assets.precompile += %w( jquery.cookie.js )
Rails.application.config.assets.precompile += %w( cms/layout_templates.js )
Rails.application.config.assets.precompile += %w( lp.js )
Rails.application.config.assets.precompile += %w( cms/events.js )
Rails.application.config.assets.precompile += %w( storage_system.js )
Rails.application.config.assets.precompile += %w( froogaloop2.min.js )
Rails.application.config.assets.precompile += %w( video.js )
Rails.application.config.assets.precompile += %w( jquery.min.js )
Rails.application.config.assets.precompile += %w( cms/project_requests.js )
Rails.application.config.assets.precompile += %w( cms/project_request_statuses.js )
Rails.application.config.assets.precompile += %w( cms/project_request_types.js )
Rails.application.config.assets.precompile += %w( style_guide.js )
Rails.application.config.assets.precompile += %w( addthisevent.theme7.css )
Rails.application.config.assets.precompile += %w( landing_pages/vmworld.css )
Rails.application.config.assets.precompile += %w( landing_pages/sp-benchmark.css )
Rails.application.config.assets.precompile += %w( landing_pages/vmworld.js )
Rails.application.config.assets.precompile += %w( solutions/overview.css )
Rails.application.config.assets.precompile += %w( solutions/cloud_orchestration.css )
Rails.application.config.assets.precompile += %w( solutions/openstack.css )
Rails.application.config.assets.precompile += %w( solutions/cloudstack.css )
Rails.application.config.assets.precompile += %w( solutions/flexiant.css )
Rails.application.config.assets.precompile += %w( solutions/onapp.css )
Rails.application.config.assets.precompile += %w( solutions/database.css )
Rails.application.config.assets.precompile += %w( solutions/oracle.css )
Rails.application.config.assets.precompile += %w( solutions/mysql.css )
Rails.application.config.assets.precompile += %w( solutions/mssql.css )
Rails.application.config.assets.precompile += %w( solutions/mongodb.css )
Rails.application.config.assets.precompile += %w( solutions/vdi.css )
Rails.application.config.assets.precompile += %w( solutions/citrix_xen_desktop.css )
Rails.application.config.assets.precompile += %w( solutions/vmware_horizon_view.css )
Rails.application.config.assets.precompile += %w( solutions/agile_infrastructure.css )
Rails.application.config.assets.precompile += %w( solutions/virtual_infrastructure.css )
Rails.application.config.assets.precompile += %w( solutions/data_protection.css )
Rails.application.config.assets.precompile += %w( solutions/managed_dedicated_san.css )
Rails.application.config.assets.precompile += %w( solutions/elementx.css )
Rails.application.config.assets.precompile += %w( solutions/case_studies.css )
Rails.application.config.assets.precompile += %w( home_new.css )
Rails.application.config.assets.precompile += %w( solutions/competitive_comparison.css )
Rails.application.config.assets.precompile += %w( why_solidfire.js )
Rails.application.config.assets.precompile += %w( platform/platform_overview.css )
Rails.application.config.assets.precompile += %w( platform/product_specifications.css )
Rails.application.config.assets.precompile += %w( platform/nodes.css )
Rails.application.config.assets.precompile += %w( analyst-reviews.css )
Rails.application.config.assets.precompile += %w( platform.js )
Rails.application.config.assets.precompile += %w( openstack.js )
Rails.application.config.assets.precompile += %w( product_specifications.js )
Rails.application.config.assets.precompile += %w( nodes.js )
Rails.application.config.assets.precompile += %w( vmware.js )
Rails.application.config.assets.precompile += %w( index.js )
Rails.application.config.assets.precompile += %w( platform_overview.js )
Rails.application.config.assets.precompile += %w( global_efficiencies.js )
Rails.application.config.assets.precompile += %w( demo_form.js )
Rails.application.config.assets.precompile += %w( platform/elements_os.css )
Rails.application.config.assets.precompile += %w( platform/automated_management.css )
Rails.application.config.assets.precompile += %w( platform/guaranteed_performance.css )
Rails.application.config.assets.precompile += %w( platform/scale_out.css )
Rails.application.config.assets.precompile += %w( platform/data_assurance.css )
Rails.application.config.assets.precompile += %w( platform/global_efficiencies.css )
Rails.application.config.assets.precompile += %w( platform/support.css )
Rails.application.config.assets.precompile += %w( platform/resources.css )
Rails.application.config.assets.precompile += %w( jquery.flip.min.js)
Rails.application.config.assets.precompile += %w( solutions/vmware_solutions.css )
Rails.application.config.assets.precompile += %w( solutions/infrastructure_as_a_service.css )
Rails.application.config.assets.precompile += %w( how_to_buy.css )
Rails.application.config.assets.precompile += %w( how_to_buy.js )
Rails.application.config.assets.precompile += %w( element-os.js )
Rails.application.config.assets.precompile += %w( automated_management.js )
Rails.application.config.assets.precompile += %w( success_stories.js )
Rails.application.config.assets.precompile += %w( solutions/service_providers.css )
Rails.application.config.assets.precompile += %w( solutions/success_stories.css )
Rails.application.config.assets.precompile += %w( solutions/desktop_as_a_service.css )
Rails.application.config.assets.precompile += %w( solutions/database_as_a_service.css )
Rails.application.config.assets.precompile += %w( landing_pages/dm.css )
Rails.application.config.assets.precompile += %w( cms/sections.js )
Rails.application.config.assets.precompile += %w( company/customers.css )
Rails.application.config.assets.precompile += %w( company/support.css )
Rails.application.config.assets.precompile += %w( shared/subnav.css )
Rails.application.config.assets.precompile += %w( shared/support_subnav.css )
Rails.application.config.assets.precompile += %w( support.js )
Rails.application.config.assets.precompile += %w( service_providers.js )
Rails.application.config.assets.precompile += %w( scale_out.js )
Rails.application.config.assets.precompile += %w( guaranteed_performance.js )
Rails.application.config.assets.precompile += %w( contact_form.js )
Rails.application.config.assets.precompile += %w( agile_infrastructure.js )
Rails.application.config.assets.precompile += %w( shared/fixed_subnav.css )
Rails.application.config.assets.precompile += %w( company/support.js )
Rails.application.config.assets.precompile += %w( storage_system/global_efficiencies.css )
Rails.application.config.assets.precompile += %w( landing_pages/flash_with_you.css )
Rails.application.config.assets.precompile += %w( landing_pages/mtfbwy.js )
Rails.application.config.assets.precompile += %w( careers/careers.css )
Rails.application.config.assets.precompile += %w( careers/careers.js )
Rails.application.config.assets.precompile += %w( data_assurance.js )
Rails.application.config.assets.precompile += %w( storage_system/global_efficiencies.js )
Rails.application.config.assets.precompile += %w( competitive_comparison.js )
Rails.application.config.assets.precompile += %w( jquery.visible.min.js )
Rails.application.config.assets.precompile += %w( why_solidfire.css )
Rails.application.config.assets.precompile += %w( home.css )
Rails.application.config.assets.precompile += %w( cms/cms-sitewide.css )
Rails.application.config.assets.precompile += %w( cms/project_requests.css )
Rails.application.config.assets.precompile += %w( solutions/openstack_infrastructure.css )
Rails.application.config.assets.precompile += %w( company/contact.css )
Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( platform/element_os_resources.css )
Rails.application.config.assets.precompile += %w( landing_pages/openstack_tokyo.css )
Rails.application.config.assets.precompile += %w( landing_pages/analyst-day.css )
Rails.application.config.assets.precompile += %w( landing_pages/analyst-day.js )
Rails.application.config.assets.precompile += %w( landing_pages/idc.css )
Rails.application.config.assets.precompile += %w( landing_pages/architectural_comparisons.css )
Rails.application.config.assets.precompile += %w( overview.css )
Rails.application.config.assets.precompile += %w( overview.js )
Rails.application.config.assets.precompile += %w( landing_pages/unstoppable.css )
Rails.application.config.assets.precompile += %w( landing_pages/berlin-live.css )
Rails.application.config.assets.precompile += %w( all_resources.css )
Rails.application.config.assets.precompile += %w( comparing_platforms.js )
Rails.application.config.assets.precompile += %w( cms/project_request_items.js )
Rails.application.config.assets.precompile += %w( cms.js )
Rails.application.config.assets.precompile += %w( parallex.css )























require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
# require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'
require 'susy'
require 'rack/reverse_proxy'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SolidfireRails
  class Application < Rails::Application

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    # load all files in config/locales/
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    config.i18n.enforce_available_locales = false
    config.exceptions_app = self.routes

    # temp--until we get a cdn this is our new assets location
    # config.assets.prefix = "/assets"
    config.middleware.use Rack::Deflater

    config.middleware.insert_before(Rack::Runtime, Rack::Rewrite) do
      #r301 %r{^/(.*)/$}, '/$1'

      r301 '/ai/' , '/solutions/agile-infrastructure'
      r301 '/ai',	'/solutions/agile-infrastructure'
      r301 '/AI',	'/solutions/agile-infrastructure'
      r301 '/contact',	'/about/contact'
      r301 '/contact/',	'/about/contact'
      r301 '/customers',	'/about/customers'
      r301 '/comparing-flash-storage-platforms/' , '/lp/comparing-flash-storage-platforms'
      r301 '/demand-great/' , '/lp/what-is-the-next-generation-data-center'
      r301 '/demand-great' , '/lp/what-is-the-next-generation-data-center'
      r301 '/elementos6-pressrelease' , '/platform/element-os'
      r301 '/elementos7-pressrelease' , '/platform/element-os'
      r301 '/fueled-by-solidfire/' , '/solutions/success-stories#fueled'
      r301 '/get-started/' , '/how-to-buy'
      r301 '/gmq-lp' , '/lp/gmq-2015'
      r301 '/nodes' , '/platform/hardware'
      r301 '/nodes/' , '/platform/hardware'
      r301 '/pricing/' , '/how-to-buy'
      r301 '/partners/' , '/how-to-buy'
      r301 '/resources/' , '/resources'
      r301 '/resources/gartner-2014-magic-quadrant-for-solid-state-arrays' , '/resources/gartner-2015-magic-quadrant-for-solid-state-arrays'
      r301 '/resources/solidfire-api-reference-guide/' , '/resources'
      r301 '/resources/whitepaper-how-to-understand-and-implement-quality-of-service' , '/resources/solidfire-definitive-guide-guaranteeing-storage-performance'
      r301 '/saas-ebook/' , 'http://info.solidfire.com/saas-ebook.html'
      r301 '/sf9010/' , '/platform/product-specifications'
      r301 '/sf9010' , '/platform/product-specifications'
      r301 '/solution/' , '/solutions'
      r301 '/solution/how-to-buy/' , '/how-to-buy'
      r301 '/solution/openstack/' , '/solutions/cloud-orchestration/openstack'
      r301 '/solution/overview/' , '/solutions'
      r301 '/solution/performance/' , '/platform/element-os/guaranteed-performance'
      r301 '/solution/scalability/' , '/platform/element-os/scale-out'
      r301 '/solution/solidfire-storage-system/' , '/platform'
      r301 '/solutions/cloud-orchestration/vmware/' , '/solutions/vmware-solutions'
      r301 '/solutions/data-protection/' , '/platform/element-os/data-assurance'
      r301 '/solutions/openstack/' , '/solutions/cloud-orchestration/openstack'
      r301 '/solutions/partners/' , '/about/channel-partners/'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/brinkster' , '/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/brinkster/' , '/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/drfortress/' , '/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/getcloudservices/' , '/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/viawest/' , '/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/' , '/solutions/success-stories#fueled'
      r301 '/storage-system' , '/platform'
      r301 '/storage-system/' , '/platform'
      r301 '/storage-system/automated-management' , '/platform/element-os/automated-management'
      r301 '/storage-system/automated-management/' , '/platform/element-os/automated-management'
      r301 '/storage-system/data-assurance' , '/platform/element-os/data-assurance'
      r301 '/storage-system/data-assurance/' , '/platform/element-os/data-assurance'
      r301 '/storage-system/elementos8/' , '/platform/element-os'
      r301 '/flashforward' , '/flash-forward'
      r301 '/flashforward/' , '/flash-forward'
      r301 '/scale-out' , '/platform/element-os/scale-out'
      r301 '/storage-system/flash-forward' , '/flash-forward'
      r301 '/storage-system/global-efficiencies' , '/platform/element-os/global-efficiencies'
      r301 '/storage-system/global-efficiencies/' , '/platform/element-os/global-efficiencies'
      r301 '/storage-system/guaranteed-performance' , '/platform/element-os/guaranteed-performance'
      r301 '/storage-system/guaranteed-performance/' , '/platform/element-os/guaranteed-performance'
      r301 '/storage-system/high-availability' , '/platform/element-os/data-assurance'
      r301 '/storage-system/high-availability/' , '/platform/element-os/data-assurance'
      r301 '/storage-system/in-line-efficiency' , '/platform/element-os/global-efficiencies'
      r301 '/storage-system/in-line-efficiency/' , '/platform/element-os/global-efficiencies'
      r301 '/storage-system/product-specifications' , '/platform/product-specifications'
      r301 '/storage-system/product-specifications/' , '/platform/product-specifications'
      r301 '/storage-system/qos-benchmark' , '/platform/element-os/guaranteed-performance'
      r301 '/storage-system/qos-benchmark/' , '/platform/element-os/guaranteed-performance'
      r301 '/storage-system/scale-out' , '/platform/element-os/scale-out'
      r301 '/storage-system/scale-out/' , '/platform/element-os/scale-out'
      r301 '/support' , '/platform/support'
      r301 '/support/' , '/platform/support'
      r301 '/vmware-storage-infrastructure/' , '/solutions/vmware-solutions'
      r301 '/webinar-archive/' , '/resources/content-type/webinars'
      r301 '/webinar/' , '/about/webinars'
      r301 '/what-is-the-next-generation-data-center' , '/lp/what-is-the-next-generation-data-center'
      r301 '/converged-infrastructure/' , '/'
      r301 '/flash-array/' , '/'
      r301 '/products/' , '/platform'
      r301 '/technology/' , '/platform'
      r301 '/about/webinars/' , '/resources/content-type/webinars'
      r301 '/about/' , '/about/overview'
      r301 '/careers' , '/about/careers'
      r301 '/careers/' , '/about/careers'
      r301 '/gartner' , '/lp/gmq-2015'
      r301 '/about/events' , '/events'
      r301 '/about/management' , '/about/overview#management'
      r301 '/about/management/' , '/about/overview#management'
      r301 '/resources/the-value-of-the-next-generation-data-center', '/resources/value-of-the-next-generation-data-center'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/crucial/',	'/solutions/case-studies/crucial'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/databarracks/',	'/solutions/case-studies/databarracks'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/datapipe/',	'/solutions/case-studies/datapipe'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/elastx/',	'/solutions/case-studies/elastx'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/hosted-network/',	'/solutions/case-studies/hosted_network'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/iweb/',	'/solutions/case-studies/iweb'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/1and1com/',	'/solutions/case-studies/one_and_one'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/servint/',	'/solutions/case-studies/servint'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/sungard/',	'/solutions/case-studies/sungard'
      r301 '/how-to-buy/comparing-flash-storage-platforms',	'/lp/comparing-flash-storage-platforms'
      r301 '/comparing-flash-storage-platforms',	'/lp/comparing-flash-storage-platforms'
      r301 '/what-is-the-next-generation-data-center',	'/lp/what-is-the-next-generation-data-center'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/calligo/',	'/resources/case-study-calligo'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/colt/',	'/resources/case-study-colt'
      r301 '/webinar/',	'/resources/content-type/webinars'
      r301 '/about/webinars/',	'/resources/content-type/webinars'
      r301 '/about/webinars',	'/resources/content-type/webinars'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/clearview/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/codero-hosting/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/contegix/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/gigas/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/internap/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/lunacloud/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/onramp/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/peer-1/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/quovadis/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/tagadab/',	'/solutions/service-providers'
      r301 '/solutions/service-providers/fueled-by-solidfire/customers/webhuset/',	'/solutions/service-providers'
      r301 '/competitivecomparison/',	'/solutions/competitive-comparison'
      r301 '/competitivecomparison',	'/solutions/competitive-comparison'
      r301 '/about/channel-partners/resources/',	'/about/channel-partners'
      r301 '/master-terms-agreement/',	'http://info.solidfire.com/rs/538-SKP-058/images/flash_forward_guarantee_terms_and_conditions.pdf'
      r301 '/resources/5-ways-your-storage-vendor-is-costing-you-more-than-you-think',	'/resources'
      r301 '/about/board',	'/about/overview'
      r301 '/about/board/',	'/about/overview'
      r301 '/about/investors',	'/about/overview'
      r301 '/about/investors/',	'/about/overview'
      r301 '/sitemap', '/'
      r301 '/developers', 'http://developer.solidfire.com'
      r301 '/serviceproviders', '/solutions/service-providers'
      r301 '/lp-static/five-ways-your-storage-vendor-is-costing-you-more-than-you-think', '/resources'
      r301 '/platform/support/setup-guide', 'http://info.solidfire.com/rs/538-SKP-058/images/SolidFire-Element-Setup-Guide.pdf'
      r301 '/contact/ty-support', '/lp/support-thank-you'
      r301 '/elementos', '/platform/element-os'
      r301 '/openstack', '/solutions/cloud-orchestration/openstack'
      r301 '/support/setup-guide', 'http://info.solidfire.com/rs/538-SKP-058/images/SolidFire_Element_Setup_Guide.pdf'
      r301 '/vdi', '/solutions/vdi'
      r301 '/cloudbuilders', '/about/channel-partners'
      r301 '/rsvp', '/'
      r301 '/solutions/cloud-orchestration/onapp', '/solutions/cloud-orchestration'
      r301 '/solutions/cloud-orchestration/onapp/', '/solutions/cloud-orchestration'
      r301 '/solutions/cloud-orchestration/flexiant', '/solutions/cloud-orchestration'
      r301 '/solutions/cloud-orchestration/flexiant/', '/solutions/cloud-orchestration'
      r301 '/storage-system/competitive-comparisons/', '/solutions/competitive-comparison'
      r301 '/greatfeats/gmq', '/'
      r301 '/greatfeats/competitive-comparison', '/'
      r301 '/greatfeats/ezverify-case-study', '/'
      r301 '/what-is-the-next-generation-data-center/', '/lp/what-is-the-next-generation-data-center'
      r301 '/about/fueled-by-solidfire/customers/viawest/', '/solutions/service-providers'
      r301 '/about/fueled-by-solidfire/customers/brinkster/', '/solutions/service-providers'
      r301 '/about/fueled-by-solidfire/customers/cloudsigma/', '/solutions/service-providers'
      r301 '/about/fueled-by-solidfire/customers/getcloudservices/', '/solutions/service-providers'
      r301 '/about/fueled-by-solidfire/customers/datapipe/', '/solutions/service-providers'
      r301 '/master-terms-agreement', 'http://info.solidfire.com/rs/538-SKP-058/images/MTA.pdf'
      r301 '/eula', 'http://info.solidfire.com/rs/538-SKP-058/images/EULA-SolidFire-End-User-License-Agreement.pdf'
      r301 '/blog-feed', '/blog/feed'
      r301 '/about/press-releases/', '/press-releases'
      r301 '/fueled-by-solidfire', '/solutions/success-stories#fueled'
      r301 '/solutions/vmware/', '/solutions/vmware-solutions'
      r301 '/resources/solidfire-and-netapp-all-flash-fas-architectural-comparison', '/solutions/competitive-comparison'
      r301 '/resources/solidfire-sla-best-practices-quality-of-service-(qos)-considerations/', '/resources/solidfire-definitive-guide-guaranteeing-storage-performance'
      r301 '/resources/reference-architecture-citrix-cloudplatform-3-dot-0-5-and-solidfire-sf3010', '/resources'
      r301 '/spbenchmarkreport', '/resources/2015-service-provider-storage-services-benchmark-report'
      r301 '/master-terms-agreement/', 'master-terms-agreement'

      # Wildcards
      r301 %r{^(/resources/[\?])}i, '/resources'
      r301 %r{^(/about/events.)}i, '/events'
      r301 %r{^(/converged-infrastructure/.)}i, '/'
      r301 %r{^(/flash-array/.)}i, '/'
      r301 %r{^(/about//events/.)}i, '/events'
      r301 %r{^(/products/.)}i, '/platform'
      r301 %r{^(/technology/.)}i, '/platform'
      r301 %r{^(/about/webinars/.)}i, '/resources/content-type/webinars'
      r301 %r{^(/about/newslette.)}i, '/'
      r301 %r{^(/about/job.)}i, '/about/careers/jobs'
      r301 %r{^(/news.)}i, '/news'
      r301 %r{^(/about/news.)}i, '/news'
      r301 %r{^(/search-result.)}i, '/'
      r301 %r{^(/about/channel-partners/deal-registratio.)}i, 'https://solidfirepartners.force.com/CloudBuilders/PortalLogin'

      r301 %r{^(/storage-system/global-efficiencies/.)}i, '/platform/element-os/global-efficiencies'
      r301 %r{^(/storage-system/guaranteed-performance/.)}i, '/platform/element-os/guaranteed-performance'
      r301 %r{^(/storage-system/automated-management/.)}i, '/platform/element-os/automated-management'
      r301 %r{^(/storage-system/in-line-efficiency/.)}i, '/platform/element-os/global-efficiencies'
      r301 %r{^(/storage-system/qos-benchmark/.)}i, '/platform/element-os/guaranteed-performance'
      r301 %r{^(/what-is-the-next-generation-data-center.)}i, '/lp/what-is-the-next-generation-data-center'
      r301 %r{^(/demand-great/.)}i, '/lp/what-is-the-next-generation-data-center'
      r301 %r{^(/nodes/.)}i, '/platform/hardware'
      r301 %r{^(/storage-system/.)}i, '/platform'

      #r301 '/resources/?type=Resource-Whitepaper&tags=Horizon+View',   '/resources'
      #r301 '/resources/?type=Resource-Document&tags=XenDesktop', '/resources'

      # custom 404 page
    #require Rails.root.join("lib/custom_public_exceptions")
    #config.exceptions_app = CustomPublicExceptions.new(Rails.public_path)
    end
  end
end

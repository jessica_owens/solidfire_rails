Rails.application.routes.draw do

  # route the site root to home index
  root :to => 'home#index'

  match '/', :to => "home#index", :as => :home_index, :via => :get
  match '/home', :to => "home#home", :as => :home, :via => :get
  match '/index', :to => "home#index", :as => :home_page_index, :via => :get
  match '/modal-test', :to => "home#modal_test", :as => :modal_test, :via => :get

  match '/home-new', :to => "home#home_new", :as => :home_new, :via => :get

  match '/search/:q', :to => "search#index", :as => :search, :via => :get
  match '/search-term/:q/:num', :to => "search#search_term", :as => :search_term, :via => :get

  match '/video', :to => "home#video", :as => :video, :via => :get

  match '/video-take-over', :to => "video#index", :as => :video_take_over, :via => :get

  match '/resources', :to => "resources#index", :as => :resources, :via => :get
  match '/resources/tag/:tag', :to => "resources#load_tags", :as => :load_tags, :via => :get
  match '/resources/:id', :to => "resources#show_resource", :as => :show_resource, :via => :get

  match '/resources/cat/:category', :to => "resources#load_cats", :as => :load_cats, :via => :get
  match '/resources/all/:all', :to => "resources#all_resources", :as => :all_resources, :via => :get
  match '/resources/category/:category', :to => "resources#category_result", :as => :category_result, :via => :get
  match '/resources/content-type/:id', :to => "resources#type_result", :as => :type_result, :via => :get

  match '/about/customers', :to => "company#customers", :as => :customers, :via => :get

  match '/about/overview', :to => "company#overview", :as => :overview, :via => :get

  match '/about/analyst-reviews', :to => "company#analyst_reviews", :as => :analyst_reviews, :via => :get

  #match '/contact', :to => "company#contact", :as => :contact, :via => :get

  #match '/support', :to => "company#support", :as => :support, :via => :get

  match '/about/contact', :to => "company#contact", :as => :contact, :via => :get

  match '/about/technology-partners', :to => "company#technology_partners", :as => :technology_partners, :via => :get

  match '/events', :to => "company#events", :as => :events, :via => :get

  match '/elementx', :to => "solutions#elementx", :as => :elementx, :via => :get

  match '/infrastructure-continuum', :to => "solutions#infrastructure_continuum", :as => :infrastructure_continuum, :via => :get

  match '/developers', :to => "developers#index", :as => :developers_index, :via => :get

  match '/gmq-lp', :to => "landing_pages#gmq", :as => :gmq, :via => :get

  match '/gmq-lp-b', :to => "landing_pages#gmq_lp_b", :as => :gmq_b, :via => :get

  match '/lp-ebook-getting-it-right-openstack-private-cloud-storage', :to => "landing_pages#openstack_getting_it_right", :as => :openstack_getting_it_right, :via => :get

  match '/lp-storage-mrd-template', :to => "landing_pages#storage_mrd_template", :as => :storage_mrd_template, :via => :get

  match '/esg', :to => "landing_pages#esg", :as => :esg, :via => :get

  match '/openstack-summit-vancouver', :to => "landing_pages#openstack_summit_vancouver", :as => :openstack_summit_vancouver, :via => :get

  match '/404', :to => "errors#error_404", :as => :error_404, :via => :all

  match '/flash-array-guides/:id', :to => "flash_array_guides#show_guide", :as => :organic_landing_page, :via => :get

  match '/news', :to => "news#index", :as => :news, :via => :get

  match '/press-releases/', :to => "press_release#index", :as => :pr_index, :via => :get

  match '/press-releases/:id', :to => "press_release#show_pr", :as => :show_pr, :via => :get

  match '/analyst-day-2016', :to => "landing_pages#analyst_day_2016", :as => :analyst_day_2016, :via => :get
  match '/lp-static/cisco-live-berlin', :to => "landing_pages#cisco_live_berlin", :as => :cisco_live_berlin, :via => :get

  match '/greatfeats', :to => "great_feats#index", :as => :great_feats, :via => :get
  match '/greatfeats/:id', :to => "great_feats#show_great_feats", :as => :show_great_feat, :via => :get
  match '/greatfeats/thank-you/:id', :to => "great_feats#thank_you", :as => :great_feats_thank_you, :via => :get
  match '/greatfeats/form/:id', :to => "great_feats#form", :as => :great_feats_form, :via => :get
  match '/greatfeats/form/:id/:type', :to => "great_feats#form", :as => :great_feats_form_mobile, :via => :get

  match '/lp-static/five-ways-your-storage-vendor-is-costing-you-more-than-you-think', :to => "landing_pages#five_ways_your_storage_vendor_is_costing_you_more_than_you_think", :as => :five_ways_your_storage_vendor_is_costing_you_more_than_you_think, :via => :get

  match '/lp-static/cisco-live', :to => "landing_pages#cisco_live", :as => :cisco_live, :via => :get
  match '/lp-static/idc', :to => "landing_pages#idc", :as => :idc, :via => :get
  match '/lp-static/comparing-flash-storage-platforms', :to => "landing_pages#comparing_flash_storage_platforms", :as => :architectural_comparisons, :via => :get
  match '/lp-static/sp-benchmark', :to => "landing_pages#sp_benchmark", :as => :sp_benchmark, :via => :get
  match '/lp-static/sp-benchmark-german', :to => "landing_pages#sp_benchmark_german", :as => :sp_benchmark_german, :via => :get
  match '/lp-static/sp-benchmark-fr', :to => "landing_pages#sp_benchmark_french", :as => :sp_benchmark_french, :via => :get

  match '/lp-static/dell-training-lp', :to => "landing_pages#denver_metro_solidfire_sales_training_dell", :as => :denver_metro_solidfire_sales_training_dell, :via => :get

  match '/lp/unstoppable', :to => "landing_pages#unstoppable", :as => :unstoppable, :via => :get

  get '/may-the-flash-be-with-you', to: redirect('/')
  get '/may-the-flash-be-with-you-cambridge-computer', to: redirect('/')

  match '/may-the-flash-be-with-you-field', :to => "landing_pages#may_the_flash_be_with_you_field", :as => :flash_be_with_you_field, :via => :get

  match '/lp/:id', :to => "lp#index", :as => :custom_landing, :via => :get

  match '/netapp-sales', :to => "lp#index", :as => :netapp_sales, :via => :get, :id => 'netapp-sales'

  match '/flash-forward', :to => "storage_system#flash_forward", :as => :flash_forward, :via => :get

  match '/gmq-video', :to => "video#index", :as => :gmq_video, :via => :get

  match '/elementos', :to => "landing_pages#element_os_8", :as => :element_os_8, :via => :get


  match '/about/channel-partners', :to => "company#channel_partners", :as => :channel_partners, :via => :get

  match '/about/careers', :to => "company#careers", :as => :careers, :via => :get
  match '/about/careers/team/:id', :to => "company#team", :as => :team, :via => :get
  match '/about/careers/jobs', :to => "company#jobs", :as => :jobs, :via => :get

  match '/style-guide', :to => "style_guide#index", :as => :style_guide, :via => :get

  match '/about/request-access-form', :to => "company#request_access_form", :as => :request_access_form, :via => :get

  match '/solutions', :to => "solutions#solutions_overview", :as => :solutions_overview, :via => :get
  match '/solutions/cloud-orchestration', :to => "solutions#cloud_orchestration", :as => :cloud_orchestration, :via => :get
  match '/solutions/cloud-orchestration/openstack', :to => "solutions#openstack", :as => :openstack, :via => :get
  match '/solutions/cloud-orchestration/cloudstack', :to => "solutions#cloudstack", :as => :cloudstack, :via => :get
  match '/solutions/cloud-orchestration/flexiant', :to => "solutions#flexiant", :as => :flexiant, :via => :get
  match '/solutions/cloud-orchestration/onapp', :to => "solutions#onapp", :as => :onapp, :via => :get
  match '/solutions/database', :to => "solutions#database", :as => :database, :via => :get
  match '/solutions/database/oracle', :to => "solutions#oracle", :as => :oracle, :via => :get
  match '/solutions/database/mysql', :to => "solutions#mysql", :as => :mysql, :via => :get
  match '/solutions/database/mssql', :to => "solutions#mssql", :as => :mssql, :via => :get
  match '/solutions/database/mongodb', :to => "solutions#mongodb", :as => :mongodb, :via => :get
  match '/solutions/vdi', :to => "solutions#vdi", :as => :vdi, :via => :get
  match '/solutions/vdi/citrix-xendesktop', :to => "solutions#citrix_xen_desktop", :as => :citrix_xen_desktop, :via => :get
  match '/solutions/vdi/vmware-horizon-view', :to => "solutions#vmware_horizon_view", :as => :vmware_horizon_view, :via => :get
  match '/solutions/agile-infrastructure/openstack', :to => "solutions#openstack_infrastructure", :as => :openstack_infrastructure, :via => :get
  match '/solutions/vmware-solutions/', :to => "solutions#vmware_solutions", :as => :vmware_solutions, :via => :get
  match '/solutions/competitive-comparison', :to => "solutions#competitive_comparison", :as => :competitive_comparison, :via => :get
  match '/solutions/data-protection', :to => "solutions#data_protection", :as => :data_protection, :via => :get
  match '/solutions/service-providers', :to => "solutions#service_providers", :as => :service_providers, :via => :get
  match '/solutions/success-stories', :to => "solutions#success_stories", :as => :success_stories, :via => :get
  match '/solutions/service-providers/managed-dedicated-san/', :to => "solutions#managed_dedicated_san", :as => :managed_dedicated_san, :via => :get
  match '/solutions/service-providers/infrastructure-as-a-service/', :to => "solutions#infrastructure_as_a_service", :as => :infrastructure_as_a_service, :via => :get
  match '/solutions/service-providers/desktop-as-a-service/', :to => "solutions#desktop_as_a_service", :as => :desktop_as_a_service , :via => :get
  match '/solutions/service-providers/database-as-a-service/', :to => "solutions#database_as_a_service", :as => :database_as_a_service , :via => :get
  match '/solutions/case-studies/:id', :to => "solutions#case_studies", :as => :case_studies , :via => :get
  match '/solutions/agile-infrastructure', :to => "solutions#agile_infrastructure", :as => :agile_infrastructure, :via => :get
  match '/solutions/agile-infrastructure/virtual-infrastructure', :to => "solutions#virtual_infrastructure", :as => :virtual_infrastructure, :via => :get

  match '/vmworld', :to => "landing_pages#vmworld", :as => :vmworld, :via => :get
  match '/vmworldus', :to => "landing_pages#vmworldus", :as => :vmworldus, :via => :get
  match '/vmworld-vip', :to => "landing_pages#vmworld_vip", :as => :vmworld_vip, :via => :get

  match '/why-solidfire', :to => "why_solidfire#index", :as => :why_solidfire, :via => :get

  match '/platform', :to => "platform#platform_overview", :as => :platform_overview, :via => :get
  match '/platform/element-os', :to => "platform#elements_os", :as => :elements_os, :via => :get
  match '/platform/hardware', :to => "platform#hardware", :as => :hardware, :via => :get
  match '/platform/product-specifications', :to => "platform#product_specifications", :as => :product_specifications, :via => :get
  match '/platform/resources', :to => "platform#resources", :as => :platform_resources, :via => :get
  match '/platform/support', :to => "platform#support", :as => :support, :via => :get
  match '/platform/element-os/scale-out', :to => "platform#scale_out", :as => :scale_out, :via => :get
  match '/platform/element-os/guaranteed-performance', :to => "platform#guaranteed_performance", :as => :guaranteed_performance, :via => :get
  match '/platform/element-os/automated-management', :to => "platform#automated_management", :as => :automated_management, :via => :get
  match '/platform/element-os/data-assurance', :to => "platform#data_assurance", :as => :data_assurance, :via => :get
  match '/platform/element-os/global-efficiencies', :to => "platform#global_efficiencies", :as => :global_efficiencies, :via => :get
  match '/platform/element-os/resources', :to => "platform#element_os_resources", :as => :element_os_resources, :via => :get

  match '/legal', :to => "legal#index", :as => :legal, :via => :get
  match '/legal/privacy', :to => "legal#privacy", :as => :privacy, :via => :get
  match '/legal/cookies', :to => "legal#cookies", :as => :cookies, :via => :get

  match '/how-to-buy', :to => "how_to_buy#index", :as => :how_to_buy, :via => :get

  match '/dm/:id', :to => "landing_pages#dm", :as => :direct_mailing, :via => :get

  match '/openstacksummit', :to => "landing_pages#openstack_tokyo", :as => :openstack_tokyo, :via => :get

  # backend admin section
  namespace :cms do
    match '/', :to => "home#index", :as => :home_index, :via => :get
    resources :resources
    resources :tags
    resources :users
    resources :password_resets, only: [:new, :create, :edit, :update]
    resources :categories
    resources :resource_images
    resources :organic_landings
    resources :news
    resources :press_releases
    resources :landing_pages
    resources :layout_templates
    resources :events
    resources :project_requests
    resources :project_request_statuses
    resources :project_request_types
    resources :project_request_items
    resources :sections
    match '/login', :to => "login#index", :as => :login_index, :via => :get
    match '/logout', :to => "login#logout", :as => :logout, :via => :get
    match '/check-login', :to => "login#check_login", :as => :login_check, :via => :post
    match '/all-resource-images', :to => "resources#all_resource_images", :as => :all_resource_images, :via => :get
    match '/resources-order', :to => "resource_order#index", :as => :resource_order_index, :via => :get
    match '/resource-order-update/id/:id/order/:order', :to => "resource_order#update_order", :as => :update_order, :via => :get
    match '/project-request/archive/:id', :to => "project_requests#archive", :as => :project_request_archive, :via => :get
    match '/project-request/unarchive/:id', :to => "project_requests#unarchive", :as => :project_request_unarchive, :via => :get
    match '/project-request/all-unarchive', :to => "project_requests#all_unarchive", :as => :project_request_all_unarchive, :via => :get
    match '/project-request/all-archive', :to => "project_requests#all_archive", :as => :project_request_all_archive, :via => :get
    match '/landing-pages/copy-document/:id', :to => "landing_pages#copy_document", :as => :copy_document, :via => :post
    match '/project-request-item/item/:id', :to => "project_request_items#load_items", :as => :load_items, :via => :get
  end

  # Marketing API Routes
  namespace :api, :path => 'api' do
    namespace :v1 do
      match '/customer-tracking', :to => "customer_tracking#set_sfmktops", :as => :set_sfmktops, :via => :get
    end
  end

  # redirects
  get '/get-started', to: redirect("/how-to-buy", status: 301)
  #get '/resources/?a*all', to: redirect('/resources', status: 301)

  get '/dellworld', to: redirect('http://www.solidfire.com/lp/solidfire-at-dell-world-2015', status: 301)

  #get '/resources/?a*all', to: redirect('/resources', status: 301)
  #get '/resources/?type=Resource-Whitepaper&tags=Horizon+View', to: redirect('/resources', status: 301)

  # catch all pages not found
  get "*any", via: :all, :to => "errors#error_404", :as => :error_404_all

end

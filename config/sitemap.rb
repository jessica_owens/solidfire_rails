require 'rubygems'
require 'sitemap_generator'

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'http://www.solidfire.com'

SitemapGenerator::Sitemap.public_path = 'public/sitemaps/'

SitemapGenerator::Sitemap.create do

  add resources_path
  add overview_path
  add analyst_reviews_path
  add contact_path
  add technology_partners_path
  add events_path
  add customers_path
  add elementx_path
  add infrastructure_continuum_path
  add news_path
  add pr_index_path
  add element_os_8_path
  add channel_partners_path
  add careers_path
  add solutions_overview_path
  add cloud_orchestration_path
  add openstack_path
  add cloudstack_path
  add flexiant_path
  add onapp_path
  add database_path
  add oracle_path
  add mysql_path
  add mssql_path
  add mongodb_path
  add vdi_path
  add citrix_xen_desktop_path
  add vmware_horizon_view_path
  add agile_infrastructure_path
  add virtual_infrastructure_path
  add openstack_infrastructure_path
  add vmware_solutions_path
  add data_protection_path
  add service_providers_path
  add success_stories_path
  add managed_dedicated_san_path
  add infrastructure_as_a_service_path
  add desktop_as_a_service_path
  add database_as_a_service_path
  add why_solidfire_path
  add platform_overview_path
  add elements_os_path
  add hardware_path
  add product_specifications_path
  add platform_resources_path
  add support_path
  add scale_out_path
  add guaranteed_performance_path
  add automated_management_path
  add data_assurance_path
  add global_efficiencies_path
  add element_os_resources_path
  add legal_path
  add privacy_path
  add cookies_path
  add how_to_buy_path

  Cms::Resource.all.each do |res|
    add show_resource_path(res.slug)
  end

  Cms::PressRelease.all.each do |pr|
    add show_pr_path(pr.slug)
  end

  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end

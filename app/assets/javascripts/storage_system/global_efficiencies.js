//= require jquery

$(function () {
  var selects, workloads, compressions, thinProvisionings, percents, numberOfVMs, divergences, workloadEfficiencySelectors, deduplicationSelectors, tooltips, tooltipBoxes, emptyPercentOfSystemInputIndexes;

  emptyPercentOfSystemInputIndexes = [];

  workloads = [{percentOfSystem: 0, numberOfVMs: 0, divergence: 0, compression: 0, thinProvisioning: 0},
               {percentOfSystem: 0, numberOfVMs: 0, divergence: 0, compression: 0, thinProvisioning: 0},
               {percentOfSystem: 0, numberOfVMs: 0, divergence: 0, compression: 0, thinProvisioning: 0}];

  selects = [$('#workload-one'), $('#workload-two'), $('#workload-three')];
  compressions = [$('#compression .workload-one'), $('#compression .workload-two'), $('#compression .workload-three')];
  thinProvisionings = [$('#thin-provisioning .workload-one'), $('#thin-provisioning .workload-two'), $('#thin-provisioning .workload-three')];
  percents = [$('#percent-of-system .workload-one input'), $('#percent-of-system .workload-two input'), $('#percent-of-system .workload-three input')];
  numberOfVMs = [$('#number-of-vms .workload-one input'), $('#number-of-vms .workload-two input'), $('#number-of-vms .workload-three input')];
  divergences = [$('#divergence .workload-one input'), $('#divergence .workload-two input'), $('#divergence .workload-three input')];
  workloadEfficiencySelectors = [$('#workload-efficiency .workload-one'), $('#workload-efficiency .workload-two'), $('#workload-efficiency .workload-three')];
  deduplicationSelectors = [$('#deduplication .workload-one'), $('#deduplication .workload-two'), $('#deduplication .workload-three')];
  tooltips = {0: {percentOfSystem: $('#percent-of-system .workload-one .tooltip'), numberOfVMs: $('#number-of-vms .workload-one .tooltip'), divergence: $('#divergence .workload-one .tooltip')},
              1: {percentOfSystem: $('#percent-of-system .workload-two .tooltip'), numberOfVMs: $('#number-of-vms .workload-two .tooltip'), divergence: $('#divergence .workload-two .tooltip')},
              2: {percentOfSystem: $('#percent-of-system .workload-three .tooltip'), numberOfVMs: $('#number-of-vms .workload-three .tooltip'), divergence: $('#divergence .workload-three .tooltip')}};
  tooltipBoxes = [{percentOfSystem: $('#percent-of-system .workload-one .tooltip-box'), numberOfVMs: $('#number-of-vms .workload-one .tooltip-box'), divergence: $('#divergence .workload-one .tooltip-box')},
                  {percentOfSystem: $('#percent-of-system .workload-two .tooltip-box'), numberOfVMs: $('#number-of-vms .workload-two .tooltip-box'), divergence: $('#divergence .workload-two .tooltip-box')},
                  {percentOfSystem: $('#percent-of-system .workload-three .tooltip-box'), numberOfVMs: $('#number-of-vms .workload-three .tooltip-box'), divergence: $('#divergence .workload-three .tooltip-box')}];

  $('.refresh').on('click', function () {
    selects[0].val('N/A');
    selects[1].val('N/A');
    selects[2].val('N/A');
    changeForm(0);
    changeForm(1);
    changeForm(2);

    $('#total-efficiency td.total').text('');
  });

  tooltips[0]['percentOfSystem'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[0]['percentOfSystem'];
    toggleInfoBox($this, box);
  });

  tooltips[0]['numberOfVMs'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[0]['numberOfVMs'];
    toggleInfoBox($this, box);
  });

  tooltips[0]['divergence'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[0]['divergence'];
    toggleInfoBox($this, box);
  });

  tooltips[1]['percentOfSystem'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[1]['percentOfSystem'];
    toggleInfoBox($this, box);
  });

  tooltips[1]['numberOfVMs'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[1]['numberOfVMs'];
    toggleInfoBox($this, box);
  });

  tooltips[1]['divergence'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[1]['divergence'];
    toggleInfoBox($this, box);
  });

  tooltips[2]['percentOfSystem'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[2]['percentOfSystem'];
    toggleInfoBox($this, box);
  });

  tooltips[2]['numberOfVMs'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[2]['numberOfVMs'];
    toggleInfoBox($this, box);
  });

  tooltips[2]['divergence'].hover(function () {
    var $this, box ;
    $this = $(this);
    box = tooltipBoxes[2]['divergence'];
    toggleInfoBox($this, box);
  });

  selects[0].change(function(){
    changeForm(0);
  });


  selects[1].change(function(){
   changeForm(1);
  });

  selects[2].change(function(){
    changeForm(2);
  });

  percents[0].on('input', function(){
    changePercents(0);
  });

  percents[1].on('input', function(){
    changePercents(1);
  });

  percents[2].on('input', function(){
   changePercents(2);
  });

  numberOfVMs[0].on('input', function() {
    var val = parseInt($('#number-of-vms .workload-one input').val());
    workloads[0]['numberOfVMs'] = val || 0;
    runCalculatorFor(0)
  });

  numberOfVMs[1].on('input', function() {
    var val = parseInt($('#number-of-vms .workload-two input').val());
    workloads[1]['numberOfVMs'] = val || 0;
    runCalculatorFor(1)
  });

  numberOfVMs[2].on('input', function() {
    var val = parseInt($('#number-of-vms .workload-three input').val());
    workloads[2]['numberOfVMs'] = val || 0;
    runCalculatorFor(2)
  });

  divergences[0].on('input', function() {
    var divergence = parseInt($('#divergence .workload-one input').val()) / 100;
    workloads[0]['divergence'] = divergence || 0;
    runCalculatorFor(0)
  });

  divergences[1].on('input', function() {
    var divergence = parseInt($('#divergence .workload-two input').val()) / 100;
    workloads[1]['divergence'] = divergence || 0;
    runCalculatorFor(1)
  });

  divergences[2].on('input', function() {
    var divergence = parseInt($('#divergence .workload-three input').val()) / 100;
    workloads[2]['divergence'] = divergence || 0;
    runCalculatorFor(2)
  });

  function changeForm(index) {
    var type;

    type = selects[index].val();

    toggleTooltipsFor(index, type);
    runCalculatorForSelectType(type, index);

    if ($.inArray(index, emptyPercentOfSystemInputIndexes) === -1) {
      emptyPercentOfSystemInputIndexes.push(index);
    }

    if(type == 'N/A') {
      removePercentIndex(index);
      clearHints(index);
      appendPercentOfSystemHints();
    } else {
      appendToolTipText(type, index);
      appendPercentOfSystemHints();
      appendNumberOfVMsHint(type, index);
      appendDivergencesHint(type, index);
    }
  };

  function toggleTooltipsFor(index, type) {
    var percentOfSystemTooltip, numberOfVMsTooltip, divergenceTooltip;

    percentOfSystemTooltip = tooltips[index]['percentOfSystem'];
    numberOfVMsTooltip = tooltips[index]['numberOfVMs'];
    divergenceTooltip = tooltips[index]['divergence'];

    toggleToolTip(percentOfSystemTooltip, type);
    toggleToolTip(numberOfVMsTooltip, type);
    toggleToolTip(divergenceTooltip, type);
  }

  function changePercents(number) {
    var percent = percents[number].val();
    workloads[number]['percentOfSystem'] = percent || 0;
    computeTotals(workloads);

    if(percent > 0) {
      removePercentIndex(number);
    } else {
      addPercentIndex(number)
    };

    appendPercentOfSystemHints();
  };

  function removePercentIndex(number) {
    var index = emptyPercentOfSystemInputIndexes.indexOf(number);

    if (index > -1) {
      emptyPercentOfSystemInputIndexes.splice(index, 1);
    };
  };

  function addPercentIndex(number) {
    emptyPercentOfSystemInputIndexes.push(number)
  };

  function toggleInfoBox($this, box) {
    if (box.hasClass('hidden')) {
      box.removeClass('hidden');
    } else {
      box.addClass('hidden');
    };
  };

  function toggleToolTip(tooltip, type) {
    if (tooltip.hasClass('hidden') && type != 'N/A') {
      tooltip.removeClass('hidden')
    } else if (!tooltip.hasClass('hidden') && type == 'N/A') {
      tooltip.addClass('hidden')
    }
  };

  function appendToolTipText(selectType, index) {
    if(selectType == 'Database') {
      tooltipBoxes[index]['percentOfSystem'].text('Enter the percentage of storage system capacity used for databases');
      tooltipBoxes[index]['numberOfVMs'].text('Enter the total number of databases');
      tooltipBoxes[index]['divergence'].text('Typical 30 to 50%');
    } else if (selectType == 'VDI') {
      tooltipBoxes[index]['percentOfSystem'].text('Enter the percentage of storage system capacity used for virtual desktops');
      tooltipBoxes[index]['numberOfVMs'].text('Enter the total number of virtual desktops');
      tooltipBoxes[index]['divergence'].text('Typical 10 to 25%');
    } else if (selectType == 'Cloud') {
      tooltipBoxes[index]['percentOfSystem'].text('Enter the percentage of storage system capacity used for cloud instances');
      tooltipBoxes[index]['numberOfVMs'].text('Enter the total number of instances');
      tooltipBoxes[index]['divergence'].text('Typical 20 to 40%');
    } else if (selectType == 'Virtualization') {
      tooltipBoxes[index]['percentOfSystem'].text('Enter the percentage of storage system capacity used for virtual machines');
      tooltipBoxes[index]['numberOfVMs'].text('Enter the total number of virtual machines');
      tooltipBoxes[index]['divergence'].text('Typical 25 to 50%');
    } else {
      tooltipBoxes[index]['percentOfSystem'].text('');
      tooltipBoxes[index]['numberOfVMs'].text('');
      tooltipBoxes[index]['divergence'].text('');
    };
  };

  function appendPercentOfSystemHints() {
    var total, placeholderOne, placeholderTwo, placeholderThree, remainder;

    total = totalPercentOfSystem();

    if (emptyPercentOfSystemInputIndexes.length == 1 && total < 100) {
      remainder = (100 - totalPercentOfSystem());
      placeholderOne = "Example: " + parseInt(remainder) + "%";
      percents[emptyPercentOfSystemInputIndexes[0]].attr("placeholder", placeholderOne);
    } else if (emptyPercentOfSystemInputIndexes.length == 2 && total < 100) {
      remainder = (100 - totalPercentOfSystem());
      placeholderOne = placeholderTwo =  "Example: " + parseInt(remainder / 2) + "%";
      percents[emptyPercentOfSystemInputIndexes[0]].attr("placeholder", placeholderOne);
      percents[ emptyPercentOfSystemInputIndexes[1]].attr("placeholder", placeholderTwo);
    } else if (emptyPercentOfSystemInputIndexes.length == 3 && total < 100) {
      remainder = (100 - totalPercentOfSystem());
      placeholderOne = placeholderTwo = placeholderThree = "Example: " + parseInt(remainder / 3) + "%";
      percents[emptyPercentOfSystemInputIndexes[0]].attr("placeholder", placeholderOne);
      percents[emptyPercentOfSystemInputIndexes[1]].attr("placeholder", placeholderTwo);
      percents[emptyPercentOfSystemInputIndexes[2]].attr("placeholder", placeholderThree);
    } else if (total >= 100) {
      $.each(emptyPercentOfSystemInputIndexes, function (i, val) {
        percents[val].attr("placeholder", "Example: 0%");
      });
    };
  };

  function clearHints(index) {
    percents[index].attr("placeholder", "");
    numberOfVMs[index].attr("placeholder", "");
    divergences[index].attr("placeholder", "");
  };

  function appendNumberOfVMsHint (type, index) {
    if(type == "Database") {
      numberOfVMs[index].attr('placeholder', "# of databases");
    } else if (type == "VDI") {
      numberOfVMs[index].attr('placeholder', "# of virtual desktops");
    } else if (type == "Cloud") {
      numberOfVMs[index].attr('placeholder', "# of instances");
    } else if (type == "Virtualization") {
      numberOfVMs[index].attr('placeholder', "# of VMs");
    };
  };

  function appendDivergencesHint (type, index) {
    if(type == "Database") {
      divergences[index].attr('placeholder', "Typical 30 to 50%");
    } else if (type == "VDI") {
      divergences[index].attr('placeholder', "Typical 10 to 25%");
    } else if (type == "Cloud") {
      divergences[index].attr('placeholder', "Typical 20 to 40%");
    } else if (type == "Virtualization") {
      divergences[index].attr('placeholder', "Typical 25 to 50%");
    };
  };

  function runCalculatorFor (number) {
    calculateDeduplication(workloads[number], deduplicationSelectors[number]);
    calculateWorkloadEfficiency(workloads[number], workloadEfficiencySelectors[number]);
    calculateTotalEfficiency(workloads);
  };

  function totalPercentOfSystem () {
    return parseInt(workloads[0]['percentOfSystem']) + parseInt(workloads[1]['percentOfSystem']) + parseInt(workloads[2]['percentOfSystem']);
  };

  function computeTotals (workloads) {
    var total = totalPercentOfSystem();

    if(total > 100) {
      percents[0].css('border-color', '#da4e32');
      percents[1].css('border-color', '#da4e32');
      percents[2].css('border-color', '#da4e32');
      $('.error-message').css('visibility', 'visible');
    } else {
      percents[0].css('border-color', '');
      percents[1].css('border-color', '');
      percents[2].css('border-color', '');
      $('.error-message').css('visibility', 'hidden');
    };

    calculateTotalEfficiency (workloads);
  };

  function runCalculatorForSelectType(type, index) {
    if(type == 'N/A') {
      emptyAndDisableFields(index);
      calculateTotalEfficiency(workloads);
    } else {
      enableFields(percents[index], numberOfVMs[index], divergences[index]);
      changeFields(type, workloads[index], compressions[index], thinProvisionings[index]);
      calculateDeduplication (workloads[index], deduplicationSelectors[index]);
      calculateWorkloadEfficiency(workloads[index], workloadEfficiencySelectors[index]);
      calculateTotalEfficiency(workloads);
    };
  };

  function emptyAndDisableFields(number) {
    workloads[number]['compression'] = 0;
    workloads[number]['thinProvisioning'] = 0;
    workloads[number]['percentOfSystem'] = 0;
    workloads[number]['numberOfVMs'] = 0;
    workloads[number]['divergence'] = 0;

    percents[number].val('').prop('disabled', true).addClass('grey');
    numberOfVMs[number].val('').prop('disabled', true).addClass('grey');
    divergences[number].val('').prop('disabled', true).addClass('grey');
    compressions[number].val('');
    thinProvisionings[number].val('');
    deduplicationSelectors[number].val('');
    workloadEfficiencySelectors[number].text('');
  };
});


function enableFields (percent, numberOfVM, divergence){
  percent.prop('disabled', false).removeClass('grey');
  numberOfVM.prop('disabled', false).removeClass('grey');
  divergence.prop('disabled', false).removeClass('grey');
}

function changeFields(type, workload, compression, thinProvisioning) {
  var newComp, newThinProv;

  newComp = findCompression(type);
  newThinProv = findThinProvisioning(type);

  workload['compression'] = newComp;
  workload['thinProvisioning'] = newThinProv;

  compression.val(newComp);
  thinProvisioning.val(newThinProv);
}

function calculateWorkloadEfficiency (workload, selector) {
  var workloadEfficiency = new Calculator(workload).calculate('workloadEfficiency') || 0;
  selector.text(workloadEfficiency);
  workload['workloadEfficiency'] = workloadEfficiency;
}

function calculateDeduplication (workload, selector) {
  var deduplication = new Calculator(workload).calculate('deduplication') || 0;
  selector.val(deduplication);
}

function calculateTotalEfficiency (workloads) {
  var  total = 0;
  for(var i = 0; i < workloads.length; i++) {
    total += new Calculator(workloads[i]).calculate('totalWorkloadEfficiency') || 0
  }

  $('#total-efficiency td.total').text(parseFloat((total / 100).toFixed(2)));
}

function findCompression (type) {
  if(type == "N/A") {
    return;
  } else if (type == "VDI") {
    return 1.5
  } else if (type == "Virtualization") {
    return 2
  } else if (type == "Database") {
    return 1.7
  } else if (type == "Cloud") {
    return 1.8
  }
};

function findThinProvisioning (type) {
  if (type == "N/A") {
    return;
  } else {
    return 2;
  };
};

function Calculator (workload) {
  this.equations = {
    deduplication: this.deduplication(workload),
    workloadEfficiency: this.workloadEfficiency(workload),
    totalWorkloadEfficiency: workload['percentOfSystem'] * this.workloadEfficiency(workload)
  };
};

Calculator.prototype.deduplication = function deduplication (workload) {
  return workload['numberOfVMs'] / (1 + (workload['numberOfVMs'] - 1) * workload['divergence']);
};

Calculator.prototype.workloadEfficiency = function workloadEfficiency (workload) {
  return workload['compression'] * workload['thinProvisioning'] * this.deduplication(workload);
};

Calculator.prototype.calculate = function calculate (equation) {
  var num = this.equations[equation];
  var result = toFloat(num, 2);
  return result;
};

function toFloat (num, float) {
  return parseFloat(num.toFixed(float));
};

$(function () {
  var windowVar = $(window);
  var overviewId, managementId, overviewClass, managementClass;

  overviewId = $('#company-overview');
  managementId = $('#management');

  overviewClass = $('.overview-link');
  managementClass = $('.management-link');

  windowVar.scroll(function () {
    if (notHighlighted(overviewId) && isScrolledIntoView(overviewId)) {
      overviewClass.css('color', '#CC1F26').attr('highlighted', true);
      managementClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(managementId) && isScrolledIntoView(managementId)) {
      managementClass.css('color', '#CC1F26').attr('highlighted', true);
      overviewClass.css('color', '#A9A9A9').attr('highlighted', false);
    }
  });

  function notHighlighted(elem) {
    return elem.attr('highlighted') != 'true';
  }

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemTop >= docViewTop));
  }
});



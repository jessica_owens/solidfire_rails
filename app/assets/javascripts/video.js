//= require froogaloop2.min
$(function () {

    var player = $('#gmq-video')[0];
    $f(player).addEvent('ready', ready);

    function addEvent(element, eventName, callback) {
        if (element.addEventListener) {
            element.addEventListener(eventName, callback, false);
        }
        else {
            element.attachEvent(eventName, callback, false);
        }
    }

    function ready(player_id) {
        var froogaloop = $f(player_id);

        function onPlay() {
            froogaloop.addEvent('play', function(data) {

            });
        }

        function onFinish() {
            froogaloop.addEvent('finish', function(data) {
                $('.h_iframe').remove();
                $('.header__container, footer, .home-section-one, .home-section-two, .home-section-three, .home-section-four, .angle-section-0, .angle-section-1, .fade-in-three').fadeToggle(400);
                $( window ).resize();
            });
        }

        onPlay();
        onFinish();
    }

    $('.close-video').click(function() {
        $('.h_iframe').remove();
        $('.header__container, footer, .home-section-one, .home-section-two, .home-section-three, .home-section-four, .angle-section-0, .angle-section-1, .fade-in-three').fadeToggle(400);
        $( window ).resize();
    });

});
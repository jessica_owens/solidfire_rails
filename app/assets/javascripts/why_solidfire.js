$(function () {
    $('.content-module-thirds').click(function() {
        var getChildLink = $(this).find('a').attr('href');

        window.location.href = getChildLink;
    });

    $('#why-card-one, #why-card-two, #why-card-three, #why-card-four, #why-card-five, #why-card-six').flip({
        trigger: 'hover'
    });
});




$(function () {
  //highlight active link
  if(window.location.href.indexOf("rfa") > -1) {
    $('a.rfa').css('font-weight', '600')
  } else if (window.location.href.indexOf("one_and_one") > -1) {
    $('a.one_and_one').css('font-weight', '600')
  } else if (window.location.href.indexOf("datapipe") > -1) {
    $('a.datapipe').css('font-weight', '600')
  } else if (window.location.href.indexOf("sungard") > -1) {
    $('a.sungard').css('font-weight', '600')
  } else if (window.location.href.indexOf("servint") > -1) {
    $('a.servint').css('font-weight', '600')
  } else if (window.location.href.indexOf("elastx") > -1) {
    $('a.elastx').css('font-weight', '600')
  } else if (window.location.href.indexOf("hosted_network") > -1) {
    $('a.hosted_network').css('font-weight', '600')
  } else if (window.location.href.indexOf("crucial") > -1) {
    $('a.crucial').css('font-weight', '600')
  } else if (window.location.href.indexOf("iweb") > -1) {
    $('a.iweb').css('font-weight', '600')
  } else if (window.location.href.indexOf("databarracks") > -1) {
    $('a.databarracks').css('font-weight', '600')
  }
})
$(".contact-form").fancybox({
  maxWidth: 275,
  maxHeight: 620,
  fitToView: true,
  width: '70%',
  height: '78%',
  autoSize: false,
  closeClick: false,
  openEffect: 'none',
  closeEffect: 'none',
  padding: 20,
  scrolling: 'no'
});


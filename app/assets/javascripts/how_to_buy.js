$(".demo").fancybox({
    maxWidth: 330,
    maxHeight: 555,
    fitToView: true,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 0,
    scrolling: 'no'
});

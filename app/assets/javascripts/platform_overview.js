$(function(){
  var elementPosition = $(' .flex-skew-fix-right').offset();
  var hidden = true;

  $(window).scroll(function () {
    if ($( window ).width() < 752 ) {
      $('.sub-nav.platform.section.hidden').css('display', 'none');

    } else if ($(window).scrollTop() > elementPosition.top + 400 && hidden == true && $( window ).width() > 752){
      $('.sub-nav.platform.section.hidden').slideToggle();
      hidden = false;

    } else if ($(window).scrollTop() < elementPosition.top + 400 && hidden == false && $( window ).width() > 752) {
      $('.sub-nav.platform.section.hidden').slideToggle();
      hidden = true;
    }
  })
});

var console = console || {
    log: function () {
    }
};

$(function() {
    $('.mobile-menu').click(function() {
        $('body').toggleClass('show-mobile-menu');
        $('.mobile-menu-toggle').toggleClass('active');
    });

});


var Browser = {
    isMobile: (function () {
        var isMobile = false;
        try {
            isMobile = ('ontouchstart' in window);
        } catch (e) {
        }
        return isMobile;
    })()
};

var HoverText = {
    init: function () {
        $('.jsHoverText').hover(function () {
            var info = $($(this).data('field'));
            if (!info.data('original')) {
                info.data('original', info.html());
            }
            info.text($(this).data('text'));
        });

        $('.icons-wrap').mouseleave(function () {
            var info = $("#icons-info");
            info.html(info.data('original'));
        });
    }
};
$(function () {
    HoverText.init();
});

var Home = {
    init: function () {
        Home.balanceHeights();
        Home.fixMenuButtonOnMobile();
        Home.parallax();
    },

    fixMenuButtonOnMobile: function () {
        $('body').on('click', '.menu-trigger', function (e) {
            var self = $(e.currentTarget);
            var isActive = self.is('.active');
            if (isActive) {
                $('#backdrop').height(0);
            }
            $('#parentMenu .active').removeClass('active');
        });
    },

    /** Look for a row of balanceHeight and even out all child elements */
    balanceHeights: function () {
        $('.balanceHeight').each(function () {
            var items = $(this).children();
            var max = 0;
            items.each(function () {
                max = Math.max(max, $(this).innerHeight());
            });
            items.each(function () {
                $(this).css('margin-top', (max - $(this).height()) / 2 + 'px');
            });
        });
    },

    parallax: function () {
        if (Browser.isMobile) {
            $('#menu').css('position', 'relative');
        } else if (!Browser.isMobile) {
            $(window).scroll(function () {
                var isSticky = $(document).scrollTop() > 50;
                $('#menu').toggleClass('sticky', isSticky);
            });
        }
        $.stellar.positionProperty.custom = {
            setTop: function ($el, newTop, originalTop) {

                var wH = $(window).height();
                // console.log($el + ' ' + newTop + ', ' + originalTop);
                if ($el.data('stellar-add-class') !== undefined || $el.data('stellar-remove-class') !== undefined) {
                    // console.log($el[0].getBoundingClientRect().top - $(window).height() + ' | ' + parseInt($el.data('stellar-add-class-offset') || '0', 10));

                    if ($el[0].getBoundingClientRect().top - $(window).height() < parseInt($el.data('stellar-add-class-offset') || '0', 10)) {
                        if ($el.data('stellar-add-class') !== undefined) {
                            $el.addClass($el.data('stellar-add-class'));
                        } else {
                            $el.removeClass($el.data('stellar-remove-class'));
                        }
                    }
                } else if ($el.data('stellar-moveleft') !== undefined) {
                    $el.css({ 'left': Math.max(0, originalTop - newTop) });
                } else if ($el.data('stellar-moveright') !== undefined) {
                    $el.css({ 'right': Math.max(0, originalTop - newTop) });
                } else if ($el.data('stellar-toggle-class-on') !== undefined || $el.data('stellar-toggle-class-off') !== undefined) {
                    var elTop = $el[0].getBoundingClientRect().top;
                    var elHeight = $el.height();
                    var topCenter = Math.floor((wH / 2) - (elHeight / 2));
                    var stellarToggleOffset = parseInt($el.data('stellar-toggle-class-offset') || '0', 10);
                    var toggle = (topCenter - stellarToggleOffset) <= elTop && elTop <= (topCenter + stellarToggleOffset + elHeight);
                    if ($el.data('stellar-toggle-class-on') !== undefined) $el.toggleClass($el.data('stellar-toggle-class-on'), toggle);
                    if ($el.data('stellar-toggle-class-off') !== undefined) $el.toggleClass($el.data('stellar-toggle-class-off'), !toggle);
                } else if ($el.data('stellar-trigger') !== undefined || $el.data('stellar-toggle-class-off') !== undefined) {
                    if ($el[0].getBoundingClientRect().top - $(window).height() < parseInt($el.data('stellar-offset') || '0', 10)) {
                        try{
                            $el.trigger($el.data('stellar-trigger'));
                        } catch(e){
                            console.log("Error in trigger:"+$el.attr("id")+" "+$el.attr("class"));
                        }
                    }
                } else {
                    $el.css({ 'top': newTop });
                }
            },

            setLeft: function ($el, newLeft, originalLeft) {
                $el.css('left', newLeft);
            }

        };
        $.stellar({
            hideDistantElements: 0,
            horizontalScrolling: false,
            positionProperty: 'custom'
        });
    }
};

var Slider = {
    init: function () {
        var update = function (event, ui) {
            var val = Math.round(ui.value);
            var storage = val * 34.560 / 1024;
            var speed = val * 75 / 1000;
            if (storage < 1) {
                $(ui.handle).find(".top .val").text((storage * 1024).toFixed(0));
                $(ui.handle).find(".top .info").text('TB');
            } else {
                $(ui.handle).find(".top .val").text(storage.toFixed(1));
                $(ui.handle).find(".top .info").text('PB');
            }

            if (speed < 1) {
                $(ui.handle).find(".bottom .val").text((speed * 1000).toFixed(0) + "K");
            } else {
                $(ui.handle).find(".bottom .val").text(speed.toFixed(1) + "M");
            }

        };
        $(".slider-well").slider({
            min: 4,
            max: 100,
            value: 5,
            slide: update,
            change: update,
            create: function (event, ui) {
                $(event.target).find(".ui-slider-handle").html('<span class="top"><span class="val" ></span><span class="info" >PB</span></span><span class="bottom" ><span class="val" ></span><span class="info" >IOPS</span>')
                update(event, { handle: event.target, value: $(event.target).slider("option", "value") });
            }
        }).bind('animate', function () {
            if (!$(this).data("animated")) {
                $(this).data("animated", true);
                $(this).animate({
                    value: 50
                }, {
                    duration: 1000,
                    step: function (now, fx) {
                        $(fx.elem).slider({ value: now || 0 });
                    }
                });
            }
        });

        var scaleUpdate = function (event, ui) {
            $(ui.handle).css('margin-left', ((ui.value / 3) * 100) + "%");
            setTimeout(function () {
                var val = 0;
                if (ui.value < 1) {
                    // 0 - 1  4-20
                    val = 4 + (ui.value) * 15;
                } else if (ui.value < 2) {
                    // 1 - 2  20-40
                    val = 20 + (ui.value - 1) * 20;
                } else {
                    // 2 - 3  40-100
                    val = 40 + (ui.value - 2) * 60;
                }
                val = Math.round(val);



                storage = val * 8.640 / 1024;
                speed = val * 50 / 1000;


                var storage3 = val * 8.640 / 1024;
                var storage6 = val * 17.280 / 1024;
                var storage9 = val * 34.6 / 1024;
                var speed6 = val * 50 / 1000;
                var speed9 = val * 75 / 1000;
                var power3 = val * 0.18;
                var power6 = val * 0.212;
                var power9 = val * 0.3;
                $(ui.handle).find("thead td").text(val + " Nodes");
                if (storage3 < 1) {
                    $(ui.handle).find(".r1").text((storage3 * 1024).toFixed(0) + "TB");
                } else {
                    $(ui.handle).find(".r1").text(storage3.toFixed(1) + "PB");
                }
                if (storage6 < 1) {
                    $(ui.handle).find(".r2").text((storage6 * 1024).toFixed(0) + "TB");
                } else {
                    $(ui.handle).find(".r2").text(storage6.toFixed(1) + "PB");
                }
                if (storage9 < 1) {
                    $(ui.handle).find(".r3").text((storage9 * 1024).toFixed(0) + "TB");
                } else {
                    $(ui.handle).find(".r3").text(storage9.toFixed(1) + "PB");
                }

                if (speed6 < 1) {
                    $(ui.handle).find(".r4").text((speed6 * 1000).toFixed(0) + "K");
                } else {
                    $(ui.handle).find(".r4").text(speed6.toFixed(1) + "M");
                }
                if (speed9 < 1) {
                    $(ui.handle).find(".r5").text((speed9 * 1000).toFixed(0) + "K");
                } else {
                    $(ui.handle).find(".r5").text(speed9.toFixed(1) + "M");
                }
                $(ui.handle).find(".r6").text(power3.toFixed(power3 < 5 ? 1 : 0) + "kW");
                $(ui.handle).find(".r7").text(power6.toFixed(power6 < 5 ? 1 : 0) + "kW");
                $(ui.handle).find(".r8").text(power9.toFixed(power9 < 5 ? 1 : 0) + "kW");
                $(ui.handle).find(".r9").text(val + "RU");
            }, 10);
        };
        $(".scale-out-slider-well").slider({
            min: 0,
            max: 3,
            value: 0,
            step: 0.01,
            slide: scaleUpdate,
            change: scaleUpdate,
            create: function (event, ui) {
                $(event.target).find(".ui-slider-handle").html(
                    '<div class="anchor" ></div>' +
                        '<div class="table-wrap">' +
                        '<table>' +
                        '<thead>' +
                        '<tr><td>20 Nodes</td></tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '<tr class="sf3 third"><td class="r1">240TB</td></tr>' +
                        '<tr class="sf6 third"><td class="r2">432TB</td></tr>' +
                        '<tr class="sf9 third"><td class="r3">692TB</td></tr>' +
                        '<tr class="sf6 sp"><td class="r4">1.2IPS</td></tr>' +
                        '<tr class="sf9"><td class="r5">692TB</td></tr>' +
                        '<tr class="sf3 third sp"><td class="r6">kW</td><//tr>' +
                        '<tr class="sf6 third"><td class="r7">kW</td><//tr>' +
                        '<tr class="sf9 third"><td class="r8">kW</td></tr>' +
                        '<tr class="sp"><td class="r9">RU</td></tr>' +
                        '</tbody>' +
                        '</table>' +
                        '</div>');
                scaleUpdate(event, { handle: event.target, value: $(event.target).slider("option", "value") });
            }
        }).bind('animate', function () {
            if (!$(this).data("animated")) {
                $(this).data("animated", true);
                $(this).animate({
                    value: 1
                }, {
                    duration: 1000,
                    step: function (now, fx) {
                        $(fx.elem).slider({ value: now || 0 });
                    }
                });
            }
        });

        var perfUpdate = function (event, ui) {
            var $slider = $(ui.handle).parent();
            if ($slider.data("dial")) {
                var $dial = $($slider.data("dial"));
                var degree = Math.round((ui.value - 1) * 180);
                // console.log(degree);
                $dial.css({
                    '-webkit-transform': 'rotate(' + degree + 'deg)',
                    '-moz-transform': 'rotate(' + degree + 'deg)',
                    '-ms-transform': 'rotate(' + degree + 'deg)',
                    '-o-transform': 'rotate(' + degree + 'deg)',
                    'transform': 'rotate(' + degree + 'deg)',
                });
            } else {
                var $fill = $($slider.data("fill"));

                // from  90% to 10%
                var top = Math.round(10 + (1 - ui.value) * 80);
                // console.log(top);
                $fill.css('top', top + '%');
            }
        };
        $(".slider-well-vert").slider({
            min: 0,
            max: 1,
            value: 0.4,
            step: 0.01,
            orientation: "vertical",
            slide: perfUpdate,
            change: perfUpdate,
            create: function (event, ui) {
                perfUpdate(event, { handle: $(event.target).children(), value: $(event.target).slider("option", "value") });
            }
        }).bind('animate', function () {
            if (!$(this).data("animated")) {
                $(this).data("animated", true);
                $(this).animate({
                    value: 0.7
                }, {
                    duration: 1000,
                    step: function (now, fx) {
                        $(fx.elem).slider({ value: now || 0 });
                    }
                });
            }
        });
        ;


    }
};


var Rotate = {
    totalFrames: 32,
    originalWidth: 800,
    originalHeight: 246,
    originalGap: 1.5,
    currentFrame: 0,
    showFrame: function (frameNum) {
        Rotate.currentFrame = Math.round(frameNum || 0) % Rotate.totalFrames;
        var ratio = $('#node360').width() / Rotate.originalWidth;
        var height = ratio * Rotate.originalHeight;
        $('#node360').css('height', height + 'px');
        var gap = ratio * Rotate.originalGap;
        $("#node360 img").css('margin-top', (-Rotate.currentFrame * (height + gap)) + "px");
    },


    init: function () {

        //  Fix for window size change
        //
        $(window).resize(function () {
            clearTimeout(Rotate.timeout);
            Rotate.timeout = setTimeout(function () {
                Rotate.showFrame(Rotate.currentFrame);
            }, 500);
        });
        var node360Update = function (event, ui) {
            Rotate.showFrame(ui.value);
        };
        $(".slider-well-360").slider({
            min: 0,
            max: 32,
            value: 0,
            slide: node360Update,
            change: node360Update,
            create: function (event, ui) {
                node360Update(event, { handle: event.target, value: $(event.target).slider("option", "value") });
            }
        }).bind('animatenode', function () {
            if ($(this).data('imageReady') && !$(this).data("animated")) {
                $(this).data("animated", true);
                $(this).animate({
                    value: 32
                }, {
                    easing: 'linear',
                    duration: 2000,
                    step: function (now, fx) {
                        $(fx.elem).slider({ value: now || 0 });
                    }
                });
            }
        });
    },
    imageReady: function () {
        $(".slider-well-360").
            data("imageReady", true).
            trigger("animatenode");
    }
};

var Zoom = {

    grabConstants: function () {
        Zoom.imageWidth = Zoom.imageWidth || Zoom.$image.width();
        Zoom.imageHeight = Zoom.imageHeight || Zoom.$image.height();

        Zoom.mouseZoneWidth = Zoom.$mouseZone.width();
        Zoom.mouseZoneHeight = Zoom.$mouseZone.height();
        Zoom.mouseZoneoffset = Zoom.$mouseZone.offset();

        Zoom.containerWidth = Zoom.$image.parent().width();
        Zoom.containerHeight = Zoom.$image.parent().height();
    },

    init: function () {
        Zoom.$image = $('#nodeZoomLarge');
        Zoom.$mouseZone = $('#nodeZoomSmall');

        $('#node360').unbind('click')
            .on('click', function (e) {
                var src = '/node360/node-angle-' + Rotate.currentFrame + '.jpg';
                //console.log(src);

                if (Browser.isMobile) {
                    window.location = src;
                } else {
                    Zoom.$mouseZone.unbind('load');
                    Zoom.$mouseZone.attr('src', src);
                    Zoom.$image.attr('src', src);

                    Zoom.$image.show();
                    Zoom.$mouseZone.css('opacity', 0);
                    $('body').append($('<div id="nodeZoomGrey"></div>').on('click', function () {
                        $('#nodeZoomWrap').hide();
                        $('#nodeZoomGrey').remove();
                    }));
                    var ready = function() {
                        $('#nodeZoomWrap').show();
                        Zoom.grabConstants();
                        Zoom.position(e,"TRUE");
                    };
                    if (Zoom.$image.get(0).complete) {
                        ready();
                    } else {
                        Zoom.$image.load(ready);
                    }
                }
            });
        $('#nodeZoomWrap').unbind('click')
            .on('click', function () {
                $('#nodeZoomWrap').hide();
                $('#nodeZoomGrey').remove();
            });

        Zoom.$mouseZone.unbind('mouseenter').unbind('mouseleave').unbind('mousemove')
          .on('mouseenter', function () {
              Zoom.$image.show();
              Zoom.$mouseZone.css('opacity', 0);
          })
          .on('mouseleave', function () {
              Zoom.$image.hide();
              Zoom.$mouseZone.css('opacity', 1);
          })
          .on('mousemove', Zoom.position);
    },

    position:function(e,isClick) {
        //console.log(isClick+":"+e.pageY + "-" + Zoom.mouseZoneoffset.top + "/" + Zoom.mouseZoneHeight);
        var leftRatio = (e.pageX - Zoom.mouseZoneoffset.left) / Zoom.mouseZoneWidth;
        var topRatio = (e.pageY - Zoom.mouseZoneoffset.top) / Zoom.mouseZoneHeight;
        //console.log(leftRatio + "," + topRatio);

        leftRatio = Math.max(Math.min(leftRatio, 1), 0);
        topRatio = Math.max(Math.min(topRatio, 1), 0);
        //console.log(Zoom.imageWidth+" - "+Zoom.containerWidth+  "," + Zoom.imageHeight +" - "+Zoom.containerHeight);
        Zoom.$image.css({
            left: (leftRatio * -(Zoom.imageWidth - Zoom.containerWidth)) + 'px',
            top: (topRatio * -(Zoom.imageHeight - Zoom.containerHeight)) + 'px'
        });
    }
};
$(function () {
    Rotate.init();
    Zoom.init();
});
$(window).load(function () {
    Rotate.imageReady();
    
    try {
        if (window.location.pathname == "/login.aspx") {
            window.location = unescape(window.location.href.match(/ReturnUrl=(.*)/)[1]);
        }
    } catch (e) {
        console.log("login redirect fail");
    }
});

var Trigger = {

    init: function () {
        // This is a rewite of the original Triggers function in main.js
        $('body').on('click', '.trigger', function (e) {
            var self = $(e.currentTarget);
            var isActive = self.is('.active');
            // Reset everyone
            $('.trigger,.trigger-content').removeClass('active');
            var remote = $(self.attr('href'));
            // Toggle
            self.toggleClass('active', !isActive);
            remote.toggleClass('active', !isActive);
            return false;
        });
    },
};

/* HEADER REPLACEMENT */
$(function() {
    $('#q').on('blur', function() {
        $('.header-nav').removeClass('search-active');
    });

    $('.js-q-toggle').on('mousedown', function(event) {
        event.preventDefault();
        var form = $('.header-nav');
        if (!form.hasClass('search-active')) {
            setTimeout(function() { document.getElementById('q').focus(); }, 50);
            form.addClass('search-active');
        } else {
            form.removeClass('search-active');
        }

    });

    window.onscroll = function(event) {
        if ($(window).scrollTop() >= 20) {
            $('header').addClass('sticky');
        } else {
            $('header').removeClass('sticky');
        }
    };

    if (Browser.isMobile) {
        $(".global-nav .level1 > li.drop-down").on('touchstart', function (e) {

            if( $(e.target).closest(".level2").length==0 ) {
                event.preventDefault();

                var selectedLink = $(this);
                var isOpen = selectedLink.hasClass('active');
                $(".global-nav .level1 > li").removeClass('active');
                selectedLink.toggleClass('active',!isOpen);
            }
        });
    }

});

/*
var Header = {

    dropDownDelay: 300,

    init: function () {
        Header.initDropdown();
        Header.initExpandSearch();
    },


    initDropdown: function () {
        $(".global-nav").hover(Header.cancelClose, Header.closeDropdown);
        $(".page-sub-header").hover(Header.cancelClose, Header.closeDropdown);
        $(".global-nav > li").mouseenter(function (e) {
            Header.openItem($(this));
        });

        if (Browser.isMobile) {
            $(".global-nav .level0 > a").click(function () {
                var $item = $(this).parent();
                Header.openItem($item);
                return $item.is('.subsfalse'); // prevent default, stop propogation
            });
        }
        $(".global-nav > li .drop-down").mouseenter(function () {
            Header.cancelClose();
            Header.cancelOpen();
        });
    },
    closeDropdown: function () {
        Header.cancelClose();
        Header.cancelOpen();
        Header.closeTimeout = setTimeout(function () {
            $(".drop-down").animate({ height: 0 }, 200, function () {
                $(".drop-down").hide();
            });
            $(".page-sub-header").css("margin-top", 0);
        }, Header.dropDownDelay);
    },
    cancelClose: function () {
        clearTimeout(Header.closeTimeout);
    },
    openItem: function ($item) {
        var dd = $item.find('.drop-down');
        if (dd.length) {
            var ul = dd.find('ul:first');
            if (!ul.data('processed')) {
                ul.data('processed', true);
                Columns.listIntoColumns(ul, 7); // 7 per column
            }
        }

        Header.cancelClose();
        Header.cancelOpen();
        Header.openTimeout = setTimeout(function () {
            var openItems = $(".drop-down:visible");
            openItems.hide();
            $item.parent().children().removeClass('active');
            $item.addClass('active');

            var dd = $item.find('.drop-down').show();
            dd.css("padding-left", $item.find('a').offset().left);
            var newHeight = dd.css("height", "auto").height();
            $(".page-sub-header").css("margin-top", newHeight || 0);
            if (!openItems.length) {
                $(".drop-down").css("height", 0).animate({ height: newHeight }, 200);
            }
        }, Header.dropDownDelay);
    },
    cancelOpen: function () {
        clearTimeout(Header.closeTimeout);
    },


    initExpandSearch: function () {
        $("#menu #searchbox").focusin(function () {
            $("#menu ul.global-nav").removeClass("searchmode-off").addClass("searchmode");
        });
        $("#menu #searchbox").focusout(function () {
            $("#menu ul.global-nav").removeClass("searchmode").addClass("searchmode-off");
        });
    }
};
*/

var WistiaVideos = {
    init: function () {
        var videos = {
            audznwvq8y: {
                el: $("#wistia_audznwvq8y"),
                thumbnail: "/images/static/storage-agility-lp-thumb.jpg"
            },
            w5e4yljpg0: {
                el: $("#wistia_w5e4yljpg0"),
                thumbnail: "/images/static/ai_video_thumbnail.jpg"
            }
        };
        $.each(videos, function (key, val) {
            if (val.el.length) {
                WistiaVideos.loadVideo(key, val.thumbnail);
            }
        });
        this.loadPopoverScript();
    },
    loadVideo: function (videoId, videoThumbnail) {
        $.getScript("http://fast.wistia.com/assets/external/E-v1.js")
            .done(function (script, textStatus) {
                Wistia.embed(videoId, {
                    stillUrl: videoThumbnail
                });
            });
    },
    loadPopoverScript: function () {
        if ($("[class^=wistia-popover]").length) {
            $.getScript("http://fast.wistia.net/static/popover-v1.js");
        }
    }
};

var Icons = {
    link: function () {
        var icons = $("img[alt$='Icon']"),
            path = window.location.pathname;
        $.each(icons, function () {
            var href = $(this).attr("alt")
                .slice(0, -5)
                .replace(/[\s]/, "-")
                .toLowerCase();
            if (path.indexOf(href) === -1) {
                $(this).wrap("<a href='" + path + href + "'></a>");
            }
        });
    }
};

var Search = {

    googleResultLoaded: function () {
        //alert("loaded")
        var backState = { term: $('input.gsc-input').val() };
        $(window).bind('popstate', function (event) {
            var term = event.originalEvent.state ? event.originalEvent.state.term : getParameterByName('q');
            backState.term = term;
            $('input.gsc-input').val(term);
            google.search.cse.element.getElement('standard0').execute(term);
        });
        Search.interval = setInterval(function () {
            var resultInfo = $('.gsc-result:first');
            if (resultInfo.length && !resultInfo.data('isOld')) {
                resultInfo.data('isOld', true);

                try {
                    var term = $('input.gsc-input').val();
                    if (backState.term != term) {
                        history.pushState({ term: term }, "Search Result:" + term, '/search-results?q=' + term);
                    }
                    backState.term = null;
                } catch (e) { }
                $('.gsc-result-info').text($('.gsc-result-info').text().replace(/\(.*\)/, ''));
                //console.log('new results');
            }
        }, 500);
    }
};


$(function () {
    Slider.init();
    // Header.init();
    Home.init();
    Trigger.init();
    WistiaVideos.init();
    Icons.link();

    $(".database .btn").click(function (e) {
        e.preventDefault();
        $("iframe").toggleClass("closed");
    });

});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var Columns = {
    listIntoColumns: function (ul, countPerColumn) {

        // This is confusing. im sorry -EG
        var t = Balance.countPerGroup;
        Balance.countPerGroup = function () {
            return countPerColumn;
        };
        Balance.listIntoColumns(ul, 4);
        Balance.countPerGroup = t;
    },
};

var Balance = {

    countPerGroup: function (sum, numberOfGroups) {
        return Math.ceil(sum / numberOfGroups);
    },

    /** Take and array and return a number of groups balancing out the number of items in each group */
    byLength: function (items, numberOfGroups) {

        var sum = 0;
        for (var i = 0; i < items.length; ++i) {
            var item = items[i];
            sum += item.length;
        }

        var countPerGroup = Balance.countPerGroup(sum, numberOfGroups);
        var groupSum = 0;
        var group = [];
        var groups = [group];
        for (var i = 0; i < items.length; ++i) {
            var item = items[i];
            if (groups.length >= numberOfGroups || (groupSum + item.length) <= countPerGroup || groupSum == 0) {
                groupSum += item.length;
                group.push(item);
            } else {
                groupSum = item.length;
                group = [item];
                groups.push(group);
            }
        }
        return groups;
    },

    listIntoColumns: function (ul, numberOfColumns) {
        var LIs = ul.children().map(function (i, li) {
            return {
                length: $(li).find('li').length + 1,
                li: $(li)
            };
        });
        var columns = Balance.byLength(LIs, numberOfColumns);

        $.each(columns, function (i, LIs) {
            var colUL = $('<ul>');
            var colLI = $('<li>').append(colUL);
            ul.append(colLI);
            $.each(LIs, function (j, LI) {
                colUL.append(LI.li);
            });
        });
    },

    height: function () {
    }
};

$(function () {
  $(".demo-form").fancybox({
    maxWidth: 275,
    maxHeight: 375,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
  });

  MktoForms2.loadForm("//app-ab03.marketo.com", "538-SKP-058", 1115);

  var windowVar = $(window);
  var vmwareId, useCasesId, integrationsId, resourcesId, vmwareClass, useCasesClass, integrationsClass, resourcesClass;
  vmwareId = $('#vmware');
  useCasesId = $('#use-cases');
  integrationsId = $('#integrations');
  resourcesId = $('#resources');

  vmwareClass = $('.vmware-link');
  useCasesClass = $('.use-cases-link');
  integrationsClass = $('.integrations-link');
  resourcesClass = $('.resources-link');

  windowVar.scroll(function () {
    if (notHighlighted(vmwareId) && isScrolledIntoView(vmwareId)) {
      vmwareClass.css('color', '#CC1F26').attr('highlighted', true);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(useCasesId) && isScrolledIntoView(useCasesId)) {
      useCasesClass.css('color', '#CC1F26').attr('highlighted', true);
      vmwareClass.css('color', '#A9A9A9').attr('highlighted', false);
      integrationsClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(integrationsId) && isScrolledIntoView(integrationsId)) {
      integrationsClass.css('color', '#CC1F26').attr('highlighted', true);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(resourcesId) && isScrolledIntoView(resourcesId)) {
      resourcesClass.css('color', '#CC1F26').attr('highlighted', true);
      vmwareClass.css('color', '#A9A9A9').attr('highlighted', false);
      integrationsClass.css('color', '#A9A9A9').attr('highlighted', false);
    }
  });

  function notHighlighted(elem) {
    return elem.attr('highlighted') != 'true';
  }

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }
});
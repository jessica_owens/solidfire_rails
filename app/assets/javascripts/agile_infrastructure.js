$(function () {
  var windowVar = $(window);
  var aiOverviewId, useCasesId, resourcesId, aiOverviewClass, useCasesClass, resourcesClass;

  aiOverviewId = $('#ai-overview');
  useCasesId = $('#use-cases');
  resourcesId = $('#resources');

  aiOverviewClass = $('.ai-overview-link');
  useCasesClass = $('.use-cases-link');
  resourcesClass = $('.resources-link');

  windowVar.scroll(function () {
    if (notHighlighted(aiOverviewId) && isScrolledIntoView(aiOverviewId)) {
      aiOverviewClass.css('color', '#CC1F26').attr('highlighted', true);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(useCasesId) && isScrolledIntoView(useCasesId)) {
      useCasesClass.css('color', '#CC1F26').attr('highlighted', true);
      aiOverviewClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(resourcesId) && isScrolledIntoView(resourcesId)) {
      resourcesClass.css('color', '#CC1F26').attr('highlighted', true);
      aiOverviewClass.css('color', '#A9A9A9').attr('highlighted', false);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
    }
  });

  function notHighlighted(elem) {
    return elem.attr('highlighted') != 'true';
  }

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }

  $('#card-one, #card-two, #card-three').flip({
    trigger: 'hover'
  });
});
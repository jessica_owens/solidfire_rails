$(function () {
  var windowVar = $(window);
  var supportId, useCasesId, resourcesId, supportClass, useCasesClass, resourcesClass;

  supportId = $('#support-section');
  resourcesId = $('#resources');

  supportClass = $('.support-link');
  resourcesClass = $('.resources-link');

  windowVar.scroll(function () {
    if (notHighlighted(supportId) && isScrolledIntoView(supportId)) {
      supportClass.css('color', '#CC1F26').attr('highlighted', true);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(resourcesId) && isScrolledIntoView(resourcesId)) {
      resourcesClass.css('color', '#CC1F26').attr('highlighted', true);
      supportClass.css('color', '#A9A9A9').attr('highlighted', false);
    }
  });

  function notHighlighted(elem) {
    return elem.attr('highlighted') != 'true';
  }

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }
});

var returnURL = "http://www.solidfire.com/contact/ty-support";

document.getElementById("retURL").value = "http://www.solidfire.com/contact/ty-support";

$(".request-form").fancybox({
  maxWidth: 275,
  maxHeight: 375,
  fitToView: true,
  width: '78%',
  height: '78%',
  autoSize: false,
  closeClick: false,
  openEffect: 'none',
  closeEffect: 'none',
  padding: 20,
  scrolling: 'no'
});
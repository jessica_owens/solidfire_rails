/*
 * require jquery.easing.1.3
 * require jquery.anyslider
 */
//= require jquery.min.1.11.2
//= require jquery-ui.min
$(function () {
    function trans_nav() {
        var getElePosition = $('.header__container .header__container--burger-menu').css('textAlign');

        if (getElePosition == 'left') {
            // $('.header__container').css({'backgroundColor':'#CC1F26','border':'none'});
        } else {
            //  $('.header__container').css({'backgroundColor':'transparent','border':'none'});
        }

        $('.header__container').removeClass('header__container--home-style-01');
        $('.header__container .header__container--bsp-container').removeClass('header__container--home-style-02');
        $('.header__container .header__container--burger-menu .header__container--burger-menu--image span, .header__container .header__container--burger-menu .header__container--burger-menu--image .burger-image, .header__container .header__container--main-nav .header__container--main-nav__item .header__container--main-nav__item--vertical-bar').removeClass('header__container--home-style-03');;
        $('.header__container .header__container--logo .header__container--logo__wrapper i.logo-icon').removeClass('header__container--home-style-04');;

    }

  //slide down nav
  $('.header__container').css('position', 'absolute');

  var elementPosition = $(' .home-section-one--content').offset();
  var hidden = true;
  var staticHeader = true;

  $(window).scroll(function () {

    if ($(window).scrollTop() > 250 && hidden == true && $(window).width() > 769) {
      $('.header__container').css('display', 'none').css('position', 'fixed');
      $('.header__container').addClass('header__container--home-style-01');
      $('.header__container .header__container--bsp-container').addClass('header__container--home-style-02');
      $('.header__container .header__container--burger-menu .header__container--burger-menu--image span, .header__container .header__container--burger-menu .header__container--burger-menu--image .burger-image, .header__container .header__container--main-nav .header__container--main-nav__item .header__container--main-nav__item--vertical-bar, .header__container .header__container--main-nav .header__container--main-nav__item a').addClass('header__container--home-style-03').removeClass('grey-text');
      $('.header__container .header__container--logo .header__container--logo__wrapper i.logo-icon').addClass('header__container--home-style-04').removeClass('white-text');

      $('.header__container--logo-container.fade-in-three').removeClass('fade-in-three');
      $('.header__container--burger-menu.fade-in-three').removeClass('fade-in-three');
      $('.header__container--main-nav.fade-in-three').removeClass('fade-in-three');
      $('.header__container--burger-menu--rollover-box--mobile.fade-in-three').removeClass('fade-in-three');
      $('.header__container--bsp-container.fade-in-three.header__container--home-style-02').removeClass('fade-in-three');
      $('.header__container').slideToggle('fast');

      hidden = false;
      staticHeader = false;

    } else if ($(window).scrollTop() < 250  && hidden == false && $(window).width() > 769) {
      $('.header__container').slideToggle('fast');
      hidden = true;
    } else if ($(window).scrollTop() <= 200 && staticHeader == false && $(window).width() > 769) {
      $('.header__container').removeClass('header__container--home-style-01').css('position', 'absolute');
      $('.header__container .header__container--bsp-container').removeClass('header__container--home-style-02');
      $('.header__container .header__container--burger-menu .header__container--burger-menu--image span, .header__container .header__container--burger-menu .header__container--burger-menu--image .burger-image, .header__container .header__container--main-nav .header__container--main-nav__item .header__container--main-nav__item--vertical-bar, .header__container .header__container--main-nav .header__container--main-nav__item a').removeClass('header__container--home-style-03').addClass('grey-text');
      $('.header__container .header__container--logo .header__container--logo__wrapper i.logo-icon').removeClass('header__container--home-style-04').addClass('white-text');
      $('.icon-link a').removeClass('grey-text');
      $('.header__container').slideToggle('fast');

      staticHeader = true;
    } else if ($(window).width() < 769) {
      $('.header__container').removeClass('header__container--home-style-01').css('position', 'absolute');
      $('.header__container .header__container--bsp-container').removeClass('header__container--home-style-02');
      $('.header__container .header__container--burger-menu .header__container--burger-menu--image span, .header__container .header__container--burger-menu .header__container--burger-menu--image .burger-image, .header__container .header__container--main-nav .header__container--main-nav__item .header__container--main-nav__item--vertical-bar, .header__container .header__container--main-nav .header__container--main-nav__item a').removeClass('header__container--home-style-03').addClass('grey-text');
      $('.header__container .header__container--logo .header__container--logo__wrapper i.logo-icon').removeClass('header__container--home-style-04').addClass('white-text');
    }
  });

  $('.whats-new-item--body').each(function(i, obj) {

        var getContent = $(this).text();
        var limitNumberWords = getContent.split(/\s+/).slice(1,15).join(" ");

        $(this).text(limitNumberWords + '...');
    });

    // updates the blog in the news bar dynamically
    var url = 'http://www.solidfire.com/blog/feed/';
    $.ajax({
        type: "GET",
        url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=1000&callback=?&q=' + encodeURIComponent(url),
        dataType: 'json',
        error: function(){
            console.log('Unable to load feed, Incorrect path or invalid feed');
        },
        success: function(xml){
            var values = xml.responseData.feed.entries[0];

            var pubDate = values.publishedDate;
            var date = new Date(pubDate);

            var months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            var formatBlogDate =  " " + months[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear()

            var dateFormat = formatBlogDate;

            $('.blog-date').html(dateFormat);
            $('.blog-title').html(values.title);
            $('.blog-author').html(values.author);
            $('.blog-link').attr('href', function(){
                return this.href = values.link;
            })
        }
    });

    trans_nav();

    $('footer').css({'marginTop':'0px','paddingTop':'60px'});

    $('.as-prev-arrow').html('&#60;');

    $('.as-next-arrow').html('&#62;');
    
});

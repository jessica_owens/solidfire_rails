//= require jquery
//= require fancybox/jquery.fancybox.js
//= require fancybox/jquery.fancybox.pack.js
//= require fancybox/jquery.fancybox-media.js
//= require sitewide
//= require elio_404



$(function () {

    var lastURL= document.referrer;
    if(!lastURL){
        $('.previousPage').css('display', 'none');
    }

    $('.previousPageLink').click(function() {
        window.open (lastURL, "_parent","newWindow");
    });



    //animation js
    var canvas, stage, exportRoot;

    createjs.MotionGuidePlugin.install();

    canvas = document.getElementById("canvas");
    images = images||{};

    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", handleFileLoad);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(lib.properties.manifest);

    function handleFileLoad(evt) {
        if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
    }

    function handleComplete() {
        exportRoot = new lib.Elio404final3();

        stage = new createjs.Stage(canvas);
        stage.addChild(exportRoot);
        stage.update();

        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
});
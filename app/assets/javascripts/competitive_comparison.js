$(".cc-pure").fancybox({
  maxWidth: 275,
  maxHeight: 375,
  fitToView: true,
  width: '70%',
  height: '78%',
  autoSize: false,
  closeClick: false,
  openEffect: 'none',
  closeEffect: 'none',
  padding: 20,
  scrolling: 'no'
});

$('.cc-pure').click(function(){
    MktoForms2.loadForm("//app-ab03.marketo.com", "538-SKP-058", 1439);
});

$(".cc-emc").fancybox({
    maxWidth: 275,
    maxHeight: 375,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$('.cc-emc').click(function(){
    MktoForms2.loadForm("//app-ab03.marketo.com", "538-SKP-058", 1440);
});


//= require isotope.pkgd.min
//= require fit-columns
$(function ()
{
    iframeWindowSize();

    function iframeWindowSize(){
        var IframeHeight = ($('#youtubeFrame').width()/16) * 9;
        $('#youtubeFrame').css('height', IframeHeight);
    }

    $( window ).resize(function() {
        iframeWindowSize();
    });

    $("input:checkbox[name=resource_all]").change(function(){
        $('.card-container').remove();
        $('.tags').prop('checked', false);
        $('#all').prop('checked', true);
        $('.tags').removeClass('active');
        $('.card-wrapper').addClass('all-resources');
        $.ajax({
            url: "/resources/all/all",
            success: function (data) {
                $(data).hide().prependTo('.card-wrapper').fadeIn( "slow", function() {});
                createPagination('card-wrapper','card-container',15);
                masonry_calls();
            }
        });
    });

    animateCards();

    $('.tags').removeAttr('checked');
    $('input:checkbox[name=resource_all]').prop('checked', true);

    // only use this logic onn the category and content type pages
    if($('.cats-tags-page').length) {
        $('input:checkbox[name=resource_all]').prop('checked', false);

        var getUrl = window.location.href;
        var lastSegment = getUrl.substr(getUrl.lastIndexOf('/') + 1);

        if($('.category-page').length) {
            $('input:checkbox[id=resource_cats_' + lastSegment + ']').prop('checked', true);
            $('input:checkbox[id=resource_cats_' + lastSegment + ']').toggleClass('active');
        } else {
            $('input:checkbox[id=cms_resource_tags_' + lastSegment + ']').prop('checked', true);
            $('input:checkbox[id=cms_resource_tags_' + lastSegment + ']').toggleClass('active');
        }

        $('.tags').change(function(){
            if ($(this).hasClass('active')) {
                $('input:checkbox[name=resource_all]').prop('checked', false);
            }
        });
    }

});

function animateCards() {
    $(".card-wrapper").delay(3000).css('width','100%');
}

if($('.card-wrapper').length) {
    createPagination('card-wrapper','card-container',15);
    $(window).load(function(){
        $('.card-wrapper').masonry();
    });
}

if($('.card-wrapper').length) {

    var container = document.querySelector('.card-wrapper');
    var msnry = new Masonry( container, {
        columnWidth: 80,
        itemSelector: '.card-container'
    });
}

$('.tags').change(function(){
    var getDataTag = $(this).attr('data-tag');
    var getId = $(this).attr('id');
    var res = getId.match(/resource_cats_/g);
    $('#all').removeAttr('checked');

    var setUrl = (res) ? "/resources/cat/" + getDataTag : "/resources/tag/" + getDataTag;

    $(this).toggleClass('active');

    if ($('.all-resources').length) {
        $( '.card-container').remove();
        $('.card-wrapper').removeClass('all-resources');
        $('#all').prop('checked', false);
    }

    if ($(this).hasClass('active')) {

        $.ajax({
            url: setUrl,
            success: function (data) {
                $(data).hide().prependTo('.card-wrapper').fadeIn( "slow", function() {});

                $('[id]').each(function (i) {
                    var ids = $('[id="' + this.id + '"]');
                    if (ids.length > 1) $('[id="' + this.id + '"]:gt(0)').remove();
                });

                createPagination('card-wrapper','card-container',15);
                masonry_calls();

            }
        });
    } else {
        $( "." + getDataTag ).remove();
        createPagination('card-wrapper','card-container',15);
        masonry_calls();

        if ($('.card-container').length == 0) {
            $('#all').prop('checked', true);
            $('.card-wrapper').addClass('all-resources');
            $.ajax({
                url: "/resources/all/all",
                success: function (data) {
                    $(data).hide().prependTo('.card-wrapper').fadeIn( "slow", function() {});
                    createPagination('card-wrapper','card-container',15);
                    masonry_calls();
                    //animateCards();
                }
            });
        }
    }
});

$(document).on("click", "a.page-link", function() {
    $('html, body').animate({
        scrollTop: $('.card-wrapper').offset().top - 100
    }, 200);
});


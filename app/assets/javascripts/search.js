$(function () {

    var getUrl = window.location.pathname;
    var url = getUrl
    var getSearchTerm = url.replace("/search/", "");

    var getTotalResultsIndex = $('.total-results-index').val() / 10;
    var setNext = '';

    if (parseInt(getTotalResultsIndex) < 10) {
        setNext = 0;
    } else {
        setNext = 1;
    }

        $.ajax({
            url: '/search-term/' + getSearchTerm + '/1',
            success: function (data) {
                $(data).hide().prependTo('.search-results').fadeIn( "slow", function() {});
                $('ul.pagination li:gt(9)').css("display", "none");
                returnSearchLinks(setNext);
                $('.page-' + '#1').addClass('active-page');
            }
        });

    $(document).on("click", ".search-page-ajax-next", function() {
        linkLogic(1);
    });

    $(document).on("click", ".search-page-ajax-prev", function() {
        linkLogic(0);
    });

    function linkLogic(link, getValue) {
        var getDataPageNext = $('.input-next').val();
        var getDataPagePrev = $('.input-prev').val();
        var getTotalResults = $('.page-result').attr('data-results') / 10;
        var getQuery = $('.query').val();


        if (link == 1) {
            var addNext = parseInt(getDataPageNext) + 10;
        } else if (link == 0){
            var addNext = parseInt(getDataPageNext) - 10;
        } else {
            addNext = $('.input-next').val();
        }

        var setDataPagePrev =  addNext - 10;

        var getNextLink = 1;
        var getPrevLink =  2;

        if (setDataPagePrev < 0) {
            setDataPagePrev = $('.input-prev').val(0);
            getPrevLink = 0;
        }

       if (addNext > getTotalResults) {
           getNextLink = 0;
        }

        if (typeof getValue === 'undefined') {
            getValue = addNext - 9;
        }

        if (getValue == 0) {
            getValue = 1;
        }

        $('.input-next').val(addNext);
        $('.input-prev').val(setDataPagePrev);

        $('.search-results').empty();

        $.ajax({
            url: '/search-term/' + getQuery + '/' + getValue,
            success: function (data) {
                $(data).hide().prependTo('.search-results').fadeIn( "slow", function() {});
                $('ul.pagination li:gt(' + addNext + ')').css("display", "none");
                $('ul.pagination li:lt(' + setDataPagePrev + ')').css("display", "none");
                returnSearchLinks(getNextLink);
                returnSearchLinks(getPrevLink);
                $('.page-' + '#' + getValue).addClass('active-page');
            }
        });
    }

    function returnSearchLinks(getValue) {
        if (getValue == 1) {
            var nextLink = $('.next').append('<a href="javascript://" class="search-page-ajax-next">next &#187;</a>');
        } else if(getValue == 2) {
            var prevLink = $('.prev').append('<a href="javascript://" class="search-page-ajax-prev">&#171; previous</a>');
        }
    }

    $(document).on("click", ".search-page-ajax", function() {
        var getPageNumber = $(this).attr('data-page');

        linkLogic(2, getPageNumber);
    });
});
$(function () {
  //first animation

  var nodes = [$('.node-four'), $('.node-three'), $('.node-two'), $('.node-one')];
  var backNodes = [];
  var blueHeight = 66;
  var yellowHeight = 100;

  $('.plus').on('click', function () {
    if(nodes.length > 0) {
      if(nodes.length == 1) {
       animateElipse($('.ellipse'), 1);
      }

      var selectedNode = nodes.pop();

      backNodes.push(selectedNode);
      blueHeight += 30;
      yellowHeight += 30;

      animateNodeForward(selectedNode, "+=2000", 500);
      growBox(blueHeight, yellowHeight);
    }
  });

  $('.minus').on('click', function () {
    if(backNodes.length > 0) {
      if(backNodes.length == 4) {
       animateElipse($('.ellipse'), 0);
      }

      var selectedNode = backNodes.pop();

      nodes.push(selectedNode);
      blueHeight -= 30;
      yellowHeight -= 30;

      animateNodeBack(selectedNode, "-=2000", 500);
      growBox(blueHeight, yellowHeight);
    }
  });

  //second animation

  var secondNodes = [$('.second-node-one'), $('.second-node-two'), $('.second-node-three'), $('.second-node-four')];
  var secondBackNodes = [];

  $('.second-plus').on('click', function () {
    if(secondNodes.length > 0) {
      if(secondNodes.length == 1) {
        animateElipse($('.ellipse-two'), 1);
      }

      var selectedNode = secondNodes.pop();
      secondBackNodes.push(selectedNode);
      animateNodeForward(selectedNode, "+=1430", 500);

      if(secondBackNodes.length == 1) {
        setTimeout(function () {switchTiles( $('.tiles'), $('.tiles-two') )}, 500);
      } else if (secondBackNodes.length == 2) {
        setTimeout(function () {switchTiles($('.tiles-two'), $('.tiles-three'))}, 500);
      } else if (secondBackNodes.length == 3) {
        setTimeout( function () {switchTiles($('.tiles-three'), $('.tiles-four'))}, 500);
      } else if (secondBackNodes.length == 4) {
        setTimeout( function () {switchTiles($('.tiles-four'), $('.tiles-five'))}, 500);
      }
    }
  });

  $('.second-minus').on('click', function () {
    if(secondBackNodes.length > 0) {
      if(secondBackNodes.length == 4) {
        animateElipse($('.ellipse-two'), 0);
      }

      var selectedNode = secondBackNodes.pop();
      secondNodes.push(selectedNode);
      setTimeout(function () {animateNodeBack(selectedNode, '-=1430', 500)}, 300);

      if(secondNodes.length == 1) {
        switchTiles($('.tiles-five'), $('.tiles-four'));
      } else if (secondNodes.length == 2) {
        switchTiles($('.tiles-four'), $('.tiles-three'));
      } else if (secondNodes.length == 3) {
        switchTiles($('.tiles-three'), $('.tiles-two'));
      } else if (secondNodes.length == 4) {
        switchTiles($('.tiles-two'), $('.tiles'))
      }
    }
  })
});

var switchTiles = function (old, next) {
  setTimeout(next.css('opacity', '1'), 3000);
  setTimeout(old.css('opacity', '0'), 3000);
};

var animateElipse = function (ellipse, opacity) {
  ellipse.animate({opacity: opacity}, 1200);
};

var growBox = function (blueHeight, yellowHeight) {
  $(".blue-curtain").animate({height:blueHeight + 'px'},200);
  $(".yellow-curtain").animate({height: yellowHeight + 'px'},200);
};

var animateNodeForward = function(node, distance, speed) {
  node.animate({
    left: distance
  }, speed, function() {
    // Animation complete.
  });
};

var animateNodeBack= function(node, distance, speed) {
  node.animate({
    left: distance
  }, speed, function() {
    // Animation complete.
  });
};

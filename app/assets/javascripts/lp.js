$(function ()
{
    iframeWindowSize();

    function iframeWindowSize(){
        var IframeHeight = ($('#youtubeFrame').width()/16) * 9;
        $('#youtubeFrame').css('height', IframeHeight);
    }

    $( window ).resize(function() {
        iframeWindowSize();
    });
});
$(function() {
  $(".request-form").fancybox({
    maxWidth: 275,
    maxHeight: 480,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
  });

  MktoForms2.loadForm("//app-ab03.marketo.com", "538-SKP-058", 1024);
});
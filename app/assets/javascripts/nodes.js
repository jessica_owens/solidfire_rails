$(function () {
  var elementPosition = $(' .section-top-right').offset();
  var hidden = true;

  $(window).scroll(function () {
    if ($( window ).width() < 752 ) {
      $('.sub-nav.section.hidden').css('display', 'none');

    } else if ($(window).scrollTop() > elementPosition.top + 500 && hidden == true && $( window ).width() > 752){
      $('.sub-nav.section.hidden').slideToggle();
      hidden = false;

    } else if ($(window).scrollTop() < elementPosition.top + 500 && hidden == false && $( window ).width() > 752) {
      $('.sub-nav.section.hidden').slideToggle();
      hidden = true;
    }
  });

  Slider.init();
});


var Slider = {
  init: function () {
    $.stellar.positionProperty.custom = {
      setTop: function ($el, newTop, originalTop) {

        var wH = $(window).height();
        // console.log($el + ' ' + newTop + ', ' + originalTop);
        if ($el.data('stellar-add-class') !== undefined || $el.data('stellar-remove-class') !== undefined) {
          // console.log($el[0].getBoundingClientRect().top - $(window).height() + ' | ' + parseInt($el.data('stellar-add-class-offset') || '0', 10));

          if ($el[0].getBoundingClientRect().top - $(window).height() < parseInt($el.data('stellar-add-class-offset') || '0', 10)) {
            if ($el.data('stellar-add-class') !== undefined) {
              $el.addClass($el.data('stellar-add-class'));
            } else {
              $el.removeClass($el.data('stellar-remove-class'));
            }
          }
        } else if ($el.data('stellar-moveleft') !== undefined) {
          $el.css({ 'left': Math.max(0, originalTop - newTop) });
        } else if ($el.data('stellar-moveright') !== undefined) {
          $el.css({ 'right': Math.max(0, originalTop - newTop) });
        } else if ($el.data('stellar-toggle-class-on') !== undefined || $el.data('stellar-toggle-class-off') !== undefined) {
          var elTop = $el[0].getBoundingClientRect().top;
          var elHeight = $el.height();
          var topCenter = Math.floor((wH / 2) - (elHeight / 2));
          var stellarToggleOffset = parseInt($el.data('stellar-toggle-class-offset') || '0', 10);
          var toggle = (topCenter - stellarToggleOffset) <= elTop && elTop <= (topCenter + stellarToggleOffset + elHeight);
          if ($el.data('stellar-toggle-class-on') !== undefined) $el.toggleClass($el.data('stellar-toggle-class-on'), toggle);
          if ($el.data('stellar-toggle-class-off') !== undefined) $el.toggleClass($el.data('stellar-toggle-class-off'), !toggle);
        } else if ($el.data('stellar-trigger') !== undefined || $el.data('stellar-toggle-class-off') !== undefined) {
          if ($el[0].getBoundingClientRect().top - $(window).height() < parseInt($el.data('stellar-offset') || '0', 10)) {
            try{
              $el.trigger($el.data('stellar-trigger'));
            } catch(e){
              console.log("Error in trigger:"+$el.attr("id")+" "+$el.attr("class"));
            }
          }
        } else {
          $el.css({ 'top': newTop });
        }
      },

      setLeft: function ($el, newLeft, originalLeft) {
        $el.css('left', newLeft);
      }

    };
    $.stellar({
      hideDistantElements: 0,
      horizontalScrolling: false,
      positionProperty: 'custom'
    });


    var update = function (event, ui) {
      var val = Math.round(ui.value);
      var storage = val * 34.560 / 1024;
      var speed = val * 75 / 1000;
      if (storage < 1) {
        $(ui.handle).find(".top .val").text((storage * 1024).toFixed(0));
        $(ui.handle).find(".top .info").text('TB');
      } else {
        $(ui.handle).find(".top .val").text(storage.toFixed(1));
        $(ui.handle).find(".top .info").text('PB');
      }

      if (speed < 1) {
        $(ui.handle).find(".bottom .val").text((speed * 1000).toFixed(0) + "K");
      } else {
        $(ui.handle).find(".bottom .val").text(speed.toFixed(1) + "M");
      }
    };

    $(".slider-well").slider({
      min: 4,
      max: 100,
      value: 5,
      slide: update,
      change: update,
      create: function (event, ui) {
        $(event.target).find(".ui-slider-handle").html('<span class="top"><span class="val" ></span><span class="info" >PB</span></span><span class="bottom" ><span class="val" ></span><span class="info" >IOPS</span>')
        update(event, { handle: event.target, value: $(event.target).slider("option", "value") });
      }
    }).bind('animate', function () {
      if (!$(this).data("animated")) {
        $(this).data("animated", true);
        $(this).animate({
          value: 50
        }, {
          duration: 1000,
          step: function (now, fx) {
            $(fx.elem).slider({ value: now || 0 });
          }
        });
      }
    });

    var scaleUpdate = function (event, ui) {
      $(ui.handle).css('margin-left', ((ui.value / 3) * 100) + "%");
      setTimeout(function () {
        var val = 0;
        if (ui.value < 1) {
          // 0 - 1  4-20
          val = 4 + (ui.value) * 15;
        } else if (ui.value < 2) {
          // 1 - 2  20-40
          val = 20 + (ui.value - 1) * 20;
        } else {
          // 2 - 3  40-100
          val = 40 + (ui.value - 2) * 60;
        }
        val = Math.round(val);



        storage = val * 8.640 / 1024;
        speed = val * 50 / 1000;

        var displayStorage = function(val){
          if (val < 1024) {
            return val.toFixed(0) + "TB";
          } else {
            return (val/1024).toFixed(1) + "PB";
          }
        };
        var displaySpeed = function(val){
          if (val < 1) {
            return (val * 1000).toFixed(0) + "K";
          } else {
            return val.toFixed(1) + "M";
          }
        };
        var base = 14.465;
        var change =  4.87102;
        var storage2405_5 = base + (val-4) * change;
        var storage4805_5 = base*2 + (val-4) * change*2;
        var storage9000_5 = base*4 + (val-4) * change*4;
        var storage2405_10 = base*2 + (val-4) * change*2;
        var storage4805_10 = base*4 + (val-4) * change*4;
        var storage9000_10 = base*8 + (val-4) * change*8;
        var speed6 = val * 50 / 1000;
        var speed9 = val * 75 / 1000;
        var power3 = val * 0.18;
        var power6 = val * 0.212;
        var power9 = val * 0.3;
        $(ui.handle).find("thead td").text(val + " Nodes");
        $(ui.handle).find(".r1").text(  displayStorage(storage2405_5) );
        $(ui.handle).find(".r2").text(  displayStorage(storage4805_5) );
        $(ui.handle).find(".r3").text(  displayStorage(storage9000_5) );

        $(ui.handle).find(".r1_10").text(  displayStorage(storage2405_10) );
        $(ui.handle).find(".r2_10").text(  displayStorage(storage4805_10) );
        $(ui.handle).find(".r3_10").text(  displayStorage(storage9000_10) );

        $(ui.handle).find(".r4").text(displaySpeed(speed6) );
        $(ui.handle).find(".r5").text(displaySpeed(speed9) );
        $(ui.handle).find(".r6").text(power3.toFixed(power3 < 5 ? 1 : 0) + "kW");
        $(ui.handle).find(".r7").text(power6.toFixed(power6 < 5 ? 1 : 0) + "kW");
        $(ui.handle).find(".r8").text(power9.toFixed(power9 < 5 ? 1 : 0) + "kW");
        $(ui.handle).find(".r9").text(val + "RU");
      }, 10);
    };

    $(".scale-out-slider-well").slider({
      min: 0,
      max: 3,
      value: 0,
      step: 0.01,
      slide: scaleUpdate,
      change: scaleUpdate,
      create: function (event, ui) {
        $(event.target).find(".ui-slider-handle").html(
          '<div class="anchor" ><span class="line"></span></div>' +
          '<div class="table-wrap">' +
          '<table>' +
          '<thead>' +
          '<tr><td>4 Nodes</td></tr>' +
          '</thead>' +
          '<tbody>' +
          '<tr class="sf3 third sp"><td class="r1">240TB</td></tr>' +
          '<tr class="sf6 third"><td class="r2">432TB</td></tr>' +
          '<tr class="sf9 third"><td class="r3">692TB</td></tr>' +

          '<tr class="sf3 third sp"><td class="r1_10">240TB</td></tr>' +
          '<tr class="sf6 third"><td class="r2_10">432TB</td></tr>' +
          '<tr class="sf9 third"><td class="r3_10">692TB</td></tr>' +

          '<tr class="sf6 sp"><td class="r4">1.2IPS</td></tr>' +
          '<tr class="sf9"><td class="r5">692TB</td></tr>' +

          '<tr class="sf3 third sp"><td class="r6">kW</td><//tr>' +
          '<tr class="sf6 third"><td class="r7">kW</td><//tr>' +
          '<tr class="sf9 third"><td class="r8">kW</td></tr>' +

          '<tr class="sf9 third sp"><td class="r9">RU</td></tr>' +
          '</tbody>' +
          '</table>' +
          '</div>');
        scaleUpdate(event, { handle: event.target, value: $(event.target).slider("option", "value") });
      }
    }).bind('animate', function () {
      if (!$(this).data("animated")) {
        $(this).data("animated", true);
        $(this).animate({
          value: 1
        }, {
          duration: 1000,
          step: function (now, fx) {
            $(fx.elem).slider({ value: now || 0 });
          }
        });
      }
    });

    var perfUpdate = function (event, ui) {
      var $slider = $(ui.handle).parent();
      if ($slider.data("dial")) {
        var $dial = $($slider.data("dial"));
        var degree = Math.round((ui.value - 1) * 180);
        // console.log(degree);
        $dial.css({
          '-webkit-transform': 'rotate(' + degree + 'deg)',
          '-moz-transform': 'rotate(' + degree + 'deg)',
          '-ms-transform': 'rotate(' + degree + 'deg)',
          '-o-transform': 'rotate(' + degree + 'deg)',
          'transform': 'rotate(' + degree + 'deg)',
        });
      } else {
        var $fill = $($slider.data("fill"));

        // from  90% to 10%
        var top = Math.round(10 + (1 - ui.value) * 80);
        // console.log(top);
        $fill.css('top', top + '%');
      }
    };

    $(".slider-well-vert").slider({
      min: 0,
      max: 1,
      value: 0.4,
      step: 0.01,
      orientation: "vertical",
      slide: perfUpdate,
      change: perfUpdate,
      create: function (event, ui) {
        perfUpdate(event, { handle: $(event.target).children(), value: $(event.target).slider("option", "value") });
      }
    }).bind('animate', function () {
      if (!$(this).data("animated")) {
        $(this).data("animated", true);
        $(this).animate({
          value: 0.7
        }, {
          duration: 1000,
          step: function (now, fx) {
            $(fx.elem).slider({ value: now || 0 });
          }
        });
      }
    });
  }
};


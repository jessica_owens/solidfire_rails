//= require jquery-ui.min
$(function () {
    createPagination();

    $("#resource-order, #resource-order-final").sortable({
        helper:"clone",
        opacity: 0.6,
        cursor: 'move',
        forcePlaceholderSize:true,
        connectWith: '.list'
    });

    $( "#resource-order, #resource-order-final" ).disableSelection();

    function sortResources() {
        var order = $("ul#resource-order-final").sortable("toArray");
        var orgOrder = $.parseJSON($('.array-holder').attr('data-array-value'));
        var difference = [];

        // compare the array of the original order list to what the final list is
        jQuery.grep(orgOrder, function(el) {
            if (jQuery.inArray(el, order) == -1) difference.push(el);
        });

        // set the resources that were removed from the order list to the default value
        var orgOrderLength = difference.length
        for (var i = 0; i < orgOrderLength; i++) {
            $.ajax({
                url: "/cms/resource-order-update/id/" + difference[i] + "/order/false"
            });
        }

        // save the order of the resources that were selected
        var orderLength = order.length;
        for (var i = 0; i < orderLength; i++) {
           $.ajax({
                url: "/cms/resource-order-update/id/" + order[i] + "/order/" + i
            });
        }

       alert('Order of resources saved.');
       location.reload();
    }

    $('#save-order').click(function(){
        sortResources();
    });

    function setCurrentSort() {
        var order = $("ul#resource-order-final").sortable("toArray");
        $('.array-holder').attr('data-array-value',JSON.stringify(order));
    }

    setCurrentSort();

});

function go_to_page(page_num){
    var show_per_page = parseInt($('#show_per_page').val());

    start_from = page_num * show_per_page;

    end_on = start_from + show_per_page;

    //hide all children elements of content div, get specific items and show them
    $('#resource-order').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

    $('.page-link[longdesc=' + page_num +']').addClass('active-page').siblings('.active-page').removeClass('active-page');

    // update the current page input field
    $('#current_page').val(page_num);

}

function createPagination() {

    var show_per_page = 20;

    var number_of_items = $('.res-item').length;

    var number_of_pages = Math.ceil(number_of_items/show_per_page);

    $('#current_page').val(0);
    $('#show_per_page').val(show_per_page);

    var navigation_html = '';
    var current_link = 0;
    while(number_of_pages > current_link){
        navigation_html += '<a class="page-link" href="javascript:go_to_page(' + current_link +')" longdesc="' + current_link +'">'+ (current_link + 1) +'</a>';
        current_link++;
    }

    $('#page-navigation').html(navigation_html);

    //add active_page class to the first page link
    $('#page-navigation .page-link:first').addClass('active-page');

    //hide all the elements inside content div
    $('#resource-order').children().css('display', 'none');

    //and show the first n (show_per_page) elements
    $('#resource-order').children().slice(0, show_per_page).css('display', 'block');

}
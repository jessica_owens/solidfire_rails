$.ajax({
    url: "/cms/all-resource-images",
    success: function (data) {
        $(".display-images").prepend(data);

        $(function () {
            createPagination('res-image-wrapper','res-image-container',6);
        });
    }
});

$('#meta-tags-button').click(function() {
    $('#meta-tags').toggle();
});

$('#add-image-button').click(function() {
    $('.display-images').toggle();
});

$('#remove-image-button').click(function() {
    $('#cms_resource_custom_image_id').attr('value','');
    $('#cms_resource_custom_image').attr('value','');
    alert('Your custom image will be removed once you click the update resource button.');
});

$("body").on("click", '.image-select', function(){
    var getImageId = $(this).attr('data-image-id');
    var getImageName = $(this).attr('data-image-name');

    $('#cms_resource_custom_image_id').attr('value',getImageId);
    $('#cms_resource_custom_image').attr('value',getImageName);

    alert(getImageName + ' has been added to this resource.');
});

if ($('form').length) {
    $('#new_cms_resource').validate();
}

$(function () {
    createPagination('res-image-wrapper','res-image-container',6);
});

//= require jquery-ui.min
//= require fancybox/jquery.fancybox.pack.js
//= require fancybox/jquery.fancybox-media.js
$(function() {
    showCertainFields();

    if ($('#cms_project_request_graphic_type').length) {
        projectTypeLogic();
        graphicsNeeded();
        collateralNeeded();
    }

    $("#cms_project_request_due_date").datepicker({dateFormat: 'dd-mm-yy'});

    $('select').on('change', function() {
        var findValue = this.value;

        if (findValue == '5595a00b6363330006000000' && !$('.additional-info').show()) {
            $('.additional-info').toggle();
        }
    });

    if ($('#cms_project_request_additional_information').val() != '') {
        $('.additional-info').show();
    }

    if ($('.project-request-index').length) {
        $(document).on("click",".archive-project-request",function(e){
            var getRequestId = $(this).attr('data-pid');

            $.ajax({
                url: '/cms/project-request/archive/' + getRequestId,
                success: function (data) {
                    $('.record-wrapper-ajax-archived, .record-wrapper-ajax-unarchived').empty();
                    $.ajax({
                        url: '/cms/project-request/all-unarchive',
                        success: function (dataUnarc) {
                            $(dataUnarc).prependTo('.record-wrapper-ajax-unarchived');
                        }
                    });
                    $.ajax({
                        url: '/cms/project-request/all-archive',
                        success: function (dataArc) {
                            $(dataArc).prependTo('.record-wrapper-ajax-archived');

                            $(function () {
                                createPagination('record-wrapper-archived','record-wrapper-item-archived',30);
                            });
                        }
                    });
                }
            });
        });

        $(document).on("click",".unarchive-project-request",function(e){
            var getRequestId = $(this).attr('data-pid');

            $.ajax({
                url: "/cms/project-request/unarchive/" + getRequestId,
                success: function (data) {
                    $('.record-wrapper-ajax-archived, .record-wrapper-ajax-unarchived').empty();
                    $.ajax({
                        url: '/cms/project-request/all-unarchive',
                        success: function (dataUnarc) {
                            $(dataUnarc).prependTo('.record-wrapper-ajax-unarchived');
                        }
                    });
                    $.ajax({
                        url: '/cms/project-request/all-archive',
                        success: function (dataArc) {
                            $(dataArc).prependTo('.record-wrapper-ajax-archived');

                            $(function () {
                                createPagination('record-wrapper-archived','record-wrapper-item-archived',30);
                            });
                        }
                    });
                }
            });

        });

        $('.show-archive-button').click(function() {
            $('.archive-section-wrapper').toggle();
            var getVal = $(this).text();

            $(this).text(getVal == 'Show Archived Project Requests' ? 'Hide Archived Project Requests' : 'Show Archived Project Requests');


        });

        // when project requests index is loaded load all open project requests
        $.ajax({
            url: '/cms/project-request/all-unarchive',
            success: function (dataUnarc) {
                $(dataUnarc).prependTo('.record-wrapper-ajax-unarchived');
            }
        });

        // when project requests index is loaded load all archived project requests
        $.ajax({
            url: '/cms/project-request/all-archive',
            success: function (data) {
                $(data).prependTo('.record-wrapper-ajax-archived');

                $(function () {
                    createPagination('record-wrapper-archived','record-wrapper-item-archived',30);
                });
            }
        });
    }

    function projectTypeLogic() {
        $("input[type=radio]").click(function(){$(this).blur();});

        $('select#cms_project_request_type').on('change', function() {
            var findValue = this.value;

            if (findValue == '5595a0546363330006010000') {

                $.ajax({
                    url: "/cms/project-request-item/item/5595a0546363330006010000",
                    success: function (data) {
                        $('.request-items').empty();
                        $(data).hide().prependTo('.request-items').fadeIn( "slow", function() {});
                    }
                });

                $('.web-page-update, .digital-item').show();
                $('.new-page, .new-graphics, .new-collateral, .graphics-collateral, .creative-brief, .file-types').hide();
            }

            if (findValue == '5595a05e6363330009040000') {
                $.ajax({
                    url: "/cms/project-request-item/item/5595a05e6363330009040000",
                    success: function (data) {
                        $('.request-items').empty();
                        $(data).hide().prependTo('.request-items').fadeIn( "slow", function() {});
                    }
                });

                $('.new-page, .digital-item, .creative-brief').show();
                $('.web-page-update, .new-graphics, .new-collateral, .graphics-collateral, .cms_project_request_new_url, .seo-requirements, .new-page, .file-types').hide();
            }

            if (findValue == '5595a0766363330006020000') {
                $.ajax({
                    url: "/cms/project-request-item/item/5595a0766363330006020000",
                    success: function (data) {
                        $('.request-items').empty();
                        $(data).hide().prependTo('.request-items').fadeIn( "slow", function() {});
                    }
                });

                $('.file-types, .creative-brief').show();
            }

        });

        $('.request-items').on('click', '#cms_project_request_item', function() {
            $('select#cms_project_request_item').on('change', function() {
                var findValue = this.value;

                if (findValue == '56d5dc02ae292b0014000000' || findValue == '56d5dc63ae292b0014000001') {
                    $('.cms_project_request_current_url').show();
                    $('.new-page, .other-info').hide();
                }

                if (findValue == '56d5dc2bae292b000a000001' || findValue == '56d5dc47ae292b000a000002') {
                    $('.cms_project_request_current_url, .other-info').hide();
                    $('.new-page').show();
                }

                if (findValue == 1) {
                    $('.other-info').show();
                } else {
                    $('.other-info').hide();
                }
            });
        });
    }

    function showCertainFields() {
        var findValue = $( 'select#cms_project_request_item option:selected' ).val();
        var findValueOther = $( '#cms_project_request_other_info' ).val();


        if (findValueOther != '') {
            $('.other-info').show();
        }

        if (findValue == '56d5dc02ae292b0014000000' || findValue == '56d5dc63ae292b0014000001') {
            $('.cms_project_request_current_url').show();
            $('.new-page').hide();
        }

        if (findValue == '56d5dc2bae292b000a000001' || findValue == '56d5dc47ae292b000a000002') {
            $('.cms_project_request_current_url').hide();
            $('.new-page').show();
        }

        var findValueType = $( 'select#cms_project_request_type option:selected' ).val();

        if (findValueType == '5595a0766363330006020000') {
            $('.file-types, .creative-brief').show();
            $('.collateral-list-full').hide();
        }
    }

    var getSelectedOption = $('#cms_project_request_type option:selected').val();

    if (getSelectedOption == '5595a0996363330009050000') {
        $('.new-graphics').show();
    }

    if (getSelectedOption == '568c0a1d6336390006000000') {
        $('.new-collateral').show();
    }

    if (getSelectedOption == '5595a05e6363330009040000') {
        newWebPageEdit();
    }

    if (getSelectedOption == '5595a0546363330006010000') {
        updateWebPageEdit();
    }

    $('.graphic-options-link').click(function() {
        $('.graphic-list').toggle();
    });

    $('.collateral-options-link').click(function() {
        $('.collateral-list').toggle();
    });

    function updateWebPageEdit() {
        $('.web-page-update, .digital-item').show();

        var getGraphicDirection = $('#cms_project_request_graphics_direction').val();
        var getContentDirection = $('#cms_project_request_content_direction').val();
        var getSeoRequirements = $('#cms_project_request_seo_requirements').val();
        var getGdocGraphics = $('#cms_project_request_google_doc_graphics').val();
        var getFinalContent = $('#cms_project_request_final_content_url').val();

        if (getGraphicDirection != '' ) {
            $('.graphics-direction').show();
            $("#creative-team-graphics").attr('checked', 'checked');
        } else if (getGdocGraphics != '') {
            $('.google-doc-graphics').show();
            $("#final-graphics").attr('checked', 'checked');
        }

        if (getContentDirection != '' ) {
            $('.content-direction').show();
            $("#creative-team-content").attr('checked', 'checked');
        } else if (getFinalContent != '') {
            $('.final-content-url').show();
            $("#final-content").attr('checked', 'checked');
        }

        if (getSeoRequirements != '' ) {
            $('.seo-requirements').show();
            $("#seo-requirements-true").attr('checked', 'checked');
        } else {
            $("#seo-requirements-false").attr('checked', 'checked');
        }

    }

    function newWebPageEdit() {
        $('.new-page, .digital-item').show();

        var getGraphicDirection = $('#cms_project_request_graphics_direction').val();
        var getContentDirection = $('#cms_project_request_content_direction').val();
        var getSeoRequirements = $('#cms_project_request_seo_requirements').val();
        var getGdocGraphics = $('#cms_project_request_google_doc_graphics').val();
        var getFinalContent = $('#cms_project_request_final_content_url').val();

        if (getGraphicDirection != '' ) {
            $('.graphics-direction').show();
            $("#creative-team-graphics").attr('checked', 'checked');
        } else if (getGdocGraphics != '') {
            $('.google-doc-graphics').show();
            $("#final-graphics").attr('checked', 'checked');
        }

        if (getContentDirection != '' ) {
            $('.content-direction').show();
            $("#creative-team-content").attr('checked', 'checked');
        } else if (getFinalContent != '') {
            $('.final-content-url').show();
            $("#final-content").attr('checked', 'checked');
        }

        if (getSeoRequirements != '' ) {
            $('.seo-requirements').show();
            $("#seo-requirements-true").attr('checked', 'checked');
        } else {
            $("#seo-requirements-false").attr('checked', 'checked');
        }

    }

    $('input[name="graphics"]').on('change', function() {
        var findValue = this.value;

        if (findValue == 0) {
            $('.graphics-direction').show();
            $('.google-doc-graphics').hide();
        }

        if (findValue == 1) {
            $('.google-doc-graphics').show();
            $('.graphics-direction').hide();
        }
    });

    $('input[name="content"]').on('change', function() {
        var findValue = this.value;

        if (findValue == 0) {
            $('.content-direction').show();
            $('.final-content-url').hide();
        }

        if (findValue == 1) {
            $('.final-content-url').show();
            $('.content-direction').hide();
        }
    });

    $('input[name="seo"]').on('change', function() {
        var findValue = this.value;

        if (findValue == 0) {
            $('.seo-requirements').show();
        }

        if (findValue == 1) {
            $('#cms_project_request_seo_requirements').val('');
            $('.seo-requirements').hide();
        }
    });

    // graphics needed section
    function graphicsNeeded() {
        // create array of graphics needed

        if ($('#cms_project_request_graphic_type').val() == '') {
            createGraphicsArray = [];
        } else {
            createGraphicsArray = $('#cms_project_request_graphic_type').val().split(",");
        }

        $(".graphics-type").change(function() {
           var getDataValue = $(this).attr('data-graphics');

            if(this.checked) {

                if (getDataValue == 'other') {
                    $('#graphics-type-other').show();
                } else {
                    addToArray(0, getDataValue);
                }

            } else {

                if (getDataValue == 'other') {
                    var getOtherValue = $('#graphics-type-other').val();

                    removeFromArray(0, getOtherValue);

                    $('#graphics-type-other').hide();
                    $('#graphics-type-other').val('');
                } else {

                    removeFromArray(0, getDataValue);
                }
            }

        });

        checkCampaignTeam();
    }

    // collateral needed section
    function collateralNeeded() {
        // create array of graphics needed

        if ($('#cms_project_request_collateral_type').val() == '') {
            createCollateralArray = [];
        } else {
            createCollateralArray = $('#cms_project_request_collateral_type').val().split(",");
        }

        $(".collateral-type").change(function() {
            var getDataValue = $(this).attr('data-collateral');

            if(this.checked) {
              addToArray(1, getDataValue);
            } else {
              removeFromArray(1, getDataValue);
            }
        });

        checkCampaignTeam();
    }

    // check if campaign team has been selected
    function checkCampaignTeam() {
        var getCampaignTeam = $('.graphics-collateral').attr('data-team');
        if (getCampaignTeam == 'yes') {
            $('.graphics-collateral').show();
            $('#campaigns-team-yes').attr('checked', 'checked');
        }

        if (getCampaignTeam == 'no') {
            $('.graphics-collateral').show();
            $('#campaigns-team-no').attr('checked', 'checked');
        }
    }

    function addToArray(getArrayName, getDataValue) {
        if (getArrayName == 0) {
            createGraphicsArray.push(getDataValue);
        }

        if (getArrayName == 1) {
            createCollateralArray.push(getDataValue);
        }
    }

    function removeFromArray(getArrayName, getDataValue) {
        if (getArrayName == 0) {
            var removeItem = createGraphicsArray.indexOf(getDataValue);

            if (removeItem > -1) {
                createGraphicsArray.splice(removeItem, 1);
            }
        }

        if (getArrayName == 1) {
            var removeItem = createCollateralArray.indexOf(getDataValue);

            if (removeItem > -1) {
                createCollateralArray.splice(removeItem, 1);
            }
        }
    }

    function checkForEmptyFields(getFormId) {
        var getProjectRequestType = $('select#cms_project_request_type').val();
        var getProjectRequestItem = $('select#cms_project_request_item').val();
        var getProjectRequestPriority = $('select#cms_project_request_priority').val();
        var getProjectRequestTitle = $('input#cms_project_request_title').val();
        var getProjectRequestDesc = $('textarea#cms_project_request_description').val();
        var getProjectRequestDuedate = $('input#cms_project_request_due_date').val();
        var getProjectRequestPmName = $('input#cms_project_request_pm_name').val();
        var getProjectRequestPmEmail = $('input#cms_project_request_pm_email').val();

        var setErrorValue = '';

        if (getProjectRequestType == '0') {
            setErrorValue = '&#149; You must select a project request type!';
            $('select#cms_project_request_type').addClass('error-red-border');
            checkForErrors('select#cms_project_request_type',0);
        }

        if (getProjectRequestItem == '0') {
            setErrorValue += '&#149; You must select a project request item!';
            $('select#cms_project_request_item').addClass('error-red-border');
            checkForErrors('select#cms_project_request_item',0);
        }

        if (getProjectRequestPriority == '0') {
            setErrorValue += '<br>&#149; You must select a project request priority!';
            $('select#cms_project_request_priority').addClass('error-red-border');
            checkForErrors('select#cms_project_request_priority',0);
        }

        if (getProjectRequestTitle == '') {
            setErrorValue += '<br>&#149; You must create a project request title!';
            $('input#cms_project_request_title').addClass('error-red-border');
            checkForErrors('input#cms_project_request_title',1);
        }

        if (getProjectRequestDesc == '') {
            setErrorValue += '<br>&#149; You must create a project request description!';
            $('textarea#cms_project_request_description').addClass('error-red-border');
            checkForErrors('textarea#cms_project_request_description',1);
        }

        if (getProjectRequestDuedate == '') {
            setErrorValue += '<br>&#149; You must specify a project request due date!';
            $('input#cms_project_request_due_date').addClass('error-red-border');
            checkForErrors('input#cms_project_request_due_date',1);
        }

        if (getProjectRequestPmName == '') {
            setErrorValue += '<br>&#149; You must enter the name of who is requesting this project!';
            $('input#cms_project_request_pm_name').addClass('error-red-border');
            checkForErrors('input#cms_project_request_pm_name',1);
        }

        if (getProjectRequestPmEmail == '') {
            setErrorValue += '<br>&#149; You must enter the email address of the person making the request!';
            $('input#cms_project_request_pm_email').addClass('error-red-border');
            checkForErrors('input#cms_project_request_pm_email',1);
        }

        $.fancybox({
            content : setErrorValue,
            maxWidth: 500,
            fitToView: true,
            width: 400,
            autoSize: false,
            autoHeight: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            padding: 20,
            scrolling: 'no'
        });

        if (getProjectRequestType != '0' && getProjectRequestItem != '0' && getProjectRequestPriority != '0' && getProjectRequestTitle != '' && getProjectRequestDesc != '' && getProjectRequestDuedate != '' && getProjectRequestPmName != '' && getProjectRequestPmEmail != '') {
            $('#' + getFormId).submit();
        }
    }

    function checkForErrors(chk,type) {

        if (type == 1) {
            $(chk).focusout(function () {
                if ($(chk).val() != '' && $(chk).hasClass('error-red-border')) {
                    $(chk).addClass('error-red-border');
                    $(chk).addClass('error-green-border');
                }
            });
        } else {
            $(chk).focusout(function () {
                if ($(chk).val() != 0 && $(chk).hasClass('error-red-border')) {
                    $(chk).addClass('error-red-border');
                    $(chk).addClass('error-green-border');
                }
            });
        }
    }

    $('#submit-project-request').click(function(event) {
        event.preventDefault();

        if ($('#graphics-type-other').val() != '') {
            var getDataValue = $('#graphics-type-other').val();

            addToArray(0, getDataValue);
        }

        $('#cms_project_request_graphic_type').val(createGraphicsArray);

        $('#cms_project_request_collateral_type').val(createCollateralArray);

        var getFormId = $('form').attr('id');

        checkForEmptyFields(getFormId);

    });

});


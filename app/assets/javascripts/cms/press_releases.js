//= require jquery-ui.min

$(function() {
    $( "#cms_press_release_pub_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#cms_press_release_schedule_day" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
//= require jquery-ui.min
//= require jquery.validate.min

$(function() {
    $( "#cms_event_start_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#cms_event_end_date" ).datepicker({ dateFormat: 'dd-mm-yy' });

    $.ajax({
        url: "/cms/all-resource-images",
        success: function (data) {
            $(".display-images").prepend(data);

            $(function () {
                createPagination('res-image-wrapper','res-image-container',6);
            });
        }
    });

    $('#add-image-button').click(function() {
        $('.display-images').toggle();
    });

    $('#remove-image-button').click(function() {
        $('#cms_event_custom_image_id').attr('value','');
        $('#cms_event_custom_image').attr('value','');
        alert('Your custom image will be removed once you click the update event button.');
    });

    $("body").on("click", '.image-select', function(){
        var getImageId = $(this).attr('data-image-id');
        var getImageName = $(this).attr('data-image-name');

        $('#cms_event_custom_image_id').attr('value',getImageId);
        $('#cms_event_custom_image').attr('value',getImageName);

        alert(getImageName + ' has been added to this event.');
    });
});

if ($('form').length) {
    $('.simple_form').validate();
}
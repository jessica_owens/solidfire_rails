$(function(){

//hidden sub-nav

  var hidden = true;

  $(window).scroll(function () {
    if ($( window ).width() < 752 && hidden == false ) {
      $('.hidden-nav').css('display', 'none');
      hidden = true;

    } else if ($( window ).width() >= 752 && $(window).scrollTop() > 400 && hidden == true) {
      $('.hidden-nav').slideToggle();
      hidden = false;

    } else if ($( window ).width() >= 752 && $(window).scrollTop() < 400 && hidden == false) {
      $('.hidden-nav').slideToggle();
      hidden = true;
    }
  });

  //anchor tags

  scrollToAnchor();

  function scrollToAnchor () {
      $(".anchorLink").click(function(e){
        e.preventDefault();

        var id     = $(this).attr("href");
        var offset = $(id).offset();

        $("html, body").animate({
          scrollTop: offset.top
        }, 300);
      });
    }


    $('#q').on('blur',function(){
        $('.header-nav').removeClass('search-active');
    });
    $('.js-q-toggle').on('mousedown',function(event){
        event.preventDefault();var form=$('.header-nav');
        if(!form.hasClass('search-active')){
            setTimeout(function(){document.getElementById('q').focus();
            },50);
            form.addClass('search-active');
        } else {
            form.removeClass('search-active');
        }
    });

    $('form#header-search-form, form#footer-search-form').on('keyup keypress', function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

    $("label[for='top-nav-search']").click(function(){
        var getQuery = $('#query').val();

        if(getQuery == '' || getQuery.replace(/\s/g, "").length == 0 ) {
          //do nothing
        } else {
          var getFormAction = $('form#header-search-form').attr('action');

          $('form#header-search-form').attr('action', getFormAction + getQuery);

          var getFormActionNew = $('form#header-search-form').attr('action');

          window.location = getFormActionNew;
        }
    });

    $('.mobile-menu').click(function(){
        $('body').toggleClass('show-mobile-menu');
        $('.mobile-menu-toggle').toggleClass('active');
        $('footer').toggleClass('active');
        $('.footer-search').toggle();
        $('.as-nav').toggle();

        // scroll to the top of the page when mobile menu is clicked
        window.scrollTo(0, 0);

        if ($('#top-footer-links').css('display') == 'none') {
            $('#top-footer-links').toggle();
        }
    });

    $("label[for='footer-nav-search']").click(function(){

        var getQuery = $('#query-footer').val();

        if(getQuery == '' || getQuery.replace(/\s/g, "").length == 0) {
          //do nothing
        } else {
          var getFormAction = $('form#footer-search-form').attr('action');

          $('form#footer-search-form').attr('action', getFormAction + getQuery);

          var getFormActionNew = $('form#footer-search-form').attr('action');

          window.location = getFormActionNew;
        }
    });

    $('.mkto-lightbox-form').click(function() {
        var marketoFormNumber  = $('.mkto-lightbox-form').data('mkto');
        if(!marketoFormNumber){
            marketoFormNumber = 2131;
        }

        MktoForms2.loadForm("//app-ab03.marketo.com", "538-SKP-058", marketoFormNumber, function (form){MktoForms2.lightbox(form).show();});
    });

    if ($('.social_icons_vertical').length) {
        // social media code resources

        var longURL = window.location.href;
        var pageTitle = $(document).find("title").text();
        pageTitle = pageTitle.replace("| SolidFire", " ");
        pageTitle = pageTitle.replace(/&/g, "and");
        var fbDevID = '482267448591251'; // facebook app id
        var fbID = '482266351924694'; // facebook app id


        var googleURL;
        jQuery.urlShortener.settings.apiKey = 'AIzaSyCrDrjE5yChRc3BcSbXf1mvFwbzvBGvTog';
        var shortURL = jQuery.urlShortener({
            longUrl: longURL,
            success: function (shortURL) {
                googleURL = shortURL;
                $('#social_twitter').attr('href', 'https://twitter.com/home?status=' + pageTitle + '%20' + googleURL + '%20%40solidfire');
                $('#social_linkedin').attr('href', 'https://www.linkedin.com/shareArticle?mini=true&url=' + googleURL + '&title=' + 'SolidFire - ' + pageTitle + '&summary=&source=');
                $('#social_google').attr('href', 'https://plus.google.com/share?url=' + googleURL);
                $('#social_facebook').attr('href','https://www.facebook.com/sharer/sharer.php?u=' + googleURL);

            }
        });
    }

    $("a.youtube-video-fancybox").click(function () {
        if ($(window).width() > 600) {
            $.fancybox({
                'padding': 10,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'width': 560,
                'height': 340,
                'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                'type': 'swf',
                'swf': {
                    'wmode': 'transparent',
                    'allowfullscreen': 'true'
                }
            });

            return false;
        } else {
            $(this).attr("target", "_blank");
        }
    });

    /* $(".fancyboxIframe").fancybox({
     type: "iframe",
     scrolling: "yes",
     height: "100%"
     }); */

    $('.youtube-video:nth-child(odd)').addClass('odd');
    if ($('html').is('.ie8')) {
        $('.large-news-ribbon a:last-child').css('margin-right', 0);
        $('.pagelet-table .wrapper .pagelet-side:nth-child(2)').css('padding-left', '30px');
        $('.pagelet.shift-content-down .wrapper .row:last-child').css('margin-top', '70px');
    }

    function calculateAngleHeights() {
        $("[class^='angle-section-'] [class^='section-top-']").each(function( index ) {
            $(".angle-section-" + index + " [class^='section-under-']").css('height', $( this ).height());
        });
    }

    // JS for angled sections
    if ($("[class^='angle-section-']").length) {
        calculateAngleHeights();
    }

    $( window ).resize(function() {
        if ($("[class^='angle-section-']").length) {
            calculateAngleHeights();
        }
    });
});

// modal window for Youtube videos
$('.fancybox-media').fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
    maxWidth  : 640,
    maxHeight : 390,
    fitToView : false,
    width   : '70%',
    height    : '70%',
    autoSize  : false,
    closeClick  : false,
    openEffect  : 'fade',
    closeEffect : 'fade',
    helpers : {
        media : {}
    },
    afterLoad  :  function(){
        onPlayerStateChange();
    }
});

function onPlayerStateChange() {
    mktoMunchkin("538-SKP-058");
    mktoMunchkinFunction('visitWebPage', {
        url: '/{{my.videoplayURL:default=DefaultVideo}}/play', params: 'x=y&2=3'
    });
}

// create ajax pagination
function go_to_page(page_num,parentDiv){
    var show_per_page = parseInt($('#show_per_page').val());

    start_from = page_num * show_per_page;

    end_on = start_from + show_per_page;

    //hide all children elements of content div, get specific items and show them
    $('.' + parentDiv).children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

    $('.page-link[longdesc=' + page_num +']').addClass('active-page').siblings('.active-page').removeClass('active-page');

    // update the current page input field
    $('#current_page').val(page_num);

    if($('.card-wrapper').length) {
        masonry_calls();
    }

}

function createPagination(parentDiv,childDiv,pageResults) {

    var show_per_page = pageResults;

    var number_of_items = $('.' + childDiv).length;

    var number_of_pages = Math.ceil(number_of_items/show_per_page);

    $('#current_page').val(0);
    $('#show_per_page').val(show_per_page);

    var navigation_html = '';
    var current_link = 0;
    while(number_of_pages > current_link){
        navigation_html += '<a class="page-link" href="javascript:go_to_page(' + current_link + ',' + '\'' + parentDiv + '\'' + ')" longdesc="' + current_link +'">'+ (current_link + 1) +'</a>';
        current_link++;
    }

    $('#page-navigation').html(navigation_html);

    //add active_page class to the first page link
    $('#page-navigation .page-link:first').addClass('active-page');

    //hide all the elements inside content div
    $('.' + parentDiv).children().css('display', 'none');

    //and show the first n (show_per_page) elements
    $('.' + parentDiv).children().slice(0, show_per_page).css('display', 'block');

}

function masonry_calls() {
    var container = document.querySelector('.card-wrapper');
    var msnry = new Masonry( container, {
        columnWidth: 80,
        itemSelector: '.card-container',
        isAnimated: true
    });

    msnry.layout();
}

// New header actions
/*$('.header__container--burger-menu--image').click(function() {
 $('.header__container--burger-menu--rollover-box').toggle();
 });*/

$('.header__container--burger-menu--image').on('click tap', function (event) {
    event.stopPropagation();
    var getElePosition = $('.header__container .header__container--burger-menu').css('textAlign');

    if (getElePosition == 'left') {
        $('.header__container--burger-menu--rollover-box--mobile').fadeToggle();
    } else {
        $('.header__container--burger-menu--rollover-box').fadeToggle();
    }


    //$('.header__container--burger-menu--rollover-box').toggleClass('open');
});

/*$(document).click( function(){

 //alert($(this).parent());

 if ($('.header__container--burger-menu--rollover-box').hasClass('open') && $(this).parent().is('.header__container--burger-menu--rollover-box')) {
 $('.header__container--burger-menu--rollover-box').hide();
 //alert($(this).attr('class'));
 $('.header__container--burger-menu--rollover-box').removeClass('open');
 }
 });*/

$(document).click(function(event) {

    if ( !$(event.target).closest( ".header__container--burger-menu--rollover-box" ).length ) {
        $('.header__container--burger-menu--rollover-box').hide();
    }

    if ( !$(event.target).closest( ".country" ).length ) {
        $('.country-list').hide();
    }

});

// Fix subnav on scroll
var elementPosition = $('.sub-navigation').offset();
if (elementPosition && !$('.sub-navigation').hasClass('fixed')) {
    $(window).scroll(function () {
        if ($(window).scrollTop() > elementPosition.top - 100) {
            $('.sub-navigation').css('position', 'fixed').css('top', '100px');
        } else {
            $('.sub-navigation').css('position', 'static');
        }
    });
}

$('.expand-nav').on('click tap', function () {
    var getDataSubSection = $(this).attr('data-sub-section');
    var setString  = '.' + getDataSubSection;

    $('.rollover-box--mobile-sub-nav' + setString).fadeToggle();
    $('.minus-close' + setString + ', .plus-expand' + setString).toggle();
});

$('.country').on('click tap', function () {
    $('.country-list').toggle();
});

$(".header-demo").fancybox({
    maxWidth: 330,
    maxHeight: 555,
    fitToView: true,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 0,
    scrolling: 'no'
});






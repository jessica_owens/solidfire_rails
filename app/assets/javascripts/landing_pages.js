//= require jquery.easing.1.3
//= require jquery.anyslider
//= require jquery-ui.min
$(function () {
    //checks for the top div so that it only will run on the openstack summit vancouver page
    if ($('.slider-wrapper').length) {

        // load the slider
        $('.slider1').anyslider({
            easing: 'swing',
            interval: 8000,
            keyboard: false,
            speed: 200
        });

        var target, scroll;

        $("a[href*=#]:not([href=#])").on("click", function (e) {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                target = $(this.hash);
                target = target.length ? target : $("[id=" + this.hash.slice(1) + "]");

                if (target.length) {
                    if (typeof document.body.style.transitionProperty === 'string') {
                        e.preventDefault();

                        var avail = $(document).height() - $(window).height();

                        scroll = target.offset().top;

                        if (scroll > avail) {
                            scroll = avail;
                        }
                        if ($(window).width() <= 768) {
                            if ($('#opestack__summit__vancouver__navigation').position().top >= 10) {
                                scroll -= 150;
                            }
                            else {
                                scroll -= 70;
                            }
                        }
                        else {
                            if ($('#opestack__summit__vancouver__navigation').position().top >= 10) {
                                scroll -= 70;
                            }
                        }

                        $("html").css({
                            "margin-top": ( $(window).scrollTop() - scroll ) + "px",
                            "transition": ".75s ease-in-out"
                        }).data("transitioning", true);
                    } else {
                        $("html, body").animate({
                            scrollTop: scroll
                        }, 1000);
                        return;
                    }
                }

            }
        });

        $("html").on("transitionend webkitTransitionEnd msTransitionEnd oTransitionEnd", function (e) {
            if (e.target == e.currentTarget && $(this).data("transitioning") === true) {
                $(this).removeAttr("style").data("transitioning", false);
                $("html, body").scrollTop(scroll);
                return;
            }
        });

        $(window).scroll(navigation_sticky);
        navigation_sticky();

        $(window).resize(resize_header);
        resize_header();
    }

    if($('#net_app_lp').length)
    MktoForms2.loadForm("//app-ab03.marketo.com", "538-SKP-058", 2170, function(form){
        //Add an onSuccess handler
        form.onSuccess(function(values, followUpUrl){
            //Take the lead to a different page on successful submit, ignoring the form's configured followUpUrl.
            location.href = 'http://www.solidfire.com/lp/five-ways-your-storage-vendor-is-costing-you-more-than-you-think-thank-you';
            //return false to prevent the submission handler continuing with its own processing
            return false;
        });
    });
});


function navigation_sticky() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#opestack__summit__vancouver__navigation').addClass('stick');

        $('section').each(function(i) {
            if ($(this).position().top <= window_top + 70) {
                $('.nav-section').removeClass('active');
                $('.nav-section').eq(i).addClass('active');
            }
        });

        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            $('.nav-section').removeClass('active');
            $('.last').addClass('active');

        }
    }
    else {
        $('#opestack__summit__vancouver__navigation').removeClass('stick');
        $('.nav-section').removeClass('active');
    }
}

function resize_header(){
    var header_height = $('.take-your-cloud__slide-wrapper').height();
    if(window.outerWidth < 470){
        $('.slider-wrapper').height(header_height + 231);
    }
    else if(window.outerWidth < 790) {
        $('.slider-wrapper').height(header_height + 201);
    }
    else{
        $('.slider-wrapper').height(header_height + 151);
    }
}



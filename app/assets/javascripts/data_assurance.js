$(function () {

  //animations

  var greyBarAnimationPending = true;
  var nodesAnimationPending = true;
  var yellowGraphAnimationPending = true;
  var redGraphAnimationPending = true;
  var windowVar = $(window);

  windowVar.scroll(function () {

    if (greyBarAnimationPending && isScrolledIntoView($('#grey-bar-animation'))) {
      greyBarAnimationPending = false;
      greyBar.showLetters();
    }

    if (nodesAnimationPending && isScrolledIntoView($('.nodes'))) {
      nodesAnimationPending = false;
      animateNode($( "#node-two" ));

      setTimeout(function() {
        animateNode($( "#node-one" ))
      }, 500);
    }

    if (yellowGraphAnimationPending && isScrolledIntoView($('.yellow-graph'))) {
      yellowGraphAnimationPending = false;
      $('.yellow-graph-topper').animate({
        opacity: 1
      }, 1000);
    }

    if (redGraphAnimationPending && isScrolledIntoView($('.red-graph'))) {
      redGraphAnimationPending = false;
      $('.red-graph-topper').animate({
        opacity: 1
      }, 1000);
    }
  });

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }
});

var animateNode = (function(node) {
  node.animate({
    left: "-=1000"
  }, 500, function() {
    // Animation complete.
  });
});

var animateGraph = (function(graph) {
  graph.toggle( "slide" );
});

var greyBar = (function () {
  var letters, index, selector, timeout;

  letters = [$(".a"), $(".b"), $(".c"), $(".d")];
  index = 0;

  function incrementIndex () {
    index += 1;
  }

  return {
    showLetters: function () {
      var interval = window.setInterval(function(){

        if (index <= 3) {
          selector = letters[index];
          selector.children().first().animate({'opacity': '1'}, 500);
          selector.children().last().animate({'opacity': '1'}, 500);
          incrementIndex();
        } else {
          window.clearInterval(interval)
        }

      }, 500);
    }
  }
})();
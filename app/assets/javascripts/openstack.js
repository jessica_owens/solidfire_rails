$(function () {
  var windowVar = $(window);
  var useCasesId, integrationsId, resourcesId, openstackId, useCasesClass, integrationsClass, resourcesClass, openstackClass;
  useCasesId = $('#use-cases');
  integrationsId = $('#integrations');
  resourcesId = $('#resources');
  openstackId = $('#openstack');

  useCasesClass = $('.use-cases-link');
  integrationsClass = $('.integrations-link');
  resourcesClass = $('.resources-link');
  openstackClass = $('.openstack-link');


  windowVar.scroll(function () {
    if (needsHighlight(useCasesId)) {
      useCasesClass.css('color', '#CC1F26').attr('highlighted', true);
      openstackClass.css('color', '#A9A9A9').attr('highlighted', false);
      integrationsClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (needsHighlight(integrationsId)) {
      integrationsClass.css('color', '#CC1F26').attr('highlighted', true);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);
      openstackClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (needsHighlight(resourcesId)) {
      resourcesClass.css('color', '#CC1F26').attr('highlighted', true);
      integrationsClass.css('color', '#A9A9A9').attr('highlighted', false);
      openstackClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (needsHighlight(openstackId)) {
      openstackClass.css('color', '#CC1F26').attr('highlighted', true);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
    }
  });

  function needsHighlight(elem) {
    return notHighlighted(elem) && isScrolledIntoView(elem)
  }

  function notHighlighted(elem) {
    return elem.attr('highlighted') != 'true';
  }

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }
});


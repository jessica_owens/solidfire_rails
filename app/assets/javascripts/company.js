$(function () {
    if ($('.month-header').length) {
        $('.events-item__description >  p').not(":first-child").hide();

        $('a.event-item').click(function () {
            var getDataId = $(this).attr('data-id');

            var getVal = $('#' + getDataId).text();

            $('.' + getDataId + '.events-item__description >  p').not(":first-child").slideToggle();

            $('#' + getDataId).text(getVal == 'More Details' ? 'Close' : 'More Details');
        });

        $('.view-old-events, .view-old-events-top').click(function () {
            $('.past-events').slideToggle();
            $('.events-archive-header').slideToggle();

            if ($('.view-old-events').text() == 'close past events') {
                $('.view-old-events').text('view past events');
            } else {
                $('.view-old-events').text('close past events');
            }

            if ($('.view-old-events-top').text() == 'close past events') {
                $('.view-old-events-top').text('view past events');
            } else {
                $('.view-old-events-top').text('close past events');
            }
        });

        $('a.more-link').click(function () {
            var getVal = $(this).text();

            $(this).text(getVal == 'Close' ? 'Close' : 'More Details');
        });
    }

    function getFullMonthName(month) {
        if(month == 01) {
            return monthName = 'January';
        } else if (month == 02) {
            return monthName = 'February';
        } else if (month == 03) {
            return monthName = 'March';
        } else if (month == 04) {
            return monthName = 'April';
        } else if (month == 05) {
            return monthName = 'May';
        } else if (month == 06) {
            return monthName = 'June';
        } else if (month == 07) {
            return monthName = 'July';
        } else if (month == 08) {
            return monthName = 'August';
        } else if (month == 09) {
            return monthName = 'September';
        } else if (month == 10) {
            return monthName = 'October';
        } else if (month == 11) {
            return monthName = 'November';
        } else if (month == 12) {
            return monthName = 'December';
        }
    }

    if ($('.month-header').length) {
        $('.month-header').each(function (i) {
            var dataDate = $(this).attr('data-date');

            var getElementCl = $('.' + dataDate).text();

            var dateVar = getElementCl;
            var dsplit = dateVar.split("-");
            var d = dsplit[0];
            var month = dsplit[1];

            var twoDigitYear = d.replace('20', '');

            $('.' + dataDate).text(getFullMonthName(month) + ' ' + twoDigitYear);

        });
    }
});

$(".request-demo-link").fancybox({
    maxWidth: 330,
    maxHeight: 575,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});
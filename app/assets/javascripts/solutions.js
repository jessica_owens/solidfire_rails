$(function () {
    $("#smallScale").click(function(){ services('smallScale'); return false; })
    $("#ease").click(function(){ services('ease'); return false; })
    $("#vendorLockin").click(function(){ services('vendorLockin'); return false; })
    $("#flexibility").click(function(){ services('flexibility'); return false; })
    $("#largeScale").click(function(){ services('largeScale'); return false; })
    var clicks = 0;
    var firstClick;
    var secondClick;
    var backgroundColor = '#D8D8D8';
    function services(selection)
    {
        clicks++;
        if(clicks ==3)
        {
            clicks = 1;
            $('.selectable').css('background-color', '#D8D8D8');
            $( '#smallScale' ).css('cursor', 'pointer');
            $( '#largeScale' ).css('cursor', 'pointer');
            $('#continuum .col3').css('background-color', '#77787B');
        }

        if(clicks <= 2) {
            $('#' + selection).css('background-color', '#8AC06D'); // changes button color to green
            if (clicks == 1) {
                firstClick = selection;
                if (firstClick == 'smallScale') {
                    $("#largeScale").css('background-color', '#fff').css('cursor', 'default');
                }
                if (firstClick == 'largeScale') {
                    $("#smallScale").css('background-color', '#fff').css('cursor', 'default');
                }
                switch (selection) {
                    case 'smallScale':
                    case 'ease':
                        $('.bestOfBreed').css('background-color', backgroundColor);
                        $('.softwareCommodity').css('background-color', backgroundColor);
                        break;
                    case 'vendorLockin':
                    case 'flexibility':
                    case 'largeScale':
                        $('.asService').css('background-color', backgroundColor);
                        $('.hyperConverged').css('background-color', backgroundColor);
                        break;
                }
            }
            else {
                secondClick = selection;
                if (firstClick == 'smallScale' || firstClick == 'ease') {
                    if (secondClick == 'smallScale' || secondClick == 'ease') {
                        $('.convergedInfrastructure').css('background-color', backgroundColor);
                    }
                    else {
                        $('.asService').css('background-color', backgroundColor);
                    }
                }
                else {
                    if (secondClick == 'smallScale' || secondClick == 'ease') {
                        $('.softwareCommodity').css('background-color', backgroundColor);
                    }
                    else {

                        $('.convergedInfrastructure').css('background-color', backgroundColor);
                    }
                }

                if ($('#largeScale').css('background-color') == "rgb(255, 255, 255)") {
                    $('#largeScale').css('background-color', backgroundColor)
                }
                if ($('#smallScale').css('background-color') == "rgb(255, 255, 255)") {
                    $('#smallScale').css('background-color', backgroundColor)
                }
            }
        }
    }
});

$(".openstack-cisco-link").fancybox({
    maxWidth: 330,
    maxHeight: 365,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".openstack-dell-link").fancybox({
    maxWidth: 330,
    maxHeight: 365,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".vexpert-form-lunch").fancybox({
    maxWidth: 330,
    maxHeight: 425,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".vexpert-form-dinner").fancybox({
    maxWidth: 275,
    maxHeight: 475,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".lunch-n-learn").fancybox({
    maxWidth: 330,
    maxHeight: 475,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".confidential").fancybox({
    maxWidth: 330,
    maxHeight: 490,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".lunch-n-learn-vmware").fancybox({
    maxWidth: 330,
    maxHeight: 485,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".government").fancybox({
    maxWidth: 330,
    maxHeight: 490,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});

$(".breakfast-channel").fancybox({
    maxWidth: 330,
    maxHeight: 540,
    fitToView: true,
    width: '70%',
    height: '78%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'no'
});







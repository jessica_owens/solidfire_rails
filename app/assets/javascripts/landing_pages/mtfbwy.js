
$(".confidential").fancybox({
    maxWidth: 600,
    maxHeight: 700,
    fitToView: true,
    width: '90%',
    height: '90%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    padding: 20,
    scrolling: 'yes'
});
$(function () {
  $('#home-card-one, #home-card-two, #home-card-three').flip({
    trigger: 'hover'
  });

  setTimeout( function () {
    $('#red-overlay-fade-out').fadeToggle(500, 'linear');
  }, 100);

  setTimeout( function () {
    $('.fade-in-three').css('opacity', '1');
  }, 100)
});
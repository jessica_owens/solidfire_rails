$(function () {
  $('#card-one, #card-two, #card-three, #card-four, #card-five, #card-six').flip({
    trigger: 'hover'
  });

  var windowVar = $(window);
  var successId, useCasesId, fueledId, resourcesId,
      successClass, useCasesClass, fueledClass, resourcesClass;

  successId = $('#success');
  useCasesId = $('#success-use-cases');
  fueledId = $('#fueled');
  resourcesId = $('#success-resources');

  successClass = $('.success-stories-link');
  useCasesClass = $('.use-cases-link');
  fueledClass = $('.fueled-link');
  resourcesClass = $('.resources-link');

  windowVar.scroll(function () {
    if (notHighlighted(successId) && isScrolledIntoView(successId)) {
      successClass.css('color', '#CC1F26').attr('highlighted', true);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
      fueledClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(useCasesId) && isScrolledIntoView(useCasesId)) {
      useCasesClass.css('color', '#CC1F26').attr('highlighted', true);
      successClass.css('color', '#A9A9A9').attr('highlighted', false);
      fueledClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(fueledId) && isScrolledIntoView(fueledId)) {
      fueledClass.css('color', '#CC1F26').attr('highlighted', true);
      useCasesClass.css('color', '#A9A9A9').attr('highlighted', false);
      resourcesClass.css('color', '#A9A9A9').attr('highlighted', false);
      successClass.css('color', '#A9A9A9').attr('highlighted', false);

    } else if (notHighlighted(resourcesId) && isScrolledIntoView(resourcesId)) {
      resourcesClass.css('color', '#CC1F26').attr('highlighted', true);
      fueledClass.css('color', '#A9A9A9').attr('highlighted', false);
      successClass.css('color', '#A9A9A9').attr('highlighted', false);
    }
  });

  function notHighlighted(elem) {
    return elem.attr('highlighted') != 'true';
  }

  function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }
});

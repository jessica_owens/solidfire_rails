class Cms::ResourceOrderController < ApplicationController
  before_filter :check_login

  layout 'cms'

  def index
    @cms_resources = Cms::Resource.where(order_id: '').order_by(:id.desc)

    @cms_resources_order_list = Cms::Resource.where(:order_id.ne => '').order_by(:order_id => :asc)
  end

  def update_order

    get_param_id = params[:id]
    get_order_number = params[:order]

    Cms::Resource.where(id: get_param_id).update(order_id: get_order_number)

    render layout: false
  end
end

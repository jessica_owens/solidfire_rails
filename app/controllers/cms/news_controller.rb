class Cms::NewsController < ApplicationController
  before_action :set_cms_news, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560083a26562370006010000' }

  layout 'cms'

  # GET /cms/news
  def index
    @cms_news = Cms::News.all.order_by(:pub_date.desc).paginate(:page => params[:page], :per_page => 30)
  end

  # GET /cms/news/1
  def show
  end

  # GET /cms/news/new
  def new
    @cms_news = Cms::News.new
  end

  # GET /cms/news/1/edit
  def edit
  end

  # POST /cms/news
  def create

    params[:cms_news]

    url = Googl.shorten(params[:cms_news][:url], request.remote_ip, "AIzaSyCrDrjE5yChRc3BcSbXf1mvFwbzvBGvTog")

    params[:cms_news][:url_shorten] =  url.short_url

    @cms_news = Cms::News.new(cms_news_params)

    if @cms_news.save
      redirect_to @cms_news, notice: 'News was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/news/1
  def update
    params[:cms_news]

    url = Googl.shorten(params[:cms_news][:url], request.remote_ip, "AIzaSyCrDrjE5yChRc3BcSbXf1mvFwbzvBGvTog")

    params[:cms_news][:url_shorten] =  url.short_url

    if @cms_news.update(cms_news_params)
      redirect_to @cms_news, notice: 'News was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/news/1
  def destroy
    @cms_news.destroy
    redirect_to cms_news_index_url, notice: 'News was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_news
      @cms_news = Cms::News.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_news_params
      params.require(:cms_news).permit(:title, :url, :url_shorten, :pub_date, :publication)
    end
end

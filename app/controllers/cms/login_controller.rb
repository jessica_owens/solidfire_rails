class Cms::LoginController < ApplicationController
  layout 'cms'

  def index
    @user = Cms::User.new

    if (cookies[:cms_login])
      get_user_key
    end

    if (cookies[:cms_login] && @user_key.user_key == @get_login_key_value)
      redirect_to cms_home_index_path
    end

  end

  def check_login

    @user = Cms::User.where(email: params[:cms_user][:email]).first

    if @user && @user.authenticate(params[:cms_user][:password])

      @hash_random = Digest::SHA1.hexdigest(@user.id.to_s + Time.now.to_s)

      setup_login_array = {
          'id' => @user.id.to_s,
          'key' => @hash_random
      }

      encodeJson = setup_login_array.to_json
      cookies[:cms_login]={:value => Base64.strict_encode64(encodeJson)}

      update_user_key(@user.id.to_s,@hash_random)

      redirect_section = cms_home_index_path
    end

    if (!@user || !@user.authenticate(params[:cms_user][:password]))
      flash[:error] = 'The email address and password that you entered do not match!'
      redirect_section = cms_login_index_path
    end

    if (cookies[:cms_login] && @user.user_key == @get_login_key_value)
      redirect_section = cms_home_index_path
    end

    redirect_to redirect_section

  end

  def logout
    if @user = Cms::User.where({id: '55088b5f6a76656217000000'}).update(:user_key => '')
      cookies.delete :cms_login
    end

    flash[:success] = 'You have successfully logged out!'
    redirect_to cms_login_index_path
  end

  def update_user_key(uid,ukey)
    Cms::User.where({id: uid}).update(:user_key => ukey)
  end

end

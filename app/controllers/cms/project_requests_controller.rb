class Cms::ProjectRequestsController < ApplicationController
  before_action :set_cms_project_request, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560083f2656237000d040000' }

  layout 'cms'

  # GET /cms/project_requests
  def index

  end

  # GET /cms/project_requests/1
  def show
  end

  # GET /cms/project_requests/new
  def new
    @cms_project_request = Cms::ProjectRequest.new
    @cms_project_request_status = Cms::ProjectRequestStatus.all
    @cms_project_request_types = Cms::ProjectRequestType.all
    @cms_project_request_items = Cms::ProjectRequestItem.all
  end

  # GET /cms/project_requests/1/edit
  def edit
    @cms_project_request_status = Cms::ProjectRequestStatus.all
    @cms_project_request_types = Cms::ProjectRequestType.all

    if @cms_project_request.graphic_type
      @get_graphic = @cms_project_request.graphic_type.split(',')
    end

    if @cms_project_request.collateral_type
      @get_collateral = @cms_project_request.collateral_type.split(',')
    end

    find_status_type = Cms::ProjectRequest.where({id: params[:id]})

    find_status_type.each do |stat_type|
      unless stat_type.status.nil?
        @cms_status_find = Cms::ProjectRequestStatus.where({id: stat_type.status})
      end

      unless stat_type.type.nil?
        @cms_type_find = Cms::ProjectRequestType.where({id: stat_type.type})
      end

      unless stat_type.item.nil?
        @cms_item_find = Cms::ProjectRequestItem.where({id: stat_type.item})
        @cms_project_request_items = Cms::ProjectRequestItem.where({type_id: stat_type.type})
      end
    end

    @is_edit = true
  end

  # POST /cms/project_requests
  def create
    @cms_project_request = Cms::ProjectRequest.new(cms_project_request_params)

    if @cms_project_request.save

      cms_status_find = Cms::ProjectRequestStatus.where({id: @cms_project_request[:status]})

      cms_status_find.each do |status|
        @get_status_name = status.name
      end

      emails_to_send_pm = @cms_project_request.pm_email.split(',')
      emails_to_send_client = @cms_project_request.client_email.split(',')

      get_project_request_type = @cms_project_request[:type]

      (@cms_project_request[:type] == '5595a0766363330006020000' ? @graphic_design = 'design' : @graphic_design = '')

      (@cms_project_request.due_date != '' ?  @due_date = @cms_project_request.due_date : @due_date = 'No due date provided')

      subject_title = 'SolidFire Project Request Created: ' + @cms_project_request.title

      template = ERB.new(File.read('app/views/emails/new-project-request.html.erb')).result(binding)

      require 'mail'

      Mail.defaults do
        delivery_method :smtp, { :address   => "smtp.sendgrid.net",
                                 :port      => 587,
                                 :domain    => "solidfire.com",
                                 :user_name => "jon.verson@solidfire.com",
                                 :password  => "SolidFire303!",
                                 :authentication => 'plain',
                                 :enable_starttls_auto => true }
      end

      mail = Mail.deliver do
        if get_project_request_type == '5595a0766363330006020000'
          to 'patrick.hunt@solidfire.com'
        else
          to  emails_to_send_pm.each { |x| puts x + ',' } + emails_to_send_client.each { |x| puts x + ',' }
        end
        cc 'todd.may@solidfire.com, jon.verson@solidfire.com, patrick.hunt@solidfire.com'
        from 'ng-sf-creative-services@netapp.com'
        subject subject_title
        html_part do
          content_type 'text/html; charset=UTF-8'
          body template
        end
      end
      mail.to_s

      redirect_to @cms_project_request, notice: 'Project request was successfully created.'
    else
      render :new
    end
  end

  def archive
    get_pr_id = params[:id]

    if @pr = Cms::ProjectRequest.where({id: get_pr_id}).update(:archive => 1)

      @message = get_pr_id +  ' was marked as archived!'
      render layout: false
    else
      @message = 'something went wrong!'
    end
  end

  def unarchive
    get_pr_id = params[:id]

    if @pr = Cms::ProjectRequest.where({id: get_pr_id}).update(:archive => 0)

      @message = get_pr_id +  ' was removed from archived!'
      render layout: false
    else
      @message = 'something went wrong!'
    end
  end

  def all_unarchive
    @cms_project_requests = Cms::ProjectRequest.where(:archive.ne => 1).order_by(:submit_date.desc )

    cms_project_requests = Cms::ProjectRequestStatus.all

    @cms_status_find = []

    cms_project_requests.each do |request|
      @cms_status_find <<  {:id => request.id.to_s, :name => request.name}
    end

    render layout: false
  end

  def all_archive
    @cms_project_requests_archive = Cms::ProjectRequest.where(archive: 1).order_by(:submit_date.desc )

    cms_project_requests = Cms::ProjectRequestStatus.all

    @cms_status_find = []

    cms_project_requests.each do |request|
      @cms_status_find <<  {:id => request.id.to_s, :name => request.name}
    end

    render layout: false
  end

  # PATCH/PUT /cms/project_requests/1
  def update
    if @cms_project_request.update(cms_project_request_params)

      cms_status_find = Cms::ProjectRequestStatus.where({id: params[:cms_project_request][:status]})

      cms_status_find.each do |status|
        @get_status_name = status.name
      end

      emails_to_send_pm = @cms_project_request.pm_email.split(',')
      emails_to_send_client = @cms_project_request.client_email.split(',')

      (@cms_project_request.status != '5595a00b6363330006000000' ? @add_info = '' : @add_info = '<p style="color:#000000;"><strong>Additional information needed:</strong> ' + @cms_project_request.additional_information + '</p>')

      subject_title = 'SolidFire Project Request Update: ' + @cms_project_request.title

      template = ERB.new(File.read('app/views/emails/updated-project-request.html.erb')).result(binding)

      require 'mail'

      Mail.defaults do
        delivery_method :smtp, { :address   => "smtp.sendgrid.net",
                                 :port      => 587,
                                 :domain    => "solidfire.com",
                                 :user_name => "jon.verson@solidfire.com",
                                 :password  => "SolidFire303!",
                                 :authentication => 'plain',
                                 :enable_starttls_auto => true }
      end

      mail = Mail.deliver do
        to  emails_to_send_pm.each { |x| puts x + ',' } + emails_to_send_client.each { |x| puts x + ',' }
        cc 'todd.may@solidfire.com, jon.verson@solidfire.com, patrick.hunt@solidfire.com'
        from 'ng-sf-creative-services@netapp.com'
        subject subject_title
        html_part do
          content_type 'text/html; charset=UTF-8'
          body template
        end
      end
      mail.to_s

      @check_jira_id = Cms::ProjectRequest.find(params[:id])

      if (params[:cms_project_request][:status] == '5595a0186363330009010000')
        if (!@check_jira_id.jira_id?)
          project_request_fields(1,1)
          #create_jira_story(title_item, description_item, more_info, due_date_format, pm_name)
        end
      end

      redirect_to @cms_project_request, notice: 'Project request was successfully updated.'
    else
      render :edit
    end
  end

  def project_request_fields(jira,update_document)
    title_item = params[:cms_project_request][:title]
    description = params[:cms_project_request][:description]
    more_info = params[:cms_project_request][:additional_information]
    due_date = params[:cms_project_request][:due_date]
    pm_name = params[:cms_project_request][:pm_name]

    current_url = params[:cms_project_request][:current_url]
    new_url = params[:cms_project_request][:new_url]
    graphics_direction = params[:cms_project_request][:graphics_direction]
    graphics_direction_url = params[:cms_project_request][:google_doc_graphics]
    content_direction = params[:cms_project_request][:content_direction]
    final_content_url = params[:cms_project_request][:final_content_url]
    seo_requirements = params[:cms_project_request][:seo_requirements]
    priority = params[:cms_project_request][:priority]
    url_examples = params[:cms_project_request][:url_examples]
    graphic_type = params[:cms_project_request][:graphic_type]
    collateral_type = params[:cms_project_request][:collateral_type]

    (current_url != '' ? current_url = 'Current URL: ' + params[:cms_project_request][:current_url] : current_url = '')
    (new_url != '' ? new_url = 'Requested URL: ' + params[:cms_project_request][:new_url] : new_url = '')
    (graphics_direction != '' ? graphics_direction = 'Graphics direction: ' + params[:cms_project_request][:graphics_direction] : graphics_direction = '')
    (graphics_direction_url != '' ? graphics_direction_url = 'Graphics URL: ' + params[:cms_project_request][:google_doc_graphics] : graphics_direction_url = '')
    (content_direction != '' ? content_direction = 'Content direction: ' + params[:cms_project_request][:content_direction] : content_direction = '')
    (final_content_url != '' ? final_content_url = 'Final Content URL: ' + params[:cms_project_request][:final_content_url] : final_content_url = '')
    (seo_requirements != '' ? seo_requirements = 'SEO Requirements: ' + params[:cms_project_request][:seo_requirements] : seo_requirements = '')
    (priority != '' ? priority = 'Priority: ' + params[:cms_project_request][:priority] : priority = '')
    (url_examples != '' ? url_examples = 'URL examples: ' + params[:cms_project_request][:url_examples] : url_examples = '')
    (graphic_type != '' ? graphic_type = 'Graphic content: ' + params[:cms_project_request][:graphic_type] : graphic_type = '')
    (collateral_type != '' ? collateral_type = 'Collateral type: ' + params[:cms_project_request][:collateral_type] : collateral_type = '')

    if due_date != ''
      due_date_format = Date.parse(due_date).strftime("%m/%d/%Y")
    else
      due_date_format = 'No due date submitted'
    end

    description_item =  description + "\n\n" + current_url + "\n\n" + new_url + "\n\n" + graphics_direction + "\n\n" + graphics_direction_url + "\n\n" + content_direction + "\n\n" + final_content_url + "\n\n" + seo_requirements + "\n\n" + priority + "\n\n" + url_examples + "\n\n" + graphic_type + "\n\n" + collateral_type

    if jira == 1
      create_jira_story(title_item, description_item, more_info, due_date_format, pm_name, update_document)
    end
  end

  def create_jira_story(title, description, more_info, due_date, pm_name, update_document)
    require 'uri'
    require 'net/http'
    require 'net/https'
    require 'json'

    @toSend = {
        :fields => {
            :project =>
                {
                    :id => "10901"
                },
            :summary => title,
            :description => description + "\n\n" + more_info + "\n\nDue date: " + due_date + "\n\nRequested by: " + pm_name,
            :issuetype => {
                :id => "10001"
            }
        }
    }.to_json

    uri = URI.parse("https://solidfire.atlassian.net/rest/api/2/issue/")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
    req.basic_auth("webteam.app", "SolidFire1738!")
    req.body = @toSend
    res = https.request(req)

    result = res.body
    set_response = JSON.parse(result)

    @get_jira_id = set_response['key']

    if update_document == 1
      Cms::ProjectRequest.find(params[:id]).update(jira_id: @get_jira_id)
    end
  end

  # DELETE /cms/project_requests/1
  def destroy
    @cms_project_request.destroy
    redirect_to cms_project_requests_url, notice: 'Project request was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_project_request
      @cms_project_request = Cms::ProjectRequest.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_project_request_params
      params.require(:cms_project_request).permit(:title, :description, :due_date, :pm_name, :pm_email, :client_email, :additional_information, :status, :type, :submit_date, :archive, :jira_id, :current_url, :new_url, :graphics_direction, :google_doc_graphics, :content_direction, :final_content_url, :seo_requirements, :priority, :url_examples, :collateral_type, :graphic_type, :campaigns_team, :audience, :graphic_guidelines, :creative_direction, :inspiration, :file_size, :bleed, :intended_use, :submit_final, :item, :other_info)
    end

end

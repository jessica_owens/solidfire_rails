class Cms::LandingPagesController < ApplicationController
  before_action :set_cms_landing_page, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560083b7656237000d020000' }

  layout 'cms'

  # GET /cms/landing_pages
  def index
    @cms_landing_pages = Cms::LandingPage.all.order_by(:id.desc).paginate(:page => params[:page], :per_page => 30)
  end

  # GET /cms/landing_pages/1
  def show
  end

  # GET /cms/landing_pages/new
  def new
    @cms_landing_page = Cms::LandingPage.new
    @cms_layout_templates = Cms::LayoutTemplate.all
  end

  # GET /cms/landing_pages/1/edit
  def edit
    @cms_layout_templates = Cms::LayoutTemplate.all

    find_landing = Cms::LandingPage.where({slugs: params[:id]})

    find_landing.each do |layout|
      unless layout.layout_template.nil?
        @cms_layout_find = Cms::LayoutTemplate.where({id: layout.layout_template})
      end
    end

    @is_edit = true
  end

  # POST /cms/landing_pages
  def create
    @cms_landing_page = Cms::LandingPage.new(cms_landing_page_params)

    if @cms_landing_page.save
      redirect_to @cms_landing_page, notice: 'Landing page was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/landing_pages/1
  def update
    if @cms_landing_page.update(cms_landing_page_params)
      redirect_to @cms_landing_page, notice: 'Landing page was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/landing_pages/1
  def destroy
    @cms_landing_page.destroy
    redirect_to cms_landing_pages_url, notice: 'Landing page was successfully destroyed.'
  end

  def copy_document
    @cms_landing_page = Cms::LandingPage.find(params[:id])

    Cms::LandingPage.create(
        title: @cms_landing_page.title + ' new',
        landing_title: @cms_landing_page.landing_title,
        sub_title: @cms_landing_page.sub_title,
        description: @cms_landing_page.description,
        asset_link: @cms_landing_page.asset_link,
        mkto_form_id: @cms_landing_page.mkto_form_id,
        utube_id: @cms_landing_page.utube_id,
        meta_title: @cms_landing_page.meta_title,
        meta_description: @cms_landing_page.meta_description,
        meta_no_follow: @cms_landing_page.meta_no_follow,
        thank_you_url: @cms_landing_page.thank_you_url,
        layout_template: @cms_landing_page.layout_template,
        custom_image: @cms_landing_page.custom_image,
        custom_image_id: @cms_landing_page.custom_image_id,
        form_description: @cms_landing_page.form_description,
        form_message: @cms_landing_page.form_message,
        login: @cms_landing_page.login,
        vidyard_id: @cms_landing_page.vidyard_id
    )

    redirect_to cms_landing_pages_url

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_landing_page
      @cms_landing_page = Cms::LandingPage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_landing_page_params
      params.require(:cms_landing_page).permit(:title, :description, :thank_you_url, :landing_title, :sub_title, :asset_link, :mkto_form_id, :utube_id, :custom_image, :custom_image_id, :layout_template, :meta_title, :meta_description, :form_description, :form_message, :meta_no_follow, :login, :vidyard_id)
    end
end
class Cms::HomeController < Cms::BaseController
  before_filter :check_login
  before_filter :get_cms_login_details

  layout 'cms'

  def index

    if (@user.is_admin == 1)
      available_sections = Cms::Section.all
      @non_admin_sections = []
      @admin_sections = []
      @resource_sections = []

      available_sections.each do |section|
        if section.title == "Project Request Items" || section.title == "CMS Sections" || section.title == "Users" || section.title == "Resource Types" || section.title == "Resource Categories" || section.title =="Project Request Status" || section.title == "Project Request Types"
          @admin_sections << section
        else
          find_resource_and_non_admin_sections(section)
        end
      end

    else
      unless @user.sections.nil?
        available_sections = Cms::Section.find(@user.sections)
        @non_admin_sections = []
        @resource_sections = []

        available_sections.each do |section|
          find_resource_and_non_admin_sections(section)
        end
      end
    end
  end

  private

  def find_resource_and_non_admin_sections(section)
    if section.title == 'Resources' || section.title == 'Resource Images'
      @resource_sections << section
    elsif section.title == 'Project Request'
      @project_request_section = section
    else
      @non_admin_sections << section
    end
  end
end
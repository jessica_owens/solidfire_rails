class Cms::ProjectRequestItemsController < ApplicationController
  before_action :set_cms_project_request_item, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560083f2656237000d040000' }

  layout 'cms'

  # GET /cms/project_request_items
  def index
    @cms_project_request_items = Cms::ProjectRequestItem.all
  end

  # GET /cms/project_request_items/1
  def show
  end

  # GET /cms/project_request_items/new
  def new
    @cms_project_request_item = Cms::ProjectRequestItem.new
  end

  def load_items

    @get_type_id = params[:id]

    @cms_project_request_items = Cms::ProjectRequestItem.where({type_id: @get_type_id})

    render layout: false
  end

  # GET /cms/project_request_items/1/edit
  def edit
  end

  # POST /cms/project_request_items
  def create
    @cms_project_request_item = Cms::ProjectRequestItem.new(cms_project_request_item_params)

    if @cms_project_request_item.save
      redirect_to @cms_project_request_item, notice: 'Project request item was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/project_request_items/1
  def update
    if @cms_project_request_item.update(cms_project_request_item_params)
      redirect_to @cms_project_request_item, notice: 'Project request item was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/project_request_items/1
  def destroy
    @cms_project_request_item.destroy
    redirect_to cms_project_request_items_url, notice: 'Project request item was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_project_request_item
      @cms_project_request_item = Cms::ProjectRequestItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_project_request_item_params
      params.require(:cms_project_request_item).permit(:name, :type_id)
    end
end

class Cms::EventsController < ApplicationController
  before_action :set_cms_event, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560083d7656237000d030000' }

  layout 'cms'

  # GET /cms/events
  def index
    @cms_events = Cms::Event.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /cms/events/1
  def show
  end

  # GET /cms/events/new
  def new
    @cms_event = Cms::Event.new
  end

  # GET /cms/events/1/edit
  def edit
  end

  # POST /cms/events
  def create
    @cms_event = Cms::Event.new(cms_event_params)

    if @cms_event.save
      redirect_to @cms_event, notice: 'Event was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/events/1
  def update
    if @cms_event.update(cms_event_params)
      redirect_to @cms_event, notice: 'Event was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/events/1
  def destroy
    @cms_event.destroy
    redirect_to cms_events_url, notice: 'Event was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_event
      @cms_event = Cms::Event.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_event_params
      params.require(:cms_event).permit(:title, :description, :start_date, :end_date, :location, :cta_title, :cta_url, :custom_image, :custom_image_id)
    end
end

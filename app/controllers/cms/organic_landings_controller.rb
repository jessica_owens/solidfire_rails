class Cms::OrganicLandingsController < ApplicationController
  before_action :set_cms_organic_landing, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560083846562370006000000' }

  layout 'cms'

  # GET /cms/organic_landings
  def index
    @cms_organic_landings = Cms::OrganicLanding.all
  end

  # GET /cms/organic_landings/1
  def show
  end

  # GET /cms/organic_landings/new
  def new
    @cms_organic_landing = Cms::OrganicLanding.new
  end

  # GET /cms/organic_landings/1/edit
  def edit
  end

  # POST /cms/organic_landings
  def create
    @cms_organic_landing = Cms::OrganicLanding.new(cms_organic_landing_params)

    if @cms_organic_landing.save
      redirect_to @cms_organic_landing, notice: 'Organic landing was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/organic_landings/1
  def update
    if @cms_organic_landing.update(cms_organic_landing_params)
      redirect_to @cms_organic_landing, notice: 'Organic landing was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/organic_landings/1
  def destroy
    @cms_organic_landing.destroy
    redirect_to cms_organic_landings_url, notice: 'Organic landing was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_organic_landing
      @cms_organic_landing = Cms::OrganicLanding.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_organic_landing_params
      params.require(:cms_organic_landing).permit(:title, :sub_title, :cta_one, :cta_link_one, :cta_two, :cta_link_two, :cta_three, :cta_link_three, :body, :meta_title, :meta_description)
    end
end

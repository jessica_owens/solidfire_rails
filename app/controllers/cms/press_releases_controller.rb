class Cms::PressReleasesController < ApplicationController
  before_action :set_cms_press_release, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '5600832f656237000d010000' }

    layout 'cms'

  # GET /cms/press_releases
  def index
    @cms_press_releases = Cms::PressRelease.all.order_by(:pub_date.desc).paginate(:page => params[:page], :per_page => 30)
  end

  # GET /cms/press_releases/1
  def show
  end

  # GET /cms/press_releases/new
  def new
    @cms_press_release = Cms::PressRelease.new
  end

  # GET /cms/press_releases/1/edit
  def edit
  end

  # POST /cms/press_releases
  def create

    params[:cms_press_release]

    build_short_url = params[:cms_press_release][:title].to_url

    url = Googl.shorten('http://www.solidfire.com/press-releases/' + build_short_url, request.remote_ip, "AIzaSyCrDrjE5yChRc3BcSbXf1mvFwbzvBGvTog")

    params[:cms_press_release][:short_url] = url.short_url

    params[:cms_press_release][:scheduled_release] = params[:cms_press_release][:schedule_day] + ' ' + params[:cms_press_release][:schedule_time]

    @cms_press_release = Cms::PressRelease.new(cms_press_release_params)

    if @cms_press_release.save
      redirect_to @cms_press_release, notice: 'Press release was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/press_releases/1
  def update
    params[:cms_press_release]

    build_short_url = params[:cms_press_release][:title].to_url

    url = Googl.shorten('http://www.solidfire.com/press-releases/' + build_short_url, request.remote_ip, "AIzaSyCrDrjE5yChRc3BcSbXf1mvFwbzvBGvTog")

    params[:cms_press_release][:short_url] = url.short_url

    params[:cms_press_release][:scheduled_release] = params[:cms_press_release][:schedule_day] + ' ' + params[:cms_press_release][:schedule_time]

    if @cms_press_release.update(cms_press_release_params)
      redirect_to @cms_press_release, notice: 'Press release was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/press_releases/1
  def destroy
    @cms_press_release.destroy
    redirect_to cms_press_releases_url, notice: 'Press release was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_press_release
      @cms_press_release = Cms::PressRelease.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_press_release_params
      params.require(:cms_press_release).permit(:title, :sub_title, :body, :location, :about, :short_url, :pub_date, :scheduled_release, :schedule_time, :schedule_day)
    end
end

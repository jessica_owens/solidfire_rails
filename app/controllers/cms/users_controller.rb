class Cms::UsersController < ApplicationController
  before_action :set_cms_user, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '5600851c656237000d050000' }

  layout 'cms'

  # GET /cms/users
  def index
    @cms_users = Cms::User.all

  end

  # GET /cms/users/1
  def show
  end

  # GET /cms/users/new
  def new
    @cms_sections = Cms::Section.all

    @cms_user = Cms::User.new
  end

  # GET /cms/users/1/edit
  def edit
    @is_edit = true

    @cms_sections = Cms::Section.all

    user = Cms::User.where({id: params[:id]})

    user.each do |item|
      unless item.sections.nil?
        @available_sections = Cms::Section.find(item.sections)
      end
    end

    (@cms_user.is_admin == 1 ?  @set_check = true :  @set_check = false)
  end

  # POST /cms/users
  def create
    @cms_user = Cms::User.new(cms_user_params)

    if @cms_user.save
      redirect_to @cms_user, notice: 'User was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/users/1
  def update
    if @cms_user.update(cms_user_params)
      redirect_to @cms_user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/users/1
  def destroy
    @cms_user.destroy
    redirect_to cms_users_url, notice: 'User was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_user
      @cms_user = Cms::User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_user_params
      params.require(:cms_user).permit(:email, :password, :is_admin, :user_key, :reset_sent_at, :reset_digest, :sections => [])
    end
end

class Cms::ResourceImagesController < ApplicationController
  before_action :set_cms_resource_image, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560085556562370006030000' }

  layout 'cms'

  # GET /cms/resource_images
  def index
    @cms_resource_images = Cms::ResourceImage.order_by(:id.desc).paginate(:page => params[:page], :per_page => 30)

    @cms_resource_images.each do |image|

      custom_image_details(image.id)

    end

  end

  # GET /cms/resource_images/1
  def show
  end

  # GET /cms/resource_images/new
  def new
    @cms_resource_image = Cms::ResourceImage.new
  end

  # GET /cms/resource_images/1/edit
  def edit

    @is_edit = true

    @find_image = Cms::ResourceImage.where({id: params[:id]})

    @find_image.each do |image|

      @removeFileExtension = image.res_image_file_name[0..-5]

      if image.res_image_content_type == 'image/png'
        @add_extension = '.png'
      else
        @add_extension = '.jpg'
      end
    end

    @random_asset_number = (0..3).to_a.sample.to_s

  end

  # POST /cms/resource_images
  def create
    @cms_resource_image = Cms::ResourceImage.new(cms_resource_image_params)

    if @cms_resource_image.save
      redirect_to @cms_resource_image, notice: 'Resource image was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/resource_images/1
  def update
    if @cms_resource_image.update(cms_resource_image_params)
      redirect_to @cms_resource_image, notice: 'Resource image was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/resource_images/1
  def destroy
    @cms_resource_image.destroy
    redirect_to cms_resource_images_url, notice: 'Resource image was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_resource_image
      @cms_resource_image = Cms::ResourceImage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_resource_image_params
      params.require(:cms_resource_image).permit(:title, :res_image)
    end
end

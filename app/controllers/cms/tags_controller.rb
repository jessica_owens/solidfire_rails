class Cms::TagsController < ApplicationController
  before_action :set_cms_tag, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '56008595656237000d060000' }

  layout 'cms'

  # GET /cms/tags
  def index
    @cms_tags = Cms::Tag.all
  end

  # GET /cms/tags/1
  def show
  end

  # GET /cms/tags/new
  def new
    @cms_tag = Cms::Tag.new
  end

  # GET /cms/tags/1/edit
  def edit
  end

  # POST /cms/tags
  def create
    @cms_tag = Cms::Tag.new(cms_tag_params)

    if @cms_tag.save
      redirect_to @cms_tag, notice: 'Tag was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/tags/1
  def update
    if @cms_tag.update(cms_tag_params)
      redirect_to @cms_tag, notice: 'Tag was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/tags/1
  def destroy
    @cms_tag.destroy
    redirect_to cms_tags_url, notice: 'Tag was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_tag
      @cms_tag = Cms::Tag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_tag_params
      params.require(:cms_tag).permit(:title)
    end
end

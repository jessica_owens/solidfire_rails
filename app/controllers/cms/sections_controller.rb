class Cms::SectionsController < ApplicationController
  before_action :set_cms_section, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '5600840f6562370006020000' }

  layout 'cms'

  # GET /cms/sections
  def index
    @cms_sections = Cms::Section.all
  end

  # GET /cms/sections/1
  def show
  end

  # GET /cms/sections/new
  def new
    @cms_section = Cms::Section.new
  end

  # GET /cms/sections/1/edit
  def edit
  end

  # POST /cms/sections
  def create
    @cms_section = Cms::Section.new(cms_section_params)

    if @cms_section.save
      redirect_to @cms_section, notice: 'Section was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/sections/1
  def update
    if @cms_section.update(cms_section_params)
      redirect_to @cms_section, notice: 'Section was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/sections/1
  def destroy
    @cms_section.destroy
    redirect_to cms_sections_url, notice: 'Section was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_section
      @cms_section = Cms::Section.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_section_params
      params.require(:cms_section).permit(:title, :url)
    end
end

class Cms::ProjectRequestStatusesController < ApplicationController
  before_action :set_cms_project_request_status, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560086006562370006040000' }

  layout 'cms'

  # GET /cms/project_request_statuses
  def index
    @cms_project_request_statuses = Cms::ProjectRequestStatus.all
  end

  # GET /cms/project_request_statuses/1
  def show
  end

  # GET /cms/project_request_statuses/new
  def new
    @cms_project_request_status = Cms::ProjectRequestStatus.new
  end

  # GET /cms/project_request_statuses/1/edit
  def edit
  end

  # POST /cms/project_request_statuses
  def create
    @cms_project_request_status = Cms::ProjectRequestStatus.new(cms_project_request_status_params)

    if @cms_project_request_status.save
      redirect_to @cms_project_request_status, notice: 'Project request status was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/project_request_statuses/1
  def update
    if @cms_project_request_status.update(cms_project_request_status_params)
      redirect_to @cms_project_request_status, notice: 'Project request status was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/project_request_statuses/1
  def destroy
    @cms_project_request_status.destroy
    redirect_to cms_project_request_statuses_url, notice: 'Project request status was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_project_request_status
      @cms_project_request_status = Cms::ProjectRequestStatus.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_project_request_status_params
      params.require(:cms_project_request_status).permit(:name)
    end
end

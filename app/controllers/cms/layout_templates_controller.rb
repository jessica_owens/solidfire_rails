class Cms::LayoutTemplatesController < ApplicationController
  before_action :set_cms_layout_template, only: [:show, :edit, :update, :destroy]
  before_filter :check_login

  layout 'cms'

  # GET /cms/layout_templates
  def index
    @cms_layout_templates = Cms::LayoutTemplate.all
  end

  # GET /cms/layout_templates/1
  def show
  end

  # GET /cms/layout_templates/new
  def new
    @cms_layout_template = Cms::LayoutTemplate.new
  end

  # GET /cms/layout_templates/1/edit
  def edit
  end

  # POST /cms/layout_templates
  def create
    @cms_layout_template = Cms::LayoutTemplate.new(cms_layout_template_params)

    if @cms_layout_template.save
      redirect_to @cms_layout_template, notice: 'Layout template was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/layout_templates/1
  def update
    if @cms_layout_template.update(cms_layout_template_params)
      redirect_to @cms_layout_template, notice: 'Layout template was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/layout_templates/1
  def destroy
    @cms_layout_template.destroy
    redirect_to cms_layout_templates_url, notice: 'Layout template was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_layout_template
      @cms_layout_template = Cms::LayoutTemplate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_layout_template_params
      params.require(:cms_layout_template).permit(:name, :layout_file)
    end
end

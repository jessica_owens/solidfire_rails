class Cms::ResourcesController < ApplicationController
  before_action :set_cms_resource, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '56008317656237000d000000' }

  layout 'cms'

  # GET /cms/resources
  def index
    @cms_resources = Cms::Resource.all.order_by(:id.desc).paginate(:page => params[:page], :per_page => 30)

    #@cms_resources = Cms::Resource.where({tag_ids: params[:tag]})

    #render :text =>@cms_resources.inspect

  end

  def all_resource_images
    @cms_resource_images = Cms::ResourceImage.all.order_by(:id.desc)

    @random_asset_number = (0..3).to_a.sample.to_s


    render layout: false
  end

  # GET /cms/resources/1
  def show
  end

  # GET /cms/resources/new
  def new
    @cms_resource = Cms::Resource.new
    @cms_tags = Cms::Tag.all
    @cms_cats = Cms::Category.all

    @is_edit = false
  end

  # GET /cms/resources/1/edit
  def edit
    @cms_tags = Cms::Tag.all
    @cms_cats = Cms::Category.all

    Cms::ResourceImage

    find_resource = Cms::Resource.where({slugs: params[:id]})

    find_resource.each do |cat|
      unless cat.cats.nil?
        @cms_cats_find = Cms::Category.find(cat.cats)
      end
    end

    find_resource.each do |tag|
      unless tag.tags.nil?
        @cms_tags_find = Cms::Tag.find(tag.tags)
      end
    end

    @is_edit = true
  end

  # POST /cms/resources
  def create
    #@post = Post.find(params[:post_id])
    #@comment = @post.comments.create!(params[:comment])

    getParms = params[:cms_resource]

    #render :text => getParms.inspect

    @cms_resource = Cms::Resource.new(cms_resource_params)

    if @cms_resource.save
      redirect_to @cms_resource, notice: 'Resource was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/resources/1
  def update
    if @cms_resource.update(cms_resource_params)
      redirect_to @cms_resource, notice: 'Resource was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/resources/1
  def destroy
    @cms_resource.destroy
    redirect_to cms_resources_url, notice: 'Resource was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cms_resource
    @cms_resource = Cms::Resource.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def cms_resource_params
    params.require(:cms_resource).permit(:title, :landing_title, :sub_title, :description, :landing_description, :asset_link, :mkto_form_id, :utube_id, :meta_title, :meta_description, :custom_image, :order_id, :custom_image_id, :thank_you_url, :podcast_url, :vidyard_id, :cats => [], :tags => [])
  end

end
class Cms::PasswordResetsController < ApplicationController
  before_action :get_user,   only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]

  layout 'cms'

  def new
    @user = Cms::User.new
  end

  def create
    @user = Cms::User.where(email: params[:password_reset][:email]).first
    if @user
      token = @user.create_reset_token.downcase
      digest = @user.digest(token)

      result = @user.update!({reset_digest: digest, reset_sent_at: Time.zone.now})

      if result

        require 'sendgrid-ruby'

        client = SendGrid::Client.new(api_user: 'jon.verson@solidfire.com', api_key: 'SolidFire303!')

        mail = SendGrid::Mail.new do |m|
          m.to = @user.email
          m.from = 'webteam@solidfire.com'
          m.subject = 'SolidFire CMS password reset'
          m.html = '<html>
                      <body>
                        <div style="max-width:980px;font-family:sans-serif, Arial, Helvetica;font-size:18px;line-height:24px;margin:0 auto;border-top:solid 4px #9d1b1b;color:#000000;">
                          <div style="background-color:#313131;text-align:center;width:100%;">
                            <img src="https://s3-us-west-2.amazonaws.com/solidfire-static/emails/images/%402x-solidfire-logo.png" style="padding:25px 20px 28px 20px;max-width:356px;width:80%;">
                          </div>
                          <div style="float:left;width:90%;padding:50px 20px;">
                            <p style="color:#000000;">To reset your password click the link below:</p>
                            <p style="color:#000000;">' + edit_cms_password_reset_url(token, email: @user.email) + '</p>
                            <p style="color:#000000;">If you did not request your password to be reset, please ignore this email and your password will stay as it is. </p>
                          </div>
                        </div>
                      </body>
                    </html>'
        end
        client.send(mail)

        flash[:info] = "An email with password reset instructions has been sent"
        redirect_to cms_login_index_path
      else
        flash[:error] = "An error has occurred.  Please contact your system administrator."
        redirect_to new_cms_password_reset_path
      end

    else
      flash[:error] = "Email address not found"
      redirect_to new_cms_password_reset_path
    end
  end

  def update
    if params[:cms_user][:password].empty?
      @user.errors.add(:password, "can't be empty")
      render 'edit'
    elsif @user.update(password: params[:cms_user][:password], password_confirmation: params[:cms_user][:password])
      flash[:success] = "Password has been reset."
      redirect_to cms_login_index_path
    else
      render 'edit'
    end
  end

  def edit

  end

  private

  def get_user
    @user = Cms::User.where(email: params[:email]).first
  end

  # Confirms a valid user.
  def valid_user
    unless (@user && @user.authenticated?(:reset, params[:id]))
      redirect_to cms_login_index_path
    end
  end
end

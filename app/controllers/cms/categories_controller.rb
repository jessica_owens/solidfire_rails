class Cms::CategoriesController < ApplicationController
  before_action :set_cms_category, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560085b9656237000d070000' }

  layout 'cms'

  # GET /cms/categories
  def index
    @cms_categories = Cms::Category.all
  end

  # GET /cms/categories/1
  def show
  end

  # GET /cms/categories/new
  def new
    @cms_category = Cms::Category.new
  end

  # GET /cms/categories/1/edit
  def edit
  end

  # POST /cms/categories
  def create
    @cms_category = Cms::Category.new(cms_category_params)

    if @cms_category.save
      redirect_to @cms_category, notice: 'Category was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/categories/1
  def update
    if @cms_category.update(cms_category_params)
      redirect_to @cms_category, notice: 'Category was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/categories/1
  def destroy
    @cms_category.destroy
    redirect_to cms_categories_url, notice: 'Category was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_category
      @cms_category = Cms::Category.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_category_params
      params.require(:cms_category).permit(:title)
    end
end

class Cms::ProjectRequestTypesController < ApplicationController
  before_action :set_cms_project_request_type, only: [:show, :edit, :update, :destroy]
  before_filter :check_login
  before_filter { |c| c.check_access '560086286562370006050000' }

  layout 'cms'

  # GET /cms/project_request_types
  def index
    @cms_project_request_types = Cms::ProjectRequestType.all
  end

  # GET /cms/project_request_types/1
  def show
  end

  # GET /cms/project_request_types/new
  def new
    @cms_project_request_type = Cms::ProjectRequestType.new
  end

  # GET /cms/project_request_types/1/edit
  def edit
  end

  # POST /cms/project_request_types
  def create
    @cms_project_request_type = Cms::ProjectRequestType.new(cms_project_request_type_params)

    if @cms_project_request_type.save
      redirect_to @cms_project_request_type, notice: 'Project request type was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cms/project_request_types/1
  def update
    if @cms_project_request_type.update(cms_project_request_type_params)
      redirect_to @cms_project_request_type, notice: 'Project request type was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cms/project_request_types/1
  def destroy
    @cms_project_request_type.destroy
    redirect_to cms_project_request_types_url, notice: 'Project request type was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_project_request_type
      @cms_project_request_type = Cms::ProjectRequestType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cms_project_request_type_params
      params.require(:cms_project_request_type).permit(:name)
    end
end

class PlatformController < ApplicationController
  def platform_overview
    set_meta_tags :title => 'All-Flash Storage Platform Overview | SolidFire',
                  :description => "SolidFire delivers the most complete range of all-flash storage features combined with advanced capabilities no other storage can match.",
                  :canonical => "http://www.solidfire.com/platform"
  end

  def hardware
    set_meta_tags :title => "SF Series All-Flash Storage Appliances | SolidFire",
                  :description => "Scale-out storage appliances for confidently running thousands of multiple, mixed workloads on a single system. Learn about SolidFire SF Series Nodes.",
                  :canonical => "http://www.solidfire.com/platform/hardware"
  end

  def product_specifications
    set_meta_tags :title => "Storage Platform Product Specifications | SolidFire",
                  :description => "Discover what your infrastructure can do with optimally balanced storage performance and capacity. See product specifications.",
                  :canonical => "http://www.solidfire.com/platform/product-specifications"

  end

  def resources

  end

  def support
    set_meta_tags :title => 'Active Support | SolidFire',
                  :description => "SolidFire Active Support continuously monitors your systems to ensure the highest possible level of availability and performance. Learn more.",
                  :canonical => "http://www.solidfire.com/platform/support"
  end

  def automated_management
    set_meta_tags :title => 'Automated Management for Storage Infrastructure | SolidFire',
                  :description => "SolidFire's management frameworks provide a robust REST-based API capable of automating every aspect of your storage infrastructure.",
                  :canonical => "http://www.solidfire.com/platform/element-os/automated-management"
  end

  def data_assurance
    set_meta_tags :title => 'High Availability Storage and Data Protection | SolidFire',
                  :description => "Maintain your data with high availability, data protection, and complete security, architected specifically for all-flash storage infrastructures.",
                  :canonical => "http://www.solidfire.com/platform/element-os/data-assurance"
  end

  def element_os_resources
    set_meta_tags :title => 'Element OS Resources | SolidFire',
                  :description => "Learn more about SolidFire Element OS. Access videos, webinars, datasheets, whitepapers, analyst reports and more.",
                  :canonical => "http://www.solidfire.com/platform/element-os/resources"
  end

  def elements_os
    set_meta_tags title: 'Element OS - Storage Operating System | SolidFire',
                  description: 'Learn more about SolidFire Element OS. Access videos, webinars, datasheets, whitepapers, analyst reports and more.',
                  :canonical => "http://www.solidfire.com/platform/element-os"
  end

  def global_efficiencies
    set_meta_tags :title => "Data Deduplication, Compression and Thin Provisioning | SolidFire",
                  :description => "With comprehensive data reduction from in-line deduplication, compression, and thin provisioning, SolidFire makes flash at scale an economic reality.",
                  :canonical => "http://www.solidfire.com/platform/element-os/global-efficiencies"
  end

  def guaranteed_performance
    set_meta_tags :title => "Guaranteed Storage Quality of Service (QoS) | SolidFire",
                  :description => "Only SolidFire delivers guaranteed storage Quality of Service (QoS) — the key to guaranteeing system performance. Learn more.",
                  :canonical => "http://www.solidfire.com/platform/element-os/guaranteed-performance"
  end

  def scale_out
    set_meta_tags :title => "True Scale-Out Storage Architecture | SolidFire",
                  :description => "SolidFire’s true scale-out storage architecture delivers consistent linear scalability of both capacity and performance as the system grows.",
                  :canonical => "http://www.solidfire.com/platform/element-os/scale-out"
  end
end

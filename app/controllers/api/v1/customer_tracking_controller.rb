class Api::V1::CustomerTrackingController < ApplicationController

  layout false

  def set_sfmktops

    mcf_var = params[:mcf]
    mcdf_var = params[:mcdf]
    mcl_var = params[:mcl]
    mcdl_var = params[:mcdl]

    check_referer = request.referer

    if check_referer.to_s.include? 'http'
      check_referer = request.referer
    else
      check_referer = 'direct'
    end

    if cookies[:sfmktops]
      mcf_var_set = get_set_sfmktops('mcf')
      mcdf_var_set = get_set_sfmktops('mcdf')

      setup_sf_array = {
          'mcf' => mcf_var_set,
          'mcdf' => mcdf_var_set,
          'mcl' => (!mcl_var ? get_set_sfmktops('mcl') : mcl_var),
          'mcdl' => (!mcdl_var ? check_referer : mcdl_var)
      }
      encodeJson = setup_sf_array.to_json

      cookies.delete :sfmktops

      cookies[:sfmktops]={:value => Base64.strict_encode64(encodeJson), :expires => 1.year.from_now}
    end

    if !cookies[:sfmktops]
      setup_sf_array = {
          'mcf' => (!mcf_var ? 'direct' : mcf_var),
          'mcdf' => (!mcdf_var ? check_referer : mcdf_var),
          'mcl' => (!mcl_var ? 'direct' : mcl_var),
          'mcdl' => (!mcdl_var ? check_referer : mcdl_var)
      }
      encodeJson = setup_sf_array.to_json
      cookies[:sfmktops]={:value => Base64.strict_encode64(encodeJson), :expires => 1.year.from_now}
    end

    @get_mcf = get_set_sfmktops('mcf')
    @get_mcdf = get_set_sfmktops('mcdf')
    @get_mcl = get_set_sfmktops('mcl')
    @get_mcdl = get_set_sfmktops('mcdl')

    @cookieName = cookies[:sfmktops]
  end

end
class LegalController < ApplicationController
  def index
    set_meta_tags :title => 'SolidFire | Legal Terms',
                  :description => 'SolidFire | Legal Terms',
                  :canonical => 'http://www.solidfire.com/legal'

  end

  def privacy
    set_meta_tags :title => 'SolidFire | Legal Privacy',
                  :description => 'SolidFire | Legal Privacy',
                  :canonical => 'http://www.solidfire.com/legal/privacy'

  end

  def cookies
    set_meta_tags :title => 'SolidFire | Legal Cookies',
                  :description => 'SolidFire | Legal Cookies',
                  :canonical => 'http://www.solidfire.com/legal/cookies'

  end
end

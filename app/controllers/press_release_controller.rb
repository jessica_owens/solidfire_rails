class PressReleaseController < ApplicationController
  def index

    @get_today_date_time = Time.now.utc - 7.hours

    @get_today_date_time.strftime("%Y-%m-%d %H:%M:%S")


    @pr = Cms::PressRelease.where(:scheduled_release.lte => @get_today_date_time).order_by(:pub_date.desc)

    set_meta_tags :canonical => 'http://www.solidfire.com/press-releases',
                  :title => 'Press Releases | SolidFire'

  end

  def show_pr

    #redirects
    pr_redirects = Array[{:url => '/press-releases/solidfire-ceo-dave-wright-named-ey-entrepreneur-of-the-year%C2%AE-2015-award-finalist', :url_redirect => 'http://www.solidfire.com/press-releases/solidfire-ceo-dave-wright-named-ey-entrepreneur-of-the-year-r-2015-award-finalist'}]

    @check_route = request.path

    @no_redirect = 0

    pr_redirects.each do |arr|
      @get_arr_url = arr[:url]

      if @check_route == @get_arr_url
        @check_found = true

        @arr_redirect = arr[:url_redirect]
      end

    end

    if @check_found
      redirect_to @arr_redirect, :status => 301
    end

      unless @check_found
        @pr = Cms::PressRelease.where(:_slugs => params[:id]).first

        if @pr.nil?
          #redirect_to error_404_path
          render :file => 'public/404.html', :status => 404, :layout => false
        end

        if @pr
          set_meta_tags :canonical => 'http://www.solidfire.com/press-releases/' + @pr.slug,
                        :title => @pr.title
        end
      end
  end
end

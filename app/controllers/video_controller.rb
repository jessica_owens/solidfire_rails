class VideoController < ApplicationController
  def index
    set_meta_tags :title => 'All-Flash Array for Next-Generation Data Centers | SolidFire',
                  :description => 'High performance all-flash array designed for public and private clouds. Deliver new applications and capabilities faster with SolidFire.',
                  :canonical => 'http://www.solidfire.com/index'

    render layout: 'no_header_no_footer'
  end
end

class StyleGuideController < ApplicationController
  def index
    set_meta_tags :title => 'Style Guide | SolidFire',
                  :description => "Solidfire style guide",
                  :canonical => 'http://www.solidfire.com/style-guide',
                  :noindex => true,
                  :nofollow => true
  end
end

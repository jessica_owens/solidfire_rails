class FlashArrayGuidesController < ApplicationController

  def show_guide

    @landing = Cms::OrganicLanding.where(:_slugs => params[:id]).first

    if @landing.nil?
      #redirect_to error_404_path
      render :file => 'public/404.html', :status => 404, :layout => false
    end

    if @landing
      set_meta_tags :canonical => 'http://www.solidfire.com/flash-array-guides/' + @landing.slug

      @meta_title = (@landing.meta_title? ? @landing.meta_title : @landing.title + ' | SolidFire')

      @meta_description = (@landing.meta_description? ? @landing.meta_description : @landing.body)
    end

  end
end

class SearchController < ApplicationController
  def index
    require "net/http"
    require "uri"
    require "json"

    @get_search_term = params[:q]

    @get_search_query_full = request.fullpath

    @get_search_query_full.gsub! '/search/', ''

    uri = URI.parse("https://www.googleapis.com/customsearch/v1?key=AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU&cx=007051551263750412234:symsn-coala&q=" +  @get_search_query_full + "&start=1")

    response = Net::HTTP.get_response(uri)

    data = JSON.parse(response.body)

    @get_search = []

    get_number_of_pages = data['queries']['nextPage']

    if !get_number_of_pages
      @total_results = 0
    end

    if get_number_of_pages
      get_number_of_pages.each do |results|
        @num_pages = results['totalResults'].to_i / 10
        @total_results = results['totalResults']
      end

      @num_results = (1..@num_pages)

      data['items'].each do |item|
        @get_search << {:title => item['title'], :link => item['link'], :snippet => item['snippet']}
      end
    end

  end

  def search_term
    require "net/http"
    require "uri"
    require "json"

    @get_search_term = params[:q]
    @get_search_num = params[:num]

    @get_search_query_full = request.fullpath

    @get_search_query_full.gsub! '/search-term/', ''

    @get_search_query_stripped = @get_search_query_full.sub( %r{/+[0-9]+}, '' )




    if @get_search_term == @get_search_query_full
      @tester = true;
    else
      @tester = false;
    end

    uri = URI.parse("https://www.googleapis.com/customsearch/v1?key=AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU&cx=007051551263750412234:symsn-coala&q=" + @get_search_query_stripped + "&start=" + @get_search_num)

    response = Net::HTTP.get_response(uri)

    data = JSON.parse(response.body)

    @get_search = []

    get_number_of_pages = data['queries']['nextPage']

    if get_number_of_pages
      get_number_of_pages.each do |results|
        @num_pages = results['totalResults'].to_i / 10
        @total_results = results['totalResults']
      end

      @num_results = (1..@num_pages)

      data['items'].each do |item|
        @get_search << {:title => item['title'], :link => item['link'], :snippet => item['snippet']}
      end

      render layout: false
    end
  end

end



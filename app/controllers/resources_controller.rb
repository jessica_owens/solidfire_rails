class ResourcesController < ApplicationController


  def index
    set_meta_tags :title => 'Explore the SolidFire Resource Center | SolidFire',
                  :description => 'Access product documents, case studies, videos, and more to learn how SolidFire delivers validated storage solutions for your next generation data center.',
                  :canonical => 'http://www.solidfire.com/resources'

    @cms_tags = Cms::Tag.all
    @cms_cats = Cms::Category.all.order_by(:title.asc)

    @cms_resources = Cms::Resource.where(order_id: 'false').order_by(:id.desc)

    @cms_resources_order_list = Cms::Resource.where(:order_id.ne => 'false').order_by(:order_id.asc)


    card_images

  end

  def all_resources

    @cms_tags = Cms::Tag.all
    @cms_cats = Cms::Category.all
    #@cms_resources = Cms::Resource.all.order_by(:id.desc)
    @cms_resources = Cms::Resource.where(order_id: 'false').order_by(:id.desc)

    @cms_resources_order_list = Cms::Resource.where(:order_id.ne => 'false').order_by(:order_id.asc)

    card_images

    render layout: false
  end

  def load_tags

    @get_tag_param = params[:tag]

    @cms_resources = Cms::Resource.where({tags: @get_tag_param}).order_by(:id.desc)

    #@cms_resources = Cms::Resource.any_in(tags: [params[:doc], params[:vid]]).paginate(:page => params[:page], :per_page => 15)

    card_images

    render layout: false
  end

  def load_cats

    @get_cat_param = params[:category]

    @cms_resources = Cms::Resource.where({cats: @get_cat_param}).order_by(:id.desc)

    card_images

    render layout: false
  end

  def show_resource

    @resource = Cms::Resource.where(:_slugs => params[:id]).first

    if !@resource
      #redirect_to error_404_path
      render :file => 'public/404.html', :status => 404, :layout => false
    end

    if @resource
      set_meta_tags :canonical => 'http://www.solidfire.com/resources/' + @resource.slug
      set_meta_tags :og => {
                        :title => @resource.title,
                        :url => 'http://www.solidfire.com/resources/' + @resource.slug,

                    }

      @meta_title = (@resource.meta_title? ? @resource.meta_title : @resource.title + ' | SolidFire')

      @meta_description = (@resource.meta_description? ? @resource.meta_description : @resource.description)
    end

    @resource_tags_cats = Cms::Resource.where(:_slugs => params[:id])

    @resource_tags_cats.each do |cat|
      unless cat.cats.nil?
        @cats = Cms::Category.find(cat.cats)
      end
    end

    @resource_tags_cats.each do |tag|
      unless tag.tags.nil?
        @tags = Cms::Tag.find(tag.tags)
      end
    end

  end

  def card_images
    @tag_image = Array[{:tid => '550cb8e6323664000c000000', :img => 'icon-product-documents.png', :link => 'Read this document to learn more', :cname => 'documents-image-container'},
                       {:tid => '550cb8f53236640006000000', :img => 'icon-reference-architecture.png', :link => 'Read this reference architecture to learn more', :cname => 'reference-architecture-image-container'},
                       {:tid => '550cb902323664000c010000', :img => 'icon-analytics-reports.png', :link => 'Read this analyst report to learn more', :cname => 'analytics-reports-image-container'},
                       {:tid => '550cb90f3236640006010000', :img => 'icon-case-studies.png', :link => 'Read this case study to learn more', :cname => 'case-study-image-container'},
                       {:tid => '550cb91c323664000c020000', :img => 'icon-video.png', :link => 'Watch this video to learn more', :cname => 'video-image-container'},
                       {:tid => '550cb92e3236640006020000', :img => 'icon-webinar.png', :link => 'Listen to this webinar to learn more', :cname => 'webinar-image-container'},
                       {:tid => '55b667003335300009000000', :img => 'icon-webinar.png', :link => 'Listen to this podcast to learn more', :cname => 'popcasts-image-container'}]
  end

  def category_result
    @get_cat_param = params[:category]

    @cat_name = Cms::Category.where(:_slugs => params[:category]).first

    if @cat_name.nil?
      redirect_to error_404_path
    end

    if @cat_name
      @cms_resources = Cms::Resource.where({cats: @cat_name.id.to_s}).order_by(:id.desc)

      set_meta_tags :title => @cat_name.title + ' Resources | SolidFire',
                    :canonical => 'http://www.solidfire.com/resources/category/' + @get_cat_param

      @cms_tags = Cms::Tag.all
      @cms_cats = Cms::Category.all.order_by(:title.asc)

      card_images
    end
  end

  def type_result
    @get_tag_param = params[:id]

    @tag_name = Cms::Tag.where(:_slugs => params[:id]).first

    if @tag_name.nil?
      redirect_to error_404_path
    end

    if @tag_name
      @cms_resources = Cms::Resource.where({tags: @tag_name.id.to_s}).order_by(:id.desc)

      set_meta_tags :title => @tag_name.title + ' | SolidFire',
                    :canonical => 'http://www.solidfire.com/resources/content-type/' + @get_tag_param

      @cms_tags = Cms::Tag.all
      @cms_cats = Cms::Category.all.order_by(:title.asc)

      card_images
    end

  end

end

class HowToBuyController < ApplicationController
  def index
    set_meta_tags :title => 'Contact Us For Deployment Options and Pricing | SolidFire',
                  :description => "Implement a cost-effective, high-performance all-flash solution. Contact us to discuss which option is right for you with specific pricing details.",
                  :canonical => "http://www.solidfire.com/how-to-buy"
  end
end

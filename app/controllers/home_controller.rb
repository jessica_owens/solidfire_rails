class HomeController < ApplicationController
  def index

    set_meta_tags :title => 'SolidFire + NetApp = Unstoppable All-Flash Storage',
                  :description => 'SolidFire is the industry’s most complete all-flash storage system and Gartner’s highest-rated solid-state array for overall use, two years in a row.',
                  :canonical => 'http://www.solidfire.com/'

    @news = Cms::News.all.order_by(:pub_date.desc).first

    @getMonthYearDate = Date.today.strftime("%Y-%m-%d")

    @events = Cms::Event.where(:start_date.gte => @getMonthYearDate).order_by(:start_date.asc).first

    if !@events
      @events = Cms::Event.order_by(:start_date.asc).last
    end

  end

  def home

  end

  def home_new
    set_meta_tags :title => 'All-Flash Array for Next-Generation Data Centers | SolidFire',
                  :description => 'High performance all-flash array designed for public and private clouds. Deliver new applications and capabilities faster with SolidFire.',
                  :canonical => 'http://www.solidfire.com/'

    @pr = Cms::PressRelease.all.order_by(:pub_date.desc).limit(1)

    @resources = Cms::Resource.where(:order_id.ne => 'false').order_by(:order_id.asc).limit(2)

    @news = Cms::News.all.order_by(:pub_date.desc).limit(1)
  end

  def home_new_updated
    set_meta_tags :title => 'All-Flash Array for Next-Generation Data Centers | SolidFire',
                  :description => 'High performance all-flash array designed for public and private clouds. Deliver new applications and capabilities faster with SolidFire.',
                  :canonical => 'http://www.solidfire.com/'

    @pr = Cms::PressRelease.all.order_by(:pub_date.desc).limit(1)

    @resources = Cms::Resource.where(:order_id.ne => 'false').order_by(:order_id.asc).limit(2)

    @news = Cms::News.all.order_by(:pub_date.desc).limit(1)
  end

  def modal_test

    render :layout => false
  end
end

class GreatFeatsController < ApplicationController
  def index
    set_meta_tags :title => 'Great Feats of Storage | SolidFire ',
                  :canonical => 'http://www.solidfire.com/greatfeats'

    render layout: 'no_navigation'
  end

  def show_great_feats

    @asset_id = params[:id]

    @asset_name = @asset_id.replace(@asset_id.gsub(/-/, '_'))

    case @asset_id
      when 'esg'
        @title_name = 'ESG Analyst Lab Report'
      when 'qos'
        @title_name = 'Definitive Guide: Storage Performance'
      when 'ezverify_case_study'
        @title_name = 'ezVerify Case Study'
      when 'gmq'
        @title_name = 'Gartner Magic Quadrant Report'
      when 'competitive_comparison'
        @title_name = 'SolidFire and Pure Storage Comparison'
    end

    set_meta_tags :title => @title_name + ' | Great Feats of Storage | SolidFire '

    render layout: 'no_navigation'
  end

  def thank_you
    get_asset = params[:id]

    case get_asset
      when 'esg'
        @asset_name = 'Enterprise Strategy Group Lab Report'
      when 'qos'
        @asset_name = 'Storage QoS eBook'
      when 'ezverify'
        @asset_name = 'Ezverify case study'
      when 'gmq'
        @asset_name = 'Gartner Reports'
      when 'comparison'
        @asset_name = 'Competitive Comparison paper'
    end

    render layout: 'no_header_no_footer'
  end

  def form
    @get_asset = params[:id]

    @mobile = params[:type]
    render layout: 'no_header_no_footer'
  end
end

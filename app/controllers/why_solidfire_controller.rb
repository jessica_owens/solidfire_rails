class WhySolidfireController < ApplicationController
  def index
    set_meta_tags :title => 'All-Flash Array for the Next-Generation Data Center | SolidFire',
                  :description => 'SolidFire’s all-flash array solves the challenges of resource allocation, simplified management, and application predictability at scale.',
                  :canonical => 'http://www.solidfire.com/why-solidfire'
  end

  def index_updated
    set_meta_tags :title => 'All-Flash Array for Next-Generation Data Centers | SolidFire',
                  :description => 'High performance all-flash array designed for public and private clouds. Deliver new applications and capabilities faster with SolidFire.',
                  :canonical => 'http://www.solidfire.com/why-solidfire'
  end
end

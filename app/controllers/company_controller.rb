class CompanyController < ApplicationController
  def index
  end

  def contact
    set_meta_tags :title => 'Contact Us | SolidFire',
                  :canonical => 'http://www.solidfire.com/about/contact'
  end

  def technology_partners
    set_meta_tags :title => 'Technology Partners | SolidFire',
                  :canonical => 'http://www.solidfire.com/about/technology-partners'

  end

  def overview
    set_meta_tags :title => 'Overview | SolidFire',
                  :description => 'Learn how we deliver high performance applications from a multi-tenant infrastructure with SolidFire all-SSD storage systems.',
                  :canonical => 'http://www.solidfire.com/about/overview/'

    @images = Array[{:src =>ActionController::Base.helpers.asset_path('company/Top-100-Badge.jpg'), :alt => "Top 100", :url => "http://www.builtincolorado.com/2014/11/19/top-100-digital-employers-colorado"},
                    {:src =>ActionController::Base.helpers.asset_path('company/BiBA-2014-bronze-midres.jpg'), :alt => "Best Biz 2014", :url => "http://www.bestinbizawards.com/2014-winners-company/"},
                    {:src =>ActionController::Base.helpers.asset_path('company/bestplacestowork_logo_2014-01.png'), :alt => "Best Places to Work", :url =>  "http://www.outsideonline.com/outdoor-adventure/best-jobs/best-places-to-work-2014/SolidFire-Inc.html"},
                    {:src =>ActionController::Base.helpers.asset_path('company/twp_top100_denver_portrait_2014_aw.png'), alt: "Top 100 Workplaces 2014", :url =>  "http://www.topworkplaces.com/frontend.php/regional-list/company/denverpost/solidfire-inc"},
                    {:src =>ActionController::Base.helpers.asset_path('company/G_Approved_HealthyCo.png'), :alt => "Healthiest Companies 2014", :url => "http://greatist.com/health/healthiest-companies"},
                    {:src =>ActionController::Base.helpers.asset_path('company/topco_finalist_2014_webicon-1-.png'), :alt => "Colorado Biz 2014", :url => "http://www.cobizmag.com/articles/top-company-2014-technology"},
                    {:src =>ActionController::Base.helpers.asset_path('company/bestinbizawards-2013-silver.jpg'), :alt => "Best in Biz 2013", :url =>  "http://www.bestinbizawards.com/2013-winners-company/"},
                    {:src =>ActionController::Base.helpers.asset_path('company/colorado.png'), :alt => "Colorado Companies to Watch", :url =>  "https://colorado.companiestowatch.org/index.ctw?page=honorees&honoreeYear=2012"},
                    {:src =>ActionController::Base.helpers.asset_path('company/ABA13_Bronze_H.gif'), :alt => "Stevie Winner", :url => "http://www.stevieawards.com/pubs/awards/403_2662_22382.cfm"},
                    {:src =>ActionController::Base.helpers.asset_path('company/red-herring.png'), :alt => "Red Herring Winner", :url => "http://www.redherring.com/red-herring-americas/americas-2012-top-100-winners/"},
                    {:src =>ActionController::Base.helpers.asset_path('company/up-start.png'), :alt => "UP-Start", :url => "http://up-con.com/winners-start-2012-awards"},
                    {:src =>ActionController::Base.helpers.asset_path('company/top-company.png'), :alt => "Top Company", :url => "http://www.cobizmag.com/"}]
  end

  def technology_partners
    set_meta_tags :title => 'SolidFire | Technology Partners',
                  :description => 'SolidFireâ€™s partnerships align our innovative all-flash architecture with the organizations our customers need to achieve the true promises of cloud computing.',
                  :canonical => 'http://www.solidfire.com/about/technology-partners'
  end

  def events_redirect
    redirect_to :controller => 'company', :action => 'events', :status => 301
  end

  def events
    require 'date'

    set_meta_tags :title => 'Events | SolidFire',
                  :canonical => 'http://www.solidfire.com/events'

    @getMonth = Date.today.strftime("%Y-%m")

    @getMonthYearDate = Date.today.strftime("%Y-%m-01")

    @events = Cms::Event.where(:start_date.gte => @getMonthYearDate).order_by(:start_date.asc).group_by {|d| d.start_date.strftime("%Y-%m")}

    @events_past = Cms::Event.where(:start_date.lte => @getMonthYearDate).order_by(:start_date.desc).group_by {|d| d.start_date.strftime("%Y-%m")}

    #@events = Cms::Event.all.order_by(:start_date.desc).group_by {|d| d.start_date.strftime("%Y-%m")}

    @getMonthNew = Date.today.strftime("%y %B")

  end

  def channel_partners
    set_meta_tags :title => 'Cloud Builder Partner Program | SolidFire',
                  :description => 'The SolidFire Cloud Builder Partner Program enables you to provide technology solutions for the Next Generation Data Center to your enterprise customers.',
                  :canonical => 'http://www.solidfire.com/about/channel-partners'
  end

  def request_access_form
    set_meta_tags :noindex => true,
                  :nofollow => true
    render layout: 'no_header_no_footer'
  end

  def careers
    set_meta_tags :title => 'Careers | SolidFire',
                  :description => 'Are you ready to disrupt the IT storage industry? See why you should work at SolidFire and current job openings.',
                  :canonical => 'http://www.solidfire.com/about/careers'
  end

  def team
    @jobs = FeedJira.new.parse_jobs

    @category = params[:id]

    if params[:id] == 'marketing'
      @department = 'Marketing [we promote]'
      @tagline = 'We love making our mark on transformative technology.'
    elsif params[:id] == 'productandengineering'
      @department = 'Product & Engineering [we build]'
      @tagline = 'We are developing code every day to help transform the data storage of tomorrow.'
    elsif params[:id] == 'sales'
      @department = 'Sales [we sell]'
      @tagline = 'We are driven to create solutions for customers with integrity.'
    elsif params[:id] == 'operationsandadministration'
      @department = 'Operations [we keep it running]'
      @tagline = 'We are firmly committed to the success of our organization and our customers.'
    elsif params[:id] == 'interns'
      @tagline = 'Develop new skills through hands-on experiences and direct customer impacts.'
    end

    set_meta_tags :title => 'Job Openings | SolidFire',
                  :description => 'View current job openings at SolidFire in Product & Engineering, Marketing, Operations, and Sales.',
                  :canonical => 'http://www.solidfire.com/about/careers/team/' + @category

  end

  def jobs
    set_meta_tags :title => 'Jobs | SolidFire',
                  :description => 'Are you ready to disrupt the IT storage industry? See why you should work at SolidFire and current job openings.',
                  :canonical => 'http://www.solidfire.com/about/jobs'

  end

  def customers
    set_meta_tags :title => 'Customers | SolidFire',
                  :description => 'Meet the team that manages SolidFire, the company delivering high performance applications from a multi-tenant infrastructure with all-SSD storage systems.',
                  :canonical => 'http://www.solidfire.com/customers'
  end

  def analyst_reviews
    set_meta_tags :title => 'Analyst Reviews and Reports | SolidFire',
                  :canonical => 'http://www.solidfire.com/about/analyst-reviews'
  end

  private

  class FeedJira
    require 'nokogiri'
    require 'open-uri'
    require 'feedjira'

    def initialize
      @jobs = {}
    end

    def parse_jobs
      get_location
      get_department
      feed = get_feed

      feed.entries.each_with_index do |entry, index|
        @jobs[entry.department] = Array.new if @jobs[entry.department].nil?
        @jobs[entry.department] << {'title' => entry.title, 'location' => entry.location, 'department' => entry.department, 'summary' => entry.summary, 'id' => entry.id, 'job_id' => index}
      end
      @jobs
    end

    private

    def get_location
      Feedjira::Feed.add_common_feed_entry_element("newton:location", :as => :location)
    end

    def get_department
      Feedjira::Feed.add_common_feed_entry_element("newton:department", :as => :department)
    end

    def get_feed
      Feedjira::Feed.fetch_and_parse("http://newton.newtonsoftware.com/career/CareerAtomFeed.action?clientId=8a699b9842ea121b014307fe4e91096f")
    end
  end
end



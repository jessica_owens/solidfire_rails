class NewsController < ApplicationController
  def index
    @news = Cms::News.all.order_by(:pub_date.desc)

    set_meta_tags :title => 'News | SolidFire',
                  :canonical => 'http://www.solidfire.com/news'
  end
end

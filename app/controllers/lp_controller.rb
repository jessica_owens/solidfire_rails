class LpController < ApplicationController
  def index
    @lp = Cms::LandingPage.where(:_slugs => params[:id]).first

    if @lp.nil?
      #redirect_to error_404_path
      render :file => 'public/404.html', :status => 404, :layout => false
    end

    if @lp

      if (@lp.login == true && !cookies[:cms_login])
        return redirect_to(cms_login_index_path)
      end


      #if cookies[:sfmktops]
      #  @get_mcf = get_set_sfmktops('mcf')
      #  @get_mcdf = get_set_sfmktops('mcdf')
      #  @get_mcl = get_set_sfmktops('mcl')
      #  @get_mcdl = get_set_sfmktops('mcdl')
      #end

      set_meta_tags :canonical => 'http://www.solidfire.com/lp/' + @lp.slug

      @meta_title = (@lp.meta_title? ? @lp.meta_title : @lp.title + ' | SolidFire')

      @meta_description = (@lp.meta_description? ? @lp.meta_description : @lp.description)

      @no_follow = @lp.meta_no_follow

      @lp_layout = Cms::LandingPage.where(:_slugs => params[:id])

      @lp_layout.each do |layout|
        unless layout.layout_template.nil?
          @layout_find = Cms::LayoutTemplate.where({id: layout.layout_template})
        end
      end

      @layout_find.each do |lp_template|
        render layout: lp_template.layout_file
      end
    end

  end
end

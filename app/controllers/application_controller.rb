class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :get_environment
  before_action :set_locale
  before_action :fire_customer_tracking_api

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def get_header
    require 'nokogiri'
    require 'open-uri'

    page = Nokogiri::HTML(open("http://www.solidfire.com/"))
    page.css("header").text
  end

  def get_environment
    get_env = request.host

    if (get_env == 'solidfire_rails.dev')
      @get_env = 'local-dev'
    elsif (get_env == 'solidfire-development.herokuapp.com')
      @get_env = 'development'
    else
      @get_env = 'production'
    end

  end

  def custom_image_details(custom_image)
    @find_image = Cms::ResourceImage.where({id: custom_image})

    @find_image.each do |image|

      @removeFileExtension = image.res_image_file_name[0..-5]

      if image.res_image_content_type == 'image/png'
        @add_extension = '.png'
      else
        @add_extension = '.jpg'
      end
    end

    @random_asset_number = (0..3).to_a.sample.to_s
  end

  def check_access(access_id)
    get_cms_login_details

    user = Cms::User.where(id: @get_login_id_value).first

    if user.sections.exclude? access_id
     no_access = true
    end

    if (no_access == true && user.is_admin != 1)
      flash[:error] = 'You do not have access to the section you were trying to request!'
      redirect_to cms_home_index_path
    end

  end

  def get_cms_login_details

    if (cookies[:cms_login])
      decodeLogin = ActiveSupport::JSON.decode(Base64.decode64(cookies[:cms_login]))

      @get_login_id_value = decodeLogin['id']

      @get_login_key_value = decodeLogin['key']

      @get_login_email_value = decodeLogin['email']

      if (@get_login_email_value)
        cookies.delete :cms_login
      end

      @user = Cms::User.where(id: @get_login_id_value).first
    end
  end

  def get_user_key
    get_cms_login_details

    @user_key = Cms::User.where(id: @get_login_id_value).first
  end

  def check_login
    get_user_key

    if (!cookies[:cms_login] )
      redirect_to cms_login_index_path
    end

    if (cookies[:cms_login] && @user_key.user_key != @get_login_key_value)
      redirect_to cms_logout_path
    end

  end

  def fire_customer_tracking_api
   # require 'uri'
   # require 'net/http'

   # mcf_var = params[:mcf]
   # mcdf_var = params[:mcdf]
   # mcl_var = params[:mcl]
   # mcdl_var = params[:mcdl]

   # uri = URI.parse('http://solidfire-development.herokuapp.com/api/v1/customer-tracking?' + (mcf_var ? 'mcf=' + mcf_var : '') + (mcdf_var ? '&mcdf=' + mcdf_var : '') + (mcl_var ? '&mcl=' + mcl_var : '') +  (mcdl_var ? '&mcdl=' + mcdl_var : ''))
    #response = Net::HTTP.get_response(uri)

    if cookies[:sfmktops]
      cookies.delete :sfmktops
    end

  end

  def get_set_sfmktops(sf_value)
   decodeRerral = ActiveSupport::JSON.decode(Base64.decode64(cookies[:sfmktops]))

   decodeRerral[sf_value]
  end

  def blog
    redirect_to "http://www-solidfire-prod.ec2.barkleylabs.com/blog/#{request.fullpath.gsub('/blog','')}", :status => :moved_permanently
  end

  def solidsolutionsblog
    redirect_to "http://www-solidfire-prod.ec2.barkleylabs.com/solidsolutionsblog/#{request.fullpath.gsub('/solidsolutionsblog','')}", :status => :moved_permanently
  end

end

class StorageSystemController < ApplicationController
  def flash_forward
    set_meta_tags :title => 'FlashForward Program | SolidFire',
                  :description => "Eliminate your storage refresh cycles, compatibility concerns & drive wear for good with the FlashForward Program, the storage industry's best guarantee.",
                  :canonical => 'http://www.solidfire.com/flash-forward'
  end

  def global_efficiencies
    set_meta_tags :title => 'Data Deduplication, Compression & Thin Provisioning | SolidFire',
                  :description => "With comprehensive data reduction from in-line deduplication, compression, and thin provisioning, SolidFire makes flash at scale an economic reality.",
                  :canonical => 'http://www.solidfire.com/platform/element-os/global-efficiencies'

  end
end

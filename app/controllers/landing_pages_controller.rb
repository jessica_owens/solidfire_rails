class LandingPagesController < ApplicationController

  def gmq
    set_meta_tags :description => 'Gartner Solid State Array and Storage reports rank SolidFire #1 for overall flash use case in the Critical Capabilities Study and recognizes SolidFire as a Visionary in the 2014 Magic Quadrant for Solid-State Arrays.',
                  :canonical => 'http://www.solidfire.com/gmq-lp'

    @meta_title = 'Gartner Solid State Array & Storage Magic Quadrant | SolidFire '

  end

  def gmq_lp_b
    set_meta_tags :title => 'Gartner Solid State Array and Storage Magic Quadrant | SolidFire',
                  :description => 'Gartner Solid State Array and Storage reports rank SolidFire #1 for overall flash use case in the Critical Capabilities Study and recognizes SolidFire as a Visionary in the 2014 Magic Quadrant for Solid-State Arrays.',
                  :canonical => 'http://www.solidfire.com/gmq-lp',
                  :nofollow => true,
                  :noindex => true

  end

  def esg
    set_meta_tags :title => 'Economic Value of All-Flash Storage - ESG Lab Report | SolidFire',
                  :description => 'Discover how SolidFire all-flash storage will lower your operating expenses by 67% over traditional storage. Download the ESG analyst report!',
                  :canonical => 'http://www.solidfire.com/esg'

  end

  def openstack_getting_it_right
    set_meta_tags :title => 'OpenStack Private Cloud Storage Options | SolidFire',
                  :description => 'If you’ve chosen or are considering OpenStack, you need to read the blueprint for getting OpenStack storage right. Get in-depth OpenStack storage guide.',
                  :canonical => 'http://www.solidfire.com/LP-Article-Getting-it-right-OpenStack-private-cloud-storage-OpenStack',
                  :noindex => true,
                  :nofollow => true

    render layout: 'no_navigation'

  end

  def storage_mrd_template
    set_meta_tags :title => 'Storage Market Requirements Doc (MRD) Template | SolidFire',
                  :description => 'Get help productizing your cloud and managed storage services with a storage market requirements document (MRD) template. Download the free template.',
                  :canonical => 'http://solidfire.com/lp-storage-mrd-template',
                  :noindex => true,
                  :nofollow => true

    render layout: 'no_navigation'
  end

  def openstack_summit_vancouver
    set_meta_tags :title => 'SolidFire at OpenStack Summit Vancouver | SolidFire',
                  :description => 'Join us at OpenStack Summit Vancouver to see how our all-flash storage platform is built to enable the demands of production-ready OpenStack environments.',
                  :canonical => 'http://www.solidfire.com/openstack-summit-vancouver'

    render layout: 'no_header'

  end

  def five_ways_your_storage_vendor_is_costing_you_more_than_you_think
    set_meta_tags :title => '5 Ways Your Storage Vendor is Costing You | SolidFire',
                  :description => 'Is your storage vendor keeping up with today’s demands, or holding your business back from penetrating new markets and attracting customers? Find out.',
                  :noindex => true,
                  :nofollow => true

    render layout: 'no_navigation'

  end

  def cisco_live
    set_meta_tags :title => 'Join SolidFire at Cisco Live San Diego 2015 | SolidFire',
                  :description => 'Join SolidFire at Cisco Live San Diego 2015 to talk about deploying OpenStack in your Cisco environment in minutes, not weeks. Get event details.',
                  :canonical => 'http://www.solidfire.com/lp-static/cisco-live'

    render layout: 'no_header'

  end

  def denver_metro_solidfire_sales_training_dell
    set_meta_tags :title => 'Denver Metro SolidFire Sales Training for Dell - June 12, 2015 | SolidFire',
                  :description => 'Denver Metro SolidFire Sales Training for Dell - June 12, 2015',
                  :canonical => 'http://www.solidfire.com/lp-static/dell-training-lp',
                  :noindex => true,
                  :nofollow => true
  end

  def element_os_8
    set_meta_tags :title => 'Element OS for the Next-Generation Data Center | SolidFire',
                  :description => 'Discover how Element OS 8 (Oxygen) expands the capabilities of the preferred storage OS for cloud computing. Learn more.',
                  :canonical => 'http://www.solidfire.com/elementos'
  end

  def vmworld
    set_meta_tags :title => 'VMworld 2015 | SolidFire',
                  :description => 'VMworld 2015',
                  :canonical => 'http://solidfire.com/vmworld'

    render layout: 'no_header_no_footer'
  end

  def vmworldus
    set_meta_tags :title => 'VMworld 2015 | SolidFire',
                  :description => 'VMworld 2015',
                  :canonical => 'http://solidfire.com/vmworldus'

    render layout: 'no_header_no_footer'
  end

  def vmworld_vip
    set_meta_tags :title => 'VMworld 2015 | SolidFire',
                  :description => 'VMworld 2015',
                  :canonical => 'http://solidfire.com/vmworld-vip'

    render layout: 'no_header_no_footer'
  end

  def dm
    @get_params = params[:id]

    @dm_id = @get_params.gsub '-', ' '

    set_meta_tags :title => @dm_id + ' | SolidFire',
                  :description => @dm_id,
                  :canonical => 'http://solidfire.com/dm/' + @dm_id,
                  :noindex => true,
                  :nofollow => true

    @dm_details = Array[{:img => 'liquid-web.png', :cname => 'LiquidWeb', :qname => 'liquidweb'}]

    @dm_details.each do |company|
      if @dm_id == company[:qname]
        @dm_flag = true
        @dm_img = company[:img]
        @dm_name = company[:cname]
      end
    end

    render layout: 'logo_min_footer'

  end

  def openstack_tokyo
    set_meta_tags :title => 'OpenStack Summit Tokyo 2015 | SolidFire',
                  :canonical => 'http://solidfire.com/openstack-tokyo'

    render layout: 'no_header_no_footer'
  end

  def may_the_flash_be_with_you_field
    set_meta_tags :title => 'May the Flash be With You | SolidFire',
                  :canonical => 'http://solidfire.com/may-the-flash-be-with-you-field',
                  :noindex => true,
                  :nofollow => true

    render layout: 'no_header_no_footer'
  end

  def analyst_day_2016
    set_meta_tags :title => 'Analyst Day 2016 | SolidFire',
                  :canonical => 'http://solidfire.com/analyst-day-2016'

    render layout: 'no_header_no_footer'
  end

  def cisco_live_berlin
    set_meta_tags :title => 'Cisco Live Berlin | SolidFire',
                  :canonical => 'http://solidfire.com/cisco-live-berlin'

    render layout: 'no_header_no_footer'
  end

  def sp_benchmark
    render layout: 'no_header'
  end

  def sp_benchmark_german
    render layout: 'no_header'
  end

  def sp_benchmark_french
    render layout: 'no_header'
  end

  def idc
    render layout: 'logo_min_footer_with_disclaimer'
  end

  def comparing_flash_storage_platforms
    set_meta_tags :title => 'Architectural comparisons of SolidFire, Pure Storage, and EMC XtremIO',
                  :canonical => 'http://solidfire.com/lp-static/comparing-flash-storage-platforms',
                  :noindex => true,
                  :nofollow => true

    render layout: 'no_header'
  end

  def unstoppable
    set_meta_tags :title => 'SolidFire + NetApp | SolidFire',
                  :canonical => 'http://solidfire.com/lp/unstoppable',
                  :noindex => true,
                  :nofollow => true

    render layout: 'no_header'
  end
end


class SolutionsController < ApplicationController
  def elementx
    set_meta_tags :title => 'Element X Program | SolidFire',
                  :description => 'Qualified hyper-scale customers can license SolidFire’s Element OS to run on hardware chosen from SolidFire’s existing Hardware Compatibility List (HCL).',
                  :canonical => 'http://www.solidfire.com/elementx'
  end

  def infrastructure_continuum
    set_meta_tags :title => 'Infrastructure Consumption Continuum | SolidFire',
                  :description => 'Welcome to the SolidFire Infrastructure Consumption Continuum, your step-by-step guide to choosing your next generation infrastructure. Learn more.',
                  :canonical => 'http://www.solidfire.com/infrastructure-continuum'
  end

  def agile_infrastructure
    set_meta_tags :title => 'Agile Infrastructure for a Next Generation Cloud | SolidFire',
                  :description => 'SolidFire Agile Infrastructure (AI) eliminates the complexity of delivering predictable performance to every workload at scale, every time.',
                  :canonical => "http://www.solidfire.com/solutions/agile-infrastructure"
  end

  def desktop_as_a_service
    set_meta_tags :title => 'Desktop as a Service - Virtual Desktop Services | SolidFire',
                  :description => 'SolidFire is the only storage system that can help service providers deliver large scale virtual desktop infrastructure (VDI) environment for its customers.',
                  :canonical => "http://www.solidfire.com/solutions/service-providers/desktop-as-a-service"
  end

  def openstack_infrastructure
    set_meta_tags :title => 'Agile Infrastructure for OpenStack | SolidFire',
                  :description => 'SolidFire\'s AI for Openstack is a pre-validated solution that enables users to efficiently deploy a functional OpenStack cloud infrastructure.',
                  :canonical => 'http://www.solidfire.com/solutions/agile-infrastructure/openstack'
  end

  def success_stories
    set_meta_tags :title => 'Success Stories | SolidFire',
                  :description => 'SolidFire powers some of the world’s most demanding data centers',
                  :canonical => 'http://www.solidfire.com/solutions/success-stories'
  end

  def infrastructure_as_a_service
    set_meta_tags :title => 'Infrastructure as a Service (IaaS) Offerings | SolidFire',
                  :description => 'No one understands IaaS challenges better than SolidFire. See how SolidFire delivers the most comprehensive storage solution to fuel your IaaS offerings.',
                  :canonical => 'http://www.solidfire.com/solutions/service-providers/infrastructure-as-a-service'
  end

  def managed_dedicated_san
    set_meta_tags :title => 'Managed Dedicated SAN Deployments | SolidFire',
                  :description => 'Managed dedicated SAN deployments that are Fueled by SolidFire are easier to deploy, manage and scale than any other storage infrastructure. Learn more.',
                  :canonical => 'http://www.solidfire.com/solutions/service-providers/managed-dedicated-san'
  end

  def database_as_a_service
    set_meta_tags :title => 'Database as a Service From a Single Storage Platform | SolidFire',
                  :description => 'Service providers are now able to offer a broad set of Database as a Service offerings and host them all from a single storage platform. Learn more.',
                  :canonical => 'solutions/service-providers/database-as-a-service'
  end

  def desktop_as_a_service
    set_meta_tags :title => 'Desktop as a Service - Virtual Desktop Services | SolidFire',
                  :description => 'SolidFire is the only storage system that can help service providers deliver large scale virtual desktop infrastructure (VDI) environment for its customers.',
                  :canonical => 'solutions/service-providers/desktop-as-a-service'
  end

  def mssql
    set_meta_tags :title => 'Microsoft SQL Server Database Storage | SolidFire',
                  :description => 'SolidFire is the only storage system that can guarantee performance to each and every Microsoft SQL Server volume. Learn more.',
                  :canonical => 'solutions/database/mssql'
  end

  def case_studies
    @case = params[:id]

    if params[:id] == 'rfa'
      set_meta_tags :title => 'Customer Case Studies - RFA | SolidFire',
                    :description => "SolidFire's greater density significantly reduces rack and power consumption and raises RFA’s overall storage return on investment (ROI). Learn more.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/rfa'
    elsif params[:id] == 'one_and_one'
      set_meta_tags :title => 'Customer Case Studies - 1&1 | SolidFire',
                    :description => "SolidFire's storage platform allowed 1&1 to more closely align itself to its goal of simplifying the cloud experience for its customers.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/one_and_one'
    elsif params[:id] == 'datapipe'
      set_meta_tags :title => 'Customer Case Studies - Datapipe | SolidFire',
                    :description => "See how SolidFire helped Datapipe’s rich Stratosphere platform, which delivers support for a wide range of cloud solutions.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/datapipe'
    elsif params[:id] == 'sungard'
      set_meta_tags :title => 'Customer Case Studies - Sungard | SolidFire',
                    :description => "Sungard AS chose SolidFire to help them ensure availability, recoverability and support for complex IT environments for their customers. Learn more.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/sungard'
    elsif params[:id] == 'servint'
      set_meta_tags :title => 'Customer Case Studies - ServInt | SolidFire',
                    :description => "SolidFire's guaranteed QoS powers a new ServInt VPS offering that delivers a level of guaranteed performance unheard of from a hosted VPS solution.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/servint'

    elsif params[:id] == 'elastx'
      set_meta_tags :title => 'Customer Case Studies - Elastx | SolidFire',
                    :description => "Elastx chose SolidFire in order to have one storage system that could support both Elastx cloud platforms while also scaling efficiently as business grew.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/elastx'

    elsif params[:id] == 'hosted_network'
      set_meta_tags :title => 'Customer Case Studies - Hosted Network | SolidFire',
                    :description => "Hosted Network is Australia’s first and only white-label DaaS provider, specifically built and marketed to Managed Service Providers and IT integrators.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/hosted_network'

    elsif params[:id] == 'crucial'
      set_meta_tags :title => 'Customer Case Studies - Crucial | SolidFire',
                    :description => "Learn how SolidFire is helping Crucial with competitive differentiation and new enterprise business through guaranteed performance and full system automation.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/crucial'

    elsif params[:id] == 'iweb'
      set_meta_tags :title => 'Customer Case Studies - iWeb | SolidFire',
                    :description => "Leveraging OpenStack and SolidFire, iWeb architects a cloud platform designed from the ground up to deliver predictable performance. Learn more.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/iweb'

    else params[:id] == 'databarracks'
      set_meta_tags :title => 'Customer Case Studies - Databarracks | SolidFire',
                    :description => "Databarracks is using SolidFire high-performance all-flash storage to give their customers the infrastructure they’ve been asking for. Learn more.",
                    :canonical => 'http://www.solidfire.com/solutions/case-studies/databarracks'
    end
  end

  def solutions_overview
    set_meta_tags :title => 'All-Flash Storage System Solutions Overview | SolidFire',
                  :description => "All-flash storage system solutions that make your next generation data center deployment more cohesive, automated, and dynamically scalable.",
                  :canonical => "http://www.solidfire.com/solutions"
  end

  def cloud_orchestration
    set_meta_tags :title => "Block Storage Solutions for OpenStack Cloud | SolidFire",
                  :description => "SolidFire delivers block storage integration with the industry’s leading cloud orchestration softwares like OpenStack, Cloudstack, OnApp & Flexiant.",
                  :canonical => "http://www.solidfire.com/solutions/cloud-orchestration"
  end

  def cloudstack
    set_meta_tags :title => "CloudStack Block Storage Architecture | SolidFire",
                  :description => "Demand more from the storage behind your CloudStack infrastructure. Learn about our block storage solution for Cloudstack.",
                  :canonical => "http://www.solidfire.com/solutions/cloud-orchestration/cloudstack"
  end

  def flexiant
    set_meta_tags :title => "Flexiant Cloud Orchestration Solutions | SolidFire",
                  :description => "Build cloud service offerings that are uniquely differentiated across both cost and QoS with Flexiant Cloud Orchestrator and SolidFire storage.",
                  :canonical => "http://www.solidfire.com/solutions/cloud-orchestration/flexiant"
  end

  def onapp
    set_meta_tags :title => "Block Storage for OnApp Cloud Platform | SolidFire",
                  :description => "When building an OnApp-powered IaaS cloud to host performance sensitive applications, there is only one choice for block storage; SolidFire.",
                  :canonical => "http://www.solidfire.com/solutions/cloud-orchestration/onapp"
  end

  def openstack
    set_meta_tags :title => "OpenStack Block Storage Solutions | SolidFire",
                  :description => "Discover what’s possible with your OpenStack cloud when block storage works for you, not against you. Learn about SolidFire & OpenStack.",
                  :canonical => "http://www.solidfire.com/solutions/cloud-orchestration/openstack"
  end

  def database
    set_meta_tags :title => "Storage Platform for Database Architectures | SolidFire",
                  :description => "Mold a single shared storage platform to meet the unique capacity and performance demands of each of your database architectures with SolidFire.",
                  :canonical => "http://www.solidfire.com/solutions/database/"
  end

  def mongodb
    set_meta_tags :title => "MongoDB Database Storage | SolidFire",
                  :description => "SolidFire is the only storage system that combines dynamic performance control with the simplicity and scalability delivered in a MongoDB environment.",
                  :canonical => "http://www.solidfire.com/solutions/database/mongodb"
  end

  def mysql
    set_meta_tags :title => "MySQL Database Storage | SolidFire",
                  :descriptin => "SolidFire’s scale-out architecture and guaranteed QoS are a perfect fit for consolidating multiple MySQL databases and database copies.",
                  :canonical => "http://www.solidfire.com/solutions/database/mysql"
  end

  def oracle
    set_meta_tags :title => "Oracle Database Storage | SolidFire",
                  :description => "SolidFire is the only storage system that can guarantee performance to each and every Oracle Database volume. See benefits of Oracle on SolidFire.",
                  :canonical => "http://www.solidfire.com/solutions/database/oracle"
  end

  def service_providers
    set_meta_tags :title => "All-Flash Storage System for Service Providers | SolidFire",
                  :description => "Monetize your storage like never before. SolidFire is the only storage system designed from the ground up for service providers. See why.",
                  :canonical => "http://www.solidfire.com/solutions/service-providers"
  end

  def vdi
    set_meta_tags :title => "Virtual Desktop Infrastructure (VDI) Storage | SolidFire",
                  :description => "SolidFire storage systems enable agile, scalable storage for your virtual",
                  :canonical => "http://www.solidfire.com/solutions/vdi"
  end

  def citrix_xen_desktop
    set_meta_tags :title => "Citrix XenDesktop Cloud Storage Solutions | SolidFire",
                  :desription => "SolidFire's Citrix cloud solutions offer flexibility and adaptability for XenDesktop environments, with granular QoS controls & scale-out architecture.",
                  :canonical => "http://www.solidfire.com/solutions/vdi/citrix-xendesktop"
  end

  def vmware_horizon_view
    set_meta_tags :title => "VDI Storage for VMware Horizon View | SolidFire",
                  :description => "Key benefits of VMware Horizon View on SolidFire include adaptable infrastructure, granular scalability, and simplified VDI administration.",
                  :canonical => "http://www.solidfire.com/solutions/vdi/vmware-horizon-view"
  end

  def vmware_solutions
    set_meta_tags :title => "VMware Software-Defined Data Center Storage | SolidFire",
                  :description => "Meet the diverse storage needs of the VMware Software-Defined Data Center with a single platform. Learn about SolidFire integration with VMware.",
                  :canonical => "http://www.solidfire.com/solutions/vmware-solutions"
  end

  def competitive_comparison
    set_meta_tags :title => "All-Flash Array Vendors - Comparison | SolidFire",
                  :description => "Comparing the design choices of all-flash storage vendors will help you decide which product will best enable your next generation data center. Learn more!",
                  :canonical => "http://www.solidfire.com/solutions/competitive-comparison"
  end
end

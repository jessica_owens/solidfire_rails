class Cms::User
  include Mongoid::Document
  include ActiveModel::SecurePassword
  field :email, type: String
  field :password_digest, type: String
  field :sections, type: Array
  field :is_admin, type: Integer
  field :user_key, type: String
  field :reset_digest, type: String
  field :reset_sent_at, type: DateTime
  has_secure_password

  #password reset
  attr_accessor :activation_token, :reset_token

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Returns the hash digest of the given string.
  def digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
      BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def new_token
    SecureRandom.urlsafe_base64
  end

  def create_reset_token
    new_token
  end
end

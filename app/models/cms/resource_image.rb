class Cms::ResourceImage
  include Mongoid::Document
  include Mongoid::Paperclip

  field :title, type: String
  field :res_image, type: String


  has_mongoid_attached_file :res_image,
    :path           => '/uploads/resources/:id-:filename',
    :storage        => :s3,
    :url            => 'solidfire-web.s3-website-us-west-2.amazonaws.com',
    :s3_host_alias  => 'd3ng1ewoxzx77l.cloudfront.net',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml'),
    :convert_options => { :all => '-background white -flatten +matte' }

  validates_attachment :res_image, :presence => true,
                       :size => { :in => 0..2.megabytes },
                       content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

end

class Cms::News
  include Mongoid::Document
  field :title, type: String
  field :url, type: String
  field :url_shorten, type: String
  field :pub_date, type: Date
  field :publication, type: String
end

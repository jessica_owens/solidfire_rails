class Cms::ProjectRequestType
  include Mongoid::Document
  field :name, type: String
end

class Cms::LandingPage
  include Mongoid::Document
  include Mongoid::Slug

  field :title, type: String
  field :landing_title, type: String
  field :sub_title, type: String
  field :description, type: String
  field :asset_link, type: String
  field :mkto_form_id, type: String
  field :utube_id, type: String
  field :meta_title, type: String
  field :meta_description, type: String
  field :meta_no_follow, type: Integer
  field :thank_you_url, type: String
  field :layout_template, type: String
  field :custom_image, type: String
  field :custom_image_id, type: String
  field :form_description, type: String
  field :form_message, type: String
  field :login, type: Boolean
  field :vidyard_id, type: String

  slug :title

end

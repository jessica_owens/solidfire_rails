class Cms::ProjectRequestStatus
  include Mongoid::Document
  field :name, type: String
end

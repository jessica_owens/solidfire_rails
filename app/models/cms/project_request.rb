class Cms::ProjectRequest
  include Mongoid::Document
  field :title, type: String
  field :description, type: String
  field :due_date, type: String
  field :pm_name, type: String
  field :pm_email, type: String
  field :client_email, type: String
  field :status, type: String
  field :additional_information, type: String
  field :type, type: String
  field :submit_date, type: Date
  field :archive, type: Integer
  field :jira_id, type: String
  field :other_info, type: String

  field :current_url, type: String
  field :new_url, type: String
  field :graphics_direction, type: String
  field :google_doc_graphics, type: String
  field :content_direction, type: String
  field :final_content_url, type: String

  field :seo_requirements, type: String
  field :priority, type: String
  field :url_examples, type: String

  field :graphic_type, type: String
  field :collateral_type, type: String

  field :audience, type: String
  field :graphic_guidelines, type: String
  field :creative_direction, type: String
  field :inspiration, type: String

  field :file_size, type: String
  field :bleed, type: String
  field :intended_use, type: String
  field :submit_final, type: String
  field :item, type: String

end

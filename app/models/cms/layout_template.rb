class Cms::LayoutTemplate
  include Mongoid::Document
  field :name, type: String
  field :layout_file, type: String
end

class Cms::Event
  include Mongoid::Document
  include Mongoid::Slug

  field :title, type: String
  field :description, type: String
  field :start_date, type: Date
  field :end_date, type: Date
  field :location, type: String
  field :cta_title, type: String
  field :cta_url, type: String
  field :custom_image, type: String
  field :custom_image_id, type: String

  slug :title

end

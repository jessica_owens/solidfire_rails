class Cms::PressRelease
  include Mongoid::Document
  include Mongoid::Slug

  field :title, type: String
  field :sub_title, type: String
  field :body, type: String
  field :location, type: String
  field :pub_date, type: Date
  field :about, type: String
  field :short_url, type: String
  field :schedule_day, type: String
  field :schedule_time, type: String
  field :scheduled_release, type: String


  slug :title

end

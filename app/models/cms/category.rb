class Cms::Category
  include Mongoid::Document
  include Mongoid::Slug
  field :title, type: String

  slug :title

  #embedded_in :resource, :inverse_of => 'categories'
end

class Cms::ProjectRequestItem
  include Mongoid::Document
  field :name, type: String
  field :type_id, type: String
end

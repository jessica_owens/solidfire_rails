class Cms::OrganicLanding
  include Mongoid::Document
  include Mongoid::Slug

  field :title, type: String
  field :sub_title, type: String
  field :cta_one, type: String
  field :cta_link_one, type: String
  field :cta_two, type: String
  field :cta_link_two, type: String
  field :cta_three, type: String
  field :cta_link_three, type: String
  field :body, type: String
  field :meta_title, type: String
  field :meta_description, type: String

  slug :title
end

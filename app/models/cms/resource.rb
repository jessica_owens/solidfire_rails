class Cms::Resource
  include Mongoid::Document
  include Mongoid::Slug

  #embeds_many :tags
  #accepts_nested_attributes_for :tags

  field :title, type: String
  field :landing_title, type: String
  field :sub_title, type: String
  field :description, type: String
  field :landing_description, type: String
  field :asset_link, type: String
  field :mkto_form_id, type: String
  field :utube_id, type: String
  field :meta_title, type: String
  field :meta_description, type: String
  field :tags, type: Array
  field :cats, type: Array
  field :custom_image, type: String
  field :custom_image_id, type: String
  field :order_id, type: Integer
  field :thank_you_url, type: String
  field :podcast_url, type: String
  field :vidyard_id, type: String

  has_one :resource_image

  slug :title

  validates_presence_of :title, :message => "is required"
  validates_presence_of :description, :message => "is required"

  # embeds_many :categories, class_name: 'Category'
end

#class Cms::Tag
#  include Mongoid::Document
#  field :title, type: String

#  embedded_in :resource, inverse_of: :tags
#end
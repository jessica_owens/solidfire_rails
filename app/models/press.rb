#require 'press_uploader'

class Press
  include Mongoid::Document

  field :title, type: String
  field :summary, type: String
  field :url, type: String
  field :logo_image_name, type: String
 # mount_uploader :logo, PressUploader

end

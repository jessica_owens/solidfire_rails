(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 800,
	height: 600,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"images/Bitmap1.png", id:"Bitmap1"}
	]
};



// symbols:



(lib.Bitmap1 = function() {
	this.initialize(img.Bitmap1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,101,67);


(lib.whitebox = function() {
	this.initialize();

	// white
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhczAOnIAA9NMC5mAAAIAAdNg");
	this.shape.setTransform(0,0,1,1,0,0,0,-594,-93.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1188,187);


(lib.smack = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#9D1D21").ss(4,1).p("AiuAAIFdAA");
	this.shape.setTransform(17.5,0);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,39,4);


(lib.shipFlames1 = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCA30").s().p("AiCgYQgghxgGh2IFRAAQgFB2ghBxQgrCXhYCBQhXiBgriXg");
	this.shape.setTransform(22.4,25.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F8A531").s().p("AjRiLQgWibAOh2IGzAAQAOB2gWCbQgqEyioD2Qinj3gqkxg");
	this.shape_1.setTransform(22.4,41.4);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,44.8,82.7);


(lib.rocks = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(2).p("ACvAAQAAAvg0AhQgzAjhIAAQhHAAg0gjQgzghAAgvQAAguAzgiQA0giBHAAQBIAAAzAiQA0AiAAAug");
	this.shape.setTransform(14,0,0.75,0.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ah7BRQgzgiAAgvQAAguAzgiQA0ghBHAAQBIAAAzAhQA0AigBAuQABAvg0AiQgzAhhIAAQhHAAg0ghg");
	this.shape_1.setTransform(14,0,0.75,0.75);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,-12.4,32.3,24.9);


(lib.legupper = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B7B84").s().p("AgxFeIAAq7IBjAAIAAK7g");
	this.shape.setTransform(0,35.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,0.1,10,70);


(lib.legmid = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(9).p("ACsAAQAABHgyAzQgzAyhHAAQhGAAgzgyQgygzAAhHQAAhGAygzQAzgyBGAAQBHAAAzAyQAyAzAABGg");
	this.shape.setTransform(17.3,17.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ah5B6QgygzAAhHQAAhGAygzQAzgyBGAAQBHAAAzAyQAyAzAABGQAABHgyAzQgzAyhHAAQhGAAgzgyg");
	this.shape_1.setTransform(17.3,17.3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.5,-4.5,43.6,43.6);


(lib.leglower = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B7B84").s().p("AgxGuIAAtbIBiAAIAANbg");
	this.shape.setTransform(0,43);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,0,10,86);


(lib.helmet = function() {
	this.initialize();

	// helmet
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B7B84").s().p("AgdCDQgqg5AAhKQAAhKAqg4QAmg4A+gOIAAGRQg+gPgmg3g");
	this.shape.setTransform(7.2,96.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6B7B84").s().p("AhGjIQA9AOAnA4QAqA4AABKQAABKgqA5QgnA3g9APg");
	this.shape_1.setTransform(191.2,96.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#6B7B84").ss(9,0,0,4).p("ApIkmQgbAAgaAOQg1AaAABCIAAF5IACARQADAUAIAQQAbA1BCAAISRAAIARgBQAUgEAQgIQA1gaAAhDIAAl5QAAgagNgbQgbg1hCAAg");
	this.shape_2.setTransform(99.2,96.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ApIEnQhCAAgbg1QgIgRgDgTIgCgRIAAl5QAAhCA1gbQAagNAbAAISRAAQBCAAAbA1QANAaAAAbIAAF5QAABDg1AaQgQAIgUADIgRACg");
	this.shape_3.setTransform(99.2,96.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#6B7B84").ss(9,0,0,4).p("ALgKJQB5jaAAj8QAAi5hDirQhBilh3iAQh3h/iahGQifhJiuAAQitAAifBJQiaBGh3B/Qh3CAhBClQhDCrAAC5QAAD7B5DbQBLAXCCAXQEDAuEOAAQEMAAEGguQCDgXBMgXg");
	this.shape_4.setTransform(99.2,86.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AoSK3QiCgXhLgXQh5jbAAj7QAAi5BDirQBBilB3iAQB3h/CahGQCfhJCtAAQCuAACfBJQCaBGB3B/QB3CABBClQBDCrAAC5QAAD8h5DaQhMAXiDAXQkGAukMAAQkOAAkDgug");
	this.shape_5.setTransform(99.2,86.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6B7B84").s().p("AhDBEQgcgdAAgnQAAgnAcgcQAcgcAnAAQAnAAAdAcQAcAcAAAnQAAAngcAdQgdAcgnAAQgnAAgcgcg");
	this.shape_6.setTransform(162.8,9.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#6B7B84").ss(7.5).p("AAAB2IAAjr");
	this.shape_7.setTransform(162.8,30.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#6B7B84").ss(9,1,1).p("Aq+iKIgwC2QBGAZB/AZQEAAxEmAAQEjAAEFgxQCDgZBHgZIg/i6");
	this.shape_8.setTransform(100.1,172.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AopBeQh/gZhGgZIAxi2IVtgEIA/C6QhIAZiCAZQkFAxkjAAQkmAAkAgxg");
	this.shape_9.setTransform(100.1,172.5);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,198.4,191.4);


(lib.head = function() {
	this.initialize();

	// ears
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B7B84").s().p("Ag4ixQAyATAfAwQAgAxAAA9QAAA+ggAyQgfAwgyASg");
	this.shape.setTransform(166.3,60.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6B7B84").s().p("AgYBwQgggyAAg+QAAg9AggxQAegxAzgSIAAFjQgzgSgegwg");
	this.shape_1.setTransform(5.8,60.8);

	// eyes
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#6B7B84").ss(5.5).p("AIQAAQAAg6gtgpQgtgqg/AAIrtAAQg/AAgtAqQgtApAAA6QAAA6AtAqQAtAqA/AAILtAAQA/AAAtgqQAtgpAAg7g");
	this.shape_2.setTransform(86,53.4);

	// head
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#6B7B84").ss(9).p("AnJJXQhlAAhIhUQhHhUAAh2IAApxQAAh2BHhUQBIhUBlAAIOSAAQBmAABHBUQBIBUAAB2IAAJxQAAB2hIBUQhHBUhmAAg");
	this.shape_3.setTransform(86,59.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AnJJXQhlAAhIhUQhHhUAAh2IAApxQAAh2BHhUQBIhUBlAAIOSAAQBmAABHBUQBIBUAAB2IAAJxQAAB2hIBUQhHBUhmAAg");
	this.shape_4.setTransform(86,59.9);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-4.5,181.8,128.9);


(lib.hand = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(10,0,0,4).p("ADICXQARgnAAguQAAhZg/g/QhAhAhaAAQhZAAg/BAQhAA/AABZQAAAsARAp");
	this.shape.setTransform(21.8,15.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,-5,53.6,40.4);


(lib.ground = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(2).p("Eg+cABkMAw6AAAQB0AACQhVQAOgJANgGQALgHALgGQAfgTASgKQAxgaAhAAQADAAADABQAWABAVAJQAOAGAOAJQATANAnAoQAkAmAbAQQApAZAzAAQBYABBIhbQAiguAQgSQAagfAWgDQA4gIA7AdQAgAQBMA2QBIAyAxAXQBNAiBSAAMA0tAAA");
	this.shape.setTransform(427.9,10.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2).p("AAdAUIgBAAIgPgEIgOgDAgCAMQABAAAAABQABABABACQAOADAOABAgHAQIAMAOIAiAJIACABIAAgBIgFgHIgIACIgXgEIgVgFIghgGAAnAnQAAAAACAAQAIABAIAAQAkADAigLQAQgFAPgHABHAVIgjALIgHgMQAVACAVgBQATgBATgEQAkgPAigLAgyAXQAVAKAYACQAUACAYACABeAOIAPACIAtAAABeAOIgJACIgOAFAC0gLIhWAZIh3gUQACABADACQAAABABABAi1gJIABAAAi1gIIABAAAi0gFIgDgBAgigIIgBAAIAAgBIAAgDQAFACAFAEIgJgCIAHAGAgjgJIABABAg7ghIAYAYAhDgsQAJAIAJAIQAGAEAIAMIgYgVAgjgIIgRgEAgyAXIAiAKIAAgIIAAgHAhEgpIAJAIIgKgIAgjgCIAAgGAhDASIARAF");
	this.shape_1.setTransform(406.6,19);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EhCjAQOIAA9MMA1gAAAQADgCAFAAQAeAAAZgDQAGgBAOgFIAUgIIApgOQAWgIASgJQAegPAqgZIATgLIgBgFIAAgCQACgKAUgDIAGgBIAFAAIAGgFQAmgcA1gOQAQgFANgBQAegDANANIADAEQAhAUAVAXIAHAJQAHADAGAFQAOALABANIABABQAHAJgBACIgCADIgBACIAAABIgBABIALARIAGALIADAIIAHACIAIAAIAHAAIAJAAQASACAAAIIgBAGQgCADgDABIAKAAIALAAIBJAAIAXAAQgTADgSABIAOgEIgOAEIgJABIgIAAIAAAAIgBAAIgVgBIgEAAIAIALIAjgLQASgBATgDIAtAAQgDgEAAgFQAAgEADgDQADgEAFAAIAEAAQACgCAGAAQABgDAEgEIABgBQgDgBgBgDIgBgGQAAgHAOgGQAQgJAHgIQANgUALgMIAZghIAZgfIANgRQAIgIANgGQApgUAiAVIAFAFQANgIAzAeQAnAWBSA5IAwAfQAeASAbAMQAIADAvAKQAgAJAJAHMA5TAAAIAAdMgAh5smQAYAAAXgGIAAAAIAGgCIABAAIACgBQAPgFAPgHQgPAHgPAFIgCABIgBAAIgGACIAAAAQgXAGgYAAIgBAAIAAAAIgMAAIgBAAIgRgBIgEgIIgIADIgXgFIgOgNIAOANIAhAKIACAAIAAAAIARABIABAAIAMAAIAAAAIABAAgAjIsrIAuAEIgugEQgXgCgVgKIAiAKIAAgIIAXAEIgXgEIAAgIIAAAIIgigHIAiAHIAAAIIgigKQAVAKAXACgAj0s3IgRgFgAiks6IAAgBIgQgDIAQADIAAABIgbgEIAbAEgAnYs+IASAAIgEgBIgJAAIgFABgArQs+IAfAAQgCgCAAgFIAAgEIgdALgAl3tbIAAABIgBABIgCADIgBABIAFAAIAAAAIAAgDIgBgBIAAgCgABWuWIAAAAIAAAAgAoxuqIAWgNIgWANgAlnvjQgUgJgXgBQAXABAUAJgAiasnIACAAIAAAAgAiasnIghgKIAXAFIAIgDIAEAIIgCAAgAiks6IAEAAIAVABIABAAIAAAAIAIAAIAJgBIgjALgAi7sxgAjSs1gAh5s6g");
	this.shape_2.setTransform(426,103.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-2.3,852.1,210);


(lib.foot = function() {
	this.initialize();

	// elio
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B7B83").s().p("AjwDzQgcAAgZgLQgZgLgRgVQgSgVgHgbQgHgbAFgbIAojiQAJgxAmghQAnggAygBIF1AAQAyABAnAgQAnAhAIAxIAoDiQAFAbgHAbQgHAbgRAVQgSAVgZALQgZALgcAAgAjYiNQgOAMgDAQIgoDjQgDARAJALQALALAQAAIHhAAQARAAAKgLQAJgLgCgRIgpjjQgDgQgOgMQgNgMgRAAIl1AAQgQAAgOAMg");
	this.shape.setTransform(36.6,24.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AjwDGQgmABgXgcQgXgbAGgmIAojjQAGggAagXQAbgVAhgBIF1AAQAhABAaAVQAbAXAFAgIAoDjQAHAlgXAcQgYAcglgBg");
	this.shape_1.setTransform(36.6,24.4);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,73.1,48.8);


(lib.cloudcircleyellow = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F8A531").s().p("AlZFaQiQiPAAjLQAAjJCQiRQCPiQDKAAQDKAACQCQQCQCRAADJQAADLiQCPQiQCRjKgBQjKABiPiRg");
	this.shape.setTransform(49,49.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,98.1,98.2);


(lib.cloudcircleorglt = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F58220").s().p("AlZFaQiQiPAAjLQAAjJCQiRQCPiQDKAAQDKAACQCQQCQCRAADJQAADLiQCPQiQCRjKgBQjKABiPiRg");
	this.shape.setTransform(49,49.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,98.1,98.2);


(lib.cloudcircleorgdk = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E76625").s().p("AlZFaQiQiPAAjLQAAjJCQiRQCPiQDKAAQDKAACQCQQCQCRAADJQAADLiQCPQiQCRjKgBQjKABiPiRg");
	this.shape.setTransform(49,49.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,98.1,98.2);


(lib.bodycopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(9).p("AMEt7QBXAaBIAiQCxBUAzBlIAAYCMgkNAAAIAA4AQA7hnC4hUQBLgiBYga");
	this.shape.setTransform(116,98.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AyGORIAA4BQA7hmC5hUQBKgiBYgbQAOAOARAFQAKABAPADIABAAIABAAIA4ANQA3AFAgAGIA1AMIA3AMQAgAFA4AFQA8AFATAJQALAFBiAAQAgAAAJAEQAFABABAEIABgBIA5gDIAIABIAKACIACAAIAQABIAvAAIALgBIBEAAIA0gGIA2gGQAVgDAngNQAaAAAqAPIDcgmQBPgXAdAAQADAAAMADIAJgBIAKgHIAHAAIAcgMQAOgHAIAAIATgKQBXAbBIAiQCxBUAzBlIAAYCgAFtqLIAgAAIAOgEIABgBQgUADgbACgAKQqiIABADIAAgMIgBAJgAFJqpIgDABQAEABAigDIAJgBgACXrOQAEAHAAADIAAACIALAAQgJgGgCgDIgBgDIgDAAgAl7rNIAAAAIgEgCIgkgLQAdANALAAgAGRrsIgCABIAmgFQAegDAGgBIACgBQhCAHgIACgAsQt+QgDgJAAgJIABABIADABIAAAEIAHAMQAFAGAFADIAAAFIACADQgNgDgHgOg");
	this.shape_1.setTransform(116,96.3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.5,-7.7,240.9,199.9);


(lib.body = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(9).p("AyGOqIAA4BQA7hmC4hUQFMiYJTAAQJXAAFACYQCxBUAzBlIAAYCg");
	this.shape.setTransform(116,93.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AyGOqIAA4BQA7hmC5hUQFLiYJUAAQJWAAFACYQCxBUAzBlIAAYCg");
	this.shape_1.setTransform(116,93.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-18.4,-4.5,254.9,196.7);


(lib.arm = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(10,1,0,4).p("AmcAAIM5AA");
	this.shape.setTransform(41.3,0);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,-5,92.6,10.1);


(lib.antenna = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B7B84").s().p("AhDBEQgcgcAAgoQAAgmAcgdQAcgcAnAAQAnAAAdAcQAcAdAAAmQAAAogcAcQgdAcgnAAQgnAAgcgcg");
	this.shape.setTransform(9.7,9.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#6B7B84").ss(7.5).p("AAAB2IAAjr");
	this.shape_1.setTransform(9.7,30.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,19.4,45.7);


(lib.smackMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.smack();
	this.instance.setTransform(0.4,11.6,0.76,0.985,0,-16.7,-25.4,0.3,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:25.4,y:-0.4},7,cjs.Ease.get(1)).wait(1));

	// Layer 2
	this.instance_1 = new lib.smack();
	this.instance_1.setTransform(0.5,18.7,1,1,0,0,0,0.4,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({x:28.5,y:18.8},7,cjs.Ease.get(1)).wait(1));

	// Layer 1
	this.instance_2 = new lib.smack();
	this.instance_2.setTransform(0.4,25.7,0.738,0.992,0,19.3,26.1,0.4,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:25.4,y:38.7},7,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.9,-1.8,39,41.4);


(lib.shipFlamesMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shipFlames1();
	this.instance.setTransform(0.1,-40.9,1,1,0,0,0,22.4,0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:1.18},4,cjs.Ease.get(1)).to({scaleY:1},5,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.3,-41.3,44.8,82.7);


(lib.ship = function() {
	this.initialize();

	// ship
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#97A4AD").s().p("AhrBZQgchYgUhZIE3AAIAACxg");
	this.shape.setTransform(43,220.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#97A4AD").s().p("AhhDPQBUjkBvi5IAAGdg");
	this.shape_1.setTransform(48.8,20.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6B7B84").s().p("AhjDPIAAmTIAFgKQBuC1BUDog");
	this.shape_2.setTransform(68.2,20.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#6B7B84").s().p("AieBZIAAixIE8AAQgUBcgcBVg");
	this.shape_3.setTransform(74.1,220.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#6B7B84").s().p("AiLCBQAZhHAbhxQA3jnAGjiICmCGIgIN7g");
	this.shape_4.setTransform(103.6,221.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#97A4AD").s().p("AiLl6ICliGQAIDiA2DnQAaBxAaBHIkPGAg");
	this.shape_5.setTransform(14.1,221.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#F58220").ss(4.3,0,0,4).p("AAACTQA9AAAqgrQArgsAAg8QAAg7grgsQgqgrg9AAQg7AAgrArQgrAsAAA7QAAA8ArAsQArArA7AAg");
	this.shape_6.setTransform(58.7,91.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhnBnQgrgrAAg8QAAg7ArgsQAsgqA7AAQA8AAArAqQArAsAAA7QAAA8grArQgrAsg8gBQg7ABgsgsg");
	this.shape_7.setTransform(58.7,91.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC2027").s().p("AiEMkQhVmgArnSQAkmAB0lVIDTAAIAAZHg");
	this.shape_8.setTransform(39.7,126.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#9D1D21").s().p("Ai/MkIAA5HIDYAAQB1FUAjGBQArHQhVGig");
	this.shape_9.setTransform(77.4,126.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AkHR6QhKjwgakWQgbkhAckvQAflUBgk2QBekrCNjoIAAAAQCPDoBdErQBgE2AfFUQAcEvgbEhQgaEWhKDwg");
	this.shape_10.setTransform(58.6,114.9);

	// flames1
	this.instance = new lib.shipFlamesMC();
	this.instance.setTransform(81.1,312.1,1,1,0,0,0,22.4,41.5);

	this.addChild(this.instance,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,117.7,311.9);


(lib.rocks1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.rocks();
	this.instance.setTransform(0,0,1,1,0,0,0,14,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({rotation:24.5,x:0.1},0).wait(1).to({rotation:49.1,x:0,y:0.1},0).wait(1).to({rotation:73.6,x:0.1},0).wait(1).to({rotation:98.2,x:0,y:0},0).wait(1).to({rotation:122.7,y:0.1},0).wait(1).to({rotation:147.3,y:0},0).wait(1).to({rotation:171.8,y:0.1},0).wait(1).to({rotation:196.4,y:0},0).wait(1).to({rotation:220.9},0).wait(1).to({rotation:245.5},0).wait(1).to({rotation:270},0).wait(1).to({rotation:294.5},0).wait(1).to({rotation:319.1,x:0.1},0).wait(1).to({rotation:343.6},0).wait(1).to({rotation:368.2,x:0,y:0.1},0).wait(1).to({rotation:392.7,x:0.1,y:0},0).wait(1).to({rotation:417.3,x:0,y:0.1},0).wait(1).to({rotation:441.8,x:0.1,y:0},0).wait(1).to({rotation:466.4,x:0,y:0.1},0).wait(1).to({rotation:490.9},0).wait(1).to({rotation:515.5,y:0},0).wait(1).to({rotation:540},0).wait(1).to({rotation:564.5},0).wait(1).to({rotation:589.1},0).wait(1).to({rotation:613.6},0).wait(1).to({rotation:638.2,x:0.1},0).wait(1).to({rotation:662.7,x:0},0).wait(1).to({rotation:687.3,x:0.1},0).wait(1).to({rotation:711.8,x:0},0).wait(1).to({rotation:736.4,x:0.1,y:0.1},0).wait(1).to({rotation:760.9,y:0},0).wait(1).to({rotation:785.5,x:0,y:0.1},0).wait(1).to({rotation:810,y:0},0).wait(1).to({rotation:834.5,y:0.1},0).wait(1).to({rotation:859.1,y:0},0).wait(1).to({rotation:883.6,y:0.1},0).wait(1).to({rotation:908.2,y:0},0).wait(1).to({rotation:932.7},0).wait(1).to({rotation:957.3},0).wait(1).to({rotation:981.8},0).wait(1).to({rotation:1006.4,x:0.1},0).wait(1).to({rotation:1030.9,x:0},0).wait(1).to({rotation:1055.5,x:0.1},0).wait(1).to({rotation:1080,x:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.1,-12.4,32.3,24.9);


(lib.head2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.antenna();
	this.instance.setTransform(119.6,23.8,1,1,0,0,0,9.7,22.8);

	this.instance_1 = new lib.head();
	this.instance_1.setTransform(90.9,106.6,1,1,0,0,0,90.9,59.9);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,1,181.8,170);


(lib.cloudBurst3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.cloudcircleyellow();
	this.instance.setTransform(133.9,44.5,0.877,0.877,0,0,0,48.9,49);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.42,scaleY:1.42,x:144,y:-38.4},29).wait(1));

	// Layer 4
	this.instance_1 = new lib.cloudcircleyellow();
	this.instance_1.setTransform(183,76.5,1.062,1.062,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regY:49,scaleX:1.74,scaleY:1.74,x:180,y:83},29).wait(1));

	// Layer 3
	this.instance_2 = new lib.cloudcircleyellow();
	this.instance_2.setTransform(102,84,1,1,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:49.1,scaleX:1.49,scaleY:1.49,x:85,y:81.6},29).wait(1));

	// Layer 1
	this.instance_3 = new lib.cloudcircleyellow();
	this.instance_3.setTransform(183,24.4,0.47,0.47,0,0,0,48.9,49);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1.01,scaleY:1.01,x:216,y:-15.6},29).wait(1));

	// Layer 2
	this.instance_4 = new lib.cloudcircleyellow();
	this.instance_4.setTransform(61,69.4,0.47,0.47,0,0,0,48.9,49);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:49,scaleX:1.39,scaleY:1.39,x:69,y:-22.5},29).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(38,1.3,197.1,131.8);


(lib.CloudBurst2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.cloudcircleyellow();
	this.instance.setTransform(140.9,40.1,0.815,0.815,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.23,scaleY:2.23,x:272.9,y:-24.9},59).wait(1));

	// Layer 6
	this.instance_1 = new lib.cloudcircleyellow();
	this.instance_1.setTransform(180.9,80.1,0.907,0.907,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:2.58,scaleY:2.58,x:217.7,y:80.9},59).wait(1));

	// Layer 5 copy
	this.instance_2 = new lib.cloudcircleyellow();
	this.instance_2.setTransform(88.8,29.1,0.815,0.815,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:48.9,scaleX:2.54,scaleY:2.54,x:166.6,y:-76.6},59).wait(1));

	// Layer 5
	this.instance_3 = new lib.cloudcircleyellow();
	this.instance_3.setTransform(154.9,104.1,0.815,0.815,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1.52,scaleY:1.52,x:184,y:186.5},59).wait(1));

	// Layer 4
	this.instance_4 = new lib.cloudcircleyellow();
	this.instance_4.setTransform(94.9,114.1,0.814,0.652,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:48.9,scaleX:2.46,scaleY:2.51,x:79.1,y:132.4},59).wait(1));

	// Layer 3
	this.instance_5 = new lib.cloudcircleyellow();
	this.instance_5.setTransform(79,67.1,1.069,1.069,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regX:48.9,scaleX:2.63,scaleY:2.63,x:-8.3,y:-5},59).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(26.6,-10.9,198.8,157);


(lib.cloudBurst1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 22
	this.instance = new lib.cloudcircleorglt();
	this.instance.setTransform(61.2,140.2,1.245,1.245,0,0,0,49.1,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.18,scaleY:1.8,x:-21.8,y:139.2},29).wait(1));

	// Layer 21
	this.instance_1 = new lib.cloudcircleorglt();
	this.instance_1.setTransform(131.1,181,1.021,0.877,0,0,0,49.1,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:2.76,scaleY:2.37,x:115.2,y:224},29).wait(1));

	// Layer 20
	this.instance_2 = new lib.cloudcircleorglt();
	this.instance_2.setTransform(142.1,98,1.488,1.488,0,0,0,49.1,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:2.95,scaleY:2.88,x:97.5,y:-7.9},29).wait(1));

	// Layer 19
	this.instance_3 = new lib.cloudcircleorglt();
	this.instance_3.setTransform(217.1,82,0.632,0.632,0,0,0,49.1,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1.14,scaleY:1.14,x:259.1,y:28},29).wait(1));

	// Layer 18
	this.instance_4 = new lib.cloudcircleorglt();
	this.instance_4.setTransform(196.1,175,0.632,0.632,0,0,0,49.1,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:49,scaleX:1.47,scaleY:1.47,x:238,y:238},29).wait(1));

	// Layer 17
	this.instance_5 = new lib.cloudcircleorglt();
	this.instance_5.setTransform(236.1,154,1.245,1.245,0,0,0,49.1,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({scaleX:3.44,scaleY:2.43,x:330,y:141},29).wait(1));

	// Layer 16
	this.instance_6 = new lib.cloudcircleorgdk();
	this.instance_6.setTransform(250,104,0.572,0.572,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({regY:49.2,scaleX:1.84,scaleY:1.84,x:354.2,y:56.1},29).wait(1));

	// Layer 15
	this.instance_7 = new lib.cloudcircleorgdk();
	this.instance_7.setTransform(44,117.5,1.084,1.084,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({scaleX:1.61,scaleY:1.61,x:-21.9,y:51},29).wait(1));

	// Layer 14
	this.instance_8 = new lib.cloudcircleorgdk();
	this.instance_8.setTransform(61.1,79,1,1,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({scaleX:1.79,scaleY:1.79,x:-16.9,y:-22},29).wait(1));

	// Layer 13
	this.instance_9 = new lib.cloudcircleorgdk();
	this.instance_9.setTransform(186.1,82,1.204,1.204,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({regX:49.1,scaleX:2.22,scaleY:1.71,x:180.2,y:-55},29).wait(1));

	// Layer 24
	this.instance_10 = new lib.cloudcircleorgdk();
	this.instance_10.setTransform(88,194.5,0.572,0.572,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({regX:48.9,scaleX:1.59,scaleY:1.51,x:-51.1,y:216.4},29).wait(1));

	// Layer 5
	this.instance_11 = new lib.cloudcircleorgdk();
	this.instance_11.setTransform(184,203.4,1,1,0,0,0,49,49.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({scaleX:2.12,scaleY:1.73,x:234,y:279.4},29).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.1,22.9,306.2,229.7);


(lib.armrtupper = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.arm();
	this.instance.setTransform(38.9,0.6,1,1,0,0,0,38.9,0.6);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,-5,92.6,10.1);


(lib.armrtlower = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.arm();
	this.instance.setTransform(39,0,1,1,0,0,0,39,0);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,-5,92.6,10.1);


(lib.eliosmall = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// helmet
	this.instance = new lib.helmet();
	this.instance.setTransform(61.1,38,0.126,0.126,85.4,0,0,100,95.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20));

	// hand
	this.instance_1 = new lib.hand();
	this.instance_1.setTransform(52.3,7.2,0.126,0.126,0,-84.7,95.3,22.8,-1.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:21.8,regY:15.2,skewX:-92,skewY:88,x:53.8,y:6.7},0).wait(1).to({skewX:-99.3,skewY:80.7,x:53.2,y:6.2},0).wait(1).to({skewX:-106.7,skewY:73.3,x:52.7,y:5.7},0).wait(1).to({skewX:-114.1,skewY:65.9,x:52,y:5.1},0).wait(1).to({skewX:-121.4,skewY:58.6,x:51.4,y:4.7},0).wait(1).to({skewX:-128.8,skewY:51.2,x:50.7,y:4.2},0).wait(1).to({skewX:-136.2,skewY:43.8,x:50,y:3.7},0).wait(1).to({skewX:-143.5,skewY:36.5,x:49.3,y:3.3},0).wait(1).to({skewX:-150.9,skewY:29.1,x:48,y:2.8},0).wait(1).to({skewX:-158.3,skewY:21.7,x:46.6,y:2.3},0).wait(1).to({skewX:-165.6,skewY:14.4,x:45.2,y:2},0).wait(1).to({skewX:-173,skewY:7,x:43.9,y:1.6},0).wait(1).to({skewX:-180.4,skewY:-0.4,x:42.7,y:1.7},0).wait(1).to({skewX:-187.7,skewY:-7.7,x:41.4,y:1.8},0).wait(1).to({skewX:-195.1,skewY:-15.1,x:40.3,y:1.9},0).wait(1).to({skewX:-202.5,skewY:-22.5,x:39.1,y:2.1},0).wait(1).to({skewX:-209.8,skewY:-29.8,x:38,y:2.3},0).wait(1).to({skewX:-217.2,skewY:-37.2,x:36.9,y:2.5},0).wait(1).to({skewX:-224.5,skewY:-44.5,x:35.7,y:2.8},0).wait(1));

	// arm rt lower
	this.instance_2 = new lib.armrtlower();
	this.instance_2.setTransform(44.2,13.9,0.126,0.126,0,140.1,-39.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:41.3,skewX:137,skewY:-43,x:47.8,y:10.4},0).wait(1).to({skewX:133.8,skewY:-46.2,x:47.3,y:10.2},0).wait(1).to({skewX:130.7,skewY:-49.3,x:46.9,y:10.1},0).wait(1).to({skewX:127.5,skewY:-52.5,x:46.5,y:9.9},0).wait(1).to({skewX:124.4,skewY:-55.6,x:46,y:9.8},0).wait(1).to({skewX:121.2,skewY:-58.8,x:45.5,y:9.7},0).wait(1).to({skewX:118,skewY:-62,x:45.1,y:9.6},0).wait(1).to({skewX:114.9,skewY:-65.1,x:44.6,y:9.5},0).wait(1).to({skewX:111.7,skewY:-68.3,x:44.1},0).wait(1).to({skewX:108.6,skewY:-71.4,x:43.6,y:9.4},0).wait(1).to({skewX:105.4,skewY:-74.6,x:43},0).wait(1).to({skewX:102.3,skewY:-77.7,x:42.4},0).wait(1).to({skewX:99.1,skewY:-80.9,x:41.8},0).wait(1).to({skewX:95.9,skewY:-84.1,x:41.2,y:9.5},0).wait(1).to({skewX:92.8,skewY:-87.2,x:40.6},0).wait(1).to({skewX:89.6,skewY:-90.4,x:39.9,y:9.6},0).wait(1).to({skewX:86.5,skewY:-93.5,x:39.4,y:9.7},0).wait(1).to({skewX:83.3,skewY:-96.7,x:38.7,y:9.8},0).wait(1).to({skewX:80.2,skewY:-99.8,x:38.1,y:9.9},0).wait(1));

	// arm rt upper
	this.instance_3 = new lib.armrtupper();
	this.instance_3.setTransform(44.8,24.3,0.126,0.126,0,87.2,-93);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({regX:41.3,skewX:85.6,skewY:-94.6,x:44.4,y:19.1},0).wait(1).to({skewX:84,skewY:-96.2,x:44.3},0).wait(1).to({skewX:82.5,skewY:-97.8,x:44.1,y:19.2},0).wait(1).to({skewX:80.9,skewY:-99.3,x:44},0).wait(1).to({skewX:79.3,skewY:-100.9,x:43.8},0).wait(1).to({skewX:77.7,skewY:-102.5,x:43.7},0).wait(1).to({skewX:76.1,skewY:-104.1,x:43.6,y:19.3},0).wait(1).to({skewX:74.6,skewY:-105.7,x:43.4},0).wait(1).to({skewX:73,skewY:-107.2,x:43.3},0).wait(1).to({skewX:71.4,skewY:-108.8,x:43.1,y:19.4},0).wait(1).to({skewX:69.8,skewY:-110.4,x:43},0).wait(1).to({skewX:68.2,skewY:-112,x:42.9,y:19.5},0).wait(1).to({skewX:66.7,skewY:-113.5,x:42.7},0).wait(1).to({skewX:65.1,skewY:-115.1,x:42.6,y:19.6},0).wait(1).to({skewX:63.5,skewY:-116.7,x:42.5,y:19.7},0).wait(1).to({skewX:61.9,skewY:-118.3,x:42.4},0).wait(1).to({skewX:60.4,skewY:-119.9,x:42.2,y:19.8},0).wait(1).to({skewX:58.8,skewY:-121.4,x:42.1,y:19.9},0).wait(1).to({skewX:57.2,skewY:-123,x:42},0).wait(1));

	// body
	this.instance_4 = new lib.body();
	this.instance_4.setTransform(39.7,38.7,0.126,0.127,85.5,0,0,108.5,94.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(20));

	// hand
	this.instance_5 = new lib.hand();
	this.instance_5.setTransform(53.6,71.4,0.126,0.126,-51.4,0,0,21.1,-1.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1).to({regX:21.8,regY:15.2,rotation:-56.4,x:56.1,y:71.8},0).wait(1).to({rotation:-61.4,x:57,y:71.1},0).wait(1).to({rotation:-66.4,x:57.8,y:70.4},0).wait(1).to({rotation:-71.4,x:58.6,y:69.6},0).wait(1).to({rotation:-76.4,x:59.4,y:68.8},0).wait(1).to({rotation:-81.4,x:60.2,y:68.1},0).wait(1).to({rotation:-86.4,x:61,y:67.2},0).wait(1).to({rotation:-91.4,x:61.8,y:66.4},0).wait(1).to({rotation:-96.4,x:62.6,y:65.5},0).wait(1).to({rotation:-101.4,x:63.3,y:64.6},0).wait(1).to({rotation:-106.4,x:64,y:63.7},0).wait(1).to({rotation:-111.4,x:64.8,y:62.8},0).wait(1).to({rotation:-116.4,x:65.5,y:61.9},0).wait(1).to({rotation:-121.4,x:65.8,y:60.8},0).wait(1).to({rotation:-126.4,x:66.2,y:59.7},0).wait(1).to({rotation:-131.4,x:66.6,y:58.7},0).wait(1).to({rotation:-136.4,x:66.9,y:57.6},0).wait(1).to({rotation:-141.4,x:67.3,y:56.6},0).wait(1).to({rotation:-146.4,x:67.6,y:55.6},0).wait(1));

	// arm rt lower
	this.instance_6 = new lib.armrtlower();
	this.instance_6.setTransform(44.8,64.6,0.126,0.126,37.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({regX:41.3,rotation:35.1,x:49.6,y:67.5},0).wait(1).to({rotation:32.4,x:50.4,y:67.2},0).wait(1).to({rotation:29.7,x:51.1,y:66.9},0).wait(1).to({rotation:26.9,x:51.8,y:66.6},0).wait(1).to({rotation:24.2,x:52.5,y:66.3},0).wait(1).to({rotation:21.5,x:53.3,y:65.9},0).wait(1).to({rotation:18.8,x:54.1,y:65.6},0).wait(1).to({rotation:16,x:54.8,y:65.2},0).wait(1).to({rotation:13.3,x:55.5,y:64.8},0).wait(1).to({rotation:10.6,x:56.2,y:64.4},0).wait(1).to({rotation:7.9,x:56.9,y:63.8},0).wait(1).to({rotation:5.1,x:57.7,y:63.3},0).wait(1).to({rotation:2.4,x:58.4,y:62.7},0).wait(1).to({rotation:-0.3,x:58.8,y:62},0).wait(1).to({rotation:-3.1,x:59.3,y:61.3},0).wait(1).to({rotation:-5.8,x:59.7,y:60.6},0).wait(1).to({rotation:-8.5,x:60.1,y:59.9},0).wait(1).to({rotation:-11.2,x:60.5,y:59.2},0).wait(1).to({rotation:-14,x:60.9,y:58.5},0).wait(1));

	// arm rt upper
	this.instance_7 = new lib.armrtupper();
	this.instance_7.setTransform(47.1,53.9,0.126,0.126,104.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regX:41.3,rotation:101.1,x:46.1,y:59},0).wait(1).to({rotation:97.3,x:46.4,y:59.1},0).wait(1).to({rotation:93.6,x:46.8},0).wait(1).to({rotation:89.8,x:47.1},0).wait(1).to({rotation:86.1,x:47.4},0).wait(1).to({rotation:82.3,x:47.8,y:59},0).wait(1).to({rotation:78.6,x:48.1},0).wait(1).to({rotation:74.8,x:48.4,y:58.9},0).wait(1).to({rotation:71.1,x:48.8,y:58.8},0).wait(1).to({rotation:67.3,x:49.1,y:58.7},0).wait(1).to({rotation:63.6,x:49.4,y:58.5},0).wait(1).to({rotation:59.8,x:49.7,y:58.4},0).wait(1).to({rotation:56.1,x:50,y:58.2},0).wait(1).to({rotation:52.3,x:50.3,y:58},0).wait(1).to({rotation:48.6,x:50.5,y:57.8},0).wait(1).to({rotation:44.8,x:50.8,y:57.6},0).wait(1).to({rotation:41.1,x:51,y:57.3},0).wait(1).to({rotation:37.3,x:51.2,y:57},0).wait(1).to({rotation:33.6,x:51.4,y:56.8},0).wait(1));

	// foot
	this.instance_8 = new lib.foot();
	this.instance_8.setTransform(4.7,48.7,0.126,0.127,64.2,0,0,32.1,5.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(20));

	// leg mid
	this.instance_9 = new lib.legmid();
	this.instance_9.setTransform(18.1,50.3,0.126,0.127,94.2,0,0,17.2,16.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(20));

	// leg upper
	this.instance_10 = new lib.legupper();
	this.instance_10.setTransform(24.3,48.3,0.126,0.127,64.2,0,0,-0.8,35.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(20));

	// leg lower
	this.instance_11 = new lib.leglower();
	this.instance_11.setTransform(10.4,49.7,0.126,0.127,94.2,0,0,0.4,43.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(20));

	// foot
	this.instance_12 = new lib.foot();
	this.instance_12.setTransform(7.4,22.2,0.126,0.127,138.9,0,0,35.6,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(20));

	// leg mid
	this.instance_13 = new lib.legmid();
	this.instance_13.setTransform(19.9,26.7,0.126,0.127,108.9,0,0,17.4,17.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(20));

	// leg upper
	this.instance_14 = new lib.legupper();
	this.instance_14.setTransform(24.1,31.6,0.126,0.127,138.9,0,0,1.2,34.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(20));

	// leg lower
	this.instance_15 = new lib.leglower();
	this.instance_15.setTransform(12.5,24.2,0.126,0.127,108.9,0,0,0.6,44.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,3.6,75.9,73.3);


(lib.eliolarge = function() {
	this.initialize();

	// helmet
	this.instance = new lib.helmet();
	this.instance.setTransform(66.8,34.5,0.36,0.36,0,0,0,99,95.7);

	// hand lt
	this.instance_1 = new lib.hand();
	this.instance_1.setTransform(17.9,111.6,0.36,0.361,0,30.6,-149.4,21.9,0.6);

	// arm lt upper
	this.instance_2 = new lib.armrtupper();
	this.instance_2.setTransform(23.7,75.2,0.359,0.359,0,-21.4,158.3);

	// arm lt lower
	this.instance_3 = new lib.armrtlower();
	this.instance_3.setTransform(-5.2,87.1,0.361,0.36,0,47.3,-132.7,83.2,-0.1);

	// hand rt
	this.instance_4 = new lib.hand();
	this.instance_4.setTransform(116.4,110.4,0.359,0.36,-15,0,0,20.4,-0.1);

	// arm rt upper
	this.instance_5 = new lib.armrtupper();
	this.instance_5.setTransform(109.8,76.2,0.36,0.36,0,17,17.2);

	// arm rt lower
	this.instance_6 = new lib.armrtlower();
	this.instance_6.setTransform(139.3,86.2,0.359,0.358,130.9,0,0,0.2,-2);

	// body2
	this.instance_7 = new lib.bodycopy();
	this.instance_7.setTransform(64.2,95.5,0.36,0.36,0,0,0,109,93.8);

	// foot rt
	this.instance_8 = new lib.foot();
	this.instance_8.setTransform(83,205.6,0.36,0.361,0,0,0,36.7,24.7);

	// leg rt mid
	this.instance_9 = new lib.legmid();
	this.instance_9.setTransform(83,160.3,0.36,0.361,0,0,0,17.5,17.9);

	// leg rt upper
	this.instance_10 = new lib.legupper();
	this.instance_10.setTransform(83,129.2,0.36,0.361);

	// leg rt lower
	this.instance_11 = new lib.leglower();
	this.instance_11.setTransform(82.6,196.9,0.36,0.361,0,0,0,-1.1,84.2);

	// foot lt
	this.instance_12 = new lib.foot();
	this.instance_12.setTransform(50.5,205.6,0.36,0.361,0,0,0,36.7,24.7);

	// leg lt mid
	this.instance_13 = new lib.legmid();
	this.instance_13.setTransform(50.5,160.3,0.36,0.361,0,0,0,17.7,17.9);

	// leg lt upper
	this.instance_14 = new lib.legupper();
	this.instance_14.setTransform(50.5,129.2,0.36,0.361);

	// leg lt lower
	this.instance_15 = new lib.leglower();
	this.instance_15.setTransform(50.5,196.7,0.36,0.361,0,0,0,-0.1,83.9);

	this.addChild(this.instance_15,this.instance_14,this.instance_13,this.instance_12,this.instance_11,this.instance_10,this.instance_9,this.instance_8,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-7.7,0,149,214.3);


(lib.elioend = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.hand();
	this.instance.setTransform(23.7,95.7,0.36,0.36,0,146.5,-33.5,21.7,0.3);

	this.instance_1 = new lib.armrtupper();
	this.instance_1.setTransform(74.7,128.7,0.357,0.357,0,23.8,-156.4);

	this.instance_2 = new lib.armrtlower();
	this.instance_2.setTransform(46.6,115.7,0.359,0.358,0,-137.8,42.2,84.3,2.1);

	this.instance_3 = new lib.head2();
	this.instance_3.setTransform(62.7,81.7,0.359,0.36,-95.6,0,0,82.5,167);

	this.instance_4 = new lib.hand();
	this.instance_4.setTransform(59.6,7.6,0.359,0.359,89.1,0,0,21.2,1.6);

	this.instance_5 = new lib.armrtupper();
	this.instance_5.setTransform(98.1,46.9,0.356,0.356,0,-114.1,-113.9,-1.4,-0.7);

	this.instance_6 = new lib.armrtlower();
	this.instance_6.setTransform(86.7,20.1,0.357,0.356,-155.4,0,0,-1.4,1);

	this.instance_7 = new lib.body();
	this.instance_7.setTransform(103.5,94,0.359,0.36,-75.7,0,0,111.1,93.7);

	this.instance_8 = new lib.foot();
	this.instance_8.setTransform(224.2,69.4,0.359,0.36,-119.8,0,0,36.6,24.4);

	this.instance_9 = new lib.legmid();
	this.instance_9.setTransform(175.7,77.7,0.36,0.361,0,0,0,17.5,17.9);

	this.instance_10 = new lib.legupper();
	this.instance_10.setTransform(139.9,91.8,0.359,0.479,-112,0,0,-0.1,-0.1);

	this.instance_11 = new lib.leglower();
	this.instance_11.setTransform(181.5,75.6,0.359,0.427,-92,0,0,-1.1,0.1);

	this.instance_12 = new lib.foot();
	this.instance_12.setTransform(202.8,143.6,0.359,0.36,-51.7,0,0,5,44.8);

	this.instance_13 = new lib.legmid();
	this.instance_13.setTransform(171.7,105.7,0.36,0.361,0,0,0,17.7,17.9);

	this.instance_14 = new lib.legupper();
	this.instance_14.setTransform(134.7,109.6,0.359,0.513,-95.8,0,0,0.1,0.1);

	this.instance_15 = new lib.leglower();
	this.instance_15.setTransform(197.9,125.3,0.359,0.343,-53.4,0,0,-0.3,84);

	this.addChild(this.instance_15,this.instance_14,this.instance_13,this.instance_12,this.instance_11,this.instance_10,this.instance_9,this.instance_8,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(1.5,-1.8,236.9,149.6);


(lib.cloudBurst4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.whitebox();
	this.instance.setTransform(163.3,-21.6,1.342,5.683,0,0,0,594.1,93.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0},5).to({_off:true},1).wait(23).to({_off:false},0).to({alpha:1},20).wait(1));

	// Layer 1
	this.instance_1 = new lib.cloudBurst1();
	this.instance_1.setTransform(0,0,0.618,0.618,0,0,0,143.8,137.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-634,-553,1594.4,1062.8);


(lib.movieClip1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//this.gotoAndPlay(140);
	}
	this.frame_234 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(234).call(this.frame_234).wait(1));

	// helmet 2 copy
	this.instance = new lib.helmet();
	this.instance.setTransform(693.6,540.1,0.36,0.36,-169,0,0,98.2,96.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(174).to({_off:false},0).wait(1).to({regX:99.2,regY:95.7,rotation:-127.2,x:704.9,y:517.5},0).wait(1).to({rotation:-113.5,x:708.1,y:513.4},0).wait(1).to({rotation:-101.7,x:711.4,y:509.8},0).wait(1).to({rotation:-91.5,x:714.2,y:507.3},0).wait(1).to({rotation:-82.2,x:716.8,y:505.5},0).wait(1).to({rotation:-74.2,x:718.8,y:504.4},0).wait(1).to({rotation:-66.3,x:720.6,y:503.6},0).wait(1).to({rotation:-58,x:722.6,y:503},0).wait(1).to({rotation:-48.9,x:724.7,y:502.6},0).wait(1).to({rotation:-38.6,x:727,y:502.4},0).wait(1).to({rotation:-26.7,x:729.7,y:502.6},0).wait(1).to({rotation:-13.2,x:732.8,y:503.5},0).wait(1).to({rotation:1.4,x:736.6,y:505.2},0).wait(1).to({rotation:18.5,x:741.1,y:508.6},0).wait(1).to({rotation:40.9,x:747.2,y:515.7},0).wait(1).to({regX:98.4,regY:96.5,rotation:100.7,x:755.7,y:539.8},0).to({regX:185.1,regY:88.9,x:752.6,y:571},1).to({regX:184.9,regY:88.8,rotation:79.2},22,cjs.Ease.get(-1)).wait(22));

	// ground blur
	this.instance_1 = new lib.Bitmap1();
	this.instance_1.setTransform(762.5,531.4);

	this.instance_2 = new lib.Bitmap1();
	this.instance_2.setTransform(-60.5,532.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1}]}).wait(235));

	// ground
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#6B7B84").ss(2).p("Eg+fAAAMB8/AAA");
	this.shape.setTransform(399,570);

	this.instance_3 = new lib.ground();
	this.instance_3.setTransform(398,652.6,1,1,0,0,0,426,102.7);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.instance_3}]},30).to({state:[{t:this.instance_3}]},30).to({state:[{t:this.instance_3}]},18).wait(157));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(30).to({_off:false},0).wait(30).to({regX:426.1,scaleX:1.75,scaleY:1.75,x:213.1,y:717.6},18,cjs.Ease.get(1)).wait(157));

	// white box
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhczAOnIAA9MMC5mAAAIAAdMg");
	this.shape_1.setTransform(400,664);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).to({_off:true},30).wait(205));

	// elio large
	this.instance_4 = new lib.eliolarge();
	this.instance_4.setTransform(442,465.1,1,1,0,0,0,69.3,107.2);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(78).to({_off:false},0).to({alpha:1},21).to({_off:true},15).wait(121));

	// lines
	this.instance_5 = new lib.smackMC();
	this.instance_5.setTransform(453.2,364.1,1,1,-110.8,0,0,17.4,17.6);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(160).to({_off:false},0).to({alpha:0},7,cjs.Ease.get(1)).to({_off:true},7).wait(61));

	// lines
	this.instance_6 = new lib.smackMC();
	this.instance_6.setTransform(472.7,417,1,1,75,0,0,20.6,17.6);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(160).to({_off:false},0).to({alpha:0},7,cjs.Ease.get(1)).to({_off:true},7).wait(61));

	// fire
	this.instance_7 = new lib.shipFlamesMC();
	this.instance_7.setTransform(267.7,517.7,1.177,1.177,-163.7);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(180).to({_off:false},0).wait(31).to({alpha:0},6).wait(18));

	// fire
	this.instance_8 = new lib.shipFlamesMC();
	this.instance_8.setTransform(219.2,517.1,1,1,156.6);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(180).to({_off:false},0).wait(31).to({alpha:0},13).wait(11));

	// ship
	this.instance_9 = new lib.ship();
	this.instance_9.setTransform(189,463,0.92,0.92,0,0,0,59,155.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({regX:58.8,regY:163.4,x:188.5,y:469.5},0).wait(1).to({y:467.9},0).wait(1).to({y:465.1},0).wait(1).to({y:461.2},0).wait(1).to({y:456.1},0).wait(1).to({y:449.8},0).wait(1).to({y:442.2},0).wait(1).to({y:433.2},0).wait(1).to({y:422.9},0).wait(1).to({x:188.4,y:411.1},0).wait(1).to({y:397.9},0).wait(1).to({y:383.1},0).wait(1).to({x:188.3,y:366.6},0).wait(1).to({y:348.4},0).wait(1).to({y:328.5},0).wait(1).to({x:188.2,y:306.7},0).wait(1).to({y:282.8},0).wait(1).to({rotation:-0.1,x:188.1,y:256.9},0).wait(1).to({y:228.6},0).wait(1).to({x:188,y:198},0).wait(1).to({rotation:26.5,x:190,y:163.9},0).wait(1).to({rotation:52.2,x:213.2,y:132},0).wait(1).to({rotation:77,x:246.5,y:112.7},0).wait(1).to({rotation:95.1,x:289.4,y:107.7},0).wait(1).to({rotation:118.1,x:334.7,y:118.2},0).wait(1).to({rotation:149.3,x:373.7,y:148.7},0).wait(1).to({rotation:180.1,x:384.5,y:201.5},0).wait(1).to({rotation:180,x:384.3,y:265},0).wait(1).to({x:384.2,y:338.3},0).wait(1).to({regX:58.6,regY:156.2,x:385,y:445},0).to({scaleX:1.06},3,cjs.Ease.get(1)).to({scaleX:0.92},4,cjs.Ease.get(1)).wait(23).to({scaleX:1.85,scaleY:1.85,x:192,y:318},18,cjs.Ease.get(1)).wait(6).to({regY:0,y:607},0).to({regY:0.1,rotation:181.6,y:606.8},17,cjs.Ease.get(1)).to({regY:0,rotation:180,y:607},18,cjs.Ease.get(-1)).to({regX:58.5,rotation:178.2,x:192.1,y:606.9},15,cjs.Ease.get(1)).to({regX:58.6,rotation:180,x:192,y:607},14,cjs.Ease.get(-1)).wait(1).to({regX:40.8,regY:35.1,rotation:180.4,x:224.8,y:542},0).wait(1).to({regX:58.8,regY:163.4,x:193.1,y:304.4},0).wait(1).to({rotation:180.5,x:193.5},0).wait(1).to({rotation:180.6,x:194.1,y:304.3},0).wait(1).to({rotation:180.8,x:194.9},0).wait(1).to({rotation:181.1,x:196,y:304.1},0).wait(1).to({rotation:181.4,x:197.2,y:304},0).wait(1).to({rotation:181.8,x:198.8,y:303.8},0).wait(1).to({rotation:182.2,x:200.8,y:303.5},0).wait(1).to({rotation:182.8,x:203,y:303.3},0).wait(1).to({rotation:183.4,x:205.6,y:303.1},0).wait(1).to({rotation:184.1,x:208.5,y:302.9},0).wait(1).to({rotation:184.9,x:211.8,y:302.7},0).wait(1).to({rotation:185.8,x:215.6,y:302.5},0).wait(1).to({rotation:186.8,x:219.8,y:302.4},0).wait(1).to({rotation:187.9,x:224.5},0).wait(1).to({rotation:189.2,x:229.7},0).wait(1).to({rotation:190.6,x:235.5,y:302.6},0).wait(1).to({rotation:192.1,x:242,y:303},0).wait(1).to({rotation:193.8,x:249.2,y:303.5},0).wait(1).to({rotation:195.8,x:257.2,y:304.5},0).wait(1).to({rotation:197.9,x:266.1,y:305.9},0).wait(1).to({rotation:200.3,x:276.1,y:307.9},0).wait(1).to({rotation:203.1,x:287.3,y:310.6},0).wait(1).to({rotation:206.2,x:299.8,y:314.3},0).wait(1).to({rotation:209.8,x:314,y:319.5},0).wait(1).to({rotation:214.1,x:330.3,y:326.7},0).wait(1).to({rotation:219.3,x:349.2,y:337.1},0).wait(1).to({rotation:225.8,x:371.8,y:352.6},0).wait(1).to({rotation:235.1,x:400.4,y:378.8},0).wait(1).to({regX:40.8,regY:35.1,rotation:260.7,x:224.8,y:541.8},0).wait(1).to({regX:-0.4,regY:182.4,x:506,y:573.1},0).wait(1).to({regX:58.8,regY:163.4,rotation:264.3,x:460.1,y:467.5},0).wait(1).to({rotation:265.3,x:462,y:466.8},0).wait(1).to({rotation:266,x:463.2,y:466.2},0).wait(1).to({rotation:266.4,x:464,y:465.9},0).wait(1).to({rotation:266.8,x:464.7,y:465.7},0).wait(1).to({rotation:267,x:465.1,y:465.4},0).wait(1).to({rotation:267.2,x:465.5,y:465.3},0).wait(1).to({rotation:267.4,x:465.7},0).wait(1).to({rotation:267.5,x:466,y:465.2},0).wait(1).to({rotation:267.6,x:466.1,y:465.1},0).wait(1).to({x:466.3},0).wait(1).to({rotation:267.7,y:465},0).wait(1).to({x:466.4},0).wait(1).to({regX:-0.4,regY:182.4,x:505.9,y:573.1},0).wait(1).to({rotation:260.7,x:506},14,cjs.Ease.get(1)).wait(26));

	// white box end
	this.instance_10 = new lib.whitebox();
	this.instance_10.setTransform(403,271,0.774,3.443,0,0,0,594,93.5);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(219).to({_off:false},0).to({alpha:1},15).wait(1));

	// cloudBurst4 copy
	this.instance_11 = new lib.cloudBurst4();
	this.instance_11.setTransform(470.1,517.8);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(194).to({_off:false},0).wait(41));

	// elio end
	this.instance_12 = new lib.elioend();
	this.instance_12.setTransform(340.4,503.4,1,1,0,0,0,119,73);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(174).to({_off:false},0).to({alpha:0},7).to({_off:true},49).wait(5));

	// cloudBurst4
	this.instance_13 = new lib.cloudBurst4();
	this.instance_13.setTransform(404.8,535.1);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(175).to({_off:false},0).wait(60));

	// whiteBox2
	this.whiteBox1 = new lib.whitebox();
	this.whiteBox1.setTransform(396.1,308.8,0.704,3.13,0,0,0,594,93.5);
	this.whiteBox1.alpha = 0;
	this.whiteBox1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.whiteBox1).wait(81).to({_off:false},0).to({alpha:1},8).to({_off:true},1).wait(145));

	// cloudBurst2
	this.instance_14 = new lib.CloudBurst2();
	this.instance_14.setTransform(426.7,565,0.35,0.35,0,0,0,154.5,92.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(30).to({_off:false},0).to({regY:92.4,scaleX:0.71,scaleY:0.71,x:466.5,y:553.8},20,cjs.Ease.get(1)).wait(10).to({scaleX:1.03,scaleY:1.03,x:331.9},18,cjs.Ease.get(1)).wait(1).to({regX:154.6,regY:92.3,scaleX:1.24,scaleY:1.24,x:399.1,y:553.6},10).to({_off:true},1).wait(145));

	// hand lt
	this.instance_15 = new lib.hand();
	this.instance_15.setTransform(463,444,0.36,0.36,0,-82.7,97.3,21.9,0.1);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(159).to({_off:false},0).wait(1).to({regX:21.8,scaleY:0.36,skewX:-89.7,skewY:90.3,x:457.2,y:442},0).wait(1).to({scaleY:0.36,skewX:-84.7,skewY:95.3,x:450,y:454},0).wait(1).to({regX:21.9,scaleX:0.36,skewX:-59.9,skewY:120.1,x:433.6,y:486.6},0).wait(1).to({regY:0.2,skewX:-39.6,skewY:140.4,x:412.5,y:507.2},0).wait(1).to({regX:21.8,regY:0.3,skewX:-23.8,skewY:156.2,x:392.3,y:519.2},0).wait(1).to({regX:21.9,regY:-0.1,skewX:-12.5,skewY:167.5,x:377,y:526.2},0).wait(1).to({regY:0.1,scaleX:0.36,skewX:2.7,skewY:182.7,x:367.9,y:526.4},0).wait(1).to({regX:21.8,regY:0.4,skewX:22.2,skewY:202.2,x:364.7,y:526.9},0).to({_off:true},1).wait(67));

	// hand lt
	this.instance_16 = new lib.hand();
	this.instance_16.setTransform(381,488.9,0.36,0.361,0,-14.7,165.2,21.9,0.2);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(114).to({_off:false},0).wait(1).to({regX:22.1,regY:-0.1,skewX:0.2,skewY:180.2,x:376.8,y:489.2},0).wait(1).to({regX:21.8,regY:0.1,skewX:30.2,skewY:210.2,x:364.1,y:485},0).wait(1).to({regX:21.6,regY:-0.1,skewX:45.2,skewY:225.2,x:346.1,y:470},0).wait(1).to({regX:21.7,regY:0.1,skewX:112.5,skewY:292.4,x:335,y:439},0).wait(1).to({regX:21.6,regY:0,skewX:150.2,skewY:330.2,x:349.8,y:400.8},0).wait(1).to({regX:21.9,regY:0.2,skewX:270.2,skewY:450.3,x:392,y:391},0).wait(11).to({x:391.8,y:390.3},0).wait(1).to({x:391.9,y:387.4},0).wait(1).to({x:391.4,y:381.8},0).wait(1).to({x:389.7,y:376.2},0).wait(1).to({x:388,y:372},0).wait(1).to({skewX:207.1,skewY:387.1,x:393.3,y:377.1},0).wait(5).to({skewX:232.5,skewY:412.5,x:420.3,y:381.8},0).wait(1).to({regY:0.1,skewX:255,skewY:435,x:440.6,y:395.8},0).wait(1).to({regX:21.8,skewX:244.5,skewY:424.5,x:447.6,y:403.3},0).wait(1).to({regX:21.9,skewX:258.3,skewY:438.3,x:455.9,y:414.8},0).wait(1).to({regY:0,skewX:274.7,skewY:454.7,x:462.5,y:431.3},0).wait(1).to({regX:21.7,regY:0.1,skewX:285.2,skewY:465.2,x:464.6,y:440.5},0).wait(1).to({skewX:295.4,skewY:475.4,x:462.1,y:450.9},0).wait(1).to({regX:21.6,skewX:302.6,skewY:482.6,x:459.3,y:458.1},0).wait(1).to({regX:21.5,regY:0.3,skewX:300.7,skewY:480.7,x:460.5,y:455.8},0).wait(1).to({regY:0.2,skewX:287.5,skewY:467.5,x:460.8,y:454.3},0).wait(1).to({regX:21.6,skewX:273.1,skewY:453.1,x:461.4,y:452.2},0).wait(1).to({skewX:269.9,skewY:449.9,x:461.7,y:450.5},0).wait(1).to({skewX:259.5,skewY:439.5,x:462.1,y:448.7},0).wait(1).to({regX:21.4,regY:0.3,skewX:256.2,skewY:436.2,x:462.6,y:446.4},0).to({_off:true},5).wait(9).to({_off:false,regX:21.8,regY:0.4,scaleY:0.36,skewX:382.2,skewY:562.2,x:364.7,y:526.9},0).wait(1).to({regX:21.9,regY:0.2,skewX:388.2,skewY:568.2,x:351.5,y:532.6},0).wait(1).to({skewX:401.5,skewY:581.5,x:317.3,y:535.8},0).wait(1).to({regX:21.8,skewX:326.5,skewY:506.5,x:321.3,y:552.7},0).wait(1).to({regY:0.1,skewX:206.5,skewY:386.5,x:291.7,y:511.2},0).wait(1).to({regX:21.7,regY:0.3,skewX:146.5,skewY:326.5,x:245.9,y:526},0).to({_off:true},1).wait(61));

	// arm lt upper
	this.instance_17 = new lib.armrtupper();
	this.instance_17.setTransform(397.9,436.1,0.359,0.359,0,-50.4,129.4);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(114).to({_off:false},0).wait(1).to({skewX:-47.6,skewY:132.1,x:397.6,y:435.5},0).wait(1).to({skewX:-39.4,skewY:140.4,x:397.5,y:436.5},0).wait(1).to({skewX:-25.3,skewY:154.4,x:397,y:437},0).wait(1).to({scaleY:0.36,skewX:-5.8,skewY:174,x:396.2,y:437.7},0).wait(1).to({scaleY:0.36,skewX:19.5,skewY:199.3,x:395.2,y:438.5},0).wait(1).to({skewX:50.1,skewY:229.9,x:394,y:439.6},0).wait(1).to({x:394.1},0).wait(9).to({scaleY:0.36},0).wait(1).to({skewX:51.2,skewY:230.9,x:394},0).wait(1).to({skewX:54.9,skewY:234.6},0).wait(1).to({skewX:60.9,skewY:240.7,y:439.5},0).wait(1).to({scaleX:0.36,skewX:69.2,skewY:249,y:439.6},0).wait(1).to({skewX:80,skewY:259.7},0).wait(1).to({skewX:87.9,skewY:267.7,x:394.1},0).wait(5).to({skewX:113.4,skewY:293.2,y:438.5},0).wait(1).to({skewX:135.9,skewY:315.6,x:394.7,y:438.2},0).wait(1).to({skewX:146.3,skewY:326.1,x:395.5,y:437.9},0).wait(1).to({skewX:159.1,skewY:338.8,x:397.5,y:437.1},0).wait(1).to({skewX:178.8,skewY:358.6,x:400.1,y:434.4},0).wait(1).to({skewX:189.3,skewY:369,x:402.7,y:432.2},0).wait(1).to({skewX:199.5,skewY:379.3,x:402.6,y:431.8},0).wait(1).to({skewX:206.7,skewY:386.5,x:402.8,y:431.6},0).wait(1).to({skewX:204.7,skewY:384.5},0).wait(10).to({scaleX:0.36,scaleY:0.36,skewX:199.8,skewY:379.6,x:403,y:432},0).wait(1).to({skewX:193.1,skewY:372.8,x:396.5,y:437.3},0).wait(1).to({skewX:199.5,skewY:379.3,x:390.5,y:443.3},0).wait(1).to({skewX:228.2,skewY:407.9,x:384.9,y:450.8},0).wait(1).to({skewX:252,skewY:431.7,x:380.3,y:457},0).wait(1).to({skewX:270.3,skewY:450,x:376.4,y:462.7},0).wait(1).to({skewX:283.5,skewY:463.3,x:374.1,y:467.7},0).wait(1).to({skewX:291.1,skewY:470.9,x:372.6,y:467.3},0).wait(1).to({skewX:293.8,skewY:473.6,x:372,y:468},0).wait(2).to({skewX:298.3,skewY:478.1,x:368.2,y:473.5},0).wait(1).to({skewX:311.8,skewY:491.6,x:356.5,y:487.7},0).wait(1).to({skewX:334.3,skewY:514.1,x:337.2,y:510.8},0).wait(1).to({skewX:365.8,skewY:545.6,x:310.9,y:542.1},0).wait(1).to({skewX:383.8,skewY:563.6,x:296.3,y:558.7},0).to({_off:true},1).wait(61));

	// arm lt lower
	this.instance_18 = new lib.armrtlower();
	this.instance_18.setTransform(379,458.9,0.361,0.36,0,85.7,-94.3,83.2,-0.2);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(114).to({_off:false},0).wait(1).to({regY:-0.1,skewX:91.8,skewY:-88.2,x:378,y:457.8},0).wait(1).to({regY:-0.2,scaleX:0.36,scaleY:0.36,skewX:110,skewY:-70,x:374.6,y:456.2},0).wait(1).to({regX:83,skewX:140.4,skewY:-39.6,x:369.2,y:450.7},0).wait(1).to({regX:83.2,scaleX:0.36,scaleY:0.36,skewX:183,skewY:3,x:366,y:440.9},0).wait(1).to({scaleX:0.36,scaleY:0.36,skewX:237.9,skewY:57.9,x:366.5,y:428.5},0).wait(1).to({skewX:304.6,skewY:124.6,x:374.1,y:416},0).wait(1).to({regX:83},0).wait(9).to({regX:-1.2,regY:-0.7,x:391,y:391.1},0).wait(1).to({skewX:303.6,skewY:123.6,x:391.1,y:390.6},0).wait(1).to({regY:-0.6,skewX:300.6,skewY:120.6,x:391.9,y:388.7},0).wait(1).to({regY:-0.7,skewX:295.3,skewY:115.3,x:392,y:385.7},0).wait(1).to({skewX:288.3,skewY:108.3,x:392.7,y:382.7},0).wait(1).to({regY:-0.8,skewX:279,skewY:99,x:393,y:379.3},0).wait(1).to({regX:81.9,regY:-1.6,skewX:271,skewY:91,x:392.4,y:408.9},0).wait(5).to({regX:82,regY:-1.5,skewX:296.5,skewY:116.5,x:405.8,y:410.1},0).wait(1).to({regY:-1.7,skewX:319,skewY:139,x:416.3,y:416.4},0).wait(1).to({regY:-1.9,skewX:326.9,skewY:146.9,x:420.7,y:420.3},0).wait(1).to({regX:82.3,regY:-1.7,skewX:339.7,skewY:159.7,x:425.9,y:425.5},0).wait(1).to({regY:-1.8,skewX:356.2,skewY:176.2,x:430.7,y:433.1},0).wait(1).to({skewX:366.6,skewY:186.6,x:433,y:436.5},0).wait(1).to({regX:82.4,regY:-1.7,skewX:376.9,skewY:196.9,x:431.6,y:441.4},0).wait(1).to({regY:-1.9,skewX:384.1,skewY:204.1,x:430.3,y:444.8},0).wait(1).to({regX:82.5,skewX:382.1,skewY:202.1,x:431.1,y:443.5},0).wait(1).to({regX:82.4,regY:-1.8,skewX:378.7,skewY:198.6,x:430.8,y:443.8},0).wait(1).to({regX:82.5,regY:-1.9,skewX:375.2,skewY:195.2,x:430.7,y:443.5},0).wait(1).to({regX:82.4,skewX:372,skewY:192,x:430.6,y:443.6},0).wait(1).to({regY:-2,skewX:368.5,skewY:188.5},0).wait(1).to({regX:82.3,skewX:365,skewY:185,x:430.9,y:443.3},0).wait(5).to({regX:84.5,regY:2.1,scaleX:0.36,skewX:362.6,skewY:182.5,x:431,y:443},0).wait(1).to({scaleY:0.36,skewX:355.9,skewY:175.9,x:425.6,y:444.9},0).wait(1).to({regX:84.4,regY:2,scaleX:0.36,scaleY:0.36,skewX:360.5,skewY:180.5,x:418.3,y:454},0).wait(1).to({regX:84.5,regY:1.9,scaleX:0.36,scaleY:0.36,skewX:385.3,skewY:205.3,x:404.7,y:473.9},0).wait(1).to({skewX:405.6,skewY:225.6,x:389.3,y:486.3},0).wait(1).to({skewX:421.4,skewY:241.4,x:375.7,y:493.2},0).wait(1).to({regX:84.6,regY:2.1,skewX:432.7,skewY:252.7,x:366.4,y:497.4},0).wait(1).to({regX:84.5,regY:2.3,skewX:439.5,skewY:259.5,x:361.1,y:496},0).wait(1).to({regY:2,skewX:441.7,skewY:261.7,x:359.5,y:495.9},0).wait(1).to({regX:84.4,regY:2.1,x:359.7,y:496.2},0).wait(1).to({regX:84.5,regY:2,skewX:453.3,skewY:273.3,x:353.3,y:500.3},0).wait(1).to({regY:2.2,skewX:488.1,skewY:308.1,x:335.3,y:510.6},0).wait(1).to({regX:84.4,regY:2.1,skewX:425.7,skewY:245.7,x:308,y:524.6},0).wait(1).to({regX:84.5,skewX:296.1,skewY:116.1,x:279.6,y:540},0).wait(1).to({skewX:222.2,skewY:42.2,x:268.9,y:546.1},0).to({_off:true},1).wait(61));

	// helmet 2
	this.instance_19 = new lib.helmet();
	this.instance_19.setTransform(429.8,352.6,0.36,0.36,-4.5,0,0,99,95.7);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(137).to({_off:false},0).wait(3).to({x:429.9,y:352.9},0).to({regX:99.2,regY:95.8,rotation:56.4,guide:{path:[429.9,352.9,456.8,350,497.3,377.9,531.9,401.7,572.6,445.1,606.7,481.4,635.7,520.7,640.6,527.5,644.8,533.4]}},9).to({regX:99.4,regY:95.6,scaleX:0.36,scaleY:0.36,rotation:-360,x:512.9,y:400.4},10,cjs.Ease.get(0.53)).to({regX:99.2,regY:95.7,rotation:-372.8,guide:{path:[512.8,400.3,510.1,398.4,507.4,396.6]}},1).to({regX:98.2,regY:96.5,scaleX:0.36,scaleY:0.36,rotation:191,guide:{path:[507.3,396.8,513.7,394.4,520.1,392.7,531.2,389.8,542.1,389.1,551.2,388.5,560.1,389.4,567.1,390.2,574,391.9,603.8,399.5,629.5,425,653.7,449,672.3,487.1,684.2,511.4,693,539.8]}},14,cjs.Ease.get(-0.48)).to({_off:true},1).wait(60));

	// helmet
	this.instance_20 = new lib.helmet();
	this.instance_20.setTransform(440.3,392.6,0.36,0.36,0,0,0,99,95.7);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(114).to({_off:false},0).wait(1).to({regX:99.2,rotation:-0.1,x:440.1},0).wait(1).to({rotation:-0.5,x:439.5},0).wait(1).to({rotation:-1.1,x:438.5},0).wait(1).to({rotation:-2,x:437},0).wait(1).to({rotation:-3.1,x:435.2},0).wait(1).to({rotation:-4.5,x:432.9},0).wait(1).to({regX:99},0).wait(9).to({x:432.8},0).wait(1).to({regX:99.2,x:432.9,y:391.7},0).wait(1).to({y:389.1},0).wait(1).to({y:384.7},0).wait(1).to({y:378.5},0).wait(1).to({y:370.6},0).wait(1).to({regX:99,x:429.8,y:352.6},0).to({_off:true},1).wait(98));

	// head 2
	this.instance_21 = new lib.head2();
	this.instance_21.setTransform(435,413.5,0.36,0.361,-5.1,0,0,83,167.1);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(141).to({_off:false},0).wait(1).to({regX:86,regY:86,rotation:-4.4,x:434.4,y:384.2},0).wait(1).to({rotation:-2,x:437.5},0).wait(1).to({rotation:2,x:442.6,y:384.1},0).wait(1).to({rotation:7.6,x:449.8,y:384.4},0).wait(1).to({rotation:14,x:458,y:385},0).wait(4).to({regX:82.9,regY:167.2,scaleY:0.36,rotation:13.8,x:449.9,y:413.2},0).wait(9).to({regX:82.5,regY:166.9,scaleX:0.36,x:450},0).wait(1).to({regX:86,regY:86,scaleX:0.36,rotation:-7.8,x:428.5,y:385.8},0).wait(1).to({rotation:-23.7,x:406,y:390.6},0).wait(1).to({rotation:-32,x:392.7,y:397.4},0).wait(1).to({rotation:-38.8,x:382,y:403.4},0).wait(1).to({rotation:-44,x:373.9,y:408.2},0).wait(1).to({rotation:-47.8,x:368.2,y:411.8},0).wait(1).to({rotation:-50,x:364.8,y:414},0).wait(1).to({rotation:-50.8,x:363.7,y:414.7},0).wait(1).to({regX:82.5,regY:166.9,rotation:-50.7,x:385.5,y:434.2},0).wait(1).to({regX:86,regY:86,rotation:-53,x:357.9,y:419.5},0).wait(1).to({rotation:-59.8,x:340.7,y:433.9},0).wait(1).to({rotation:-71,x:312.6,y:458.5},0).wait(1).to({rotation:-86.7,x:275.2,y:493.5},0).wait(1).to({rotation:-95.7,x:254.8,y:513.7},0).to({_off:true},1).wait(61));

	// head
	this.instance_22 = new lib.head();
	this.instance_22.setTransform(435,413,0.36,0.361,-5.2,0,0,85.2,120.8);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(130).to({_off:false},0).wait(6).to({_off:true},5).wait(94));

	// hand rt
	this.instance_23 = new lib.hand();
	this.instance_23.setTransform(500,476.7,0.359,0.36,-14,0,0,43.8,30.2);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(114).to({_off:false},0).wait(1).to({regX:20.9,regY:0.3,rotation:-2.8,x:506.9,y:483.8},0).wait(1).to({regX:21,regY:0.2,rotation:-11.3,x:517,y:474},0).wait(1).to({regX:20.8,rotation:-42,x:526.9,y:466.1},0).wait(1).to({regX:20.9,regY:0.4,rotation:-103,x:538.9,y:415.8},0).wait(1).to({regX:21,regY:0.2,rotation:-155.9,x:523.2,y:392.8},0).wait(1).to({regX:20.9,rotation:-289.8,x:476.1,y:385.6},0).wait(10).to({rotation:-289.8,x:476.4},0).wait(1).to({x:475.8,y:384.9},0).wait(1).to({x:476.1,y:381.1},0).wait(1).to({x:475.1,y:375.2},0).wait(1).to({x:475.6,y:368.3},0).wait(1).to({x:476.1,y:362},0).wait(1).to({regX:35.7,regY:8.3,rotation:-216.8,x:466.3,y:370.2},0).wait(5).to({regX:23.2,regY:1.9,rotation:-240.6,x:503.4,y:375.6},0).wait(1).to({rotation:-245.8,x:525.5,y:400.5},0).wait(1).to({regY:1.8,rotation:-193.3,x:532.5,y:420.3},0).wait(1).to({regX:22.2,regY:0.3,rotation:-111,x:537.9,y:447},0).wait(1).to({regX:22.3,regY:0.4,rotation:-94.3,x:534.2,y:464.7},0).wait(1).to({regX:22.2,regY:0.2,rotation:-73.1,x:534.5,y:473.3},0).wait(1).to({regX:22.1,regY:0.4,rotation:-55.9,x:532.1,y:478.6},0).wait(2).to({regX:22.2,regY:0.3,rotation:-59.4,x:534.6,y:475.7},0).wait(1).to({regX:22.1,regY:0.1,rotation:-63.1,x:536.9,y:472.5},0).wait(1).to({regX:22.2,rotation:-67.1,x:539.2,y:468.8},0).wait(1).to({regX:22.4,rotation:-84.8,x:540.5,y:465.2},0).wait(1).to({regX:22.3,rotation:-92.3,x:541.4,y:461.3},0).wait(1).to({regY:-0.1,rotation:-99.3,x:541.6,y:457.3},0).wait(1).to({rotation:-111.3,x:541,y:450.7},0).wait(4).to({regX:20.9,regY:0.4,rotation:-49.8,x:537,y:476.1},0).wait(1).to({x:534,y:464},0).wait(1).to({rotation:-94.8,x:532.5,y:443},0).wait(1).to({rotation:-139.8,x:521,y:406},0).wait(1).to({regX:21,regY:0.5,rotation:-169.8,x:493.1,y:382},0).wait(1).to({regX:20.9,rotation:-214.8,x:469.3,y:370.3},0).wait(1).to({regY:0.4,rotation:-229.8,x:448.5,y:369.3},0).wait(1).to({rotation:-255.2,x:438.8,y:367.3},0).wait(1).to({regY:0.5,rotation:-214.8,x:433.8,y:367.1},0).wait(1).to({regY:1.7,scaleY:0.36,rotation:-210.9,x:433,y:366.9},0).wait(1).to({x:426.8,y:369.1},0).wait(1).to({regY:1.6,rotation:-211.2,x:404.7,y:377.8},0).wait(1).to({regX:21,rotation:-201.7,x:363.5,y:392.1},0).wait(1).to({regX:21.1,regY:1.7,rotation:-198.7,x:309,y:420.5},0).wait(1).to({regX:21.2,regY:1.6,rotation:-190.7,x:279.5,y:436.7},0).to({_off:true},1).wait(61));

	// arm rt lower
	this.instance_24 = new lib.armrtlower();
	this.instance_24.setTransform(511.5,443.4,0.359,0.358,130.6,0,0,0.5,0.4);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(114).to({_off:false},0).wait(1).to({regX:0.6,regY:-1.8,rotation:89.6,x:507.4,y:453.6},0).wait(1).to({regX:0.7,regY:-1.9,rotation:78.6,x:511.6,y:444.4},0).wait(1).to({regX:0.6,rotation:56.2,x:511.3,y:440.7},0).wait(1).to({regX:0.5,scaleX:0.36,scaleY:0.36,rotation:-18.2,x:510.6,y:425.2},0).wait(1).to({regX:0.6,regY:-2,scaleX:0.36,scaleY:0.36,rotation:-53.7,x:505.2,y:416.6},0).wait(1).to({rotation:-134.1,x:496.7,y:408.2},0).wait(10).to({regX:83.5,regY:0,scaleX:0.36,scaleY:0.36,x:476.7,y:386.3},0).wait(1).to({regX:83.6,rotation:-132.6,x:476.4,y:385.8},0).wait(1).to({regX:83.5,regY:-0.1,rotation:-128.6,x:476.5,y:383.3},0).wait(1).to({regX:83.7,regY:0,rotation:-121.6,x:475.2,y:379.3},0).wait(1).to({regX:83.5,regY:-0.1,scaleX:0.36,scaleY:0.36,rotation:-111.8,x:475.4,y:375.2},0).wait(1).to({rotation:-99,x:475,y:372},0).wait(1).to({regX:2.7,regY:2.7,x:476,y:400.6},0).wait(5).to({regX:-1.1,regY:1,scaleX:0.36,scaleY:0.36,rotation:-81.8,x:499.1,y:408.3},0).wait(1).to({regX:-1.2,regY:1.2,rotation:-57.8,x:508.5,y:429.8},0).wait(1).to({rotation:-40.2,x:506.9,y:442},0).wait(1).to({regX:-1.4,rotation:-4.6,x:505.6,y:450.3},0).wait(1).to({regX:-1.2,rotation:12.1,x:502.3,y:458.6},0).wait(1).to({regY:1.1,rotation:23.1,x:504.5,y:461.4},0).wait(1).to({regX:-1.1,rotation:33.6,x:504.7,y:461.3},0).wait(2).to({regY:1,rotation:30.1,x:506.1,y:460.1},0).wait(1).to({rotation:26.4,x:507.6,y:458.8},0).wait(1).to({regX:-1,rotation:22.4,x:509,y:457.3},0).wait(1).to({regX:-0.9,rotation:15.4,x:509.1,y:457.4},0).wait(1).to({regX:-0.8,rotation:7.9,x:509.2,y:457.5},0).wait(1).to({regX:-1,rotation:0.9,x:509.3},0).wait(1).to({regX:-1.1,regY:1.1,rotation:-11.1,x:509.4,y:457.7},0).wait(4).to({regX:0.2,regY:1.5,rotation:29.8,y:460.8},0).wait(1).to({regX:0.3,regY:1.4,rotation:22.8,x:504.8,y:453.3},0).wait(1).to({regX:0.1,rotation:3.8,x:501.2,y:442.7},0).wait(1).to({regX:0.2,regY:1.6,rotation:-37.4,x:495.9,y:424.8},0).wait(1).to({rotation:-71.2,x:483.6,y:410.5},0).wait(1).to({rotation:-97.3,x:472,y:400.8},0).wait(1).to({regY:1.5,rotation:-116.1,x:462.7,y:395.4},0).wait(1).to({regX:0.1,rotation:-127.4,x:457,y:391.8},0).wait(1).to({regX:0.3,regY:1.6,rotation:-131.1,x:454.9,y:390.7},0).wait(1).to({regX:-1.4,regY:1,scaleX:0.36,scaleY:0.36,x:454.1,y:390},0).wait(1).to({rotation:-132.4,x:447.7,y:392.9},0).wait(1).to({rotation:-135.9,x:426.5,y:399.5},0).wait(1).to({regX:-1.5,scaleX:0.36,scaleY:0.36,rotation:-142,x:389.7,y:412.1},0).wait(1).to({scaleX:0.36,scaleY:0.36,rotation:-150.4,x:337,y:435.2},0).wait(1).to({regX:-1.4,regY:1.1,rotation:-155.4,x:307.8,y:449.2},0).to({_off:true},1).wait(61));

	// arm rt upper
	this.instance_25 = new lib.armrtupper();
	this.instance_25.setTransform(482.7,434.1,0.36,0.36,0,16.7,17);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(114).to({_off:false},0).wait(1).to({skewX:36.4,skewY:36.7,x:482.8,y:435},0).wait(1).to({skewX:16,skewY:16.2,x:482.1,y:435.7},0).wait(1).to({scaleY:0.36,skewX:9.7,skewY:9.9,x:481.1,y:435.2},0).wait(1).to({skewX:-16.6,skewY:-16.3,x:480.7,y:434.7},0).wait(1).to({scaleX:0.36,skewX:-32.8,skewY:-32.6,x:479.7,y:433.9},0).wait(1).to({scaleX:0.36,skewX:-52.4,skewY:-52.2,x:478.4,y:432.9},0).wait(10).to({regX:-1.1,regY:-1.2,scaleX:0.36,x:477.9,y:433},0).wait(1).to({regX:-1,skewX:-53.7,skewY:-53.5,x:478},0).wait(1).to({regX:-1.1,skewX:-58,skewY:-57.7},0).wait(1).to({regX:-1.2,scaleY:0.36,skewX:-65.2,skewY:-64.9},0).wait(1).to({regX:-1.1,skewX:-75,skewY:-74.7},0).wait(1).to({regX:-1,regY:-1.1,scaleX:0.36,skewX:-87.8,skewY:-87.5},0).wait(1).to({regX:-1.2,regY:-1,skewX:-96,skewY:-95.7},0).wait(5).to({regX:-1.1,regY:-0.6,skewX:-47.3,skewY:-47,x:477.3,y:430.4},0).wait(1).to({skewX:-0.8,skewY:-0.6,x:477.2,y:429.2},0).wait(1).to({regX:-1.2,skewX:21.7,skewY:21.9,x:478.2,y:429.5},0).wait(1).to({regY:-0.4,skewX:34.7,skewY:34.9,x:480.5,y:432.1},0).wait(1).to({regX:-1.4,regY:-0.6,skewX:51.3,skewY:51.6,x:483.4,y:433.9},0).wait(1).to({x:486.4,y:437.2},0).wait(3).to({regX:-1.3,regY:-0.4,skewX:47.9,skewY:48.1,y:437.3},0).wait(1).to({regX:-1.4,skewX:44.1,skewY:44.4},0).wait(1).to({regX:-1.2,skewX:40.1,skewY:40.4,x:486.3},0).wait(8).to({regY:-0.7,scaleX:0.36,scaleY:0.36,skewX:40.9,skewY:41.1,x:486.4,y:440},0).wait(1).to({regY:-0.8,skewX:34.1,skewY:34.4,x:480,y:435.4},0).wait(1).to({regX:-1,regY:-0.7,skewX:21.1,skewY:21.3,x:472.5,y:431.1},0).wait(1).to({regX:-1.1,scaleY:0.36,skewX:-3.8,skewY:-3.5,x:464.4,y:426.8},0).wait(1).to({regX:-1.3,regY:-0.6,skewX:-24.1,skewY:-23.8,x:455.2,y:422.9},0).wait(1).to({regX:-1.2,regY:-0.8,scaleY:0.36,skewX:-39.9,skewY:-39.6,x:448.2,y:420.4},0).wait(1).to({regX:-1.1,regY:-0.7,skewX:-50.9,skewY:-50.7,x:443.2,y:418.7},0).wait(1).to({skewX:-57.7,skewY:-57.4,x:440.4,y:417.5},0).wait(1).to({regX:-1.2,regY:-0.8,skewX:-59.9,skewY:-59.7,x:439.5,y:417.1},0).wait(1).to({regX:-1.4,regY:-0.5,scaleX:0.36,scaleY:0.36,y:417.2},0).wait(1).to({regY:-0.4,skewX:-62.5,skewY:-62.2,x:433.4,y:420.2},0).wait(1).to({regX:-1.5,regY:-0.5,skewX:-70.7,skewY:-70.5,x:415.4,y:429.2},0).wait(1).to({regX:-1.4,regY:-0.6,skewX:-84.3,skewY:-84,x:385.9,y:443.2},0).wait(1).to({regY:-0.3,skewX:-103.3,skewY:-103.1,x:343.5,y:465.2},0).wait(1).to({regY:-0.7,skewX:-114.1,skewY:-113.9,x:319.4,y:477.2},0).to({_off:true},1).wait(61));

	// body
	this.instance_26 = new lib.body();
	this.instance_26.setTransform(437.7,453.3,0.36,0.361,0,0,0,109.7,93.7);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(114).to({_off:false},0).wait(1).to({regX:109.8,regY:93.6},0).wait(1).to({regX:109.7,regY:93.8,rotation:-0.3,x:437.5,y:453.4},0).wait(1).to({rotation:-1,x:437.1},0).wait(1).to({regY:93.6,rotation:-1.8,x:436.6,y:453.3},0).wait(1).to({regY:93.7,rotation:-3,x:435.9},0).wait(1).to({rotation:-4.5,x:435.1},0).wait(1).to({regX:110},0).wait(20).to({regX:110.2,regY:93.8,x:435,y:453.4},0).wait(1).to({regX:116,rotation:-4.1,x:437.3,y:453.2},0).wait(1).to({rotation:-3,x:438.1},0).wait(1).to({rotation:-1.1,x:439.2,y:453.3},0).wait(1).to({rotation:1.5,x:440.9,y:453.4},0).wait(1).to({rotation:4.5,x:442.8,y:453.5},0).wait(4).to({regX:110.4,rotation:4.3,x:440.8,y:453.4},0).wait(9).to({regX:110.9,regY:93.7,y:453.3},0).wait(1).to({regX:116,regY:93.8,rotation:-2.6,x:438.3,y:453.9},0).wait(1).to({rotation:-9.7,x:433.9,y:454.7},0).wait(1).to({rotation:-17.9,x:429.1,y:456.1},0).wait(1).to({rotation:-24.6,x:425.2,y:457.3},0).wait(1).to({rotation:-29.8,x:422.1,y:458.2},0).wait(1).to({rotation:-33.6,x:419.9,y:458.9},0).wait(1).to({rotation:-35.8,x:418.6,y:459.3},0).wait(1).to({rotation:-36.5,x:418.1,y:459.5},0).wait(1).to({regX:111,regY:93.5,scaleX:0.36,scaleY:0.36,rotation:-36.6,x:416.6,y:460.5},0).wait(1).to({regX:116,regY:93.8,rotation:-38.5,x:413.5,y:462.7},0).wait(1).to({rotation:-44.4,x:399.6,y:472.1},0).wait(1).to({rotation:-54.1,x:376.4,y:487.8},0).wait(1).to({rotation:-67.8,x:343.9,y:510},0).wait(1).to({rotation:-75.5,x:325.3,y:522.7},0).to({_off:true},1).wait(61));

	// foot rt
	this.instance_27 = new lib.foot();
	this.instance_27.setTransform(456.4,563.8,0.36,0.361,0,0,0,36.7,24.7);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(114).to({_off:false},0).wait(1).to({regX:36.5,regY:24.4,x:456.3,y:563.6},0).wait(6).to({regX:36.7,regY:24.7,x:456.4,y:563.8},0).wait(39).to({regX:36.5,regY:24.4,rotation:-37.4,x:483,y:551.6},0).wait(1).to({rotation:-61.4,x:500.9,y:541.3},0).wait(1).to({rotation:-65.6,x:505.8,y:532.7},0).wait(1).to({rotation:-69,x:509.8,y:525.6},0).wait(1).to({rotation:-71.6,x:512.9,y:520.1},0).wait(1).to({rotation:-73.5,x:515.2,y:516.2},0).wait(1).to({rotation:-74.6,x:516.5,y:513.9},0).wait(1).to({rotation:-75,x:516.9,y:513},0).wait(1).to({regX:36.7,scaleX:0.36,scaleY:0.36,x:517.1},0).wait(1).to({regX:36.5,rotation:-77.2,x:513.5,y:512.4},0).wait(1).to({rotation:-84,x:502.8,y:510.4},0).wait(1).to({rotation:-95.2,x:484.8,y:507.1},0).wait(1).to({rotation:-111,x:459.8,y:502.4},0).wait(1).to({rotation:-119.9,x:445.5,y:499.7},0).to({_off:true},1).wait(61));

	// leg rt mid
	this.instance_28 = new lib.legmid();
	this.instance_28.setTransform(456.4,518.4,0.36,0.361,0,0,0,17.5,17.9);
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(114).to({_off:false},0).wait(1).to({regX:17.3,regY:17.3,x:456.3,y:518.1},0).wait(1).to({y:517.6},0).wait(1).to({y:516.9},0).wait(1).to({y:516},0).wait(1).to({y:514.7},0).wait(1).to({y:513.2},0).wait(1).to({regX:17.5,regY:17.9,x:456.4,y:513.4},0).wait(21).to({regX:17.3,regY:17.3,x:456.3,y:513.7},0).wait(1).to({y:515},0).wait(1).to({y:517.3},0).wait(1).to({y:520.5},0).wait(1).to({y:524.2},0).wait(4).to({regX:17.5,regY:17.9,x:456.4,y:524.4},0).wait(10).to({regX:17.3,regY:17.3,x:469.8,y:513.3},0).wait(1).to({x:478.3,y:505.2},0).wait(1).to({x:479.4,y:500.8},0).wait(1).to({x:480.3,y:497.2},0).wait(1).to({x:481,y:494.4},0).wait(1).to({x:481.5,y:492.4},0).wait(1).to({x:481.8,y:491.2},0).wait(1).to({x:481.9,y:490.8},0).wait(1).to({regX:17.5,regY:17.9,x:482,y:491},0).wait(1).to({regX:17.3,regY:17.3,x:477.6,y:491.7},0).wait(1).to({x:464.9,y:494.2},0).wait(1).to({x:443.6,y:498.5},0).wait(1).to({x:413.9,y:504.4},0).wait(1).to({x:396.9,y:507.8},0).to({_off:true},1).wait(61));

	// leg rt upper
	this.instance_29 = new lib.legupper();
	this.instance_29.setTransform(456.4,487.4,0.36,0.361);
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(114).to({_off:false},0).wait(1).to({regY:35.1,y:499.9},0).wait(1).to({y:499.8},0).wait(1).to({y:499.5},0).wait(1).to({y:499.1},0).wait(1).to({y:498.6},0).wait(1).to({y:498},0).wait(1).to({regY:0,y:485.4},0).wait(20).to({scaleY:0.48},0).wait(19).to({regY:35.1,rotation:-28.1,x:462.9,y:498.7},0).wait(1).to({rotation:-47.4,x:466.2,y:494},0).wait(1).to({rotation:-54.1,x:466.4,y:491.4},0).wait(1).to({rotation:-59.5,y:489.1},0).wait(1).to({rotation:-63.8,x:466.3,y:487.3},0).wait(1).to({rotation:-66.8,y:486},0).wait(1).to({rotation:-68.6,x:466.2,y:485.2},0).wait(1).to({rotation:-69.2,y:485},0).wait(1).to({regY:0,scaleX:0.36,scaleY:0.48,x:450.4,y:479},0).wait(1).to({regY:35.1,rotation:-71.4,x:461.9,y:486.6},0).wait(1).to({rotation:-77.8,x:449,y:491.2},0).wait(1).to({rotation:-88.5,x:427.1,y:498.8},0).wait(1).to({rotation:-103.5,x:395.5,y:509.5},0).wait(1).to({rotation:-112,x:376.9,y:515.8},0).to({_off:true},1).wait(61));

	// leg rt lower
	this.instance_30 = new lib.leglower();
	this.instance_30.setTransform(456,555,0.36,0.361,0,0,0,-1.1,84.2);
	this.instance_30._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(114).to({_off:false},0).wait(1).to({regX:0,regY:43,scaleY:0.36,x:456.4,y:540},0).wait(1).to({scaleY:0.37,y:539.8},0).wait(1).to({scaleY:0.38,y:539.4},0).wait(1).to({scaleY:0.39,y:538.9},0).wait(1).to({scaleY:0.41,y:538.2},0).wait(1).to({scaleY:0.43,y:537.4},0).wait(1).to({regX:-1.1,regY:84.2,x:456,y:555},0).wait(38).to({regY:0,y:519},0).wait(1).to({regX:0,regY:43,rotation:-13.7,x:476.7,y:532.2},0).wait(1).to({rotation:-25,x:490.1,y:526.4},0).wait(1).to({rotation:-33.2,x:493.4,y:520.3},0).wait(1).to({rotation:-39.9,x:496,y:515},0).wait(1).to({rotation:-45.1,x:497.8,y:510.7},0).wait(1).to({rotation:-48.9,x:499.1,y:507.7},0).wait(1).to({rotation:-51.1,x:499.8,y:505.8},0).wait(1).to({rotation:-51.8,x:500,y:505.1},0).wait(1).to({regX:-1.1,regY:-0.1,scaleX:0.36,scaleY:0.43,rotation:-51.9,x:485.3,y:494.1},0).wait(1).to({regX:0,regY:43,scaleY:0.43,rotation:-53.9,x:496.3,y:505.2},0).wait(1).to({rotation:-59.9,x:484.9,y:505.3},0).wait(1).to({rotation:-70,x:465.6},0).wait(1).to({rotation:-84.1,x:437.5,y:505},0).wait(1).to({rotation:-92.1,x:421,y:504.8},0).to({_off:true},1).wait(61));

	// foot lt
	this.instance_31 = new lib.foot();
	this.instance_31.setTransform(424,563.8,0.36,0.361,0,0,0,36.7,24.7);
	this.instance_31._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(114).to({_off:false},0).wait(1).to({regX:36.5,regY:24.4,x:423.9,y:563.6},0).wait(6).to({regX:36.7,regY:24.7,x:424,y:563.8},0).wait(39).to({regX:36.5,regY:24.4,rotation:-9.3,x:429.6,y:563.9},0).wait(1).to({rotation:-14.8,x:433,y:564},0).wait(7).to({regX:4.9,regY:44.8,scaleX:0.36,scaleY:0.36,x:424.1,y:574},0).wait(1).to({regX:36.5,regY:24.4,rotation:-16.6,x:432.8,y:563.6},0).wait(1).to({rotation:-22.2,x:431.8,y:562.9},0).wait(1).to({rotation:-31.4,x:429.9,y:561.7},0).wait(1).to({rotation:-44.4,x:427.1,y:560.8},0).wait(1).to({rotation:-51.7,x:425.3,y:560.5},0).to({_off:true},1).wait(61));

	// leg lt mid
	this.instance_32 = new lib.legmid();
	this.instance_32.setTransform(424,518.4,0.36,0.361,0,0,0,17.7,17.9);
	this.instance_32._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(114).to({_off:false},0).wait(1).to({regX:17.3,regY:17.3,x:423.8,y:518.3},0).wait(1).to({y:518.6},0).wait(1).to({y:519.2},0).wait(1).to({y:520},0).wait(1).to({y:521},0).wait(1).to({y:522.2},0).wait(1).to({regX:17.7,regY:17.9,x:424,y:522.4},0).wait(21).to({regX:17.3,regY:17.3,x:423.8,y:521.8},0).wait(1).to({y:520.7},0).wait(1).to({y:518.8},0).wait(1).to({y:516.1},0).wait(1).to({y:513.2},0).wait(4).to({regX:17.7,regY:17.9,x:424,y:513.4},0).wait(10).to({regX:17.3,regY:17.3,x:428.2,y:522.3},0).wait(1).to({x:432,y:527.5},0).wait(1).to({x:435.3,y:526.7},0).wait(1).to({x:438,y:526},0).wait(1).to({x:440.1,y:525.5},0).wait(1).to({x:441.6,y:525.1},0).wait(1).to({x:442.5,y:524.9},0).wait(1).to({x:442.8,y:524.8},0).wait(1).to({regX:17.7,regY:17.9,x:443,y:525},0).wait(1).to({regX:17.3,regY:17.3,x:440.3,y:525.3},0).wait(1).to({x:432.8,y:527},0).wait(1).to({x:420.3,y:529.7},0).wait(1).to({x:402.8,y:533.6},0).wait(1).to({x:392.8,y:535.8},0).to({_off:true},1).wait(61));

	// leg lt upper
	this.instance_33 = new lib.legupper();
	this.instance_33.setTransform(424,487.4,0.36,0.361);
	this.instance_33._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(114).to({_off:false},0).wait(1).to({regY:35.1,scaleY:0.36,y:500.1},0).wait(1).to({scaleY:0.37,y:500.2},0).wait(1).to({scaleY:0.37,y:500.5},0).wait(1).to({scaleY:0.38,y:500.8},0).wait(1).to({scaleY:0.39,y:501.2},0).wait(1).to({scaleY:0.41,y:501.7},0).wait(1).to({regY:0,y:487.4},0).wait(39).to({regY:35.1,scaleY:0.47,rotation:-6.5,x:425.9,y:503.9},0).wait(1).to({scaleY:0.51,rotation:-11.9,x:427.7,y:505},0).wait(1).to({rotation:-16,x:428.9,y:504.7},0).wait(1).to({rotation:-19.3,x:429.9,y:504.4},0).wait(1).to({rotation:-21.9,x:430.7,y:504.1},0).wait(1).to({rotation:-23.8,x:431.2,y:503.9},0).wait(1).to({rotation:-24.9,x:431.6,y:503.7},0).wait(1).to({rotation:-25.3,x:431.7},0).wait(1).to({regY:0,scaleX:0.36,scaleY:0.51,x:424,y:487.4},0).wait(1).to({regY:35.1,scaleY:0.51,rotation:-28.8,x:429.3,y:505.8},0).wait(1).to({rotation:-39.4,x:421.8,y:511.8},0).wait(1).to({rotation:-57,x:408.5,y:520.8},0).wait(1).to({rotation:-81.7,x:387.4,y:532.1},0).wait(1).to({rotation:-95.8,x:373.9,y:538.2},0).to({_off:true},1).wait(61));

	// leg lt lower
	this.instance_34 = new lib.leglower();
	this.instance_34.setTransform(423.9,554.9,0.36,0.361,0,0,0,-0.1,83.9);
	this.instance_34._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(114).to({_off:false},0).wait(1).to({regX:0,regY:43,x:424,y:540.1},0).wait(6).to({regX:-0.1,regY:83.9,x:423.9,y:554.9},0).wait(20).to({scaleY:0.46},0).wait(19).to({regX:0,regY:43,scaleY:0.39,rotation:5.1,x:429.7,y:540.3},0).wait(1).to({scaleY:0.34,rotation:9.5,x:433.3,y:543},0).wait(1).to({rotation:13.2,x:434.2,y:543.2},0).wait(1).to({rotation:16.3,x:434.9,y:543.4},0).wait(1).to({rotation:18.7,x:435.4,y:543.5},0).wait(1).to({rotation:20.4,x:435.8,y:543.7},0).wait(1).to({rotation:21.4,x:436.1,y:543.8},0).wait(1).to({rotation:21.7},0).wait(1).to({regX:-0.2,regY:84,scaleX:0.36,scaleY:0.34,rotation:21.5,x:430.9,y:556.8},0).wait(1).to({regX:0,regY:43,scaleY:0.34,rotation:17.8,x:434.7,y:543.4},0).wait(1).to({rotation:6.6,x:430.2,y:542.6},0).wait(1).to({rotation:-12.2,x:422.7,y:542.5},0).wait(1).to({rotation:-38.4,x:412.9,y:544.8},0).wait(1).to({rotation:-53.4,x:407.9,y:547.2},0).to({_off:true},1).wait(61));

	// elio small
	this.instance_35 = new lib.eliosmall();
	this.instance_35.setTransform(397.2,491,0.9,0.9,-104.7,0,0,37.4,38.8);
	this.instance_35._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(30).to({_off:false},0).wait(1).to({regX:36,regY:37.6,rotation:-102,x:399.8,y:489},0).wait(1).to({rotation:-99,x:403,y:485.8},0).wait(1).to({rotation:-95.5,x:406.8,y:482.3},0).wait(1).to({rotation:-91.4,x:411,y:478.6},0).wait(1).to({rotation:-86.8,x:415.6,y:474.9},0).wait(1).to({rotation:-81.2,x:420.8,y:471.1},0).wait(1).to({rotation:-74.6,x:426.5,y:467.4},0).wait(1).to({rotation:-66.2,x:433,y:464.1},0).wait(1).to({rotation:-55.5,x:440.4,y:461.2},0).wait(1).to({rotation:-41.4,x:448.9,y:459.5},0).wait(1).to({rotation:-23.8,x:457.8,y:459.7},0).wait(1).to({rotation:-3.8,x:467.1,y:462.7},0).wait(1).to({rotation:14.5,x:476,y:468.8},0).wait(1).to({rotation:28.4,x:483.2,y:476.9},0).wait(1).to({rotation:40.7,x:489.6,y:487.7},0).wait(1).to({rotation:50.6,x:494.5,y:499.4},0).wait(1).to({rotation:58.8,x:498,y:511.1},0).wait(1).to({rotation:66.5,x:500.5,y:522.9},0).wait(1).to({rotation:74.5,x:502.3,y:535.4},0).wait(1).to({regX:36.4,regY:38.7,rotation:82.7,x:502.1,y:550},0).to({_off:true},1).wait(184));

	// rock1 copy
	this.instance_36 = new lib.rocks1MC();
	this.instance_36.setTransform(417.8,563.9,1,1,0,0,0,2,0);
	this.instance_36._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(30).to({_off:false},0).to({guide:{path:[417.7,563.8,445.4,532.3,483.9,517.1,519.1,503.2,560.1,503.9,598.3,504.6,636.2,517.7,672.7,530.4,701.9,552]},alpha:0},35,cjs.Ease.get(1)).to({_off:true},1).wait(169));

	// rock3
	this.instance_37 = new lib.rocks1MC();
	this.instance_37.setTransform(414.2,564,1.258,1.258,51.9,0,0,1.9,0);
	this.instance_37._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(30).to({_off:false},0).to({guide:{path:[414.2,564,424.4,529.2,459.9,496.8,496.3,463.5,546,445.2,600.3,425.2,655.7,429.1,718,433.4,771.9,468]},alpha:0},29,cjs.Ease.get(1)).to({_off:true},1).wait(175));

	// rock2
	this.instance_38 = new lib.rocks1MC();
	this.instance_38.setTransform(318.9,564,1.258,1.258,51.9,0,0,1.9,0);
	this.instance_38._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_38).wait(30).to({_off:false},0).to({guide:{path:[318.9,564,307.6,524.8,294.4,496.4,278.2,461.5,257.2,437.9,220.9,397.3,167.4,387.2]},alpha:0},14,cjs.Ease.get(1)).to({_off:true},1).wait(190));

	// rock1
	this.instance_39 = new lib.rocks1MC();
	this.instance_39.setTransform(319,564,1,1,0,0,0,2,0);
	this.instance_39._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(30).to({_off:false},0).to({guide:{path:[318.9,563.9,237.1,438.4,98.1,476.9]},alpha:0},35,cjs.Ease.get(1)).to({_off:true},1).wait(169));

	// whiteBox3
	this.whiteBox1_1 = new lib.whitebox();
	this.whiteBox1_1.setTransform(177,431,0.32,2.032,0,0,0,594,93.5);
	this.whiteBox1_1.alpha = 0;
	this.whiteBox1_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.whiteBox1_1).wait(23).to({_off:false},0).to({alpha:1},7,cjs.Ease.get(1)).to({_off:true},1).wait(204));

	// cloudBurst3
	this.instance_40 = new lib.cloudBurst3();
	this.instance_40.setTransform(177,576.4,1,1,0,0,0,136.5,67.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_40).to({scaleX:0.98,scaleY:1.23,y:537.3},30,cjs.Ease.get(1)).to({_off:true},1).wait(204));

	// whiteBox1
	this.whiteBox1_2 = new lib.whitebox();
	this.whiteBox1_2.setTransform(396.1,431.1,0.704,2.563,0,0,0,594,93.5);
	this.whiteBox1_2.alpha = 0;
	this.whiteBox1_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.whiteBox1_2).wait(52).to({_off:false},0).to({alpha:1},8,cjs.Ease.get(1)).to({_off:true},1).wait(174));

	// cloudBurst1
	this.cloudBurst1 = new lib.cloudBurst1();
	this.cloudBurst1.setTransform(384.8,588.7,0.53,0.53,0,0,0,148.5,124.5);
	this.cloudBurst1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cloudBurst1).wait(30).to({_off:false},0).to({regX:148.6,scaleX:1.02,scaleY:1.02,x:384.9,y:551.7},30,cjs.Ease.get(1)).to({_off:true},1).wait(174));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-194,319.8,1188,437.8);


// stage content:



(lib.Elio404final3 = function() {
	this.initialize();

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Eg+fAu4MAAAhdvMB8/AAAMAAABdvg");
	mask.setTransform(400,300);

	// movieClip1
	this.instance = new lib.movieClip1();
	this.instance.setTransform(16.2,9.8,1,1,0,0,0,16.2,9.8);

	this.instance.mask = mask;

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(400,619.8,800,280.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;